//
//  FAQViewController.m
//  Q-Workerbee
//
//  Created by santosh kumar singh on 14/05/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import "FAQViewController.h"
#import "ODSAccordionView.h"
#import "ODSAccordionSectionStyle.h"


@interface FAQViewController ()<WebServiceDelegate>
{
    IBOutlet  ODSAccordionView *_accordionView;
    NSMutableArray *sections;
}
@end

@implementation FAQViewController

- (void)viewDidLoad
{
   self.view.backgroundColor = [Alert colorWithPatternImage2:kBG_IMAGE];
     sections = [[NSMutableArray alloc] init];
                
    self.title =@"FAQs";
    [self serviceCompletedList];
    
  
    
 
    //_accordionView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    [self navigationBarConfiguration];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
#pragma mark - Configure Navigation Controller

-(void)navigationBarConfiguration
{
    
    self.navigationController.navigationBarHidden = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    UIButton * menuButton  = [UIButton buttonWithType:UIButtonTypeSystem];
    menuButton.frame = CGRectMake(8, 18, 24, 24);
    [menuButton setBackgroundImage:[UIImage imageNamed:NAV_SLIDER_IMAGE] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(drawer:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *accountBarItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    self.navigationItem.leftBarButtonItem = accountBarItem;
  
    NSMutableArray *arrRightBarItems = [[NSMutableArray alloc] init];
         UIButton *btnSetting = [UIButton buttonWithType:UIButtonTypeCustom];
         [btnSetting setImage:[UIImage imageNamed:@"Frequency"] forState:UIControlStateNormal];
         btnSetting.frame = CGRectMake(0, 0, 40, 40);
         btnSetting.showsTouchWhenHighlighted=YES;
     [btnSetting addTarget:self action:@selector(onSettings:) forControlEvents:UIControlEventTouchUpInside];
         UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSetting];
       
         UIButton *btnLib = [UIButton buttonWithType:UIButtonTypeCustom];
         [btnLib setImage:[UIImage imageNamed:@"logo1.png"] forState:UIControlStateNormal];
         btnLib.frame = CGRectMake(0, 0, 40, 40);
         btnLib.showsTouchWhenHighlighted=YES;
        [btnLib addTarget:self action:@selector(onMyLibrary:) forControlEvents:UIControlEventTouchUpInside];
         UIBarButtonItem *barButtonItem2 = [[UIBarButtonItem alloc] initWithCustomView:btnLib];
         [arrRightBarItems addObject:barButtonItem2];
         [arrRightBarItems addObject:barButtonItem];
         self.navigationItem.rightBarButtonItems=arrRightBarItems;
        
      
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName :[UIColor whiteColor]}];
}
-(void)onMyLibrary:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}

-(void)onSettings:(id)sender
{
  
    NSString*myurl=@"https://www.hypdemand.com/";
     NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
       [[UIApplication sharedApplication] openURL:url];
    }
}

#pragma mark - Drawer class
-(void)drawer:(id)sender
{
    
    KYDrawerController *elDrawer = (KYDrawerController*)self.navigationController.parentViewController;
    [elDrawer setDrawerState:KYDrawerControllerDrawerStateOpened animated:YES];
    
    
}


-(void)serviceCompletedList{
    
    // SERVER CONNCETION
    [Alert svProgress:@"Please Wait..."];
    
    
    WebService * connection = [[WebService alloc]init];
    connection.delegate = self;
    NSMutableDictionary * dict                              = [NSMutableDictionary dictionary];
    
    [dict setObject:@"faq"                     forKey:kAPI_ACTION];
   
    NSLog(@"dict is ---%@",dict);
    
    [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:@"faq"];
    
}

#pragma mark - Sevice delgate methods

-(void)getDataFormWebService:(NSDictionary *)jsonResults urlStr:(NSString *)yourUR methodName:(NSString *)methodName{
    
    [SVProgressHUD dismiss];
    
    SWITCH (methodName) {
        CASE (@"faq")
        {
            NSLog(@"jsonResults =%@",jsonResults);
            //   resultArray=[jsonResults valueForKey:@"response"];
            
            arr = [jsonResults valueForKey:@"data"];
          
            ODSAccordionSectionStyle *style = [[ODSAccordionSectionStyle alloc] init];
                 style.arrowColor = [UIColor whiteColor];
                 style.headerStyle = ODSAccordionHeaderStyleLabelLeft;
                 style.headerTitleLabelTextColor = [UIColor whiteColor];
                 style.headerTitleLabelFont = [UIFont boldSystemFontOfSize:18];
                 style.backgroundColor =[UIColor clearColor];
                 style.headerBackgroundColor = [UIColor clearColor];
                 style.dividerColor = [UIColor whiteColor];
                 style.headerHeight = 50;
                 style.stickyHeaders = YES;
                 
               
            for (int i = 0; i<= arr.count-1; i++)
            {
               
                [sections addObject:[[ODSAccordionSection alloc] initWithTitle:[[arr valueForKey:@"question"] objectAtIndex:i]
                                                                       andView:[self textView:[[arr valueForKey:@"answer"] objectAtIndex:i]]]];
            }
//           sections = @[
//                 [[ODSAccordionSection alloc] initWithTitle:@"Text"
//                                                    andView: [self textView]],
//                                    [[ODSAccordionSection alloc] initWithTitle:@"Text"
//                                                                       andView: [self textView]]
//                                        ];
             [ _accordionView initWithSections:sections andSectionStyle:style];
            
        }
        
        
        break;
        
        DEFAULT
        {
            break;
        }
    }
    
}
// WEB SERVER ERROR MSG LOCAL SERVER (APPLICATION SERVER)
-(void) webServiceFailWithApplicationServerMSG:(NSString *)msg
{
    
    [Alert svError:msg];
    
}

// WEB SERVICE FAIL WITH REGION TCP/IP
-(void) webServiceFail:(NSError *)error{
    
    
    [Alert svError:[error localizedDescription]];
    
    
}

-(void)viewDidAppear:(BOOL)animated {
    [_accordionView flashScrollIndicators];
}

-(UITextView *)textView:(NSString*)text
{
   
    UITextView *textView = [[UITextView alloc] init];
    textView.frame = CGRectMake(0, 0, 0, 100);
    textView.textColor = [UIColor whiteColor];
    textView.backgroundColor = [UIColor clearColor];
    
    NSDictionary *options = @{ NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType };
  
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithData:[text dataUsingEncoding:NSUTF8StringEncoding] options:options documentAttributes:nil error:nil];
    
  
    textView.attributedText = attrString;
    return textView;
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
