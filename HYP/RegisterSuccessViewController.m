//
//  RegisterSuccessViewController.m
//  Q-Workerbee
//
//  Created by santosh kumar singh on 09/05/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import "RegisterSuccessViewController.h"
#import "DashboardViewController.h"
#import "MenuViewController.h"

@interface RegisterSuccessViewController ()
{
    AppDelegate * appDelegate;
}

@end

@implementation RegisterSuccessViewController

- (void)viewDidLoad
{
    
    NSString *path=[[NSBundle mainBundle]pathForResource:@"waiting" ofType:@"gif"];
       NSURL *url=[[NSURL alloc] initFileURLWithPath:path];
       NSData *date= [NSData dataWithContentsOfURL:url];
       self.bgImageView.image= [UIImage sd_imageWithGIFData:date];
       
    
    self.title= @"REQUEST SENT";
    
    [super viewDidLoad];
    
    self.lblName.text = self.servicename;
    [self navigationBarConfiguration];
    
    if ([self.fromScreen isEqualToString:@"Carbooking"])
    {
        [self serviceLoginUserByEmail];
    }
    // Do any additional setup after loading the view.
}

-(void)navigationBarConfiguration
{
    
    self.navigationController.navigationBarHidden = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.navigationController.navigationBarHidden = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    UIButton * menuButton  = [UIButton buttonWithType:UIButtonTypeSystem];
    menuButton.frame = CGRectMake(8, 18, 24, 26);
    [menuButton setImage:[UIImage imageNamed:IMG_BACK] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *accountBarItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    self.navigationItem.leftBarButtonItem = accountBarItem;
    
     NSMutableArray *arrRightBarItems = [[NSMutableArray alloc] init];
     UIButton *btnSetting = [UIButton buttonWithType:UIButtonTypeCustom];
     [btnSetting setImage:[UIImage imageNamed:@"Frequency"] forState:UIControlStateNormal];
     btnSetting.frame = CGRectMake(0, 0, 40, 40);
     btnSetting.showsTouchWhenHighlighted=YES;
     [btnSetting addTarget:self action:@selector(onSettings:) forControlEvents:UIControlEventTouchUpInside];
     UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSetting];
     
     UIButton *btnLib = [UIButton buttonWithType:UIButtonTypeCustom];
     [btnLib setImage:[UIImage imageNamed:@"logo1.png"] forState:UIControlStateNormal];
     btnLib.frame = CGRectMake(0, 0, 40, 40);
     btnLib.showsTouchWhenHighlighted=YES;
     [btnLib addTarget:self action:@selector(onMyLibrary:) forControlEvents:UIControlEventTouchUpInside];
     UIBarButtonItem *barButtonItem2 = [[UIBarButtonItem alloc] initWithCustomView:btnLib];
     [arrRightBarItems addObject:barButtonItem2];
     [arrRightBarItems addObject:barButtonItem];
     self.navigationItem.rightBarButtonItems=arrRightBarItems;
     
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor],
    }];
    
}
-(void)onMyLibrary:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}
-(void)onSettings:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}


-(void)back:(id)sender
{
    appDelegate = UIAppDelegate;
    DashboardViewController * homePassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBID_ARRIVED_VC);
    UIAppDelegate.isCall  = YES;
    MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
    [appDelegate manageDrawerRear:menuViewController front:homePassengerViewController];
}


- (void)serviceLoginUserByEmail
{
    [Alert svProgress:@"Please Wait..."];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // time-consuming task
        
        NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults] dictionaryForKey:kLOGIN];
        NSURL* requestURL = [NSURL URLWithString:kURL_BASE];
        
        NSString * user_id  = [dictLogin valueForKey:@"userId"];
        
        NSMutableDictionary * dict                              = [NSMutableDictionary dictionary];
        [dict setObject:@"carbooking"                    forKey:kAPI_ACTION];
        [dict setObject:user_id                     forKey:@"userId"];
        [dict setObject:self.invoiceId  forKey:@"invoiceId"];
        [dict setObject:self.vehicleId  forKey:@"categoryId"];
        [dict setObject:self.servicename  forKey:@"serviceName"];
        [dict setObject:self.pickLocation  forKey:@"RequestPickupAddress"];
        [dict setObject:self.dropLocation forKey:@"RequestDropAddress"];
        [dict setObject:self.picklatlong forKey:@"RequestPickupLatLong"];
        [dict setObject:self.droplatlong  forKey:@"RequestDropLatLong"];
       
        NSLog(@"dict is ---%@",dict);
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setHTTPShouldHandleCookies:NO];
        [request setTimeoutInterval:60];
        [request setHTTPMethod:@"POST"];
        NSString *boundary = @"------VohpleBoundary4QuqLuM1cE5lMwCy";
        NSString* FileParamConstant = @"image";
        
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
        [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        
        for (NSString *param in dict)
        {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@\r\n", [dict objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        if (self.imagedata)
        {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"invoiceImage\"; filename=\"image.jpg\"\r\n", FileParamConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:self.imagedata];
            [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        [body appendData:[[NSString stringWithFormat:@"--%@--\r\n\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // setting the body of the post to the reqeust
        [request setHTTPBody:body];
        
        // set the content-length
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        // set URL
        [request setURL:requestURL];
        
        NSURLResponse *response = nil;
        NSError *requestError = nil;
        NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
        
        
        if (requestError == nil)
        {
            [SVProgressHUD dismiss];
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:returnData options:kNilOptions error:&requestError];
            
            NSLog(@"json is ---%@",json);
            dispatch_async(dispatch_get_main_queue(), ^{
            
            if ([[json objectForKey:@"status"] isEqualToString:@"success"])
            {
                [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"Editbusinessaddresss"];
                [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"lat"];
                [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"long"];
            
            }
            else
            {
                UIAlertController * alert = [UIAlertController
                                             alertControllerWithTitle:[json objectForKey:@"msg"]
                                             message:nil
                                             preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* yesButton = [UIAlertAction
                                            actionWithTitle:@"Ok"
                                            style:UIAlertActionStyleDestructive
                                            handler:^(UIAlertAction * action)
                                            {
                       
                        [SVProgressHUD dismiss];
                        DISMISS_VIEW_CONTROLLER;
                  
                    
                }];
                [alert addAction:yesButton];
                
                [self presentViewController:alert animated:YES completion:nil];
            }
            });
        }
        else
        {
            
            [Alert svError:requestError.localizedDescription];
        }
       
    });
    
}



/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
