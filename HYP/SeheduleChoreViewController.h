//
//  SeheduleChoreViewController.h
//  CHORETHING
//
//  Created by santosh kumar singh on 06/04/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FSCalendar/FSCalendar.h>




@interface SeheduleChoreViewController : UIViewController < WSCalendarViewDelegate >
{
    NSString *dateString;
}
@property (strong, nonatomic) NSDictionary * dict;
@property (strong, nonatomic) NSString * addressType;
@property (strong, nonatomic) NSString * addCurrentlocaton;
@property (nonatomic, retain) NSString* latitudeString;
@property (nonatomic, retain) NSString* longitudeString;
@property (strong, nonatomic) IBOutlet AsyncImageView *imageview;
@property (weak, nonatomic) IBOutlet UITextField *txtHours;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLable;

@property (strong, nonatomic) NSString * groupid;
@end


