//
//  AppDelegate.m
//  HYP
//
//  Created by santosh kumar singh on 29/08/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import "AppDelegate.h"
#import "MenuViewController.h"
#import <CoreLocation/CoreLocation.h>
#import <UserNotifications/UserNotifications.h>
#import "DashboardViewController.h"
#import "MenuViewController.h"
#import "BookingConfirmedViewController.h"
#import "InvoiceDetailViewController.h"
#import "ChoreDetailViewController.h"
#import "DriverDashboardViewController.h"
#import "DriverRegisterViewController.h"
#import "WaitingPassengerViewController.h"


@import Firebase;
@import FirebaseMessaging;


@interface AppDelegate ()<CLLocationManagerDelegate,UNUserNotificationCenterDelegate,FIRMessagingDelegate,WebServiceDelegate>
{
    
    CLLocationManager *locationManager;
}

@end

@implementation AppDelegate
NSString *const kGCMMessageIDKey = @"gcm.message_id";


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    UIAppDelegate.cellDefaltSeleted = YES;
    
    [GMSServices provideAPIKey:@"AIzaSyDEYP9nenPV-zJjlCSbPuyf9eYSOfBdKlk"];
    [GMSPlacesClient provideAPIKey:@"AIzaSyDEYP9nenPV-zJjlCSbPuyf9eYSOfBdKlk"];
    
    
    [UINavigationBar appearance].barTintColor = [Alert colorFromHexString:kCOLOR_NAV];
    [UINavigationBar appearance].tintColor = [Alert colorFromHexString:kCOLOR_TINT_NAV];
    if (locationManager == nil)
    {
        locationManager = [[CLLocationManager alloc] init];
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
        locationManager.delegate = self;
        [locationManager requestAlwaysAuthorization];
        
    }
    
    [locationManager startUpdatingLocation];
    
    [FIRApp configure];
    [FIRMessaging messaging].remoteMessageDelegate = self;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tokenRefreshNotification:)
                                                 name:kFIRInstanceIDTokenRefreshNotification object:nil];
    
    if ([UNUserNotificationCenter class] != nil) {
        // iOS 10 or later
        // For iOS 10 display notification (sent via APNS)
        [UNUserNotificationCenter currentNotificationCenter].delegate = self;
        UNAuthorizationOptions authOptions = UNAuthorizationOptionAlert |
        UNAuthorizationOptionSound | UNAuthorizationOptionBadge;
        [[UNUserNotificationCenter currentNotificationCenter]
         requestAuthorizationWithOptions:authOptions
         completionHandler:^(BOOL granted, NSError * _Nullable error) {
            // ...
        }];
    } else
    {
        // iOS 10 notifications aren't available; fall back to iOS 8-9 notifications.
        UIUserNotificationType allNotificationTypes =
        (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings =
        [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
        [application registerUserNotificationSettings:settings];
    }
    [application registerForRemoteNotifications];
    
    [AppDelegate appDelegate].genderArr = @[@"Male",@"Female",@"Transgender",@"Other"];
    [AppDelegate appDelegate].yesNoArr = @[@"Yes",@"No"];
    
    [AppDelegate appDelegate].sendArr = @[@"Cash app",@"Paypal",@"Stripe Payment"];
    
    [[STPPaymentConfiguration sharedConfiguration] setPublishableKey:@"pk_live_51HPptWKjKY5Kxs7Ijo6OWRboN3uYXIS3e6uFM8FAYHoiDjKPEb7xuH6gjYSOk6G3eRD7a7YLTBgecNEtCcLxxTrh00iS6Ot1rK"];
    
    
    [self serviceStateList];
    [self serviceVehicleList];
    [self serviceCategoryList];
    [self serviceBenefitList];
    
    NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults] objectForKey:kLOGIN];
    
    if (!dictLogin){
        
    }
    else
    {
        
        NSString * str = [dictLogin valueForKey:@"role"];
        
        if ([str isEqualToString:@"Driver"])
        {
            
            self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
            [self.window makeKeyAndVisible];
            
            DriverDashboardViewController * homePassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"DriverDashboardViewController");
            MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
            [self manageDrawerRear:menuViewController front:homePassengerViewController];
            
        }
        else
        {
            
            self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
            [self.window makeKeyAndVisible];
            
            DashboardViewController * homePassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBID_ARRIVED_VC);
            MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
            [self manageDrawerRear:menuViewController front:homePassengerViewController];
        }
    }
    // Override point for customization after application launch.
    return YES;
}

+ (AppDelegate *)appDelegate
{        // Static accessor
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    NSLog(@"Cannot find the location.");
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *mostRecentLocation = locations.lastObject;
    NSLog(@"Current location: %@ %@", @(mostRecentLocation.coordinate.latitude), @(mostRecentLocation.coordinate.longitude));
    
    [AppDelegate appDelegate].latitude = [NSString stringWithFormat:@"%f",mostRecentLocation.coordinate.latitude];
    [AppDelegate appDelegate].longitude = [NSString stringWithFormat:@"%f",mostRecentLocation.coordinate.longitude];
    
    NSLog(@"*dLatitude : %@", [AppDelegate appDelegate].latitude);
    NSLog(@"*dLongitude : %@",[AppDelegate appDelegate].longitude);
    
    
}
#pragma mark - manage Drawer
-(void)manageDrawerRear:(UIViewController *)rear front:(UIViewController *)front
{
    
    KYDrawerController * kYDrawerController =[[KYDrawerController alloc]init];
    
    kYDrawerController.mainViewController = [[UINavigationController alloc]initWithRootViewController:front];
    
    kYDrawerController.drawerViewController = rear;
    
    kYDrawerController.drawerDirection = KYDrawerControllerDrawerDirectionLeft;
    kYDrawerController.drawerWidth     = 270;
    
    [[UIApplication sharedApplication].keyWindow setRootViewController:kYDrawerController];
    
}

#pragma mark - set Initial View Controller
-(void)setInitialViewController:(UIViewController *)controller
{
    UINavigationController * navigationController = [[UINavigationController alloc]initWithRootViewController:controller];
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    [self.window makeKeyAndVisible];
    
    [[UIApplication sharedApplication].keyWindow setRootViewController:navigationController];
    
}
-(void)serviceBenefitList{
    
    // SERVER CONNCETION
    [Alert svProgress:@"Please Wait..."];
    
    WebService * connection = [[WebService alloc]init];
    connection.delegate = self;
    NSMutableDictionary * dict                              = [NSMutableDictionary dictionary];
    
    [dict setObject:@"benifits"                     forKey:kAPI_ACTION];
    
    NSLog(@"dict is ---%@",dict);
    [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:@"benifits"];
    
}
-(void)serviceCategoryList{
    
    // SERVER CONNCETION
    [Alert svProgress:@"Please Wait..."];
    
    WebService * connection = [[WebService alloc]init];
    connection.delegate = self;
    NSMutableDictionary * dict                              = [NSMutableDictionary dictionary];
    
    [dict setObject:@"profession"                     forKey:kAPI_ACTION];
    
    NSLog(@"dict is ---%@",dict);
    [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:@"profession"];
    
}

-(void)serviceStateList{
    
    // SERVER CONNCETION
    [Alert svProgress:@"Please Wait..."];
    
    WebService * connection = [[WebService alloc]init];
    connection.delegate = self;
    NSMutableDictionary * dict                              = [NSMutableDictionary dictionary];
    
    [dict setObject:@"statelist"                     forKey:kAPI_ACTION];
    
    NSLog(@"dict is ---%@",dict);
    [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:@"statelist"];
    
}
-(void)serviceVehicleList{
    
    // SERVER CONNCETION
    [Alert svProgress:@"Please Wait..."];
    
    WebService * connection = [[WebService alloc]init];
    connection.delegate = self;
    NSMutableDictionary * dict                              = [NSMutableDictionary dictionary];
    
    [dict setObject:@"vehiclelist"                     forKey:kAPI_ACTION];
    
    NSLog(@"dict is ---%@",dict);
    [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:@"vehiclelist"];
    
}

#pragma mark - Sevice delgate methods

-(void)getDataFormWebService:(NSDictionary *)jsonResults urlStr:(NSString *)yourUR methodName:(NSString *)methodName{
    
    [SVProgressHUD dismiss];
    
    SWITCH (methodName) {
        
        CASE (@"profession")
        {
            
            [AppDelegate appDelegate].professionArr = [jsonResults objectForKey:@"data"];
            
        }
        CASE (@"benifits")
        {
            
            [AppDelegate appDelegate].benefitArr = [jsonResults objectForKey:@"data"];
            
        }
        CASE (@"statelist")
        {
            
            [AppDelegate appDelegate].stateArr = [jsonResults objectForKey:@"data"];
            
        }
        CASE (@"vehiclelist")
        {
            
            [AppDelegate appDelegate].vehicleArr = [jsonResults objectForKey:@"data"];
            
        }
        break;
        
        DEFAULT
        {
            break;
        }
    }
    
}
-(void) webServiceFailWithApplicationServerMSG:(NSString *)msg
{
    
    [Alert svError:msg];
    
}

// WEB SERVICE FAIL WITH REGION TCP/IP
-(void) webServiceFail:(NSError *)error{
    
    
    [Alert svError:[error localizedDescription]];
    
    
}
#pragma mark - Core Data stack

@synthesize persistentContainer = _persistentContainer;

- (NSPersistentContainer *)persistentContainer {
    // The persistent container for the application. This implementation creates and returns a container, having loaded the store for the application to it.
    @synchronized (self) {
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"HYP"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                if (error != nil) {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    
                    /*
                     Typical reasons for an error here include:
                     * The parent directory does not exist, cannot be created, or disallows writing.
                     * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                     * The device is out of space.
                     * The store could not be migrated to the current model version.
                     Check the error message to determine what the actual problem was.
                     */
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
    }
    
    return _persistentContainer;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}


// [START receive_message]
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    // If you are receiving a notification message while your app is in the background,
    // this callback will not be fired till the user taps on the notification launching the application.
    // TODO: Handle data of notification
    
    // With swizzling disabled you must let Messaging know about the message, for Analytics
    // [[FIRMessaging messaging] appDidReceiveMessage:userInfo];
    
    // Print message ID.
    if (userInfo[kGCMMessageIDKey]) {
        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
    }
    
    // Print full message.
    NSLog(@"%@", userInfo);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    // If you are receiving a notification message while your app is in the background,
    // this callback will not be fired till the user taps on the notification launching the application.
    // TODO: Handle data of notification
    
    // With swizzling disabled you must let Messaging know about the message, for Analytics
    // [[FIRMessaging messaging] appDidReceiveMessage:userInfo];
    
    // Print message ID.
    if (userInfo[kGCMMessageIDKey]) {
        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
    }
    
    // Print full message.
    NSLog(@"%@", userInfo);
    
    completionHandler(UIBackgroundFetchResultNewData);
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {
    NSDictionary *userInfo = notification.request.content.userInfo;
    
    // With swizzling disabled you must let Messaging know about the message, for Analytics
    // [[FIRMessaging messaging] appDidReceiveMessage:userInfo];
    
    // Print message ID.
    if (userInfo[kGCMMessageIDKey]) {
        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
    }
    
    // Print full message.
    NSLog(@"%@", userInfo);
    NSString * notificationType = [userInfo valueForKey:@"message"];
    
    if ([notificationType isEqualToString:@"New booking request for Confirm or Cancel."] || [[userInfo valueForKey:@"type"] isEqualToString:@"deliveryService"] )
    {
        
        DriverDashboardViewController *waitingPassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"DriverDashboardViewController");
        waitingPassengerViewController.notificationDict = userInfo;
        MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
        [self manageDrawerRear:menuViewController front:waitingPassengerViewController];
        
    }
    else   if ([[userInfo valueForKey:@"type"] isEqualToString:@"DriverConfrim"] )
    {
        WaitingPassengerViewController *waitingPassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"WaitingPassengerViewController");
        waitingPassengerViewController.dictionaryNotification = userInfo;
        MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
        [self manageDrawerRear:menuViewController front:waitingPassengerViewController];
        
    }
    else   if ([[userInfo valueForKey:@"type"] isEqualToString:@"pickuped"] )
    {
        WaitingPassengerViewController *waitingPassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"WaitingPassengerViewController");
        waitingPassengerViewController.dictionaryNotification = userInfo;
        MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
        [self manageDrawerRear:menuViewController front:waitingPassengerViewController];
        
    }
    else   if ([[userInfo valueForKey:@"type"] isEqualToString:@"deliver"] )
    {
        WaitingPassengerViewController *waitingPassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"WaitingPassengerViewController");
        waitingPassengerViewController.dictionaryNotification = userInfo;
        MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
        [self manageDrawerRear:menuViewController front:waitingPassengerViewController];
        
    }
    else   if ([[userInfo valueForKey:@"type"] isEqualToString:@"pickuplocation"] )
    {
        WaitingPassengerViewController *waitingPassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"WaitingPassengerViewController");
        waitingPassengerViewController.dictionaryNotification = userInfo;
        MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
        [self manageDrawerRear:menuViewController front:waitingPassengerViewController];
        
    }
    else  if ([[userInfo valueForKey:@"type"] isEqualToString:@"ridestart"] )
    {
        WaitingPassengerViewController *waitingPassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"WaitingPassengerViewController");
        waitingPassengerViewController.dictionaryNotification = userInfo;
        MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
        [self manageDrawerRear:menuViewController front:waitingPassengerViewController];
        
    }
    else  if ([[userInfo valueForKey:@"type"] isEqualToString:@"pickuplocation"] )
    {
        WaitingPassengerViewController *waitingPassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"WaitingPassengerViewController");
        waitingPassengerViewController.dictionaryNotification = userInfo;
        MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
        [self manageDrawerRear:menuViewController front:waitingPassengerViewController];
        
    }
    else  if ([[userInfo valueForKey:@"type"] isEqualToString:@"arrived"] )
    {
        WaitingPassengerViewController *waitingPassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"WaitingPassengerViewController");
        waitingPassengerViewController.dictionaryNotification = userInfo;
        MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
        [self manageDrawerRear:menuViewController front:waitingPassengerViewController];
        
    }
    else   if ([[userInfo valueForKey:@"type"] isEqualToString:@"Payment"] )
    {
       
    }
    else   if ([[userInfo valueForKey:@"type"] isEqualToString:@"rideend"] )
    {
        DashboardViewController *waitingPassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"DashboardViewController");
        waitingPassengerViewController.notificationDict = userInfo;
        MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
        [self manageDrawerRear:menuViewController front:waitingPassengerViewController];
        
    }
    else  if ([notificationType isEqualToString:@"New booking request to confirm or decline."] )
    {
        
        DashboardViewController *waitingPassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"DashboardViewController");
        waitingPassengerViewController.notificationDict = userInfo;
        MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
       [self manageDrawerRear:menuViewController front:waitingPassengerViewController];
        
    }
    else  if ([notificationType isEqualToString:@" has been accepted your booking successfully."])
    {
        
        BookingConfirmedViewController * settingsViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"BookingConfirmedViewController");
         settingsViewController.matchFrom = @"appdelagate";
        settingsViewController.notificationDict = userInfo;
        MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
        [self manageDrawerRear:menuViewController front:settingsViewController];
        
        
    }
    else  if ([notificationType containsString:@" sent you invoice"])
    {
        
        InvoiceDetailViewController * bookingHistoryViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"InvoiceDetailViewController");
        bookingHistoryViewController.notificationDict = userInfo;
        // settingsViewController.matchFrom = @"appdelagate";
        MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
        [self manageDrawerRear:menuViewController front:bookingHistoryViewController];
        
    }
     else if ([notificationType containsString:@"Your payment has been completed"] && [[userInfo valueForKey:@"type"] isEqualToString:@"carupdatePayment"])
       {
           
           DriverDashboardViewController *waitingPassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"DriverDashboardViewController");
            MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
           [self manageDrawerRear:menuViewController front:waitingPassengerViewController];
           
       }
    
     else if ([notificationType containsString:@"Your booking has been completed."])
       {
           
       }
     else if ([notificationType containsString:@"has been completed successfully."])
       {
           
       }
     else
     {
           
           ChoreDetailViewController * bookingHistoryViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"ChoreDetailViewController");
           bookingHistoryViewController.dict = userInfo;
            //settingsViewController.matchFrom = @"appdelagate";
           MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
           [self manageDrawerRear:menuViewController front:bookingHistoryViewController];
           
       }
    
    
    completionHandler(UNNotificationPresentationOptionAlert);
}

// Handle notification messages after display notification is tapped by the user.
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
didReceiveNotificationResponse:(UNNotificationResponse *)response
         withCompletionHandler:(void(^)(void))completionHandler {
    NSDictionary *userInfo = response.notification.request.content.userInfo;
    if (userInfo[kGCMMessageIDKey]) {
        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
    }
    
    // Print full message.
    NSLog(@"%@", userInfo);
    NSDictionary * aps = [userInfo objectForKey:@"aps"];
    
    NSString * notificationType = [userInfo valueForKey:@"message"];
    
    if ([notificationType isEqualToString:@"New booking request for Confirm or Cancel."] && [[userInfo valueForKey:@"type"] isEqualToString:@"deliveryService"] )
    {
        
        DriverDashboardViewController *waitingPassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"DriverDashboardViewController");
        waitingPassengerViewController.notificationDict = userInfo;
        
        MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
        
        [self manageDrawerRear:menuViewController front:waitingPassengerViewController];
        
    }
    else   if ([[userInfo valueForKey:@"type"] isEqualToString:@"Payment"] )
    {
       
    }
    else if ([[userInfo valueForKey:@"type"] isEqualToString:@"DriverConfrim"] )
    {
        WaitingPassengerViewController *waitingPassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"WaitingPassengerViewController");
        waitingPassengerViewController.dictionaryNotification = userInfo;
        MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
        [self manageDrawerRear:menuViewController front:waitingPassengerViewController];
        
    }
  else  if ([[userInfo valueForKey:@"type"] isEqualToString:@"deliver"] )
    {
        WaitingPassengerViewController *waitingPassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"WaitingPassengerViewController");
        waitingPassengerViewController.dictionaryNotification = userInfo;
        MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
        [self manageDrawerRear:menuViewController front:waitingPassengerViewController];
        
    }
  else  if ([[userInfo valueForKey:@"type"] isEqualToString:@"pickuplocation"] )
    {
        WaitingPassengerViewController *waitingPassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"WaitingPassengerViewController");
        waitingPassengerViewController.dictionaryNotification = userInfo;
        MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
        [self manageDrawerRear:menuViewController front:waitingPassengerViewController];
        
    }
  else  if ([[userInfo valueForKey:@"type"] isEqualToString:@"ridestart"])
    {
       
        WaitingPassengerViewController *waitingPassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"WaitingPassengerViewController");
        waitingPassengerViewController.dictionaryNotification = userInfo;
        MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
        [self manageDrawerRear:menuViewController front:waitingPassengerViewController];
        
    }
  else if ([[userInfo valueForKey:@"type"] isEqualToString:@"pickuped"] )
    {
       
        WaitingPassengerViewController *waitingPassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"WaitingPassengerViewController");
        waitingPassengerViewController.dictionaryNotification = userInfo;
        MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
        [self manageDrawerRear:menuViewController front:waitingPassengerViewController];
        
    }
  else if ([[userInfo valueForKey:@"type"] isEqualToString:@"pickuplocation"] )
    {
        WaitingPassengerViewController *waitingPassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"WaitingPassengerViewController");
        waitingPassengerViewController.dictionaryNotification = userInfo;
        MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
        [self manageDrawerRear:menuViewController front:waitingPassengerViewController];
        
    }
   else  if ([[userInfo valueForKey:@"type"] isEqualToString:@"arrived"] )
    {
      
        WaitingPassengerViewController *waitingPassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"WaitingPassengerViewController");
        waitingPassengerViewController.dictionaryNotification = userInfo;
        MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
        [self manageDrawerRear:menuViewController front:waitingPassengerViewController];
        
   }
    else  if([[userInfo valueForKey:@"type"] isEqualToString:@"rideend"] )
    {
        
       DashboardViewController *waitingPassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"DashboardViewController");
        waitingPassengerViewController.notificationDict = userInfo;
        MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
        [self manageDrawerRear:menuViewController front:waitingPassengerViewController];
        
    }
    else if([notificationType isEqualToString:@"New booking request to confirm or decline."])
    {
        DashboardViewController *waitingPassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"DashboardViewController");
        waitingPassengerViewController.notificationDict = userInfo;
        waitingPassengerViewController.matchFrom = @"appdelagate";
        MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
        [self manageDrawerRear:menuViewController front:waitingPassengerViewController];
        
    }
    else  if([notificationType isEqualToString:@" has been accepted your booking successfully."])
    {
        BookingConfirmedViewController * settingsViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"BookingConfirmedViewController");
        settingsViewController.dict = userInfo;
        settingsViewController.matchFrom = @"appdelagate";
        MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
        [self manageDrawerRear:menuViewController front:settingsViewController];
        
    }
    else if ([notificationType containsString:@" sent you invoice"])
    {
        
        InvoiceDetailViewController * bookingHistoryViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"InvoiceDetailViewController");
        bookingHistoryViewController.notificationDict = userInfo;
         //settingsViewController.matchFrom = @"appdelagate";
        MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
        [self manageDrawerRear:menuViewController front:bookingHistoryViewController];
        
    }

    else  if ([notificationType containsString:@"Your payment has been completed"] && [[userInfo valueForKey:@"type"] isEqualToString:@"carupdatePayment"])
       {
           
           DriverDashboardViewController *waitingPassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"DriverDashboardViewController");
            MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
           [self manageDrawerRear:menuViewController front:waitingPassengerViewController];
           
       }
    
     else if ([notificationType containsString:@"Your booking has been completed."])
       {
           
       }
     else if ([notificationType containsString:@"has been completed successfully."])
       {
           
       }
    else  {
          
          ChoreDetailViewController * bookingHistoryViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"ChoreDetailViewController");
          bookingHistoryViewController.dict = userInfo;
           //settingsViewController.matchFrom = @"appdelagate";
          MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
          [self manageDrawerRear:menuViewController front:bookingHistoryViewController];
          
      }
    
    completionHandler();
}

// [END ios_10_message_handling]

// [START refresh_token]
- (void)messaging:(FIRMessaging *)messaging didReceiveRegistrationToken:(NSString *)fcmToken
{
    NSLog(@"FCM registration token: %@", fcmToken);
    [[NSUserDefaults standardUserDefaults] setObject:[[FIRInstanceID  instanceID] token] forKey:kAPI_DEVICE_TOKEN];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    NSString *refreshedToken = [[FIRInstanceID instanceID] token];
    
    [self sendProviderDeviceToken:refreshedToken];
    
    [[NSUserDefaults standardUserDefaults] setObject: refreshedToken forKey:@"firebaseID"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    NSDictionary *dataDict = [NSDictionary dictionaryWithObject:fcmToken forKey:@"token"];
    [[NSNotificationCenter defaultCenter] postNotificationName:
     @"FCMToken" object:nil userInfo:dataDict];
    // TODO: If necessary send token to application server.
    // Note: This callback is fired at each app startup and whenever a new token is generated.
}
- (void)messaging:(FIRMessaging *)messaging didReceiveMessage:(FIRMessagingRemoteMessage *)remoteMessage
{
    NSLog(@"Received data message: %@", remoteMessage.appData);
}
// [END ios_10_data_message]

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"Unable to register for remote notifications: %@", error);
}


- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    
    NSString *refreshedToken = [[FIRInstanceID instanceID] token];
    
    [self sendProviderDeviceToken:refreshedToken];
    
    NSLog(@"InstanceID token: %@", refreshedToken);
    
    [[NSUserDefaults standardUserDefaults] setObject: refreshedToken forKey:kAPI_DEVICE_TOKEN];
    
    [[NSUserDefaults standardUserDefaults]synchronize];
    [self connectToFcm];
}



- (void) sendProviderDeviceToken:(NSData*)token{
    
    NSString* tokenStr = [[token description] stringByReplacingOccurrencesOfString:@"<" withString:@""];
    tokenStr = [tokenStr stringByReplacingOccurrencesOfString:@">" withString:@""];
    NSArray* words = [tokenStr componentsSeparatedByCharactersInSet :
                      [NSCharacterSet whitespaceCharacterSet]];
    tokenStr = [words componentsJoinedByString:@""];
    
    NSLog(@"APNs token retrieved: %@", tokenStr);
    
#if TARGET_IPHONE_SIMULATOR
    
    [[NSUserDefaults standardUserDefaults] setObject: @"f6a165bf39a0aa14c1909bc62396eb6981f6fa67bd1735958a9445eb88a6e1b3" forKey:@"deviceID"];
    
#else
    
    [[NSUserDefaults standardUserDefaults] setObject:tokenStr forKey:@"kAPI_DEVICE_TOKEN"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
#endif
    
}

- (void)tokenRefreshNotification:(NSNotification *)notification {
    
    
    NSString *refreshedToken = [[FIRInstanceID instanceID] token];
    NSLog(@"InstanceID token: %@", refreshedToken);
    
    [[NSUserDefaults standardUserDefaults] setObject: refreshedToken forKey:kAPI_DEVICE_TOKEN];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [self connectToFcm];
}
- (void)connectToFcm {
    
    if (![[FIRInstanceID instanceID] token])
    {
        return;
    }
    
    [[FIRMessaging messaging] disconnect];
    
    [[FIRMessaging messaging] connectWithCompletion:^(NSError * _Nullable error) {
        
        if (error != nil) {
            NSLog(@"Unable to connect to FCM. %@", error);
        } else {
            NSLog(@"Connected to FCM.");
        }
    }];
}


@end
