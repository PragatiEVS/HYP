//
//  BookingCompletedViewController.h
//  Q-Workerbee
//
//  Created by santosh kumar singh on 04/07/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface BookingCompletedViewController : UIViewController<GMSMapViewDelegate>
{
     CLLocationManager *loctionManager;
    CLLocation *myOrigin;
    CLLocation *myDestination ;
     GMSCameraPosition *camera;
     GMSMapView *mapView_;
     GMSMarker *locationMarker_;
     float latitude , latSearch;
     float longitude, lngSearch;
}


@property(nonatomic,strong)  GMSGeocoder     *geocoder;
@property(nonatomic,retain) CLLocationManager *locationManager;
@property (nonatomic, strong) IBOutlet UIView * mapView;

@property (strong, nonatomic) NSDictionary * dict;
@property (strong, nonatomic) NSString * totalAmount;
@property (strong, nonatomic) NSString * groupid;
@property (weak, nonatomic)  NSString * servicename;

@property(nonatomic, strong, readwrite) NSString *dateString;
@property(nonatomic, strong, readwrite) NSString *timeString;

@property (strong, nonatomic)IBOutlet UILabel * bokkingDateLabel;
@property (strong, nonatomic)IBOutlet UILabel * toatlAmountLabel;

@property (strong, nonatomic)IBOutlet RateView * rateview;
@property (strong, nonatomic)IBOutlet UIView * userDetailView;
@property (strong, nonatomic)IBOutlet UIView * detailView;
@property (strong, nonatomic)IBOutlet UILabel * username;
@property (strong, nonatomic)IBOutlet UIImageView * userImage;
@property (strong, nonatomic)IBOutlet UILabel * serviceNameLabel;


@property (strong, nonatomic)IBOutlet UILabel * serviceAddressLabel;

@property (strong, nonatomic)IBOutlet UILabel * currentAddressLabel;

@property (strong, nonatomic)IBOutlet UIButton * cancelBtn;
@property (strong, nonatomic)IBOutlet UIButton * callBtn;

@end


