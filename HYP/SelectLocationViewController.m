//
//  SelectLocationViewController.m
//  CHORETHING
//
//  Created by santosh kumar singh on 25/03/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import "SelectLocationViewController.h"
#import "AppDelegate.h"
#import "SeheduleChoreViewController.h"

@interface SelectLocationViewController ()

@end

@implementation SelectLocationViewController

- (void)viewDidLoad
{
    self.title= @"SELECT ADDRESS";
    
    [self navigationBarConfiguration];
    
    self.addressTextfield.layer.cornerRadius = 5.0f;
    self.addressTextfield.clipsToBounds= YES;
    
    coordinate = [self getLocation];
    latitude = coordinate.latitude;
    longitude = coordinate.longitude;
    
    [AppDelegate appDelegate].latitude = [NSString stringWithFormat:@"%f",coordinate.latitude];
    [AppDelegate appDelegate].longitude = [NSString stringWithFormat:@"%f",coordinate.longitude];
    
    self.lblName.text = [NSString stringWithFormat:@"Selected Services: %@",[_dict objectForKey:@"name"]];
    self.lblName.numberOfLines = 2;
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
     
     self.latitudeString = [AppDelegate appDelegate].latitude;
     self.longitudeString = [AppDelegate appDelegate].longitude;
       
       latitude = [[AppDelegate appDelegate].latitude floatValue];
       longitude = [[AppDelegate appDelegate].longitude floatValue];
    
      self.geocoder = [[GMSGeocoder alloc]init];
      [self showGoogleMap];
    
}
#pragma mark - Configure Navigation Controller

-(void)navigationBarConfiguration
{
    
    self.navigationController.navigationBarHidden = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    UIButton * menuButton  = [UIButton buttonWithType:UIButtonTypeSystem];
    menuButton.frame = CGRectMake(8, 18, 24, 24);
    [menuButton setBackgroundImage:[UIImage imageNamed:IMG_BACK] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *accountBarItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    self.navigationItem.leftBarButtonItem = accountBarItem;
    
    NSMutableArray *arrRightBarItems = [[NSMutableArray alloc] init];
             UIButton *btnSetting = [UIButton buttonWithType:UIButtonTypeCustom];
             [btnSetting setImage:[UIImage imageNamed:@"Frequency"] forState:UIControlStateNormal];
             btnSetting.frame = CGRectMake(0, 0, 40, 40);
             btnSetting.showsTouchWhenHighlighted=YES;
            [btnSetting addTarget:self action:@selector(onSettings:) forControlEvents:UIControlEventTouchUpInside];
             UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSetting];
           
             UIButton *btnLib = [UIButton buttonWithType:UIButtonTypeCustom];
             [btnLib setImage:[UIImage imageNamed:@"logo1.png"] forState:UIControlStateNormal];
             btnLib.frame = CGRectMake(0, 0, 40, 40);
             btnLib.showsTouchWhenHighlighted=YES;
            [btnLib addTarget:self action:@selector(onMyLibrary:) forControlEvents:UIControlEventTouchUpInside];
             UIBarButtonItem *barButtonItem2 = [[UIBarButtonItem alloc] initWithCustomView:btnLib];
             [arrRightBarItems addObject:barButtonItem2];
             [arrRightBarItems addObject:barButtonItem];
             self.navigationItem.rightBarButtonItems=arrRightBarItems;
          
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor],
    }];
    
}
-(void)onMyLibrary:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}

-(void)onSettings:(id)sender
{
  
    NSString*myurl=@"https://www.hypdemand.com/";
     NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
       [[UIApplication sharedApplication] openURL:url];
    }
}


#pragma mark --- : GOOGLE MAP :----
-(void)showGoogleMap
{
   
    CLLocation * upadetLocation = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
    
    CGRect frame = CGRectMake(0, 0, kSCREEN_WIDTH,kSCREEN_HEIGHT);
    camera = [GMSCameraPosition cameraWithLatitude:latitude longitude:longitude zoom:15];
    mapView_ = [GMSMapView mapWithFrame:frame camera:camera];
    mapView_.delegate = self;
    
    [mapView_ setCamera:camera];
   
   // satelliteFlyover
    [self reverGeoCodingUsingGoogle:upadetLocation];
    
    self->mapView_.myLocationEnabled              = YES;
    self->mapView_.indoorEnabled                  = YES;
    self->mapView_.settings.myLocationButton      = NO;
    self->mapView_.settings.scrollGestures        = YES;
    
    [_mapView addSubview:mapView_];
    
    locationMarker_ = [[GMSMarker alloc] init];
    locationMarker_.position = CLLocationCoordinate2DMake(latitude, longitude);
    locationMarker_.icon = [UIImage imageNamed:@"marker"];
    locationMarker_.map = mapView_;
    
}

-(void)reverGeoCodingUsingGoogle:(CLLocation *)location
{
    [[GMSGeocoder geocoder] reverseGeocodeCoordinate:CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude) completionHandler:^(GMSReverseGeocodeResponse* response, NSError* error)
     {
        
        GMSReverseGeocodeResult *result = response.firstResult;
        
        NSArray   * arrayAdd = result.lines;
        
        NSString  * address1;
        NSString  * address2;
        
        if (arrayAdd.count>1)
        {
            address1 = result.lines[0];
            address2 = result.subLocality;
        }
        else
        {
            address1 = result.lines[0];
        }
        
        self.state= result.administrativeArea;
        
        self.addressTextfield.titleLabel.numberOfLines = 2;
        self.addressTextfield.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 30);
        [self.addressTextfield setTitle:[NSString stringWithFormat:@"%@",address1] forState:UIControlStateNormal];
        
        
    }];
    
    
}

-(CLLocationCoordinate2D) getLocation
{
    CLLocationManager *locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager startUpdatingLocation];
    CLLocation *location = [locationManager location];
    CLLocationCoordinate2D coordinate = [location coordinate];
    
    return coordinate;
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    
    [self.locationManager stopUpdatingLocation];
    CLLocation *mostRecentLocation = [locations objectAtIndex:0];
    
}

// Handle the user's selection.
- (void)viewController:(GMSAutocompleteViewController *)viewController
didAutocompleteWithPlace:(GMSPlace *)place {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    // Do something with the selected place.
    NSLog(@"Place name %@", place.name);
    
    NSLog(@"Place address %@", place.formattedAddress);
    NSLog(@"Place latitude %f", place.coordinate.latitude);
    NSLog(@"Place longitude %f", place.coordinate.longitude);
    
    
    
    self.latitudeString = [NSString stringWithFormat:@"%f",place.coordinate.latitude];
    self.longitudeString = [NSString stringWithFormat:@"%f",place.coordinate.longitude];
    
    [mapView_ clear];
    
    CLLocation * upadetLocation = [[CLLocation alloc] initWithLatitude:place.coordinate.latitude longitude:place.coordinate.longitude];
    
    coordinate = CLLocationCoordinate2DMake(place.coordinate.latitude, place.coordinate.longitude);
    locationMarker_ = [[GMSMarker alloc] init];
    locationMarker_.position = CLLocationCoordinate2DMake(place.coordinate.latitude, place.coordinate.longitude);
    mapView_.myLocationEnabled = YES;
    locationMarker_.icon = [UIImage imageNamed:@"marker"];
    camera = [GMSCameraPosition cameraWithLatitude:coordinate.latitude longitude:coordinate.longitude zoom:ZOOM];
    [mapView_ animateToCameraPosition:camera];
    locationMarker_.map = mapView_;
    
    [self reverGeoCodingUsingGoogle:upadetLocation];
    
    
    
}
- (void)viewController:(GMSAutocompleteViewController *)viewController
didFailAutocompleteWithError:(NSError *)error {
    // TODO: handle the error.
    NSLog(@"error: %@", error);
    
    NSLog(@"error: %ld", [error code]);
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

// User canceled the operation.
- (void)wasCancelled:(GMSAutocompleteViewController *)viewController {
    NSLog(@"Autocomplete was cancelled.");
    
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - serach Location
-(IBAction)buttonLocationSearchClick:(id)sender
{
   GMSAutocompleteViewController *acController = [[GMSAutocompleteViewController alloc] init];
    acController.tintColor=[UIColor whiteColor];
        acController.delegate = self;
       [[UISearchBar appearance] setBarStyle:UIBarStyleDefault];
        [self presentViewController:acController animated:YES completion:nil];
    
}
-(IBAction)btnContinue:(UIButton*)sender
{
     SeheduleChoreViewController *forgotPasswordViewController =  GET_VIEW_CONTROLLER(kSBIDSeheduleChoreVC);
       
       forgotPasswordViewController.dict=  self.dict;
      forgotPasswordViewController.addCurrentlocaton = self.addressTextfield.titleLabel.text;
        forgotPasswordViewController.latitudeString = self.latitudeString;
        forgotPasswordViewController.longitudeString = self.longitudeString;
        
       UINavigationController * navigationController = [Alert navigationControllerWithVC:forgotPasswordViewController];
       PERSENT_VIEW_CONTOLLER(navigationController, YES);
}

-(void)back:(id)sender
{
    DISMISS_VIEW_CONTROLLER;
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
