//
//  TermsViewController.h
//  CHORETHING
//
//  Created by santosh kumar singh on 27/03/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface TermsViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView     * viewTerm;
@property (weak, nonatomic) IBOutlet UITextView * textViewTerm;
@property (weak, nonatomic) IBOutlet UIButton   * buttonCheck;
@property(nonatomic,strong)  NSString  * matchFrom;
@property(weak, nonatomic) IBOutlet MDTabBar *tabBar;
@property(weak, nonatomic)  NSString *matchRequest;
@end


