//
//  WebService.h
//  Americo
//
//  Created by Infoicon Technologies on 12/07/16.
//  Copyright © 2016 Infoicon Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppConstants.h"

@protocol WebServiceDelegate <NSObject>

-(void) getDataFormWebService:(NSDictionary *)jsonResults urlStr:(NSString *)urlStr methodName:(NSString *)methodName;
-(void) webServiceFail:(NSError *)error;
-(void) webServiceFailWithApplicationServerMSG:(NSString *)msg;

@end

@interface WebService : NSObject
{
    id <WebServiceDelegate> delegate;
}


@property (nonatomic, strong) id <WebServiceDelegate> delegate;
-(void)webServicePostString:(NSString *)postString urlString:(NSString *)urlSting methodName:(NSString *)methodName;
- (void)WebServiceUsingGETMethodURLString:(NSString *)urlSting methodName:(NSString *)methodName;
-(void)webserviceWithPostStr:(NSString *)postString withData:(NSData *)ns_data key:(NSString *)key urlString:(NSString *)urlSting methodName:(NSString *)methodName;

@end
