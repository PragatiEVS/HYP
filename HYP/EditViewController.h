//
//  EditViewController.h
//  CHORETHING
//
//  Created by santosh kumar singh on 01/04/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface EditViewController : UIViewController
{
     WSCalendarView *calendarView;
     NSData *imageData;
    int valrible;
}

@property (weak) IBOutlet NSLayoutConstraint *updateserviceConstant;

@property (strong, nonatomic) NSString *stateId;
@property(nonatomic,weak) IBOutlet UITableView  * tableView;
@property (weak, nonatomic) IBOutlet UIPickerView *dienstPicker;
@property (weak, nonatomic) IBOutlet UIButton *donePicker;
@property(nonatomic,weak)  NSString     * matchFrom;
@property(nonatomic,weak)  NSString     * matchScreen;
@property(nonatomic,weak) IBOutlet UIButton     * buttonBusinessInfo;
@property(nonatomic,weak) IBOutlet UIButton     * buttonUpdate;
@property(nonatomic,weak) IBOutlet UIButton     * buttonBank;
@property(nonatomic,weak) IBOutlet UIButton     * buttonService;

@property(nonatomic,weak) IBOutlet UIButton     * buttonImage;

@property(nonatomic,weak) IBOutlet UIView     * providerView;
@property(nonatomic,weak) IBOutlet UIView     * driverView;

@property(nonatomic,strong) NSString *lat;
@property(nonatomic,strong) NSString *longi;
@property (nonatomic, strong) UIPopoverController *popOver;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *textFName;

@property (weak) IBOutlet NSLayoutConstraint *widthConstraintOfZip;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *stateText;

@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *textLName;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *textEmail;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *phoneTextfield;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *textgender;
@property (weak, nonatomic) IBOutlet UITextField *dobtextfield;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *addressTextfield;

@end


