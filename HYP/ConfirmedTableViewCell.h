//
//  ConfirmedTableViewCell.h
//  CHORETHING
//
//  Created by santosh kumar singh on 02/04/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface ConfirmedTableViewCell : UITableViewCell

@property(nonatomic,strong)  IBOutlet UILabel *cratedLabel;
@property(nonatomic,strong)  IBOutlet UILabel *dateLabel;
@property(nonatomic,strong)  IBOutlet UILabel *pendingLabel;
@property(nonatomic,strong)  IBOutlet UILabel *nameLabel;
@property(nonatomic,strong)  IBOutlet UILabel *locationLabel;
@property(nonatomic,strong)  IBOutlet UILabel *priceLabel;
@property(nonatomic,strong)  IBOutlet UIImageView *userThumb;

@property(nonatomic,strong)  IBOutlet UIImageView *bgImage;
@end


