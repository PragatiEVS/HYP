//
//  BookingDetailsViewController.m
//  ROLUX
//
//  Created by Nitin Kumar on 08/12/16.
//  Copyright © 2016 Nitin Kumar. All rights reserved.
//

#import "BookingDetailsViewController.h"
///#import "RateSubmitViewController.h"


@interface BookingDetailsViewController ()<WebServiceDelegate>

@end

@implementation BookingDetailsViewController
@synthesize detailArray;
@synthesize indextag;
#pragma mark - ViewController Life Cycle
- (void)viewDidLoad
{
    
    
    NSLog(@"detail Array is ----%@",detailArray);
    [_picLoc setBackgroundColor:[UIColor clearColor]];
    [_dropoffLoc setBackgroundColor:[UIColor clearColor]];
    
    [_picLoc setText:nil];
    [_dropoffLoc setText:nil];

    NSString *strPic = [[detailArray objectAtIndex:0]objectForKey:@"RequestPickupAddress"];
    
    if ([strPic length]>0)
    {
         [_locNameView setHidden:NO];
        _picLoc.text = [NSString stringWithFormat:@"%@",[[detailArray objectAtIndex:0]objectForKey:@"RequestPickupAddress"]];
        _dropoffLoc.text =[NSString stringWithFormat:@"%@",[[detailArray objectAtIndex:0]objectForKey:@"RequestDropAddress"]];
      
        if ([[[detailArray objectAtIndex:0]objectForKey:@"transactionId"] isEqualToString:@""]
            )
        {
            self.buttonReview.hidden = YES;
            self.buttoninvite.hidden = NO;
           
        }
        else
        {
            self.buttonReview.hidden = NO;
            self.buttoninvite.hidden = YES;
           
        }
    }
  else
  {
      [_locNameView setHidden:YES];
  }
    
    
    if ([[[detailArray objectAtIndex:0]objectForKey:@"transactionId"] isEqualToString:@""]
        )
    {
       self.buttonReview.hidden = YES;
        self.buttoninvite.hidden = NO;
       
    }
    else
    {
        self.buttonReview.hidden = NO;
        self.buttoninvite.hidden = YES;
       
    }
    
      _viewPayment.layer.cornerRadius = 10;
      
      // border
      [_viewPayment.layer setBorderColor:[UIColor lightGrayColor].CGColor];
      [_viewPayment.layer setBorderWidth:1.5f];
      
      // drop shadow
      [_viewPayment.layer setShadowColor:[UIColor grayColor].CGColor];
      [_viewPayment.layer setShadowOpacity:0.8];
      [_viewPayment.layer setShadowRadius:3.0];
      [_viewPayment.layer setShadowOffset:CGSizeMake(1.0, 1.0)];
    
    
//       self.buttoninvite.hidden = YES;
    
       self.buttoninvite.layer.cornerRadius = 10;
    self.buttonReview.layer.cornerRadius = 10;
 
       _viewDistance.layer.cornerRadius = 10;
      
      // border
      [_viewDistance.layer setBorderColor:[UIColor lightGrayColor].CGColor];
      [_viewDistance.layer setBorderWidth:1.5f];
      
      // drop shadow
      [_viewDistance.layer setShadowColor:[UIColor grayColor].CGColor];
      [_viewDistance.layer setShadowOpacity:0.8];
      [_viewDistance.layer setShadowRadius:3.0];
      [_viewDistance.layer setShadowOffset:CGSizeMake(1.0, 1.0)];
      
     
    self.title = @"INVOICES";

    // Do any additional setup after loading the view.

    // configure view controller
    
    [self configure];
    // show map
    
    // show Google Map
   
}




-(void)showRoute
{
    NSString * pickuplatLong =[[detailArray objectAtIndex:0]objectForKey:@"pickuplatLong"];
    
    NSString * droplatLong =[[detailArray objectAtIndex:0]objectForKey:@"droplatLong"];
    
    
    NSArray *item1          = [pickuplatLong componentsSeparatedByString:@","];
    
    NSString * strLatPic    = [item1 objectAtIndex:0];
    NSString * strLongPic   = [item1 objectAtIndex:1];
    
    
    CLLocation * locPicOff       = [[CLLocation alloc]initWithLatitude:[strLatPic floatValue] longitude:[strLongPic floatValue]];
    
    
    NSArray *item2          = [droplatLong componentsSeparatedByString:@","];
    
    NSString * strLatDrop    = [item2 objectAtIndex:0];
    NSString * strLongDrop   = [item2 objectAtIndex:1];
    
    
    CLLocation * locDrop       = [[CLLocation alloc]initWithLatitude:[strLatDrop floatValue] longitude:[strLongDrop floatValue]];
    
    
    GMSMarker *picoff = [[GMSMarker alloc]init];
    picoff.position   = CLLocationCoordinate2DMake([strLatPic floatValue], [strLongPic floatValue]);
    picoff.icon = [UIImage imageNamed:kIMG_DOT_CIRCLE_MARKER];
    picoff.map = mapView_;
    
    GMSMarker *dropoff = [[GMSMarker alloc]init];
    dropoff.position = CLLocationCoordinate2DMake([strLatDrop floatValue], [strLongDrop floatValue]);
    dropoff.icon = [UIImage imageNamed:kIMG_USER_DESTI_MARKER];
    dropoff.map = mapView_;
    dispatch_async(dispatch_get_main_queue(), ^{
      
        [self drawRouteOrigin:locPicOff destination:locDrop];
        
    });
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
  //  [_viewBottom setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.7]];
  //  [_viewTopView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.7]];
    
//    [self starRatingToDriver:5.0];
   
}

#pragma mark - configure view controller
-(void)configure
{
    // navigation bar Configure
    
    [self navigationBarConfiguration];
    NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults]  objectForKey:kLOGIN];
    
    
    NSString * user_id ;
    if ([dictLogin valueForKey:@"id"] == nil)
    {
        user_id  = [dictLogin valueForKey:@"userId"];
        
    }
    else
    {
        user_id  = [dictLogin valueForKey:@"id"];
    }
    
   // SERVER CONNCETION
        
        WebService * connection = [[WebService alloc]init];
        connection.delegate = self;
        
        NSMutableDictionary * dict       = [NSMutableDictionary dictionary];
        [dict setObject:@"carbookingdetail"                    forKey:kAPI_ACTION];
      [dict setObject:user_id                     forKey:kUSER_ID];
      [dict setObject:[[self.detailArray objectAtIndex:0] objectForKey:@"bookingId"]      forKey:@"bookingId"];
   
        [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:@"bookingdetail"];

}
-(void)getDataFormWebService:(NSDictionary *)jsonResults urlStr:(NSString *)yourUR methodName:(NSString *)methodName{
    
   
    SWITCH (methodName) {
        
        CASE (@"bookingdetail")
        {
            
               self.detailArray = [jsonResults objectForKey:@"data"];
            
               self.pickupAddressLabel.text = [[jsonResults  valueForKey:@"data"]objectForKey:@"RequestPickupAddress"];

               self.dropAddressLabel.text = [[jsonResults  valueForKey:@"data"]objectForKey:@"RequestDropAddress"];
               
               self.totalLabel.text = [NSString stringWithFormat:@"$ %@",[[jsonResults  valueForKey:@"data"]objectForKey:@"FinalFare"]];

                 self.toolLabel.text = [NSString stringWithFormat:@"$ %@",[[jsonResults  valueForKey:@"data"]objectForKey:@"FinalFare"]];
               self.discountLabel.text =[NSString stringWithFormat:@"%@",[[jsonResults  valueForKey:@"data"]objectForKey:@"serviceName"]];
               self.outstandingLabel.text = [NSString stringWithFormat:@"%@",[[jsonResults  valueForKey:@"data"]objectForKey:@"vehicleName"]];
              
               self.tripLabel.text = [NSString stringWithFormat:@"$ %@",[[jsonResults  valueForKey:@"data"]objectForKey:@"totalAmount"]];
              
               self.lblDuration.text = [NSString stringWithFormat:@"$ %@",[[jsonResults  valueForKey:@"data"]objectForKey:@"totalAmount"]];

                 self.lblDistance.text = [NSString stringWithFormat:@"%@ %@",[[jsonResults  valueForKey:@"data"]objectForKey:@"totalDistance"], @"Miles"];

        }
        break;
        
        DEFAULT
        {
            break;
        }
    }
    
}

// WEB SERVER ERROR MSG LOCAL SERVER (APPLICATION SERVER)
-(void) webServiceFailWithApplicationServerMSG:(NSString *)msg
{
    
    [Alert svError:msg];
    
}

// WEB SERVICE FAIL WITH REGION TCP/IP
-(void) webServiceFail:(NSError *)error{
    
    
    [Alert svError:[error localizedDescription]];
    
    
}

#pragma mark - Navigation bar configure
-(void)navigationBarConfiguration
{
    
    self.navigationController.navigationBarHidden = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    UIButton * menuButton  = [UIButton buttonWithType:UIButtonTypeSystem];
    menuButton.frame = CGRectMake(8, 18, 24, 24);
    [menuButton setBackgroundImage:[UIImage imageNamed:IMG_BACK] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(backButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *accountBarItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    self.navigationItem.leftBarButtonItem = accountBarItem;
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor],
                                                                      NSFontAttributeName: [UIFont fontWithName:kFONT size:14]}];
    
    self.navigationController.navigationBar.tintColor = [Alert colorFromHexString:kCOLOR_NAV];
    
}

#pragma mark - Update Location Button
-(UIBarButtonItem*)buttonUpdateLocation
{
    
   UIButton *buttonUpdate = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonUpdate.frame=CGRectMake(0,0,50,30);
    [buttonUpdate setTitle:@"Update location" forState:UIControlStateNormal];
    
    
    [Alert buttonWithLayerColorAndWidth:kCOLOR_WHITE button:buttonUpdate radius:4.0 border:1.5];
    [buttonUpdate addTarget:self action:@selector(buttonUpdateLocationClick:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:buttonUpdate];
    
    return backButton;
}


-(void)buttonUpdateLocationClick:(id)sender
{
    
}

#pragma mark - button Back Click
-(void)backButtonClick:(id)sender
{
 // Go back (pop view controller)
    GOBACK;
}


#pragma mark -
#pragma mark --- :LOCATION Authorization :----
-(void)getAuthorizationForLocation
{
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    _locationManager.delegate = self;
    if (IS_iOS_8_OR_LATER)
    {
        [_locationManager requestWhenInUseAuthorization];
      //  [_locationManager requestAlwaysAuthorization];
        [_locationManager startUpdatingLocation];
    }
    else
    {
        //_locationManager = nil;
        [_locationManager startUpdatingLocation];
    }
}

#pragma mark --- : GOOGLE MAP :----
-(void)showGoogleMap
{
//    float tempLat = appDelegate.locationGlobal.coordinate.latitude;
//    if (tempLat > 0)
//    {
//        latitude = appDelegate.locationGlobal.coordinate.latitude;
//        longitude = appDelegate.locationGlobal.coordinate.longitude;
//        appDelegate.locationGlobal = nil;
//    }
//    else
//    {
//        coordinate = [self getLocation];
//        latitude = coordinate.latitude;
//        longitude = coordinate.longitude;
//    }
//    
//    CGRect frame = CGRectMake(0, 0, kSCREEN_WIDTH,kSCREEN_HEIGHT);
//    camera = [GMSCameraPosition cameraWithLatitude:latitude longitude:longitude zoom:ZOOM];
//    mapView_ = [GMSMapView mapWithFrame:frame camera:camera];
//    mapView_.delegate = self;
//    [mapView_ setCamera:camera];
//    dispatch_async(dispatch_get_main_queue(),^{
//        
//        mapView_.myLocationEnabled = YES;
//        mapView_.indoorEnabled = YES;
//        mapView_.settings.compassButton = YES;
//        mapView_.settings.myLocationButton = NO;
//        mapView_.settings.zoomGestures = YES;
//        mapView_.settings.indoorPicker = YES;
//        mapView_.settings.scrollGestures= YES;
//    });
//    [_mapView addSubview:mapView_];
}


#pragma mark ----: GET Coordinate2D :----
-(CLLocationCoordinate2D) getLocation
{
    CLLocation *location = [_locationManager location];
    coordinate = [location coordinate];
    return coordinate;
}

-(void)getMyCurrentLocation
{
    coordinate = [self getLocation];
    locationMarker_ = [[GMSMarker alloc] init];
    locationMarker_.position = CLLocationCoordinate2DMake(latitude, longitude);
    mapView_.myLocationEnabled = YES;
    camera = [GMSCameraPosition cameraWithLatitude:coordinate.latitude longitude:coordinate.longitude zoom:ZOOM];
    [mapView_ animateToCameraPosition:camera];
}


-(IBAction)buttonLocationClick:(id)sender
{
    // [[LocationHandler getSharedInstance]setDelegate:self];
    // [[LocationHandler getSharedInstance]startUpdating];
    
    [self getMyCurrentLocation];
}




#pragma mark - CLLocation Delegate methods

- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    
    [manager stopUpdatingLocation];
    
    CLLocation * currentLocation = [locations lastObject];
    
    _locationManager = manager;
    latitude = currentLocation.coordinate.latitude;
    longitude = currentLocation.coordinate.longitude;
    // NSLog(@"locations = %@",locations.lastObject);
    //_locationGlobal = currentLocation;
    
    // [self locationAddress:currentLocation];
    
}
- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    NSLog(@"error = %@",error.localizedDescription);
}

#pragma mark - Google map Delegate Method


- (void) mapView:(GMSMapView *) mapView willMove:(BOOL)gesture
{
    NSLog(@"willMove");
}
- (void) mapView:(GMSMapView *)mapView didChangeCameraPosition:(GMSCameraPosition *)position
{
    NSLog(@"didChangeCameraPosition");
}
- (void) mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position
{
    CGPoint point = mapView.center;
    coordinate = [mapView.projection coordinateForPoint:point];
    NSLog(@"mapView center location : %f %f",coordinate.latitude,coordinate.longitude);
}

-(void) mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate
{
    NSLog(@"didTapAtCoordinate");
}   




//
//#pragma mark -====: STAR RATING :====
//-(void)starRatingToDriver:(NSInteger)rating
//{
//    _starRating.backgroundColor  = [UIColor clearColor];
//    // Setup control using iOS7 tint Color
//    //  starRating.backgroundColor  = [UIColor whiteColor];
//    _starRating.starImage = [[UIImage imageNamed:@"star-template"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
//    _starRating.starHighlightedImage = [[UIImage imageNamed:@"star-highlighted-template"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
//
//    _starRating.maxRating = 5.0;
//    _starRating.delegate = self;
//    _starRating.horizontalMargin = 15.0;
//    _starRating.editable=YES;
//    _starRating.rating= rating;
//    _starRating.displayMode=EDStarRatingDisplayHalf;
//    [_starRating  setNeedsDisplay];
//    _starRating.tintColor = [Alert  colorFromHexString:kCOLOR_BUTTON];//[UIColor colorWithRed:0.11f green:0.38f blue:0.94f alpha:1.0f];
//
//    [self starsSelectionChanged:_starRating rating:rating];
//}
//
//-(void)starsSelectionChanged:(EDStarRating *)control rating:(float)rating
//{
//    NSString *ratingString = [NSString stringWithFormat:@"Rating: %.1f", rating];
//
//    if([control isEqual:_starRating] )
//    {
//        // _starRating = rating;
//        NSLog(@"Rating  := %@",ratingString);
//    }
//    else
//    {
//        NSLog(@"Rating  := %@",ratingString);
//    }
//
//}

- (void)drawRouteOrigin:(CLLocation *)myOrigin destination:(CLLocation *)myDestination
{
    [self fetchPolylineWithOrigin:myOrigin destination:myDestination completionHandler:^(GMSPolyline *polyline)
     {
        
         if(polyline)
         {
             GMSStrokeStyle *greenToRed = [GMSStrokeStyle solidColor:kCOLOR_BLACK];
             polyline.strokeWidth = 5.0;
             polyline.spans = @[[GMSStyleSpan spanWithStyle:greenToRed]];
             polyline.map = mapView_;
         }
         
         
     }];
}

- (void)fetchPolylineWithOrigin:(CLLocation *)origin destination:(CLLocation *)destination completionHandler:(void (^)(GMSPolyline *))completionHandler
{
    NSString *originString = [NSString stringWithFormat:@"%f,%f", origin.coordinate.latitude, origin.coordinate.longitude];
    NSString *destinationString = [NSString stringWithFormat:@"%f,%f", destination.coordinate.latitude, destination.coordinate.longitude];
    NSString *directionsAPI = @"https://maps.googleapis.com/maps/api/directions/json?";
    NSString *directionsUrlString = [NSString stringWithFormat:@"%@&origin=%@&destination=%@&mode=driving&key=%@", directionsAPI, originString, destinationString, kKEY_GOOGLE_MAP];
    NSURL *directionsUrl = [NSURL URLWithString:directionsUrlString];
    
    
    NSURLSessionDataTask *fetchDirectionsTask = [[NSURLSession sharedSession] dataTaskWithURL:directionsUrl completionHandler:
                                                 ^(NSData *data, NSURLResponse *response, NSError *error)
                                                 {
                                                     NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                                                     if(error)
                                                     {
                                                         if(completionHandler)
                                                             completionHandler(nil);
                                                         return;
                                                     }
                                                     
                                                                   dispatch_sync(dispatch_get_main_queue(), ^{
                                                     NSArray *routesArray = [json objectForKey:@"routes"];
                                                     
                                                     GMSPolyline *polyline = nil;
                                                     if ([routesArray count] > 0)
                                                     {
                                                         NSDictionary *routeDict = [routesArray objectAtIndex:0];
                                                         NSDictionary *routeOverviewPolyline = [routeDict objectForKey:@"overview_polyline"];
                                                         NSString *points = [routeOverviewPolyline objectForKey:@"points"];
                                                         GMSPath *path = [GMSPath pathFromEncodedPath:points];
                                                         polyline = [GMSPolyline polylineWithPath:path];
                                                         
                                                         
                                            
                                            }
                                                     
                                                     // run completionHandler on main thread
                                       
                                                         
                                                         
                                                         

                                        if(completionHandler)
                                                             completionHandler(polyline);
 
                                                     });
 
                                                 }];
    [fetchDirectionsTask resume];
}




#pragma mark - Memory Warning
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)buttonReviewClick:(id)sender
{
    GOBACK;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
