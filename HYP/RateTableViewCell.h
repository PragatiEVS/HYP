//
//  RateTableViewCell.h
//  CHORETHING
//
//  Created by santosh kumar singh on 03/04/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface RateTableViewCell : UITableViewCell


@property (nonatomic, strong) IBOutlet RateView *rateView;
@property(nonatomic,strong)  IBOutlet UILabel *pendingLabel;
@property(nonatomic,strong)  IBOutlet UILabel *nameLabel;
@property(nonatomic,strong)  IBOutlet UILabel *locationLabel;
@property(nonatomic,strong)  IBOutlet UILabel *cratedLabel;
@property(nonatomic,strong)  IBOutlet UIImageView *userThumb;
@end


