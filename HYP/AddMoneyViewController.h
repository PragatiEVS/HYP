//
//  AddMoneyViewController.h
//  GetSum
//
//  Created by santosh kumar singh on 28/04/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface AddMoneyViewController : UIViewController<UITextFieldDelegate,WebServiceDelegate,UIAlertViewDelegate,STPPaymentCardTextFieldDelegate,STPPaymentCardTextFieldDelegate>
{
    IBOutlet UITextField *txtName, *txtCardNo,*txtCvv,*txtMonth,*txtYear, *txtamount;
    NTMonthYearPicker *picker;
    UIPopoverController *popupCtrl;
    STPCardParams *cardParams;
    STPCard  *stripCard;
    IBOutlet UILabel *cardnumberLabel, *expiry;
    
}

@property (strong, nonatomic) NSString * year;
@property (strong, nonatomic) NSString * month;

@property (strong, nonatomic) NSString * invoiceId;
@property(nonatomic, strong, readwrite) NSString *dateString;
@property(nonatomic, strong, readwrite) NSString *timeString;
@property (strong, nonatomic) NSString * groupId;
@property (strong, nonatomic) NSDictionary * dict;
@property (strong, nonatomic) NSString * priceString;
@property (weak, nonatomic)  NSString * servicename;
@property (strong, nonatomic) NSString * matchString;
@property (strong, nonatomic) NSString * tipString;
@property (weak, nonatomic) IBOutlet UILabel *amountLable;
@property (weak, nonatomic) IBOutlet UILabel *nameLable;
@end


