//
//  UserDetailViewController.h
//  Q-Workerbee
//
//  Created by santosh kumar singh on 08/06/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface UserDetailViewController : UIViewController
{
    NSDictionary * dictLoginInfo;
}
@property (nonatomic, strong) IBOutlet UIButton *postcount;

@property(nonatomic,weak) IBOutlet UIButton * reviewbtn;
@property (nonatomic, strong) IBOutlet RateView *rateView;
@property (nonatomic, strong) IBOutlet UILabel *nameModel;
@property (nonatomic, strong) IBOutlet UILabel *packageType;
@property (nonatomic, strong) IBOutlet UIButton *locationLabel;

@property (strong, nonatomic) NSDictionary * dict;
@property (strong, nonatomic) NSString * priceString;

@property (weak, nonatomic)  NSString * vendorId;
@property (weak, nonatomic)  NSString * servicename;
@property (nonatomic, strong) IBOutlet UIView *notifiactionView;
@property (nonatomic, strong) IBOutlet UIButton *servicebtn;
@property (weak, nonatomic)  NSString *  serviceID;

@property(nonatomic,weak) IBOutlet UIButton * acceptbtn;
@property(nonatomic,weak) IBOutlet UIButton * rejectbtn;

@property(nonatomic,weak)  NSString * matchstatus;

@property(nonatomic,weak) IBOutlet UILabel * addressNotification;
@property(nonatomic,weak) IBOutlet UILabel * serviceNoti;
@property(nonatomic,weak) IBOutlet UILabel * dateTimenotif;

@property (nonatomic, strong) IBOutlet UIImageView *serviceImage;
@property (nonatomic, strong) IBOutlet UIImageView *modelImage;
@property (nonatomic, strong) IBOutlet UIImageView *modelBgImage;

@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *textSearch;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *textEmail;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *phoneTextfield;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextView *textDescription;
@property (weak, nonatomic) IBOutlet UILabel *dobtextfield;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextView *servicesTextfield;
@end


