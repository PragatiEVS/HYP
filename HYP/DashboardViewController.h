//
//  DashboardViewController.h
//  HYP
//
//  Created by santosh kumar singh on 31/08/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface DashboardViewController : UIViewController
{
     CLLocationManager *loctionManager;
     GMSCameraPosition *camera;
       GMSMapView *mapView_;
     GMSMarker *locationMarker_;
    float latitude , latSearch;
           float longitude, lngSearch;
    NSMutableArray *imagedistance;
     NSMutableArray* latitlong;
     int i;
    IBOutlet MDSwitch *mdSwitch;
     GMSMapView *mapView1_;
     GMSMarker *locationMarker1;
     NSIndexPath* selection;
     CLLocationCoordinate2D location1;
      NSMutableArray *resultArray;
   
    IBOutlet UITableView *tblView;
     IBOutlet UITableView *shoptblView;
   
    IBOutlet UIButton *deliverBtn;
    IBOutlet UIButton *pickupBtn;
  
     NSMutableArray *arrSelectedSectionIndex;
}

@property(nonatomic,weak) IBOutlet UIButton * emailbtn;

@property(nonatomic,weak)  NSString * matchDeliveryType;
@property(nonatomic,weak)  NSString * matchVehicleImage;
@property(nonatomic,weak)  NSString * matchVehicleName;

@property(nonatomic,weak)  NSString * matchstatus;
@property(nonatomic,weak) IBOutlet UIButton * acceptbtn;
@property(nonatomic,weak) IBOutlet UIButton * rejectbtn;
@property(nonatomic,weak) IBOutlet UILabel * addressNotification;
@property(nonatomic,weak) IBOutlet UILabel * serviceNoti;
@property(nonatomic,weak) IBOutlet UILabel * dateTimenotif;

@property (nonatomic, strong) IBOutlet UIView *notifiactionView;
@property (nonatomic,strong) NSDictionary * notificationDict;
@property (nonatomic, strong) NSDictionary *dictonartSelct;
@property (nonatomic, strong) NSString *selecteditem;
@property (nonatomic, strong) NSString *matchFrom;
@property (nonatomic, strong) IBOutlet UIView * mapView1;
@property (strong, nonatomic) IBOutlet UIView *categoryView;
@property (strong, nonatomic) IBOutlet UIView *shoppingView;
@property (strong, nonatomic) IBOutlet UIView *providerView;
@property (strong, nonatomic) IBOutlet UIView *userView;
@property (strong, nonatomic) IBOutlet UIButton *doneBtn;
@property(nonatomic,strong)  GMSGeocoder     *geocoder;
@property(nonatomic,retain) CLLocationManager *locationManager;
@property (nonatomic, strong) IBOutlet UIView * mapView;

@property (nonatomic, strong) IBOutlet UIView * statusView;
@property (nonatomic, strong) IBOutlet UIView * cartView;

@property (nonatomic, strong) IBOutlet UILabel * statusLabel;
@property(nonatomic,retain)Annotation *placemarks;


@property (nonatomic, strong) IBOutlet UIImageView * driverImageView;
@property (nonatomic, strong) IBOutlet UILabel * driverName;
@property (nonatomic, strong) IBOutlet UILabel * welcomLabel;
@property (nonatomic, strong) IBOutlet UILabel * welcomUserLabel;


@property(nonatomic,weak) IBOutlet UIView * viewPayment;
@property(nonatomic,weak) IBOutlet UIButton * buttonPaymentOk;
@property(nonatomic,weak) IBOutlet UILabel * labelPaymentAmount;
@end


