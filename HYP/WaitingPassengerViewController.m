//
//  WaitingPassengerViewController.m
//  ROLUX
//
//  Created by Nitin Kumar on 09/01/17.
//  Copyright © 2017 Nitin Kumar. inAll rights reserved.
//

#import "WaitingPassengerViewController.h"
#import "AppDelegate.h"
#import <Firebase/Firebase.h>
#import "MenuViewController.h"
#import "DashboardViewController.h"


#define degreesToRadians(x) (M_PI * x / 180.0)
#define radiansToDegrees(x) (x * 180.0 / M_PI)
#define KCOLORGRD [UIColor colorWithRed:68/255.0 green:162/255.0 blue:243/255.0f alpha:1]
#define color_D [UIColor colorWithRed:67/255.0 green:159/255.0 blue:253/255.0f alpha:1]]

@interface WaitingPassengerViewController ()<GMSMapViewDelegate,CLLocationManagerDelegate,UIGestureRecognizerDelegate,WebServiceDelegate>
{
    
    AppDelegate * appDelegate;
    BOOL isRoute;
    BOOL selected;
    FIRDatabaseReference *FIRDB, *FIRDB1322;
    FIRDatabaseHandle _refHandle;
    
}

@property (strong, nonatomic) FIRDatabaseReference *ref;
@property (nonatomic) BOOL routeData;
@property (nonatomic) float latDriverCar;
@property (nonatomic) float longiDriverCar;

@property (strong, nonatomic) NSTimer * tiemrLoc;
@property (strong, nonatomic) NSTimer * timerLocationUpdate,* getdriverLatLong;
@property (strong, nonatomic) NSString * latUser;
@property (strong, nonatomic) NSString * longiUser;
@property (strong, nonatomic) UIBarButtonItem *barButtonStartNavigation;
@property (strong, nonatomic) NSMutableArray * arrayRouteData;
@property (copy, nonatomic) CLLocation * driverLocation;
@property (copy, nonatomic) CLLocation * locationNew;
@property (copy, nonatomic) CLLocation * locPicOff ;
@property (copy, nonatomic) CLLocation * locDrop;
@property (strong, nonatomic) NSString * driverId;
@end

@implementation WaitingPassengerViewController

-(void)loadRoute
{
  
    FIRDB = [FIRDatabase database].reference;
   
    NSString * UserPhone = [_dictionaryNotification valueForKey:@"userId"];
   
    if (UserPhone == nil)
    {
        self.driverId = [_dictionaryNotification valueForKey:@"driverId"];
    }
    else{
        self.driverId = [_dictionaryNotification valueForKey:@"userId"];
       
    }
    FIRDatabaseReference *usersRef =  [[FIRDB child:@"liveTracking"]child:[NSString stringWithFormat:@"%@@%@",[_dictionaryNotification objectForKey:@"bookingId"], self.driverId ]];
  
    [usersRef observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot *snapshot)
    {
        
        [usersRef observeEventType:FIRDataEventTypeChildAdded withBlock:^(FIRDataSnapshot *snapshot)
         {
            
            NSDictionary *  Dictionary;
            
               self.firebaseDictinary = @{
                @"longs": snapshot.value [@"longs"],
                @"lat": snapshot.value [@"lat"],
                @"adress": snapshot.value [@"adress"],
                @"bookingId": snapshot.value [@"bookingId"],
                @"driverId": snapshot.value [@"driverId"]
            };
            
            Dictionary = @{
                @"longs": snapshot.value[@"longs"],
                @"lat": snapshot.value [@"lat"]
            };
            
            self.longiDriverCar = [snapshot.value[@"longs"]floatValue];
            self.latDriverCar = [snapshot.value[@"lat"]floatValue];
            
            _dropoffLoc.text =[NSString stringWithFormat:@"Driver: %@",snapshot.value [@"adress"]];
          
            _driverLocation  = [[CLLocation alloc]initWithLatitude:_latDriverCar longitude:_longiDriverCar];
            [mapView_ clear];
            
            GMSMarker*  marker = [[GMSMarker alloc] init];
            
            marker.icon = [UIImage imageNamed:@"sedan_car_icon"];
            marker.position =  _driverLocation.coordinate;
            camera =   [GMSCameraPosition cameraWithTarget:_driverLocation.coordinate zoom:15.0];
            [mapView_ animateToCameraPosition:camera];
            marker.map = mapView_;
            
            GMSMarker*  markerDestView = [[GMSMarker alloc] init];
            markerDestView.map  = nil;
            markerDestView.position = _locPicOff.coordinate;
            markerDestView.icon = [UIImage imageNamed:@"EndMark"];
            markerDestView.map = mapView_;
            
            [self drawRouteOrigin:_driverLocation destination:_locPicOff];
            _locationManager.delegate = self;
         
            CLLocationDistance distance2 = [_driverLocation distanceFromLocation:_locPicOff] * 0.000621371;
            NSLog(@"%f",distance2);

            [self.labelDistance setText:[NSString stringWithFormat:@"%.2f Miles", distance2]];

        }];
    }];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [Alert setLeftPaddingTextField:self.messageText paddingValue:36 image:[UIImage  imageNamed:kIMG_PHONE]];
   
    self.firebaseDictinary = [[NSDictionary alloc]init];
    [self navigationBarConfiguration];
    [self loadRoute];
    
    _viewLocation.layer.cornerRadius = 5;
    // border
    [_viewLocation.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [_viewLocation.layer setBorderWidth:1.5f];
    
    // drop shadow
    [_viewLocation.layer setShadowColor:[UIColor grayColor].CGColor];
    [_viewLocation.layer setShadowOpacity:0.8];
    [_viewLocation.layer setShadowRadius:3.0];
    [_viewLocation.layer setShadowOffset:CGSizeMake(1.0, 1.0)];
    
    _detailView.layer.cornerRadius = 5;
    
    [_detailView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [_detailView.layer setBorderWidth:1.5f];
    
    // drop shadow
    [_detailView.layer setShadowColor:[UIColor lightGrayColor].CGColor];
    [_detailView.layer setShadowOpacity:0.8];
    [_detailView.layer setShadowRadius:3.0];
    [_detailView.layer setShadowOffset:CGSizeMake(1.0, 1.0)];
    
    // Configure View
    self.viewDriverInfo.hidden = YES;
    
    NSInteger borderThickness = 1;
    UIView *leftBorder = [UIView new];
    leftBorder.backgroundColor = [UIColor lightGrayColor];
    leftBorder.frame = CGRectMake(0, 2, borderThickness, self.buttonCencel.frame.size.height-4);
    [self.buttonCencel
     addSubview:leftBorder];
    
    UIView *topBorder = [UIView new];
    topBorder.backgroundColor = [UIColor lightGrayColor];
    topBorder.frame = CGRectMake(0, 0, self.userDetailView.frame.size.width, borderThickness);
    
    [self.userDetailView addSubview:topBorder];
  
    UIView *bottomBorder = [UIView new];
    bottomBorder.backgroundColor = [UIColor lightGrayColor];
    bottomBorder.frame = CGRectMake(0, self.userDetailView.frame.size.height - borderThickness, self.userDetailView.frame.size.width, borderThickness);
    [self.userDetailView addSubview:bottomBorder];
   
    [self.messageView addSubview:topBorder];
  
    self.messageView.clipsToBounds = YES;
    
    self.userDetailView.clipsToBounds = YES;
    
    _picLoc.text = [NSString stringWithFormat:@"%@",[_dictionaryNotification objectForKey:@"RequestPickupAddress"]];
    _dropoffLoc.text =[NSString stringWithFormat:@"%@",[_dictionaryNotification objectForKey:@"RequestDropAddress"]];
   
    _starRating.starSize = 15;
  
       UIView *bottomBorder1 = [UIView new];
       bottomBorder1.backgroundColor = [UIColor lightGrayColor];
       bottomBorder1.frame = CGRectMake(0, self.messageView.frame.size.height - borderThickness, self.messageView.frame.size.width, borderThickness);
       [self.messageView addSubview:bottomBorder1];
       
    
    _starRating.starFillColor = [Alert colorFromHexString:@"#F4CC47"];
    if ([_dictionaryNotification objectForKey:@"rating"] == nil)
    {
        _starRating.rating= [[_dictionaryNotification objectForKey:@"AVGRating"] floatValue];
        self.labelRating.text =  [NSString stringWithFormat:@"Invoice Id: %@",[_dictionaryNotification objectForKey:@"invoiceId"]];
    }
    else
    {
        _starRating.rating= [[_dictionaryNotification objectForKey:@"rating"] floatValue];
        self.labelRating.text =  [NSString stringWithFormat:@"Invoice Id: %@",[_dictionaryNotification objectForKey:@"invoiceId"]];
       
    }
  
    NSString * name   = [_dictionaryNotification valueForKey:@"driverName"];
 
    if (name == nil)
    {
        self.labelDriverName.text =  [_dictionaryNotification valueForKey:@"fullName"];
    }
    else
    {
        self.labelDriverName.text = name;
    }
  
    
    NSString * driverimage  = [_dictionaryNotification valueForKey:@"DriverImage"];
    driverimage =  [driverimage stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    if (driverimage == nil)
    {
        [self.imageViewDriver sd_setImageWithURL:[NSURL URLWithString:[_dictionaryNotification valueForKey:@"image"]] placeholderImage:[UIImage imageNamed:@"user12.png"]];
    }
    else{
        [self.imageViewDriver sd_setImageWithURL:[NSURL URLWithString:driverimage] placeholderImage:[UIImage imageNamed:@"user12.png"]];
    }
    self.imageViewDriver.layer.cornerRadius = 25;
    self.imageViewDriver.clipsToBounds=YES;
    
    
    [self config];
    
    //
    //    dispatch_async(dispatch_get_main_queue(), ^{
    //
    [self showGoogleMap];
    
    //  });
    
    // Do any additional setup after loading the view.
}



-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (!IS_NSNull_Class(_dictionaryNotification))
    {
        NSString * msgType = [_dictionaryNotification valueForKey:@"type"];
        if ([msgType isEqualToString:@"DriverConfrim"] || ([[[_dictionaryNotification valueForKey:@"rideStatus"] stringValue]  isEqual: @"1"])
            )
        {
        
            _picLoc.text = [NSString stringWithFormat:@"Pickup: %@",[_dictionaryNotification objectForKey:@"RequestPickupAddress"]];
           
            self.title     = @"DRIVER IS IN ROUTE";
            self.labelTimeDuration.text = @"0 mins";
            self.labelDistance.text = @"0 Mi";
            NSLog(@"_dictionaryNotification:%@",_dictionaryNotification);
         
            NSString *vendorPhone   = [_dictionaryNotification valueForKey:@"driverContact"];
            
            self.labelDriverPhoneNumber.text = vendorPhone;
            NSString * reqId        = [_dictionaryNotification valueForKey:@"bookingId"];
            NSString * email        = [_dictionaryNotification valueForKey:@"emailId"];
            self.labelDriverEmailAddress.text = email;
            
            NSString * car_number        = [_dictionaryNotification objectForKey:@"vehicleName"];
          
            if (car_number == nil)
            {
                self.labelDriverCarNumber.text = [_dictionaryNotification objectForKey:@"vehicleNumber"];
            }
            else{
                self.labelDriverCarNumber.text = [_dictionaryNotification valueForKey:@"vehicleName"];
               
            }
             
            
            NSString * droplatLong      = [_dictionaryNotification valueForKey:@"RequestDropLatLong"];
            
            NSArray *item1 = [droplatLong componentsSeparatedByString:@","];
            
            NSString * strLatDrop = [item1 objectAtIndex:0];
            NSString * strLongDrop = [item1 objectAtIndex:1];
            
            _locDrop       = [[CLLocation alloc]initWithLatitude:[strLatDrop floatValue] longitude:[strLongDrop floatValue]];
            
            NSString * pickuplatLong      = [_dictionaryNotification valueForKey:@"RequestPickupLatLong"];
            
            NSArray *item2          = [pickuplatLong componentsSeparatedByString:@","];
            
            NSString * strLatPic    = [item2 objectAtIndex:0];
            NSString * strLongPic   = [item2 objectAtIndex:1];
            
            
            _locPicOff       = [[CLLocation alloc]initWithLatitude:[strLatPic floatValue] longitude:[strLongPic floatValue]];
            
            GMSMarker *picoff = [[GMSMarker alloc]init];
            picoff.position   = CLLocationCoordinate2DMake([strLatPic floatValue], [strLongPic floatValue]);
            picoff.icon = [UIImage imageNamed:kIMG_DOT_CIRCLE_MARKER];
            picoff.map = mapView_;
            
//            self.labelBookingID.text = [NSString stringWithFormat:@"Booking Id : %@",reqId];
            [self.labelBookingID setTitle:[NSString stringWithFormat:@"Booking Id : %@",reqId] forState:UIControlStateNormal];
            
            
            if (!(isNull(email))) {
                
                self.labelEmailID.text   = email;
            }
            else
            {
                self.labelEmailID.text = @"N/A";
            }
            
            NSString *strPhn = [NSString stringWithFormat:@"-%@",vendorPhone];

            [Alert viewWithLayerColorAndWidth:kCOLOR_CLEAR view:self.labelTimeDuration radius:self.labelTimeDuration.frame.size.height/2 border:1.0];
            [Alert viewWithLayerColorAndWidth:kCOLOR_CLEAR view:self.labelDistance radius:self.labelDistance.frame.size.height/2 border:1.0];

//            [self getDistancebyVehicleStart:pickuplatLong endPoint:droplatLong];
       
        }
         else if ([msgType isEqualToString:@"pickuplocation"] || ([[[_dictionaryNotification valueForKey:@"rideStatus"] stringValue]  isEqual: @"10"])
            )
         {
           
             _picLoc.text = [NSString stringWithFormat:@"Pickup: %@",[_dictionaryNotification objectForKey:@"RequestPickupAddress"]];
           
            self.title     = @"DRIVER IN ROUTE TO P/U";
            self.labelTimeDuration.text = @"0 mins";
            self.labelDistance.text = @"0 Mi";
            NSLog(@"_dictionaryNotification:%@",_dictionaryNotification);
         
            NSString *vendorPhone   = [_dictionaryNotification valueForKey:@"driverContact"];
            
            self.labelDriverPhoneNumber.text = vendorPhone;
            NSString * reqId        = [_dictionaryNotification valueForKey:@"bookingId"];
            NSString * email        = [_dictionaryNotification valueForKey:@"emailId"];
            self.labelDriverEmailAddress.text = email;
            
            NSString * car_number        = [_dictionaryNotification valueForKey:@"vehicleName"];
             if (car_number == nil)
             {
                 self.labelDriverCarNumber.text= [_dictionaryNotification objectForKey:@"vehicleNumber"];
             }
             else{
                 self.labelDriverCarNumber.text = [_dictionaryNotification valueForKey:@"vehicleName"];
                
             }
            
            NSString * droplatLong      = [_dictionaryNotification valueForKey:@"RequestDropLatLong"];
            
            NSArray *item1 = [droplatLong componentsSeparatedByString:@","];
            
            NSString * strLatDrop = [item1 objectAtIndex:0];
            NSString * strLongDrop = [item1 objectAtIndex:1];
            
            _locDrop       = [[CLLocation alloc]initWithLatitude:[strLatDrop floatValue] longitude:[strLongDrop floatValue]];
            
            NSString * pickuplatLong      = [_dictionaryNotification valueForKey:@"RequestPickupLatLong"];
            
            NSArray *item2          = [pickuplatLong componentsSeparatedByString:@","];
            
            NSString * strLatPic    = [item2 objectAtIndex:0];
            NSString * strLongPic   = [item2 objectAtIndex:1];
            
            
            _locPicOff       = [[CLLocation alloc]initWithLatitude:[strLatPic floatValue] longitude:[strLongPic floatValue]];
            
            GMSMarker *picoff = [[GMSMarker alloc]init];
            picoff.position   = CLLocationCoordinate2DMake([strLatPic floatValue], [strLongPic floatValue]);
            picoff.icon = [UIImage imageNamed:kIMG_DOT_CIRCLE_MARKER];
            picoff.map = mapView_;
            
//          self.labelBookingID.text = [NSString stringWithFormat:@"Booking Id : %@",reqId];
           
            [self.labelBookingID setTitle:[NSString stringWithFormat:@"Booking Id : %@",reqId] forState:UIControlStateNormal];
            
           if (!(isNull(email))) {
                
                self.labelEmailID.text   = email;
            }
            else
            {
                self.labelEmailID.text = @"N/A";
            }
            
            NSString *strPhn = [NSString stringWithFormat:@"-%@",vendorPhone];

            [Alert viewWithLayerColorAndWidth:kCOLOR_CLEAR view:self.labelTimeDuration radius:self.labelTimeDuration.frame.size.height/2 border:1.0];
            [Alert viewWithLayerColorAndWidth:kCOLOR_CLEAR view:self.labelDistance radius:self.labelDistance.frame.size.height/2 border:1.0];

//            [self getDistancebyVehicleStart:pickuplatLong endPoint:droplatLong];
            
            
            
            
        }
         else if ([msgType isEqualToString:@"arrived"] || ([[[_dictionaryNotification valueForKey:@"rideStatus"] stringValue]  isEqual: @"2"]))
        {
            _picLoc.text = [NSString stringWithFormat:@"Pickup: %@",[_dictionaryNotification objectForKey:@"RequestPickupAddress"]];
           
            self.title     = @"ARRIVE AT THE STORE";
            self.labelTimeDuration.text = @"0 mins";
            self.labelDistance.text = @"0 Mi";
            NSLog(@"_dictionaryNotification:%@",_dictionaryNotification);
//            NSString * name         = [_dictionaryNotification valueForKey:@"driverName"];
//
//            self.labelDriverName.text = name;
//
            
            NSString *vendorPhone   = [_dictionaryNotification valueForKey:@"driverContact"];
            
            self.labelDriverPhoneNumber.text = vendorPhone;
            NSString * reqId        = [_dictionaryNotification valueForKey:@"bookingId"];
            NSString * email        = [_dictionaryNotification valueForKey:@"emailId"];
            self.labelDriverEmailAddress.text = email;
            
            NSString * car_number        = [_dictionaryNotification valueForKey:@"vehicleName"];
            if (car_number == nil)
            {
                self.labelDriverCarNumber.text= [_dictionaryNotification objectForKey:@"vehicleNumber"];
            }
            else{
                self.labelDriverCarNumber.text = [_dictionaryNotification valueForKey:@"vehicleName"];
               
            }
            
            
            NSString * droplatLong      = [_dictionaryNotification valueForKey:@"RequestDropLatLong"];
            
            NSArray *item1 = [droplatLong componentsSeparatedByString:@","];
            
            NSString * strLatDrop = [item1 objectAtIndex:0];
            NSString * strLongDrop = [item1 objectAtIndex:1];
            
            _locDrop       = [[CLLocation alloc]initWithLatitude:[strLatDrop floatValue] longitude:[strLongDrop floatValue]];
            
            NSString * pickuplatLong      = [_dictionaryNotification valueForKey:@"RequestPickupLatLong"];
            
            NSArray *item2          = [pickuplatLong componentsSeparatedByString:@","];
            
            NSString * strLatPic    = [item2 objectAtIndex:0];
            NSString * strLongPic   = [item2 objectAtIndex:1];
            
            
            _locPicOff       = [[CLLocation alloc]initWithLatitude:[strLatPic floatValue] longitude:[strLongPic floatValue]];
            
            GMSMarker *picoff = [[GMSMarker alloc]init];
            picoff.position   = CLLocationCoordinate2DMake([strLatPic floatValue], [strLongPic floatValue]);
            picoff.icon = [UIImage imageNamed:kIMG_DOT_CIRCLE_MARKER];
            picoff.map = mapView_;
            
//            self.labelBookingID.text = [NSString stringWithFormat:@"Booking Id : %@",reqId];
            [self.labelBookingID setTitle:[NSString stringWithFormat:@"Booking Id : %@",reqId] forState:UIControlStateNormal];
            
            
            if (!(isNull(email))) {
                
                self.labelEmailID.text   = email;
            }
            else
            {
                self.labelEmailID.text = @"N/A";
            }
            
            NSString *strPhn = [NSString stringWithFormat:@"-%@",vendorPhone];
            
//            [Alert setColoredLabel:self.labelPhnNum name:name phone:strPhn colorStr1:[Alert colorFromHexString:kCOLOR_BUTTON] colorStr2:kCOLOR_BLACK];
//
            [Alert viewWithLayerColorAndWidth:kCOLOR_CLEAR view:self.labelTimeDuration radius:self.labelTimeDuration.frame.size.height/2 border:1.0];
            [Alert viewWithLayerColorAndWidth:kCOLOR_CLEAR view:self.labelDistance radius:self.labelDistance.frame.size.height/2 border:1.0];
            //  [self getLatLongOfDriver];
            
            //  [self drawRouteOrigin:_driverLocation destination:_locPicOff];
            
            //            _tiemrLoc = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(timerFiredService) userInfo:nil repeats:YES];
//            [self getDistancebyVehicleStart:pickuplatLong endPoint:droplatLong];
            
            
            
        }
         else if ([msgType isEqualToString:@"pickuped"] || ([[[_dictionaryNotification valueForKey:@"rideStatus"] stringValue]  isEqual: @"8"]))
        {
            _picLoc.text = [NSString stringWithFormat:@"Drop: %@",[_dictionaryNotification objectForKey:@"RequestDropAddress"]];
           
            self.title     = @"ORDER PICKED UP";
            self.labelTimeDuration.text = @"0 mins";
            self.labelDistance.text = @"0 Mi";
            NSLog(@"_dictionaryNotification:%@",_dictionaryNotification);
//            NSString * name         = [_dictionaryNotification valueForKey:@"driverName"];
//
//            self.labelDriverName.text = name;
//
            self.buttonCencel.hidden = YES;
            NSString *vendorPhone   = [_dictionaryNotification valueForKey:@"driverContact"];
            
            self.labelDriverPhoneNumber.text = vendorPhone;
            NSString * reqId        = [_dictionaryNotification valueForKey:@"bookingId"];
            NSString * email        = [_dictionaryNotification valueForKey:@"emailId"];
            self.labelDriverEmailAddress.text = email;
            
            NSString * car_number        = [_dictionaryNotification valueForKey:@"vehicleName"];
            if (car_number == nil)
            {
                self.labelDriverCarNumber.text= [_dictionaryNotification objectForKey:@"vehicleNumber"];
            }
            else{
                self.labelDriverCarNumber.text = [_dictionaryNotification valueForKey:@"vehicleName"];
               
            }
            
            
            NSString * droplatLong      = [_dictionaryNotification valueForKey:@"RequestDropLatLong"];
            
            NSArray *item1 = [droplatLong componentsSeparatedByString:@","];
            
            NSString * strLatDrop = [item1 objectAtIndex:0];
            NSString * strLongDrop = [item1 objectAtIndex:1];
            
            _locDrop       = [[CLLocation alloc]initWithLatitude:[strLatDrop floatValue] longitude:[strLongDrop floatValue]];
            
            NSString * pickuplatLong      = [_dictionaryNotification valueForKey:@"RequestPickupLatLong"];
            
            NSArray *item2          = [pickuplatLong componentsSeparatedByString:@","];
            
            NSString * strLatPic    = [item2 objectAtIndex:0];
            NSString * strLongPic   = [item2 objectAtIndex:1];
            
            
            _locPicOff       = [[CLLocation alloc]initWithLatitude:[strLatDrop floatValue] longitude:[strLongDrop floatValue]];
            
            GMSMarker *picoff = [[GMSMarker alloc]init];
            picoff.position   = CLLocationCoordinate2DMake([strLatPic floatValue], [strLongPic floatValue]);
            picoff.icon = [UIImage imageNamed:kIMG_DOT_CIRCLE_MARKER];
            picoff.map = mapView_;
            
//            self.labelBookingID.text = [NSString stringWithFormat:@"Booking Id : %@",reqId];
            [self.labelBookingID setTitle:[NSString stringWithFormat:@"Booking Id : %@",reqId] forState:UIControlStateNormal];
            
            
            if (!(isNull(email))) {
                
                self.labelEmailID.text   = email;
            }
            else
            {
                self.labelEmailID.text = @"N/A";
            }
            
            NSString *strPhn = [NSString stringWithFormat:@"-%@",vendorPhone];
            
//            [Alert setColoredLabel:self.labelPhnNum name:name phone:strPhn colorStr1:[Alert colorFromHexString:kCOLOR_BUTTON] colorStr2:kCOLOR_BLACK];
//
            [Alert viewWithLayerColorAndWidth:kCOLOR_CLEAR view:self.labelTimeDuration radius:self.labelTimeDuration.frame.size.height/2 border:1.0];
            [Alert viewWithLayerColorAndWidth:kCOLOR_CLEAR view:self.labelDistance radius:self.labelDistance.frame.size.height/2 border:1.0];
            //  [self getLatLongOfDriver];
            
            //  [self drawRouteOrigin:_driverLocation destination:_locPicOff];
            
            //            _tiemrLoc = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(timerFiredService) userInfo:nil repeats:YES];
//            [self getDistancebyVehicleStart:pickuplatLong endPoint:droplatLong];
            
            
            
        }
         else if ([msgType isEqualToString:@"ridestart"] || ([[[_dictionaryNotification valueForKey:@"rideStatus"] stringValue]  isEqual: @"3"]))
        {
            _picLoc.text = [NSString stringWithFormat:@"Drop: %@",[_dictionaryNotification objectForKey:@"RequestDropAddress"]];
           
            self.title     = @"ROUTE TO DROP";
            self.labelTimeDuration.text = @"0 mins";
            self.labelDistance.text = @"0 Mi";
            self.buttonCencel.hidden = YES;
            NSLog(@"_dictionaryNotification:%@",_dictionaryNotification);
//            NSString * name         = [_dictionaryNotification valueForKey:@"driverName"];
//
//            self.labelDriverName.text = name;
//
            
            NSString *vendorPhone   = [_dictionaryNotification valueForKey:@"driverContact"];
            
            self.labelDriverPhoneNumber.text = vendorPhone;
            NSString * reqId        = [_dictionaryNotification valueForKey:@"bookingId"];
            NSString * email        = [_dictionaryNotification valueForKey:@"emailId"];
            self.labelDriverEmailAddress.text = email;
            
            NSString * car_number        = [_dictionaryNotification valueForKey:@"vehicleName"];
            if (car_number == nil)
            {
                self.labelDriverCarNumber.text= [_dictionaryNotification objectForKey:@"vehicleNumber"];
            }
            else{
                self.labelDriverCarNumber.text = [_dictionaryNotification valueForKey:@"vehicleName"];
               
            }
            
            
            NSString * droplatLong      = [_dictionaryNotification valueForKey:@"RequestDropLatLong"];
            
            NSArray *item1 = [droplatLong componentsSeparatedByString:@","];
            
            NSString * strLatDrop = [item1 objectAtIndex:0];
            NSString * strLongDrop = [item1 objectAtIndex:1];
            
            _locDrop       = [[CLLocation alloc]initWithLatitude:[strLatDrop floatValue] longitude:[strLongDrop floatValue]];
            
            NSString * pickuplatLong      = [_dictionaryNotification valueForKey:@"RequestPickupLatLong"];
            
            NSArray *item2          = [pickuplatLong componentsSeparatedByString:@","];
            
            NSString * strLatPic    = [item2 objectAtIndex:0];
            NSString * strLongPic   = [item2 objectAtIndex:1];
            
            
            _locPicOff       = [[CLLocation alloc]initWithLatitude:[strLatDrop floatValue] longitude:[strLongDrop floatValue]];
            
            GMSMarker *picoff = [[GMSMarker alloc]init];
            picoff.position   = CLLocationCoordinate2DMake([strLatPic floatValue], [strLongPic floatValue]);
            picoff.icon = [UIImage imageNamed:kIMG_DOT_CIRCLE_MARKER];
            picoff.map = mapView_;
            
//            self.labelBookingID.text = [NSString stringWithFormat:@"Booking Id : %@",reqId];
            [self.labelBookingID setTitle:[NSString stringWithFormat:@"Booking Id : %@",reqId] forState:UIControlStateNormal];
            
            
            if (!(isNull(email))) {
                
                self.labelEmailID.text   = email;
            }
            else
            {
                self.labelEmailID.text = @"N/A";
            }
            
            NSString *strPhn = [NSString stringWithFormat:@"-%@",vendorPhone];
            
//            [Alert setColoredLabel:self.labelPhnNum name:name phone:strPhn colorStr1:[Alert colorFromHexString:kCOLOR_BUTTON] colorStr2:kCOLOR_BLACK];
//
            [Alert viewWithLayerColorAndWidth:kCOLOR_CLEAR view:self.labelTimeDuration radius:self.labelTimeDuration.frame.size.height/2 border:1.0];
            [Alert viewWithLayerColorAndWidth:kCOLOR_CLEAR view:self.labelDistance radius:self.labelDistance.frame.size.height/2 border:1.0];
            //  [self getLatLongOfDriver];
            
            //  [self drawRouteOrigin:_driverLocation destination:_locPicOff];
            
            //            _tiemrLoc = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(timerFiredService) userInfo:nil repeats:YES];
//            [self getDistancebyVehicleStart:pickuplatLong endPoint:droplatLong];
            
            
            
        }
         else if ([msgType isEqualToString:@"deliver"] || ([[[_dictionaryNotification valueForKey:@"rideStatus"] stringValue]  isEqual: @"11"]))
        {
            _picLoc.text = [NSString stringWithFormat:@"Drop: %@",[_dictionaryNotification objectForKey:@"RequestDropAddress"]];
           
            self.title     = @"ORDER ON IT'S WAY!!";
            self.labelTimeDuration.text = @"0 mins";
            self.labelDistance.text = @"0 Mi";
            self.buttonCencel.hidden = YES;
            NSLog(@"_dictionaryNotification:%@",_dictionaryNotification);
//            NSString * name         = [_dictionaryNotification valueForKey:@"driverName"];
//
//            self.labelDriverName.text = name;
//
            
            NSString *vendorPhone   = [_dictionaryNotification valueForKey:@"driverContact"];
            
            self.labelDriverPhoneNumber.text = vendorPhone;
            NSString * reqId        = [_dictionaryNotification valueForKey:@"bookingId"];
            NSString * email        = [_dictionaryNotification valueForKey:@"emailId"];
            self.labelDriverEmailAddress.text = email;
            
            NSString * car_number        = [_dictionaryNotification valueForKey:@"vehicleName"];
            if (car_number == nil)
            {
                self.labelDriverCarNumber.text= [_dictionaryNotification objectForKey:@"vehicleNumber"];
            }
            else{
                self.labelDriverCarNumber.text = [_dictionaryNotification valueForKey:@"vehicleName"];
               
            }
            
            
            NSString * droplatLong      = [_dictionaryNotification valueForKey:@"RequestDropLatLong"];
            
            NSArray *item1 = [droplatLong componentsSeparatedByString:@","];
            
            NSString * strLatDrop = [item1 objectAtIndex:0];
            NSString * strLongDrop = [item1 objectAtIndex:1];
            
            _locDrop       = [[CLLocation alloc]initWithLatitude:[strLatDrop floatValue] longitude:[strLongDrop floatValue]];
            
            NSString * pickuplatLong      = [_dictionaryNotification valueForKey:@"RequestPickupLatLong"];
            
            NSArray *item2          = [pickuplatLong componentsSeparatedByString:@","];
            
            NSString * strLatPic    = [item2 objectAtIndex:0];
            NSString * strLongPic   = [item2 objectAtIndex:1];
            
            
            _locPicOff       = [[CLLocation alloc]initWithLatitude:[strLatDrop floatValue] longitude:[strLongDrop floatValue]];
            
            GMSMarker *picoff = [[GMSMarker alloc]init];
            picoff.position   = CLLocationCoordinate2DMake([strLatPic floatValue], [strLongPic floatValue]);
            picoff.icon = [UIImage imageNamed:kIMG_DOT_CIRCLE_MARKER];
            picoff.map = mapView_;
            
//            self.labelBookingID.text = [NSString stringWithFormat:@"Booking Id : %@",reqId];
            [self.labelBookingID setTitle:[NSString stringWithFormat:@"Booking Id : %@",reqId] forState:UIControlStateNormal];
            
            
            if (!(isNull(email))) {
                
                self.labelEmailID.text   = email;
            }
            else
            {
                self.labelEmailID.text = @"N/A";
            }
            
            NSString *strPhn = [NSString stringWithFormat:@"-%@",vendorPhone];
            
//            [Alert setColoredLabel:self.labelPhnNum name:name phone:strPhn colorStr1:[Alert colorFromHexString:kCOLOR_BUTTON] colorStr2:kCOLOR_BLACK];
//
            [Alert viewWithLayerColorAndWidth:kCOLOR_CLEAR view:self.labelTimeDuration radius:self.labelTimeDuration.frame.size.height/2 border:1.0];
            [Alert viewWithLayerColorAndWidth:kCOLOR_CLEAR view:self.labelDistance radius:self.labelDistance.frame.size.height/2 border:1.0];
            //  [self getLatLongOfDriver];
            
            //  [self drawRouteOrigin:_driverLocation destination:_locPicOff];
            
            //            _tiemrLoc = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(timerFiredService) userInfo:nil repeats:YES];
//            [self getDistancebyVehicleStart:pickuplatLong endPoint:droplatLong];
            
            
            
        }
        else
        {
        
            self.title     = @"DRIVER HAS ARRIVED";
            
            self.labelTimeDuration.hidden = YES;
            
//            self.labelDistance.hidden = YES;
            
            NSLog(@"_dictionaryNotification:%@",_dictionaryNotification);
            
            
//            NSString * name         = [_dictionaryNotification valueForKey:@"driverName"];
//
//            self.labelDriverName.text = name;
//
            
            NSString *vendorPhone   = [_dictionaryNotification valueForKey:@"driverContact"];
            
            self.labelDriverPhoneNumber.text = vendorPhone;
            
            NSString * reqId        = [_dictionaryNotification valueForKey:@"bookingId"];
            NSString * email        = [_dictionaryNotification valueForKey:@"emailId"];
            self.labelDriverEmailAddress.text = email;
            
            NSString * vendorClat        = [_dictionaryNotification valueForKey:@"vendorClat"];
            
            NSString * car_number        = [_dictionaryNotification valueForKey:@"vehicleName"];
            
            if (car_number == nil)
            {
                self.labelDriverCarNumber.text= [_dictionaryNotification objectForKey:@"vehicleNumber"];
            }
            else{
                self.labelDriverCarNumber.text = [_dictionaryNotification valueForKey:@"vehicleName"];
               
            }
            
            
            NSString * vendorClong        = [_dictionaryNotification valueForKey:@"vendorClong"];
            
            
            NSString * droplatLong      = [_dictionaryNotification valueForKey:@"RequestDropLatLong"];
            
            NSArray *item1 = [droplatLong componentsSeparatedByString:@","];
            
            NSString * strLatDrop = [item1 objectAtIndex:0];
            NSString * strLongDrop = [item1 objectAtIndex:1];
            
            _locDrop       = [[CLLocation alloc]initWithLatitude:[strLatDrop floatValue] longitude:[strLongDrop floatValue]];
            
            NSString * pickuplatLong      = [_dictionaryNotification valueForKey:@"RequestPickupLatLong"];
            
            NSArray *item2          = [pickuplatLong componentsSeparatedByString:@","];
            
            NSString * strLatPic    = [item2 objectAtIndex:0];
            NSString * strLongPic   = [item2 objectAtIndex:1];
            
            
            _locPicOff  = [[CLLocation alloc]initWithLatitude:[strLatPic floatValue] longitude:[strLongPic floatValue]];
            
            
            _latDriverCar                   = [vendorClat floatValue];
            _longiDriverCar                 = [vendorClong floatValue];
            
            
             [self.labelBookingID setTitle:[NSString stringWithFormat:@"Booking Id : %@",reqId] forState:UIControlStateNormal];
           
    
            if (!(isNull(email))) {
                self.labelEmailID.text   = email;
            }
            else
            {
                self.labelEmailID.text = @"N/A";
            }
            
            NSString *strPhn = [NSString stringWithFormat:@"-%@",vendorPhone];
 
            _locationManager.delegate = self;
//            [self getDistancebyVehicleStart:pickuplatLong endPoint:droplatLong];
        }
       
        
    }
}

-(void)timerTicked:(id)sender
{
    if(_routeData)
    {
        [_timerLocationUpdate invalidate];
        [_locationManager startUpdatingLocation];
      
    }
}



-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [_tiemrLoc invalidate];
    _tiemrLoc = nil;
}



-(IBAction)buttonPhnClick:(id)sender;
{
    NSString *phoneURLString = [NSString stringWithFormat:@"tel:%@", @"911"];
    NSURL *phoneURL = [NSURL URLWithString:phoneURLString];
    [[UIApplication sharedApplication] openURL:phoneURL];
    
}

-(IBAction)buttonshareClick:(id)sender;
{
    
    NSString *cordinates = [NSString stringWithFormat:@"http://maps.apple.com/maps?q=%f,%f", latitude, longitude];
    
    NSArray *objectsToShare;
    objectsToShare =[[NSArray alloc]initWithObjects:cordinates,  nil];
    
    UIActivityViewController *activityViewControntroller = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    activityViewControntroller.excludedActivityTypes = @[];
    
    [self presentViewController:activityViewControntroller animated:true completion:nil];
}

#pragma mark - Config
-(void)config
{
    self.waypoints = [NSMutableArray array];
    self.waypointStrings = [NSMutableArray array];
    appDelegate = UIAppDelegate;
    
    self.mapView.backgroundColor = [Alert colorWithPatternImage2:kBG_IMAGE];
    
    [_mapView setTranslatesAutoresizingMaskIntoConstraints:YES];
    
    [self getAuthorizationForLocation];
    [self shadowOnUILabel];
   
    
}

#pragma mark - Button Location Navigation start On Bar Item
-(UIBarButtonItem*)buttonLocationnavigation
{
    UIButton * buttonLocationNav = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonLocationNav.frame=CGRectMake(0,0,32,32);
    [buttonLocationNav setBackgroundImage:SET_IMAGE(kIMG_DIRECTION) forState:UIControlStateNormal];
    buttonLocationNav.tintColor = kCOLOR_WHITE;
    [Alert buttonWithLayerColorAndWidth:kCOLOR_WHITE button:buttonLocationNav radius:4.0 border:1.0];
    buttonLocationNav.titleLabel.font = [UIFont fontWithName:kFONT size:10.0];
    
    [buttonLocationNav addTarget:self action:@selector(buttonLocationNavigationClick:) forControlEvents:UIControlEventTouchUpInside];
    
    _barButtonStartNavigation = [[UIBarButtonItem alloc] initWithCustomView:buttonLocationNav];
    
    return _barButtonStartNavigation;
}


-(IBAction)buttonDriverInfoCencelClick:(id)sender
{
    self.viewDriverInfo.hidden = YES;
}

#pragma mark - Location Navigation Click
-(void)buttonLocationNavigationClick:(id)sender
{
    
}

// Shadow on UILabe
-(void)shadowOnUILabel{
    
   // [Alert setUILabelShadow:self.labelBookingID shadowColor:kCOLOR_BLACK shadowOpacity:2.0f shadowRadius:4.0 masksToBounds:NO];
    
}

#pragma mark - color string
- (void)setColoredLabel:(NSString *)name phone:(NSString *)phone
{
    NSString *strComplete = [NSString stringWithFormat:@"%@ %@",name,phone];
    
    NSMutableAttributedString *attributedString =[[NSMutableAttributedString alloc] initWithString:strComplete];
    
    [attributedString addAttribute:NSForegroundColorAttributeName
                             value:[Alert colorFromHexString:kCOLOR_BUTTON]
                             range:[strComplete rangeOfString:name]];
    
    [attributedString addAttribute:NSForegroundColorAttributeName
                             value:[UIColor whiteColor]
                             range:[strComplete rangeOfString:phone]];
    self.labelPhnNum.attributedText = attributedString;
}


#pragma mark - Configure Navigation Controller

-(void)navigationBarConfiguration
{
    self.navigationController.navigationBarHidden = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    UIButton * menuButton  = [UIButton buttonWithType:UIButtonTypeSystem];
    menuButton.frame = CGRectMake(8, 18, 24, 26);
    [menuButton setBackgroundImage:[UIImage imageNamed:NAV_SLIDER_IMAGE] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(drawer:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *accountBarItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    self.navigationItem.leftBarButtonItem = accountBarItem;
    
    NSMutableArray *arrRightBarItems = [[NSMutableArray alloc] init];
    UIButton *btnSetting = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnSetting setImage:[UIImage imageNamed:@"Frequency"] forState:UIControlStateNormal];
    btnSetting.frame = CGRectMake(0, 0, 50, 50);
    btnSetting.showsTouchWhenHighlighted=YES;
    [btnSetting addTarget:self action:@selector(onSettings:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSetting];
    
    UIButton *btnLib = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnLib setImage:[UIImage imageNamed:@"logo1.png"] forState:UIControlStateNormal];
    btnLib.frame = CGRectMake(0, 0, 50, 40);
    btnLib.showsTouchWhenHighlighted=YES;
    [btnLib addTarget:self action:@selector(onMyLibrary:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButtonItem2 = [[UIBarButtonItem alloc] initWithCustomView:btnLib];
    [arrRightBarItems addObject:barButtonItem2];
    [arrRightBarItems addObject:barButtonItem];
    self.navigationItem.rightBarButtonItems=arrRightBarItems;
    
    
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
    self.navigationController.navigationBar.tintColor = [Alert colorFromHexString:kCOLOR_NAV];
  
}

-(void)onMyLibrary:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}

-(void)onSettings:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/hyptunesradio";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}

#pragma mark - Drawer class
-(void)drawer:(id)sender
{
   
    DashboardViewController *homeDriverViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"DashboardViewController");
    MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
    [UIAppDelegate manageDrawerRear:menuViewController front:homeDriverViewController];
    
}


#pragma mark -
#pragma mark --- :LOCATION Authorization :----
-(void)getAuthorizationForLocation
{
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [_locationManager setDistanceFilter:50];
    _locationManager.delegate = self;
    if (IS_iOS_8_OR_LATER)
    {
        [_locationManager requestWhenInUseAuthorization];
        // [_locationManager requestAlwaysAuthorization];
        [self.locationManager startMonitoringSignificantLocationChanges];
        [self.locationManager startUpdatingLocation];
    }
    else
    {
        //_locationManager = nil;
        [self.locationManager startMonitoringSignificantLocationChanges];
        [self.locationManager startUpdatingLocation];
    }
}




#pragma mark --- : GOOGLE MAP :----
-(void)showGoogleMap
{
//    float tempLat = appDelegate.locationGlobal.coordinate.latitude;
//    if (tempLat > 0)
//    {
//        latitude = appDelegate.locationGlobal.coordinate.latitude;
//        longitude = appDelegate.locationGlobal.coordinate.longitude;
//        // _str_lat_longStart = [NSString stringWithFormat:@"%@,%@",strLat,strLong];
//        appDelegate.locationGlobal = nil;
//    }
//    else
//    {
        coordinate = [self getLocation];
        latitude = coordinate.latitude;
        longitude = coordinate.longitude;
        // _str_lat_longStart = [NSString stringWithFormat:@"%@,%@",strLat,strLong];
//    }
    CGRect frame = CGRectMake(0, 0, kSCREEN_WIDTH,kSCREEN_HEIGHT);
    camera = [GMSCameraPosition cameraWithLatitude:latitude longitude:longitude zoom:15];
    mapView_ = [GMSMapView mapWithFrame:frame camera:camera];
    mapView_.delegate = self;
    locationMarker_ = [[GMSMarker alloc] init];
    [mapView_ setCamera:camera];
    dispatch_async(dispatch_get_main_queue(),^{
        mapView_.myLocationEnabled              = YES;
        mapView_.indoorEnabled                  = YES;
        mapView_.settings.compassButton         = YES;
        mapView_.settings.myLocationButton      = NO;
        mapView_.settings.zoomGestures          = YES;
        mapView_.settings.indoorPicker          = YES;
        mapView_.settings.scrollGestures        = YES;
        mapView_.trafficEnabled                 = YES;
        
    });
    [_mapView addSubview:mapView_];
}


#pragma mark ----: GET Coordinate2D :----
-(CLLocationCoordinate2D) getLocation
{
    CLLocation *location = [_locationManager location];
    
    coordinate = [location coordinate];
    return coordinate;
}

#pragma mark - Get My Current Location
-(void)getMyCurrentLocation
{
    coordinate = [self getLocation];
    mapView_.myLocationEnabled = YES;
    camera = [GMSCameraPosition cameraWithLatitude:coordinate.latitude longitude:coordinate.longitude zoom:ZOOM];
    [mapView_ animateToCameraPosition:camera];
}


#pragma mark - CLLocation Delegate methods

- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    
    CLLocation * currentLocation = [locations lastObject];
    
    _locationManager = manager;
    latitude = currentLocation.coordinate.latitude;
    longitude = currentLocation.coordinate.longitude;
    
//    YourCurrentLocationClass * locationData = [YourCurrentLocationClass getYourCurrentLocationClass];
//
//    locationData.currentLocation = currentLocation;
//
    _latUser    = [NSString stringWithFormat:@"%.8f",latitude];
    _longiUser    = [NSString stringWithFormat:@"%.8f",longitude];
    
    if (currentLocation.coordinate.latitude!= 0.000000)
        locationMarker_.position = CLLocationCoordinate2DMake(currentLocation.coordinate.latitude,currentLocation.coordinate.longitude);
    
    CLLocation * upadetLocation = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
    [self reverGeoCodingUsingGoogle:upadetLocation];
    
}

-(void)reverGeoCodingUsingGoogle:(CLLocation *)location
{
    [[GMSGeocoder geocoder] reverseGeocodeCoordinate:CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude) completionHandler:^(GMSReverseGeocodeResponse* response, NSError* error)
    {
        
        GMSReverseGeocodeResult *result = response.firstResult;
        
        NSArray   * arrayAdd = result.lines;
        
        NSString  * address1;
        NSString  * address2;
        
        if (arrayAdd.count>1)
        {
            address1 = result.lines[0];
            address2 = result.subLocality;
        }
        else
        {
            address1 = result.lines[0];
        }
    }];
    
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    NSLog(@"error = %@",error.localizedDescription);
}




-(void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading
{
    // North 0°
    float value = newHeading.magneticHeading;
    if(value >= 0 && value < 23)
    {
        //         NSLog(@"%@", [NSString stringWithFormat:@"%f° N",value]);
        //         marker_.rotation = value;
        [self markerAnimationWithDegree:value];
    }
    else if(value >=23 && value < 68)
    {
        //         NSLog(@"%@",[NSString stringWithFormat:@"%f° NE",value]);
        //         marker_.rotation = value;
        [self markerAnimationWithDegree:value];
    }
    else if(value >=68 && value < 113)
    {
        //         NSLog(@"%@",[NSString stringWithFormat:@"%f° E",value]);
        //         marker_.rotation = value;
        [self markerAnimationWithDegree:value];
    }
    else if(value >=113 && value < 185)
    {
        //         NSLog(@"%@",[NSString stringWithFormat:@"%f° SE",value]);
        //         marker_.rotation = value;
        [self markerAnimationWithDegree:value];
    }
    else if(value >=185 && value < 203)
    {
        //         NSLog(@"%@", [NSString stringWithFormat:@"%f° S",value]);
        //         marker_.rotation = value;
        [self markerAnimationWithDegree:value];
    }
    else if(value >=203 && value < 249)
    {
        //         NSLog(@"%@",[NSString stringWithFormat:@"%f° SE",value]);
        //         marker_.rotation = value;
        [self markerAnimationWithDegree:value];
    }
    else if(value >=249 && value < 293)
    {
        //         NSLog(@"%@",[NSString stringWithFormat:@"%f° W",value]);
        //         marker_.rotation = value;
        [self markerAnimationWithDegree:value];
    }
    else if(value >=293 && value < 350)
    {
        //         NSLog(@"%@",[NSString stringWithFormat:@"%f° NW",value]);
        //         marker_.rotation = value;
        [self markerAnimationWithDegree:value];
    }
}

-(void)markerAnimationWithDegree:(float)value
{
    [CATransaction begin];
    [CATransaction setAnimationDuration:2.0];
    //    marker_.position = coordinat;
    locationMarker_.rotation = value;
    locationMarker_.map = mapView_;
    [CATransaction commit];
    
    
}


#pragma mark - Button Cencel Ride
-(IBAction)buttonCencelClick:(id)sender
{
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:kAPPICATION_TITLE
                                 message:@"Are you sure you want to cancel this ride.?"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"YES"
                                style:UIAlertActionStyleDestructive
                                handler:^(UIAlertAction * action)
                                {
        BOOL network = [Alert networkStatus];
        if (network)
        {
            [self serviceCancelRide];
        }
        else
        {
            kNETWORK_PROBLEM;
        }
    }];
    
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"No"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
        //Handle no, thanks button
    }];
    
    [alert addAction:noButton];
    [alert addAction:yesButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

#pragma mark - Service Ride Cancel
-(void)serviceCancelRide
{
    [Alert addIndicatorView:self.view color:kCOLOR_BLACK];
    
    NSDictionary * dictLogin   = [[NSUserDefaults standardUserDefaults] dictionaryForKey:kLOGIN];
    
    NSString * user_id;
    if ([dictLogin valueForKey:@"id"] == nil)
    {
        user_id  = [dictLogin valueForKey:@"userId"];
    }
    else
    {
        user_id  = [dictLogin valueForKey:@"id"];
    }
    
    NSString * reqId   = [_dictionaryNotification valueForKey:@"bookingId"];
    // SERVER CONNCETION
    [Alert addIndicatorView:self.view color:kCOLOR_WHITE];
    WebService * connection = [[WebService alloc]init];
    connection.delegate = self;
    NSMutableDictionary * dict                              = [NSMutableDictionary dictionary];
    
    [dict setObject:kAPI_METHOD_CANCEL_REQ             forKey:kAPI_ACTION];
    [dict setObject:user_id                 forKey:@"userId"];
    
    
    [dict setObject:reqId                     forKey:@"reqId"];
    [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:kAPI_METHOD_CANCEL_REQ];
}


#pragma mark -  Service
-(void)getDataFormWebService:(NSDictionary *)jsonResults urlStr:(NSString *)yourUR methodName:(NSString *)methodName{
    
    [Alert removeView:self.view];
    NSLog(@"methodName is ----%@",methodName);
     if([methodName isEqualToString:kAPI_METHOD_CANCEL_REQ])
         {
             appDelegate = UIAppDelegate;
             DashboardViewController * homePassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBID_ARRIVED_VC);
             UIAppDelegate.isCall  = YES;
             MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
             [appDelegate manageDrawerRear:menuViewController front:homePassengerViewController];
 
         }
}
// WEB SERVER ERROR MSG LOCAL SERVER (APPLICATION SERVER)
-(void) webServiceFailWithApplicationServerMSG:(NSString *)msg
{
    
    
    [Alert removeView:self.view];
    [Alert alertViewDefalultWithTitleString:kAPPICATION_TITLE
                                     forMsg:msg forOK:kOK];
}

// WEB SERVICE FAIL WITH REGION TCP/IP
-(void) webServiceFail:(NSError *)error{
    [Alert removeView:self.view];
    [Alert alertViewDefalultWithTitleString:
     kAPPICATION_TITLE forMsg:[error localizedDescription] forOK:kOK];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (float)angleFromCoordinate:(CLLocationCoordinate2D)first
                toCoordinate:(CLLocationCoordinate2D)second {
    
    float deltaLongitude = second.longitude - first.longitude;
    float deltaLatitude = second.latitude - first.latitude;
    float angle = (M_PI * .5f) - atan(deltaLatitude / deltaLongitude);
    
    if (deltaLongitude > 0)      return angle;
    else if (deltaLongitude < 0) return angle + M_PI;
    else if (deltaLatitude < 0)  return M_PI;
    
    return 0.0f;
}



#pragma mark - calling button
-(IBAction)buttonCallClick:(id)sender
{
   
    if ([_dictionaryNotification objectForKey:@"contactNumber"] == nil)
    {
        [Alert makePhoneCall:[_dictionaryNotification objectForKey:@"driverContact"]];
    }
    else
    {
        [Alert makePhoneCall:[_dictionaryNotification objectForKey:@"contactNumber"]];
    }
  
}



- (void)drawRouteOrigin:(CLLocation *)myOrigin destination:(CLLocation *)myDestination
{
    
    
    [self fetchPolylineWithOrigin:myOrigin destination:myDestination completionHandler:^(GMSPolyline *polyline)
     {
        if(polyline)
        {
            GMSStrokeStyle *greenToRed = [GMSStrokeStyle solidColor:kCOLOR_BLACK];
            polyline.strokeWidth = 2.5;
            polyline.spans = @[[GMSStyleSpan spanWithStyle:greenToRed]];
            polyline.map = mapView_;
            
        }
        
    }];
    
}

- (void)fetchPolylineWithOrigin:(CLLocation *)origin destination:(CLLocation *)destination completionHandler:(void (^)(GMSPolyline *))completionHandler
{
    NSString *originString = [NSString stringWithFormat:@"%f,%f", origin.coordinate.latitude, origin.coordinate.longitude];
    NSString *destinationString = [NSString stringWithFormat:@"%f,%f", destination.coordinate.latitude, destination.coordinate.longitude];
    NSString *directionsAPI = @"https://maps.googleapis.com/maps/api/directions/json?";
    NSString *directionsUrlString = [NSString stringWithFormat:@"%@&origin=%@&destination=%@&mode=driving&key=%@", directionsAPI, originString, destinationString, kKEY_GOOGLE_MAP];
    NSURL *directionsUrl = [NSURL URLWithString:directionsUrlString];
    
    
    NSURLSessionDataTask *fetchDirectionsTask = [[NSURLSession sharedSession] dataTaskWithURL:directionsUrl completionHandler:
                                                 ^(NSData *data, NSURLResponse *response, NSError *error)
                                                 {
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        if(error)
        {
            if(completionHandler)
                completionHandler(nil);
            return;
        }
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            NSArray *routesArray = [json objectForKey:@"routes"];
            
            GMSPolyline *polyline = nil;
            if ([routesArray count] > 0)
            {
                NSDictionary *routeDict = [routesArray objectAtIndex:0];
                NSDictionary *routeOverviewPolyline = [routeDict objectForKey:@"overview_polyline"];
                NSString *points = [routeOverviewPolyline objectForKey:@"points"];
                GMSPath *path = [GMSPath pathFromEncodedPath:points];
                polyline = [GMSPolyline polylineWithPath:path];
                
                
                
            }
            
            if(completionHandler)
                completionHandler(polyline);
            
        });
        
    }];
    [fetchDirectionsTask resume];
}


-(void)getLatLongOfDriver
{
    NSDictionary * dictLogin  = [[NSUserDefaults standardUserDefaults] dictionaryForKey:kLOGIN];
    
    NSString * user_id;
    if ([dictLogin valueForKey:@"id"] == nil)
    {
        user_id  = [dictLogin valueForKey:@"userId"];
    }
    else
    {
        user_id  = [dictLogin valueForKey:@"id"];
    }
    WebService * connection = [[WebService alloc]init];
    connection.delegate = self;
    
    NSMutableDictionary * dict  = [NSMutableDictionary dictionary];
    [dict setObject:@"getlatlong" forKey:kAPI_ACTION];
    
    [dict setObject:[_dictionaryNotification valueForKey:@"vendorId"]                     forKey:kUSER_ID];
    [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:@"getlatlong"];
}


-(void)getDistancebyVehicleStart:(NSString *)startPoint endPoint:(NSString *)endPoint
{
    
    NSString *str=[NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/distancematrix/json?origins=%@&destinations=%@&mode=driving&sensor=true&key=%@",startPoint,endPoint,kKEY_GOOGLE_MAP];
    
    NSURL *url= [NSURL URLWithString:str];
    NSData* data = [NSData dataWithContentsOfURL:
                    url];
    
    NSError* error;
    
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:data //1
                          
                          options:kNilOptions
                          error:&error];
    //NSLog(@"json = %@",json);
    NSArray* arr_elements = (NSArray*)[json objectForKey:@"rows"]; //2
    
    
    if (arr_elements.count>0) {
        NSDictionary *first=(NSDictionary*)[arr_elements objectAtIndex:0];
        //NSLog(@"nitin first : %@",first.description);
        
        NSArray *element=(NSArray*)[first valueForKey:@"elements"];
        //NSLog(@"nitin element : %@",element.description);
        
        
        NSDictionary *dist=(NSDictionary*)[((NSDictionary*)[element objectAtIndex:0]) objectForKey:@"distance"];
        NSDictionary *time=(NSDictionary*)[((NSDictionary*)[element objectAtIndex:0]) objectForKey:@"duration"];
        //NSLog(@"nitin distance :%@",dist.description);
        
        
        //NSLog(@"nitin text :%@",(NSString*)[dist objectForKey:@"text"]);
        //  lbl_Drive.text=[NSString stringWithFormat:@"Drive : %@",[time objectForKey:@"text"]];
        if ([[time objectForKey:@"text"] isEqualToString:@"(null)"])
        {
            self.labelTimeDuration.text=@"0 min";
        }
        else
        {
            self.labelTimeDuration.text=[NSString stringWithFormat:@"%@",[time objectForKey:@"text"]];
        }
        
        if ([[dist objectForKey:@"text"] isEqualToString:@"(null)"])
        {
            self.labelDistance.text=@"0 MI";
        }
        else
        {
            self.labelDistance.text=[NSString stringWithFormat:@"%@",[dist objectForKey:@"text"]];
        }
    }
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
