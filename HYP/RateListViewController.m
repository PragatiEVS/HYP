//
//  RateListViewController.m
//  CHORETHING
//
//  Created by santosh kumar singh on 03/04/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import "RateListViewController.h"
#import "RateTableViewCell.h"


@interface RateListViewController ()<WebServiceDelegate, UITableViewDataSource, UITableViewDelegate>

@end

@implementation RateListViewController

- (void)viewDidLoad
{
    startidx = 1;
    self.rateView.hidden = YES;
    self.title = @"REVIEWS";
    _noListAvalible.hidden=YES;
    self.view.backgroundColor =[UIColor whiteColor];
    resultArray = [[NSMutableArray alloc]init];
    _completedTableView.hidden=YES;
    
    [self navigationBarConfiguration];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


-(void)back:(id)sender
{
    DISMISS_VIEW_CONTROLLER;
}

#pragma mark - Configure Navigation Controller

-(void)navigationBarConfiguration
{
    
    self.navigationController.navigationBarHidden = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    UIButton * menuButton  = [UIButton buttonWithType:UIButtonTypeSystem];
    menuButton.frame = CGRectMake(8, 18, 24, 24);
    [menuButton setBackgroundImage:[UIImage imageNamed:IMG_BACK] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *accountBarItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    self.navigationItem.leftBarButtonItem = accountBarItem;
    
    NSMutableArray *arrRightBarItems = [[NSMutableArray alloc] init];
    UIButton *btnSetting = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnSetting setImage:[UIImage imageNamed:@"Frequency"] forState:UIControlStateNormal];
    btnSetting.frame = CGRectMake(0, 0, 40, 40);
    btnSetting.showsTouchWhenHighlighted=YES;
    [btnSetting addTarget:self action:@selector(onSettings:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSetting];
    
    UIButton *btnLib = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnLib setImage:[UIImage imageNamed:@"logo1.png"] forState:UIControlStateNormal];
    btnLib.frame = CGRectMake(0, 0, 40, 40);
    btnLib.showsTouchWhenHighlighted=YES;
    [btnLib addTarget:self action:@selector(onMyLibrary:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButtonItem2 = [[UIBarButtonItem alloc] initWithCustomView:btnLib];
    [arrRightBarItems addObject:barButtonItem2];
    [arrRightBarItems addObject:barButtonItem];
    self.navigationItem.rightBarButtonItems=arrRightBarItems;
    
    
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor],
    }];
    
}
-(void)onMyLibrary:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}
-(void)onSettings:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}


-(void)viewWillAppear:(BOOL)animated
{
    [self serviceCompletedList];
}

-(void)serviceCompletedList{
    
    // SERVER CONNCETION
    [Alert svProgress:@"Please Wait..."];
    
    
    WebService * connection = [[WebService alloc]init];
    connection.delegate = self;
    NSMutableDictionary * dict                              = [NSMutableDictionary dictionary];
    
    [dict setObject:@"reviewlist"                     forKey:kAPI_ACTION];
    [dict setObject:self.userId                     forKey:kUSER_ID];
    [dict setObject:[NSString stringWithFormat:@"%d", startidx]                     forKey:@"pageNo"];
    NSLog(@"dict is ---%@",dict);
    
    [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:@"reviewlist"];
    
}

#pragma mark - Sevice delgate methods

-(void)getDataFormWebService:(NSDictionary *)jsonResults urlStr:(NSString *)yourUR methodName:(NSString *)methodName{
    
    [SVProgressHUD dismiss];
    
    SWITCH (methodName) {
        CASE (@"reviewlist")
        {
            NSLog(@"jsonResults =%@",jsonResults);
            //   resultArray=[jsonResults valueForKey:@"response"];
            
            arr=[jsonResults valueForKey:@"response"];
            
            
            for ( NSDictionary *dict in [jsonResults valueForKey:@"response"])
            {
                [resultArray addObject:dict];
            }
            
            if (arr.count==0)
            {
                
                if (resultArray.count==0)
                {
                    _noListAvalible.hidden=NO;
                    self.rateView.hidden = YES;
                    
                    self.completedTableView.hidden=YES;
                }
            }
            
            else
            {
                self.rateView.hidden = NO;
                
                self.ratingText.text =[NSString stringWithFormat:@"%@.0",[jsonResults valueForKey:@"AVGRating"]];
                
                [self.totalRating setTitle:[NSString stringWithFormat:@"%@ Total",[jsonResults valueForKey:@"reviewCount"]] forState:UIControlStateNormal];
              
                self.rate1ProgressView.progress = [[jsonResults valueForKey:@"ONESTAR"] floatValue];
                self.rate2ProgressView.progress= [[jsonResults valueForKey:@"ONETWO"] floatValue];
                self.rate3ProgressView.progress = [[jsonResults valueForKey:@"ONETHREE"] floatValue];
                self.rate4ProgressView.progress =[[jsonResults valueForKey:@"ONEFOUR"] floatValue];
                self.rate5ProgressView.progress = [[jsonResults valueForKey:@"ONEFIVE"] floatValue];
                
                self.rateViewsubmit.starNormalColor =[UIColor lightGrayColor];
                
                self.rateViewsubmit.starFillColor =[Alert colorFromHexString:@"#F4CC47"];
                self.rateViewsubmit.starSize =15;
                self.rateViewsubmit.rating = [[jsonResults valueForKey:@"AVGRating"] floatValue];
                
                _noListAvalible.hidden=YES;
                _completedTableView.hidden=NO;
                self.completedTableView.delegate=self;
                self.completedTableView.dataSource=self;
                [self.completedTableView reloadData];
                
            }
        }
        
        
        break;
        
        DEFAULT
        {
            break;
        }
    }
    
}
// WEB SERVER ERROR MSG LOCAL SERVER (APPLICATION SERVER)
-(void) webServiceFailWithApplicationServerMSG:(NSString *)msg
{
    
    [Alert svError:msg];
    
}

// WEB SERVICE FAIL WITH REGION TCP/IP
-(void) webServiceFail:(NSError *)error{
    
    
    [Alert svError:[error localizedDescription]];
    
    
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
   return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return resultArray.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    static  NSString *identifierDriver = @"RateTableViewCell";
    
    RateTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifierDriver forIndexPath:indexPath];
    cell.userThumb.layer.cornerRadius = 30;
    cell.userThumb.clipsToBounds=YES;
    
    
    if ([[[resultArray valueForKey:@"profile_picture"]objectAtIndex:indexPath.row] isEqualToString:@""])
    {
        
        NSString *userName = [[resultArray valueForKey:@"userName"]objectAtIndex:indexPath.row];
        [cell.userThumb setImageWithString:userName color:nil circular:YES];
        
    }
    else
    {
        
        [cell.userThumb setImageURL:[NSURL URLWithString:[[resultArray valueForKey:@"profile_picture"]objectAtIndex:indexPath.row]]];
    }
    
    
    cell.rateView.starSize = 15;
    cell.rateView.starNormalColor =[UIColor lightGrayColor];
    cell.rateView.starFillColor = [Alert colorFromHexString:@"#F4CC47"];
    cell.rateView.rating= [[[resultArray valueForKey:@"star"]objectAtIndex:indexPath.row]floatValue];
    cell.nameLabel.text= [[resultArray valueForKey:@"userName"]objectAtIndex:indexPath.row];
    cell.locationLabel.text= [[resultArray valueForKey:@"message"]objectAtIndex:indexPath.row];
  
    NSArray * testArray = [[NSArray alloc] init];
    
    NSString *testString = [[resultArray valueForKey:@"created"]objectAtIndex:indexPath.row];
    testArray = [testString componentsSeparatedByString:@","];
    
    cell.cratedLabel.text = [testArray objectAtIndex:0];
    cell.pendingLabel.text = [testArray objectAtIndex:2];
    
    return cell;
    
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell: (UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
      if (indexPath.row == [resultArray count] - 1 )
    {
        if (arr.count==0)
        {
            
        }
        else
        {
            startidx += 1;
            [self serviceCompletedList];
            
        }
        
        
    }
    
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
