//
//  BankAccountViewController.m
//  Q-Workerbee
//
//  Created by santosh kumar singh on 25/06/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import "BankAccountViewController.h"
#import "DashboardViewController.h"
#import "MenuViewController.h"
#import "TermsViewController.h"
#import "DriverDashboardViewController.h"

@interface BankAccountViewController ()<WebServiceDelegate>
{
    AppDelegate * appDelegate;
}

@end

@implementation BankAccountViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title= @"BANK INFORMATION";
    
     UITapGestureRecognizer* gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userTappedOnLink:)];
       // if labelView is not set userInteractionEnabled, you must do so
    [self.privacyLabel setUserInteractionEnabled:YES];
    [self.privacyLabel addGestureRecognizer:gesture];
       
       
    self.bankNameTextField.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 30);
    self.bankLNameTextField.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 30);
    self.holderTextField.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 30);
    self.accountNumberTextField.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 30);
    self.branchTextField.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 30);
    self.routinNumberTextField.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 30);
    self.accountTypeTextfiled.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 30);
    self.bankLNameTextField.layer.borderWidth = 0.5f;
    self.bankLNameTextField.layer.borderColor = [UIColor blackColor].CGColor;
    
    self.accountTypeTextfiled.layer.borderWidth = 0.5f;
    self.accountTypeTextfiled.layer.borderColor = [UIColor blackColor].CGColor;
    
    self.routinNumberTextField.layer.borderWidth = 0.5f;
    self.routinNumberTextField.layer.borderColor = [UIColor blackColor].CGColor;
    
    self.branchTextField.layer.borderWidth = 0.5f;
    self.branchTextField.layer.borderColor = [UIColor blackColor].CGColor;
    
    self.accountNumberTextField.layer.borderWidth = 0.5f;
    self.accountNumberTextField.layer.borderColor = [UIColor blackColor].CGColor;
    
    self.holderTextField.layer.borderWidth = 0.5f;
    self.holderTextField.layer.borderColor = [UIColor blackColor].CGColor;
    
    self.bankNameTextField.layer.borderWidth = 0.5f;
    self.bankNameTextField.layer.borderColor = [UIColor blackColor].CGColor;
    
    self.bankLNameTextField.layer.cornerRadius=8;
    self.accountTypeTextfiled.layer.cornerRadius=8;
    self.branchTextField.layer.cornerRadius=8;
    self.routinNumberTextField.layer.cornerRadius=8;
    self.holderTextField.layer.cornerRadius=8;
    self.bankNameTextField.layer.cornerRadius=8;
    self.accountNumberTextField.layer.cornerRadius=8;
    
    self.btnEditProfile.layer.cornerRadius=8;
    
    
    self.bankLNameTextField.clipsToBounds = YES;
    self.accountTypeTextfiled.clipsToBounds = YES;
    self.branchTextField.clipsToBounds = YES;
    self.routinNumberTextField.clipsToBounds = YES;
    self.holderTextField.clipsToBounds = YES;
    self.bankNameTextField.clipsToBounds = YES;
    self.accountNumberTextField.clipsToBounds = YES;
    
    
    [Alert setKeyBoardToolbar:@[self.holderTextField,self.bankNameTextField,self.accountNumberTextField,self.branchTextField,self.routinNumberTextField,self.accountTypeTextfiled,self.bankLNameTextField]];
    
    
    // Do any additional setup after loading the view.
    [self navigationBarConfiguration];
    
    [self configure];
}

- (void)userTappedOnLink:(UIGestureRecognizer*)gestureRecognizer
{

         TermsViewController *forgotPasswordViewController =  GET_VIEW_CONTROLLER(@"TermsViewController");
          forgotPasswordViewController.matchFrom = @"bank";
          UINavigationController * navigationController = [Alert navigationControllerWithVC:forgotPasswordViewController];
                                 
          PERSENT_VIEW_CONTOLLER(navigationController, YES);
    
    
}
-(void)navigationBarConfiguration
{
    
    self.navigationController.navigationBarHidden = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.navigationController.navigationBarHidden = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    UIButton * menuButton  = [UIButton buttonWithType:UIButtonTypeSystem];
    menuButton.frame = CGRectMake(8, 18, 24, 26);
    [menuButton setImage:[UIImage imageNamed:IMG_BACK] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *accountBarItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    self.navigationItem.leftBarButtonItem = accountBarItem;
    
    NSMutableArray *arrRightBarItems = [[NSMutableArray alloc] init];
    UIButton *btnSetting = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnSetting setImage:[UIImage imageNamed:@"Frequency"] forState:UIControlStateNormal];
    btnSetting.frame = CGRectMake(0, 0, 40, 40);
    btnSetting.showsTouchWhenHighlighted=YES;
    [btnSetting addTarget:self action:@selector(onSettings:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSetting];
    
    UIButton *btnLib = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnLib setImage:[UIImage imageNamed:@"logo1.png"] forState:UIControlStateNormal];
    btnLib.frame = CGRectMake(0, 0, 40, 40);
    btnLib.showsTouchWhenHighlighted=YES;
     [btnLib addTarget:self action:@selector(onMyLibrary:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButtonItem2 = [[UIBarButtonItem alloc] initWithCustomView:btnLib];
    [arrRightBarItems addObject:barButtonItem2];
    [arrRightBarItems addObject:barButtonItem];
    self.navigationItem.rightBarButtonItems=arrRightBarItems;
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor],
    }];
    
}
-(void)onMyLibrary:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}
-(void)onSettings:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}


-(void)back:(id)sender
{
    DISMISS_VIEW_CONTROLLER;
}


#pragma mark - configure
-(void)configure
{
    
    NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults]  objectForKey:kLOGIN];
    // NSDictionary * dictLogin = [NSKeyedUnarchiver unarchiveObjectWithData:data1];
    NSString * holder = [dictLogin objectForKey:@"AccountHolderName"];
    
  
    NSArray *listItems = [[dictLogin objectForKey:@"AccountHolderName"] componentsSeparatedByString:@" "];
    if (holder.length == 0)
    {
        
    }
    else
    {
        self.holderTextField.text =[listItems objectAtIndex:0];
       
        NSString *str =[listItems objectAtIndex:1];
        if([str length]>=0)
       {
            self.bankLNameTextField.text = [listItems objectAtIndex:1];
       }
        else
        {
             self.bankLNameTextField.text = [listItems objectAtIndex:2];
            
        }
       
    }
    
    self.bankNameTextField.text =[dictLogin objectForKey:@"BankName"];
    self.branchTextField.text =[dictLogin objectForKey:@"AccountNo"];
    self.accountTypeTextfiled.text =[dictLogin objectForKey:@"accountType"];
    self.accountNumberTextField.text =[dictLogin objectForKey:@"AccountNo"];
    self.routinNumberTextField.text =[dictLogin objectForKey:@"RoutingNo"];
    
}

- (IBAction)buttonSubmitPasswd:(id)sender
{
    if(self.bankNameTextField.text.length>0 &&
       self.bankLNameTextField.text.length>0 &&
       self.holderTextField.text.length>0 &&
       self.branchTextField.text.length>0 &&
       self.accountTypeTextfiled.text.length>0 &&
       self.accountNumberTextField.text.length>0 &&
       self.routinNumberTextField.text.length>0)
    {
        if ([self.accountNumberTextField.text isEqualToString:self.branchTextField.text])
        {
            [self updateState];
        }
        else
        {
            [Alert alertControllerTitle:kAPPICATION_TITLE msg:@"Account Number Missmatch!" ok:kOK controller:self.navigationController];
        }
        
        
        
    }
    else
    {
        
        [Alert alertControllerTitle:kAPPICATION_TITLE msg:kEmptyFields ok:kOK controller:self.navigationController];
    }
}

- (void)updateState
{
    [Alert svProgress:@"Please Wait..."];
    
    NSDictionary * dictLogin  = [[NSUserDefaults standardUserDefaults]  objectForKey:kLOGIN];
    
    NSString * user_id  = [dictLogin valueForKey:@"userId"];
    
    
    WebService * connection = [[WebService alloc]init];
    connection.delegate = self;
    NSMutableDictionary * dict                              = [NSMutableDictionary dictionary];
    
    [dict setObject:@"editprofile"                     forKey:kAPI_ACTION];
    [dict setObject:user_id                     forKey:kUSER_ID];
    [dict setObject:self.bankNameTextField.text                    forKey:@"BankName"];
    [dict setObject:[NSString stringWithFormat:@"%@ %@",self.holderTextField.text,self.bankLNameTextField.text]    forKey:@"AccountHolderName"];
    [dict setObject:self.accountNumberTextField.text               forKey:@"AccountNo"];
    [dict setObject:self.accountTypeTextfiled.text                    forKey:@"accountType"];
    [dict setObject:self.routinNumberTextField.text                forKey:@"RoutingNo"];
    
    NSLog(@"dict is ---%@",dict);
    [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:@"editprofile"];
    
    
}

#pragma mark - <WebServiveDelegate>
-(void)getDataFormWebService:(NSDictionary *)jsonResults urlStr:(NSString *)yourUR methodName:(NSString *)methodName{
    
    SWITCH (methodName) {
        CASE (@"editprofile")
        {
            
            NSLog(@"jsonResults =%@",jsonResults);
            [Alert svSuccess:[jsonResults objectForKey:@"msg"]];
            
            NSDictionary * dictoanryLoginData = [Alert removeNSNullClass:(NSMutableDictionary*)[jsonResults objectForKey:@"data"]];
            [[NSUserDefaults standardUserDefaults] setObject:dictoanryLoginData forKey:kLOGIN];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
              if ([[dictoanryLoginData valueForKey:@"role"] isEqualToString:@"Driver"]) {
                 
                  appDelegate = UIAppDelegate;
                  DriverDashboardViewController * homePassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"DriverDashboardViewController");
                  UIAppDelegate.isCall  = YES;
                  MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
                  [appDelegate manageDrawerRear:menuViewController front:homePassengerViewController];
      
              }
              else{
            appDelegate = UIAppDelegate;
            DashboardViewController * homePassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBID_ARRIVED_VC);
            UIAppDelegate.isCall  = YES;
            MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
            [appDelegate manageDrawerRear:menuViewController front:homePassengerViewController];
              }
            
        }
        break;
        
        DEFAULT
        {
            break;
        }
    }
    
}

// WEB SERVER ERROR MSG LOCAL SERVER (APPLICATION SERVER)
-(void) webServiceFailWithApplicationServerMSG:(NSString *)msg
{
    
    [Alert svError:msg];
    
}

// WEB SERVICE FAIL WITH REGION TCP/IP
-(void) webServiceFail:(NSError *)error{
    
    
    [Alert svError:[error localizedDescription]];
    
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
