//
//  CompleteMemberViewController.h
//  HYP
//
//  Created by santosh kumar singh on 22/11/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface CompleteMemberViewController : UIViewController

@property(nonatomic,weak) IBOutlet UIView * viewaddClick;
@property(nonatomic,weak) IBOutlet UITextField * msgtextfiled;
@property(nonatomic,weak) IBOutlet UIButton * submitbtn;
@property(nonatomic,weak) IBOutlet RateView * rateUpdate;

@property(nonatomic,weak)  NSString * rateingString;

@property (strong, nonatomic) NSDictionary * dict;

@property(nonatomic,weak) IBOutlet UIView * addReviewClick;
@property(nonatomic,weak) IBOutlet UIButton * addReviewbtn;
@property(nonatomic,weak) IBOutlet UIButton * reviewListbtn;
@property(nonatomic,weak) IBOutlet UIButton * ratingButton;
@end


