//
//  DriverBookingViewController.m
//  HYP
//
//  Created by VA pvt ltd on 13/09/21.
//  Copyright © 2021 Pragati Porwal. All rights reserved.
//

#import "DriverBookingViewController.h"
#import "RequestTableViewCell.h"
#import "BookingDetailsViewController.h"
#import "ArrivingViewController.h"


@interface DriverBookingViewController ()<WebServiceDelegate,UITableViewDataSource,UITableViewDelegate>

@end

@implementation DriverBookingViewController

- (void)viewDidLoad {
    _noListAvalible.hidden=YES;
    
 //   self.view.backgroundColor = [Alert colorWithPatternImage2:kBG_IMAGE];
    
    self.title =@"BOOKING HISTORY";
  
    [self navigationBarConfiguration];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    startidx = 1;
      resultArray =[[NSMutableArray alloc]init];
      [self serviceCompletedList];
}


-(void)navigationBarConfiguration
{
    
    self.navigationController.navigationBarHidden = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    if ([self.matchFrom isEqualToString:@"Dashbord"])
    {
        UIButton * menuButton  = [UIButton buttonWithType:UIButtonTypeSystem];
               menuButton.frame = CGRectMake(8, 18, 24, 26);
               [menuButton setImage:[UIImage imageNamed:IMG_BACK] forState:UIControlStateNormal];
               [menuButton addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
               UIBarButtonItem *accountBarItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
               self.navigationItem.leftBarButtonItem = accountBarItem;
    }
    else
    {
    UIButton * menuButton  = [UIButton buttonWithType:UIButtonTypeSystem];
    menuButton.frame = CGRectMake(8, 18, 24, 24);
    [menuButton setBackgroundImage:[UIImage imageNamed:NAV_SLIDER_IMAGE] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(drawer:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *accountBarItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
        
    self.navigationItem.leftBarButtonItem = accountBarItem;
    }
    
    
      NSMutableArray *arrRightBarItems = [[NSMutableArray alloc] init];
      UIButton *btnSetting = [UIButton buttonWithType:UIButtonTypeCustom];
      [btnSetting setImage:[UIImage imageNamed:@"Frequency"] forState:UIControlStateNormal];
      btnSetting.frame = CGRectMake(0, 0, 40, 40);
      btnSetting.showsTouchWhenHighlighted=YES;
      [btnSetting addTarget:self action:@selector(onSettings:) forControlEvents:UIControlEventTouchUpInside];
      UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSetting];
      
      UIButton *btnLib = [UIButton buttonWithType:UIButtonTypeCustom];
      [btnLib setImage:[UIImage imageNamed:@"logo1.png"] forState:UIControlStateNormal];
      btnLib.frame = CGRectMake(0, 0, 40, 40);
      btnLib.showsTouchWhenHighlighted=YES;
       [btnLib addTarget:self action:@selector(onMyLibrary:) forControlEvents:UIControlEventTouchUpInside];
      UIBarButtonItem *barButtonItem2 = [[UIBarButtonItem alloc] initWithCustomView:btnLib];
      [arrRightBarItems addObject:barButtonItem2];
      [arrRightBarItems addObject:barButtonItem];
      self.navigationItem.rightBarButtonItems=arrRightBarItems;
      
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName :[UIColor whiteColor]}];
}
-(void)onMyLibrary:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}
-(void)onSettings:(id)sender
{
  
    NSString*myurl=@"https://www.hypdemand.com/";
     NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
       [[UIApplication sharedApplication] openURL:url];
    }
}


-(void)back:(id)sender
{
    DISMISS_VIEW_CONTROLLER;
}

#pragma mark - Drawer class
-(void)drawer:(id)sender
{
    
    KYDrawerController *elDrawer = (KYDrawerController*)self.navigationController.parentViewController;
    [elDrawer setDrawerState:KYDrawerControllerDrawerStateOpened animated:YES];
    
    
}


-(void)serviceCompletedList{
    
    // SERVER CONNCETION
    [Alert svProgress:@"Please Wait..."];
    
    WebService * connection = [[WebService alloc]init];
    connection.delegate = self;
    NSMutableDictionary * dict                              = [NSMutableDictionary dictionary];
    NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults]  objectForKey:kLOGIN];
   
    NSString * str = [dictLogin valueForKey:@"role"];
    
    NSString * user_id ;
    if ([dictLogin valueForKey:@"id"] == nil)
    {
        user_id  = [dictLogin valueForKey:@"userId"];
        
    }
    else
    {
        user_id  = [dictLogin valueForKey:@"id"];
    }
    [dict setObject:@"carbookinglist"                     forKey:kAPI_ACTION];
    [dict setObject:user_id                     forKey:kUSER_ID];
    [dict setObject:str                     forKey:@"usertype"];
    [dict setObject:[NSString stringWithFormat:@"%d", startidx]                     forKey:@"pageNo"];
    
    NSLog(@"dict is ---%@",dict);
    [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:@"requestbookinglist"];
    
}

#pragma mark - Sevice delgate methods

-(void)getDataFormWebService:(NSDictionary *)jsonResults urlStr:(NSString *)yourUR methodName:(NSString *)methodName{
    
    [SVProgressHUD dismiss];
    
    SWITCH (methodName) {
        CASE (@"requestbookinglist")
        {
            NSLog(@"jsonResults =%@",jsonResults);
            arr=[jsonResults valueForKey:@"data"];
            
            
            for ( NSDictionary *dict in [jsonResults valueForKey:@"data"])
            {
                [resultArray addObject:dict];
            }
            
            if (arr.count==0)
            {
                if (arr.count ==0 && resultArray.count ==0)
                {
                    _noListAvalible.hidden=NO;
                    self.completedTableView.hidden=YES;
                }
                
            }
            else
            {
               _noListAvalible.hidden=YES;
                _completedTableView.hidden=NO;
                self.completedTableView.delegate=self;
                self.completedTableView.dataSource=self;
                [self.completedTableView reloadData];
                
            }
        }
        
        CASE (@"changebookingstatus")
        {
            NSLog(@"jsonResults =%@",jsonResults);
            
            if ([[jsonResults objectForKey:@"status"] isEqualToString:@"success"])
            {
                [Alert svSuccess:[jsonResults valueForKey:@"msg"]];
                [self viewDidLoad];
            }
            
        }
        break;
        
        DEFAULT
        {
            break;
        }
    }
    
}
// WEB SERVER ERROR MSG LOCAL SERVER (APPLICATION SERVER)
-(void) webServiceFailWithApplicationServerMSG:(NSString *)msg
{
    
    [Alert svError:msg];
    
}

// WEB SERVICE FAIL WITH REGION TCP/IP
-(void) webServiceFail:(NSError *)error{
    
    
    [Alert svError:[error localizedDescription]];
    
    
}


- (void)tableView:(UITableView *)tableView willDisplayCell: (UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == [resultArray count] - 1 )
    {
        if (arr.count==0)
        {
            
        }
        else{
            
            startidx += 1;
            [self serviceCompletedList];
        }
    }
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return resultArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static  NSString *identifierDriver = @"RequestTableViewCell";
    
    RequestTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifierDriver forIndexPath:indexPath];
    
   if ([[[resultArray valueForKey:@"image"]objectAtIndex:indexPath.row] isEqualToString:@""])
    {
    
        NSString *userName = [[resultArray valueForKey:@"fullName"]objectAtIndex:indexPath.row];
        [cell.userThumb setImageWithString:userName color:nil circular:YES];
       
    }
    else
    {
        
    [cell.userThumb setImageURL:[NSURL URLWithString:[[resultArray valueForKey:@"image"]objectAtIndex:indexPath.row]]];
    }
    
    cell.userThumb.layer.cornerRadius = 30;
    cell.userThumb.clipsToBounds=YES;
    cell.dateLabel.text =[[resultArray objectAtIndex:indexPath.row]objectForKey:@"created"];
   
    cell.nameLabel.text= [[resultArray valueForKey:@"fullName"]objectAtIndex:indexPath.row];
    cell.locationLabel.text= [[resultArray valueForKey:@"RequestDropAddress"]objectAtIndex:indexPath.row];
    
    cell.locationLabel.numberOfLines=2;
    
    if([[[[resultArray valueForKey:@"rideStatus"]objectAtIndex:indexPath.row] stringValue]  isEqual: @"5"])
    {
        double ntotalPrice = [[[resultArray valueForKey:@"totalAmount"]objectAtIndex:indexPath.row]doubleValue];
        NSLog(@"n totla price is ----%f",ntotalPrice);
        
        cell.totalLabel.text = [NSString stringWithFormat:@"$ %.2f",ntotalPrice];;
        cell.cratedLabel.text = @"Completed";
        cell.cratedLabel.textColor = [UIColor blueColor];
    }
    if([[[[resultArray valueForKey:@"rideStatus"]objectAtIndex:indexPath.row] stringValue]  isEqual: @"10"])
    {
        cell.cratedLabel.text = @"Driver is in route";
        cell.cratedLabel.textColor = [UIColor redColor];
        cell.totalLabel.text = [[resultArray objectAtIndex:indexPath.row]objectForKey:@"RequestPickupAddress"];;
        
    }
    if([[[[resultArray valueForKey:@"rideStatus"]objectAtIndex:indexPath.row] stringValue]  isEqual: @"11"])
    {
        cell.cratedLabel.text = @"Driver in route to P/U";
        cell.cratedLabel.textColor = [UIColor redColor];
        cell.totalLabel.text = [[resultArray objectAtIndex:indexPath.row]objectForKey:@"RequestPickupAddress"];;
        
    }
    if([[[[resultArray valueForKey:@"rideStatus"]objectAtIndex:indexPath.row] stringValue]  isEqual: @"1"])
    {
        cell.cratedLabel.text = @"Accepted";
        cell.cratedLabel.textColor = [UIColor blackColor];
        cell.totalLabel.text = [[resultArray objectAtIndex:indexPath.row]objectForKey:@"RequestPickupAddress"];;
        
    }
    if([[[[resultArray valueForKey:@"rideStatus"]objectAtIndex:indexPath.row] stringValue]  isEqual: @"2"])
    {
        cell.cratedLabel.text = @"Driver Arrived";
        cell.cratedLabel.textColor = [UIColor magentaColor];
        cell.totalLabel.text = [[resultArray objectAtIndex:indexPath.row]objectForKey:@"RequestPickupAddress"];;
        
    }
    if([[[[resultArray valueForKey:@"rideStatus"]objectAtIndex:indexPath.row] stringValue]  isEqual: @"7"])
    {
        cell.cratedLabel.text = @"Cancel";
        cell.cratedLabel.textColor = [UIColor redColor];
        cell.totalLabel.text = [[resultArray objectAtIndex:indexPath.row]objectForKey:@"RequestPickupAddress"];;
        
    }
    if([[[[resultArray valueForKey:@"rideStatus"]objectAtIndex:indexPath.row] stringValue]  isEqual: @"8"])
    {
        cell.cratedLabel.text = @"Confirmed";
        cell.cratedLabel.textColor = [UIColor redColor];
        cell.totalLabel.text = [[resultArray objectAtIndex:indexPath.row]objectForKey:@"RequestPickupAddress"];;
        
    }
    if([[[[resultArray valueForKey:@"rideStatus"]objectAtIndex:indexPath.row] stringValue]  isEqual: @"4"])
    {
        cell.cratedLabel.text = @"Payment Pending";
        cell.cratedLabel.textColor = [UIColor greenColor];
        cell.totalLabel.text = [[resultArray objectAtIndex:indexPath.row]objectForKey:@"RequestPickupAddress"];
        
    }
    if([[[[resultArray valueForKey:@"rideStatus"]objectAtIndex:indexPath.row] stringValue]  isEqual: @"3"])
    {
        cell.cratedLabel.text = @"Order on it's way!!";
        cell.cratedLabel.textColor = [UIColor redColor];
        cell.totalLabel.text = [[resultArray objectAtIndex:indexPath.row]objectForKey:@"RequestPickupAddress"];
        
    }
   
    return cell;
}

-(IBAction)btnAccept:(UIButton*)sender
{  // SERVER CONNCETION
    
    
    [Alert svProgress:@"Please Wait..."];
    
    NSDictionary * dictLogin  = [[NSUserDefaults standardUserDefaults]  objectForKey:kLOGIN];
    
    NSString * user_id ;
    if ([dictLogin valueForKey:@"id"] == nil)
    {
        user_id  = [dictLogin valueForKey:@"userId"];
        
    }
    else
    {
        user_id  = [dictLogin valueForKey:@"id"];
    }
    WebService * connection = [[WebService alloc]init];
    connection.delegate = self;
    NSMutableDictionary * dict                              = [NSMutableDictionary dictionary];
    
    [dict setObject:@"changebookingstatus"                     forKey:kAPI_ACTION];
    [dict setObject:user_id                     forKey:kUSER_ID];
    [dict setObject:[[resultArray valueForKey:@"bookingId"]objectAtIndex:sender.tag]                     forKey:@"bookingId"];
    [dict setObject:[[resultArray valueForKey:@"groupId"]objectAtIndex:sender.tag]                     forKey:@"groupId"];
    [dict setObject:@"1"                     forKey:@"status"];
    
    NSLog(@"dict is ---%@",dict);
    [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:@"changebookingstatus"];
    
}

-(IBAction)btnReject:(UIButton*)sender
{  // SERVER CONNCETION
    
    
    [Alert svProgress:@"Please Wait..."];
    NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults]  objectForKey:kLOGIN];
    
    
    NSString * user_id ;
    if ([dictLogin valueForKey:@"id"] == nil)
    {
        user_id  = [dictLogin valueForKey:@"userId"];
        
    }
    else
    {
        user_id  = [dictLogin valueForKey:@"id"];
    }
    
    WebService * connection = [[WebService alloc]init];
    connection.delegate = self;
    NSMutableDictionary * dict                              = [NSMutableDictionary dictionary];
    
    [dict setObject:@"changebookingstatus"                     forKey:kAPI_ACTION];
    [dict setObject:user_id                     forKey:kUSER_ID];
    [dict setObject:[[resultArray valueForKey:@"bookingId"]objectAtIndex:sender.tag]                     forKey:@"bookingId"];
    [dict setObject:[[resultArray valueForKey:@"groupId"]objectAtIndex:sender.tag]                     forKey:@"groupId"];
    [dict setObject:@"3"                     forKey:@"status"];
    
    
    NSLog(@"dict is ---%@",dict);
    [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:@"changebookingstatus"];
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
   if([[[[resultArray objectAtIndex:indexPath.row] valueForKey:@"rideStatus"] stringValue]  isEqual: @"5"])
    {
        [[NSUserDefaults standardUserDefaults] setValue:@"5" forKey:@"rideStatus"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        BookingDetailsViewController * bookingDetailsViewController = GET_VIEW_CONTROLLER(@"BookingDetailsViewController");
        NSMutableArray *array =[[NSMutableArray alloc]init];
        
        [array addObject:[resultArray objectAtIndex:indexPath.row]];
        NSLog(@"array is ====%@",array);
        
        bookingDetailsViewController.detailArray = array;
        MOVE_VIEW_CONTROLLER(bookingDetailsViewController, YES);
    }
    
    if([[[[resultArray objectAtIndex:indexPath.row] valueForKey:@"rideStatus"] stringValue]  isEqual: @"4"])
    {
        [[NSUserDefaults standardUserDefaults] setValue:@"4" forKey:@"rideStatus"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        BookingDetailsViewController * bookingDetailsViewController = GET_VIEW_CONTROLLER(@"BookingDetailsViewController");
        NSMutableArray *array =[[NSMutableArray alloc]init];
        
        [array addObject:[resultArray objectAtIndex:indexPath.row]];
        NSLog(@"array is ====%@",array);
        
        bookingDetailsViewController.detailArray = array;
        MOVE_VIEW_CONTROLLER(bookingDetailsViewController, YES);
    }
    if([[[[resultArray objectAtIndex:indexPath.row] valueForKey:@"rideStatus"] stringValue]  isEqual: @"2"])
     {
         [[NSUserDefaults standardUserDefaults] setValue:@"2" forKey:@"rideStatus"];
         [[NSUserDefaults standardUserDefaults] synchronize];
         ArrivingViewController * arrivingViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"ArrivingViewController");
        arrivingViewController.dictArrived = [resultArray objectAtIndex:indexPath.row];
          arrivingViewController.matchFrom=@"Booking";
         UIAppDelegate.isCall = YES;
         MOVE_VIEW_CONTROLLER(arrivingViewController, YES);
     }
    if([[[[resultArray objectAtIndex:indexPath.row] valueForKey:@"rideStatus"] stringValue]  isEqual: @"1"])
     { [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"rideStatus"];
         [[NSUserDefaults standardUserDefaults] synchronize];
         
         ArrivingViewController * arrivingViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"ArrivingViewController");
         arrivingViewController.dictArrived = [resultArray objectAtIndex:indexPath.row];
//          arrivingViewController.matchFrom=@"Booking";
         UIAppDelegate.isCall = YES;
         MOVE_VIEW_CONTROLLER(arrivingViewController, YES);
     }
    if([[[[resultArray objectAtIndex:indexPath.row] valueForKey:@"rideStatus"] stringValue]  isEqual: @"10"])
     {
         [[NSUserDefaults standardUserDefaults] setValue:@"10" forKey:@"rideStatus"];
         [[NSUserDefaults standardUserDefaults] synchronize];
         ArrivingViewController * arrivingViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"ArrivingViewController");
         arrivingViewController.dictArrived = [resultArray objectAtIndex:indexPath.row];
//          arrivingViewController.matchFrom=@"Booking";
         UIAppDelegate.isCall = YES;
         MOVE_VIEW_CONTROLLER(arrivingViewController, YES);
     }
    if([[[[resultArray objectAtIndex:indexPath.row] valueForKey:@"rideStatus"] stringValue]  isEqual: @"11"])
     {
         [[NSUserDefaults standardUserDefaults] setValue:@"11" forKey:@"rideStatus"];
         [[NSUserDefaults standardUserDefaults] synchronize];
         ArrivingViewController * arrivingViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"ArrivingViewController");
         arrivingViewController.dictArrived = [resultArray objectAtIndex:indexPath.row];
//          arrivingViewController.matchFrom=@"Booking";
         UIAppDelegate.isCall = YES;
         MOVE_VIEW_CONTROLLER(arrivingViewController, YES);
     }
    if([[[[resultArray valueForKey:@"rideStatus"]objectAtIndex:indexPath.row] stringValue]  isEqual: @"8"])
    {
        [[NSUserDefaults standardUserDefaults] setValue:@"8" forKey:@"rideStatus"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        ArrivingViewController * arrivingViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"ArrivingViewController");
        arrivingViewController.dictArrived = [resultArray objectAtIndex:indexPath.row];
//          arrivingViewController.matchFrom=@"Booking";
        UIAppDelegate.isCall = YES;
        MOVE_VIEW_CONTROLLER(arrivingViewController, YES);
    }
    if([[[[resultArray valueForKey:@"rideStatus"]objectAtIndex:indexPath.row] stringValue]  isEqual: @"3"])
    {
        [[NSUserDefaults standardUserDefaults] setValue:@"3" forKey:@"rideStatus"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        ArrivingViewController * arrivingViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"ArrivingViewController");
        arrivingViewController.dictArrived = [resultArray objectAtIndex:indexPath.row];
//          arrivingViewController.matchFrom=@"Booking";
        UIAppDelegate.isCall = YES;
        MOVE_VIEW_CONTROLLER(arrivingViewController, YES);
    }
}


@end
