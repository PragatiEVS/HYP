//
//  ManageServicesViewController.h
//  Q-Workerbee
//
//  Created by santosh kumar singh on 17/06/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface ManageServicesViewController : UIViewController
{
    IBOutlet UITableView *tblView;
    NSMutableArray *arrSelectedSectionIndex;
    NSMutableArray *resultArray;
    int sum;
     
}

@property (weak, nonatomic) IBOutlet UIPickerView *dienstPicker;
@property (weak, nonatomic) IBOutlet UIButton *donePicker;
@property(nonatomic,strong) NSString *match;
@property(nonatomic,strong) NSString *treetrimmingString;
@property (nonatomic, strong) NSMutableDictionary *PriceDicionary;
@property (strong, nonatomic)  NSMutableArray *PriceMutable;
@property (strong, nonatomic)  NSArray *lsitArr;
@property (strong, nonatomic) NSDictionary * dict;


@property (nonatomic, assign) NSInteger selectedIndex;
@property (nonatomic, strong) NSIndexPath *selectedIndexPath;
@property (weak, nonatomic) IBOutlet UIButton *addService;
@property (weak, nonatomic) IBOutlet UIButton *removeService;

@property (weak, nonatomic) IBOutlet UILabel *totalItemLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalPriceLabel;

@end


