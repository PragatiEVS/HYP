//
//  ChoreDetailViewController.h
//  CHORETHING
//
//  Created by santosh kumar singh on 08/04/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface ChoreDetailViewController : UIViewController<UICollectionViewDataSource, UICollectionViewDelegate,WebServiceDelegate>

{
     NSMutableArray *imageBusinessArray;
     NSMutableArray *imageUserArray;
}

@property(nonatomic,weak) IBOutlet UITextField * customFiled;
@property(nonatomic,weak) IBOutlet UIButton * twentyfiveBtn;
@property(nonatomic,weak) IBOutlet UIButton * fivteenBtn;
@property(nonatomic,weak) IBOutlet UIButton * tenBtn;
@property(nonatomic,weak) IBOutlet UIStackView * stackView;

@property (strong, nonatomic) NSDictionary * dict1;
@property (strong, nonatomic) NSDictionary * dict;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) IBOutlet UICollectionViewFlowLayout *collectionViewFlowLayout;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *collectionViewWidthConstraint;


@property (strong, nonatomic) IBOutlet NSLayoutConstraint *spectialText;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionViewuser;
@property (strong, nonatomic) IBOutlet UICollectionViewFlowLayout *collectionViewFlowLayoutUser;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *collectionViewWidthConstraintuser;

@property(nonatomic) float totalTip;
@property(nonatomic,weak) IBOutlet NSLayoutConstraint * imageuselabelconstant;
@property(nonatomic,weak) IBOutlet NSLayoutConstraint * stackconstant;


@property(nonatomic,weak) IBOutlet UILabel * imageuploadedbyuser;
@property(nonatomic,weak) IBOutlet UILabel * imageuploadedby;

@property(nonatomic,weak) IBOutlet UIButton * toatalAmmountName;
@property(nonatomic,weak) IBOutlet UILabel * tipAmount;

@property(nonatomic,weak) IBOutlet UILabel * tipLabel;
@property(nonatomic,weak) IBOutlet UITextView * specialNote;
@property(nonatomic,weak) IBOutlet UILabel * discontAmount;
@property(nonatomic,weak) IBOutlet UILabel * toatalAmmount;
@property(nonatomic,weak) IBOutlet UILabel * servicesAmount;

@property(nonatomic,weak) IBOutlet UILabel * datetimeLabel;

@property(nonatomic,weak) IBOutlet UIImageView * categoryImage;

@property(nonatomic,weak) IBOutlet UILabel * startTimeLabel;
@property(nonatomic,weak) IBOutlet UILabel * endTimeLabel;

@property(nonatomic,weak) IBOutlet UILabel * username;
@property(nonatomic,weak) IBOutlet UILabel * address;
@property(nonatomic,weak) IBOutlet UILabel * phone;
@property(nonatomic,weak) IBOutlet UIImageView * userBg;

@property(nonatomic,weak) IBOutlet UILabel * rateText;

@property(nonatomic,weak) IBOutlet UIView * rateview;
@property(nonatomic,weak) IBOutlet UIView * acceptView;
@property(nonatomic,weak) IBOutlet UIView * jobView;

@property (nonatomic, strong) IBOutlet RateView *rateViewsubmit;
@property(nonatomic,weak) IBOutlet UIButton * acceptbtn;
@property(nonatomic,weak) IBOutlet UIButton * rejectbtn;
@property(nonatomic,weak) IBOutlet UIButton * donebtn;

@property(nonatomic,weak) IBOutlet UIView * ratingViewClick;

@property(nonatomic,weak) IBOutlet UIView * addReviewClick;
@property(nonatomic,weak) IBOutlet UIButton * addReviewbtn;
@property(nonatomic,weak) IBOutlet UIButton * reviewListbtn;
@property(nonatomic,weak) IBOutlet UIButton * ratingButton;


@property(nonatomic,weak) IBOutlet UIView * totalView;
@property(nonatomic,weak) IBOutlet UIView * serviceView;
@property(nonatomic,weak) IBOutlet UIView * dateView;

@property(nonatomic,weak) IBOutlet UIView * memberPendingView;
@property(nonatomic,weak) IBOutlet UIView * memberCompltedView;
@property(nonatomic,weak) IBOutlet UIView * memberConfirmedView;
@property(nonatomic,weak) IBOutlet UIView * prviderCompltedView;
@property(nonatomic,weak) IBOutlet UIView * prviderJobDoneView;

@property(nonatomic,weak) IBOutlet UIView * viewaddClick;
@property(nonatomic,weak) IBOutlet UITextField * msgtextfiled;
@property(nonatomic,weak) IBOutlet UIButton * submitbtn;
@property(nonatomic,weak) IBOutlet RateView * rateUpdate;
@property(nonatomic,weak)  NSString * tipString;
@property(nonatomic,weak)  NSString * rateingString;

@end


