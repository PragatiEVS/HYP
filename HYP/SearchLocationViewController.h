//
//  SearchLocationViewController.h
//  CHORETHING
//
//  Created by santosh kumar singh on 28/04/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface SearchLocationViewController : UIViewController
<GMSMapViewDelegate,CLLocationManagerDelegate,GMSAutocompleteViewControllerDelegate>
{
      CLLocationCoordinate2D coordinate;
      CLLocationCoordinate2D startPoint;
      GMSCameraPosition *camera;
      GMSMapView *mapView_;
      float latitude , latSearch;
      float longitude, lngSearch;
      GMSMarker *locationMarker_;
}

@property (weak, nonatomic) IBOutlet UITextField *searchTextField;
@property(nonatomic,strong)  GMSGeocoder     *geocoder;
@property (strong, nonatomic) NSArray * array;
@property(nonatomic,retain) CLLocationManager *locationManager;
@property (nonatomic, strong) IBOutlet UIView * mapView;
@property(nonatomic,weak) IBOutlet UITableView  * tableView;
@end


