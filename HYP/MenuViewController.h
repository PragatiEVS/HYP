//
//  MenuViewController.h
//  Boccia
//
//  Created by IOSdev on 12/31/19.
//  Copyright © 2019 Pragati Porwal. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface MenuViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIImageView * userbgLabel;

@property (strong, nonatomic) IBOutlet UILabel * phoneLabel;
@property (strong, nonatomic) IBOutlet UILabel * nameLabel;
@property (strong, nonatomic) NSArray    * arrayMenu;
@property (strong, nonatomic) NSArray    * imageMenu;
@property (strong, nonatomic) IBOutlet UITableView  * tableView;
@end
