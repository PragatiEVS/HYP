//
//  UserBookingViewController.m
//  HYP
//
//  Created by VA pvt ltd on 14/09/21.
//  Copyright © 2021 Pragati Porwal. All rights reserved.
//

#import "UserBookingViewController.h"
#import "RequestTableViewCell.h"
#import "ChoreDetailViewController.h"
#import "BookingConfirmedViewController.h"
#import "CompleteDetailViewController.h"
#import "CompleteMemberViewController.h"
#import "ConfirmedTableViewCell.h"
#import "WaitingPassengerViewController.h"
#import "PendingPaymentViewController.h"

@interface UserBookingViewController ()<WebServiceDelegate,UITableViewDelegate,UITableViewDataSource,MDTabBarDelegate>

@end

@implementation UserBookingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    startidx = 1;
//    resultArray =[[NSMutableArray alloc]init];
    _noListAvalible.hidden=YES;
    
    self.title = @"BOOKING HISTORY";
    [self navigationBarConfiguration];
//    [self serviceConfirmedList];
    
    
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    self.matchRequest = @"Service";
    
    [_tabBar setItems:@[@"Service", @"Shop/Delivery"]];
    _tabBar.delegate = self;
    startidx = 1;
    _tabBar.selectedIndex=0;
    resultArray =[[NSMutableArray alloc]init];
    [self serviceConfirmedList];
    _completedTableView.hidden=YES;
    _serviceTableView.hidden=YES;
}


#pragma mark - TabBar Selection
- (void)tabBar:(MDTabBar *)tabBar
didChangeSelectedIndex:(NSUInteger)selectedIndex
{
    if ( selectedIndex==0)
    {
        self.matchRequest = @"Service";
        startidx = 1;
        resultArray =[[NSMutableArray alloc]init];
        _noListAvalible.hidden=YES;
        _completedTableView.hidden=YES;
        _serviceTableView.hidden = YES;
        [self serviceConfirmedList];
        
    }
    else
    {
        self.matchRequest = @"";
        startidx = 1;
        resultArray =[[NSMutableArray alloc]init];
        _noListAvalible.hidden=YES;
        _completedTableView.hidden=YES;
        _serviceTableView.hidden = YES;
        
        [self serviceCarBookingList];
    }
    
}
-(void)navigationBarConfiguration
{
    
    self.navigationController.navigationBarHidden = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    
    UIButton * menuButton  = [UIButton buttonWithType:UIButtonTypeSystem];
    menuButton.frame = CGRectMake(8, 18, 24, 24);
    [menuButton setBackgroundImage:[UIImage imageNamed:NAV_SLIDER_IMAGE] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(drawer:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *accountBarItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    self.navigationItem.leftBarButtonItem = accountBarItem;
    
    NSMutableArray *arrRightBarItems = [[NSMutableArray alloc] init];
    UIButton *btnSetting = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnSetting setImage:[UIImage imageNamed:@"Frequency"] forState:UIControlStateNormal];
    btnSetting.frame = CGRectMake(0, 0, 40, 40);
    btnSetting.showsTouchWhenHighlighted=YES;
    [btnSetting addTarget:self action:@selector(onSettings:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSetting];
    
    UIButton *btnLib = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnLib setImage:[UIImage imageNamed:@"logo1.png"] forState:UIControlStateNormal];
    btnLib.frame = CGRectMake(0, 0, 40, 40);
    btnLib.showsTouchWhenHighlighted=YES;
    [btnLib addTarget:self action:@selector(onMyLibrary:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButtonItem2 = [[UIBarButtonItem alloc] initWithCustomView:btnLib];
    [arrRightBarItems addObject:barButtonItem2];
    [arrRightBarItems addObject:barButtonItem];
    self.navigationItem.rightBarButtonItems=arrRightBarItems;
    
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName :[UIColor whiteColor]}];
}
-(void)onMyLibrary:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}
-(void)onSettings:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}


#pragma mark - Drawer class
-(void)drawer:(id)sender
{
    
    KYDrawerController *elDrawer = (KYDrawerController*)self.navigationController.parentViewController;
    [elDrawer setDrawerState:KYDrawerControllerDrawerStateOpened animated:YES];
    
}

-(void)serviceCarBookingList{
    
    // SERVER CONNCETION
    [Alert svProgress:@"Please Wait..."];
    
    NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults]  objectForKey:kLOGIN];
    
    NSString * str = [dictLogin valueForKey:@"role"];
    
    
    NSString * user_id ;
    if ([dictLogin valueForKey:@"id"] == nil)
    {
        user_id  = [dictLogin valueForKey:@"userId"];
        
    }
    else
    {
        user_id  = [dictLogin valueForKey:@"id"];
    }
    WebService * connection = [[WebService alloc]init];
    connection.delegate = self;
    NSMutableDictionary * dict                              = [NSMutableDictionary dictionary];
    
    [dict setObject:@"carbookinglist"         forKey:kAPI_ACTION];
    [dict setObject:user_id                    forKey:kUSER_ID];
    [dict setObject:str                     forKey:@"usertype"];
    [dict setObject:[NSString stringWithFormat:@"%d", startidx]     forKey:@"pageNo"];
    [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:@"carbookinglist"];
    
}


-(void)serviceConfirmedList{
    
    // SERVER CONNCETION
    [Alert svProgress:@"Please Wait..."];
    
    NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults]  objectForKey:kLOGIN];
    NSString * str = [dictLogin valueForKey:@"role"];
   
    NSString * user_id ;
    if ([dictLogin valueForKey:@"id"] == nil)
    {
        user_id  = [dictLogin valueForKey:@"userId"];
        
    }
    else
    {
        user_id  = [dictLogin valueForKey:@"id"];
    }
    WebService * connection = [[WebService alloc]init];
    connection.delegate = self;
    NSMutableDictionary * dict                              = [NSMutableDictionary dictionary];
    
    [dict setObject:@"bookinglist"                     forKey:kAPI_ACTION];
    
    [dict setObject:user_id                    forKey:kUSER_ID];
    [dict setObject:str                     forKey:@"usertype"];
    [dict setObject:@""                     forKey:@"status"];
    [dict setObject:[NSString stringWithFormat:@"%d", startidx]                     forKey:@"pageNo"];
    NSLog(@"dict is ---%@",dict);
    [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:@"bookinglist"];
    
}


-(void)getDataFormWebService:(NSDictionary *)jsonResults urlStr:(NSString *)yourUR methodName:(NSString *)methodName{
    
    [SVProgressHUD dismiss];
    
    SWITCH (methodName) {
        CASE (@"bookinglist")
        {
            arr=[jsonResults valueForKey:@"data"];
         
            for ( NSDictionary *dict in [jsonResults valueForKey:@"data"])
            {
                [resultArray addObject:dict];
            }
            
            if (arr.count==0)
            {
                
                if (arr.count ==0 && resultArray.count ==0)
                {
                    _noListAvalible.hidden=NO;
                    self.completedTableView.hidden=YES;
                    _serviceTableView.hidden = YES;
                    
                }
            }
            else
            {
                _noListAvalible.hidden=YES;
                _completedTableView.hidden=YES;
                _serviceTableView.hidden = NO;
                
                self.serviceTableView.delegate = self;
                self.serviceTableView.dataSource = self;
                [self.serviceTableView reloadData];
                
            }
        }
        
        CASE (@"carbookinglist")
        {
            NSLog(@"jsonResults =%@",jsonResults);
            arr=[jsonResults valueForKey:@"data"];
            
            for ( NSDictionary *dict in [jsonResults valueForKey:@"data"])
            {
                [resultArray addObject:dict];
            }
            if (arr.count==0)
            {
                if (arr.count ==0 && resultArray.count ==0)
                {
                    _noListAvalible.hidden=NO;
                    self.completedTableView.hidden=YES;
                    _serviceTableView.hidden = YES;
                }
            }
            else
            {
                _noListAvalible.hidden=YES;
                _completedTableView.hidden=NO;
                _serviceTableView.hidden = YES;
                self.completedTableView.delegate=self;
                self.completedTableView.dataSource=self;
                [self.completedTableView reloadData];
                
            }
        }
        
        break;
        
        DEFAULT
        {
            break;
        }
    }
    
}
// WEB SERVER ERROR MSG LOCAL SERVER (APPLICATION SERVER)
-(void) webServiceFailWithApplicationServerMSG:(NSString *)msg
{
    
    [Alert svError:msg];
    
}

// WEB SERVICE FAIL WITH REGION TCP/IP
-(void) webServiceFail:(NSError *)error{
    
    
    [Alert svError:[error localizedDescription]];
    
    
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return resultArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView == self.serviceTableView)
    {
        
        static  NSString *identifierDriver = @"ConfirmedTableViewCell";
        
        ConfirmedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifierDriver forIndexPath:indexPath];
        
        cell.userThumb.layer.cornerRadius = 30;
        cell.userThumb.clipsToBounds=YES;
        
        if ([[[resultArray valueForKey:@"image"]objectAtIndex:indexPath.row] isEqualToString:@""])
        {
            
            NSString *userName = [[resultArray valueForKey:@"fullName"]objectAtIndex:indexPath.row];
            [cell.userThumb setImageWithString:userName color:nil circular:YES];
            
        }
        else
        {
            
            [cell.userThumb setImageURL:[NSURL URLWithString:[[resultArray valueForKey:@"image"]objectAtIndex:indexPath.row]]];
        }
        
        NSString *str =[[[resultArray valueForKey:@"status"]objectAtIndex:indexPath.row] stringValue];
        
        if ([str isEqual:@"0"])
        {
            cell.pendingLabel.text = @"Pending";
            cell.pendingLabel.textColor =[UIColor blackColor];
        }
        if ([str isEqual:@"2"])
        {
            cell.pendingLabel.text = @"Completed";
            cell.pendingLabel.textColor =[UIColor systemGreenColor];
        }
        if ([str isEqual:@"1"])
        {
            cell.pendingLabel.text = @"Confirmed";
            cell.pendingLabel.textColor =[UIColor systemBlueColor];
        }
        if ([str isEqual:@"3"])
        {
            cell.pendingLabel.text = @"Cancelled";
            cell.pendingLabel.textColor =[UIColor redColor];
            
        }
        if ([str isEqual:@"6"])
        {
            cell.pendingLabel.text = @"Accepted";
            cell.pendingLabel.textColor =[UIColor redColor];
            
        }
        cell.dateLabel.layer.cornerRadius = 10;
        cell.dateLabel.clipsToBounds= YES;
        
        cell.cratedLabel.text= [[resultArray valueForKey:@"bookingDate"]objectAtIndex:indexPath.row];
        
        cell.dateLabel.backgroundColor=[UIColor blackColor];
        cell.dateLabel.textColor = [UIColor whiteColor];
        
        cell.locationLabel.numberOfLines=2;
        cell.nameLabel.text= [[resultArray valueForKey:@"fullName"]objectAtIndex:indexPath.row];
        
        cell.locationLabel.text= [[resultArray valueForKey:@"address"]objectAtIndex:indexPath.row];
        
        cell.priceLabel.text = [[resultArray valueForKey:@"serviceName"] objectAtIndex:indexPath.row];
        
        
        
        return cell;
    }
    else
    {
      
        
        static  NSString *identifierDriver = @"RequestTableViewCell";
        
        RequestTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifierDriver forIndexPath:indexPath];
        

        
        if ([[[resultArray valueForKey:@"image"]objectAtIndex:indexPath.row] isEqualToString:@""])
         {
         
             NSString *userName = [[resultArray valueForKey:@"fullName"]objectAtIndex:indexPath.row];
             [cell.userThumb setImageWithString:userName color:nil circular:YES];
            
         }
         else
         {
             
         [cell.userThumb setImageURL:[NSURL URLWithString:[[resultArray valueForKey:@"image"]objectAtIndex:indexPath.row]]];
         }
         
         cell.userThumb.layer.cornerRadius = 30;
         cell.userThumb.clipsToBounds=YES;
         cell.dateLabel.text =[[resultArray objectAtIndex:indexPath.row]objectForKey:@"created"];
        
         cell.nameLabel.text= [[resultArray valueForKey:@"fullName"]objectAtIndex:indexPath.row];
         cell.locationLabel.text= [[resultArray valueForKey:@"RequestDropAddress"]objectAtIndex:indexPath.row];
         
         cell.locationLabel.numberOfLines=2;
         
         if([[[[resultArray valueForKey:@"rideStatus"]objectAtIndex:indexPath.row] stringValue]  isEqual: @"5"])
         {
             double ntotalPrice = [[[resultArray valueForKey:@"totalAmount"]objectAtIndex:indexPath.row]doubleValue];
             NSLog(@"n totla price is ----%f",ntotalPrice);
             
             cell.totalLabel.text = [NSString stringWithFormat:@"$ %.2f",ntotalPrice];;
             cell.cratedLabel.text = @"Completed";
             cell.cratedLabel.textColor = [UIColor blueColor];
         }
         if([[[[resultArray valueForKey:@"rideStatus"]objectAtIndex:indexPath.row] stringValue]  isEqual: @"10"])
         {
             cell.cratedLabel.text = @"Driver is in route";
             cell.cratedLabel.textColor = [UIColor redColor];
             cell.totalLabel.text = [[resultArray valueForKey:@"RequestPickupAddress"]objectAtIndex:indexPath.row];;
             
         }
        if([[[[resultArray valueForKey:@"rideStatus"]objectAtIndex:indexPath.row] stringValue]  isEqual: @"11"])
        {
            cell.cratedLabel.text = @"Driver is in route to P/U";
            cell.cratedLabel.textColor = [UIColor redColor];
            cell.totalLabel.text = [[resultArray valueForKey:@"RequestPickupAddress"]objectAtIndex:indexPath.row];;
            
        }
       
         if([[[[resultArray valueForKey:@"rideStatus"]objectAtIndex:indexPath.row] stringValue]  isEqual: @"1"])
         {
             cell.cratedLabel.text = @"Accepted";
             cell.cratedLabel.textColor = [UIColor blackColor];
             cell.totalLabel.text = [[resultArray valueForKey:@"RequestPickupAddress"]objectAtIndex:indexPath.row];;
             
         }
         if([[[[resultArray valueForKey:@"rideStatus"]objectAtIndex:indexPath.row] stringValue]  isEqual: @"2"])
         {
             cell.cratedLabel.text = @"Driver Arrived";
             cell.cratedLabel.textColor = [UIColor magentaColor];
             cell.totalLabel.text = [[resultArray valueForKey:@"RequestPickupAddress"]objectAtIndex:indexPath.row];;
             
         }
         if([[[[resultArray valueForKey:@"rideStatus"]objectAtIndex:indexPath.row] stringValue]  isEqual: @"7"])
         {
             cell.cratedLabel.text = @"Cancel";
             cell.cratedLabel.textColor = [UIColor redColor];
             cell.totalLabel.text = [[resultArray valueForKey:@"RequestPickupAddress"]objectAtIndex:indexPath.row];;
             
         }
         if([[[[resultArray valueForKey:@"rideStatus"]objectAtIndex:indexPath.row] stringValue]  isEqual: @"4"])
         {
             cell.cratedLabel.text = @"Payment Pending";
             cell.cratedLabel.textColor = [UIColor greenColor];
             cell.totalLabel.text = [[resultArray valueForKey:@"RequestPickupAddress"]objectAtIndex:indexPath.row];;
             
         }
         if([[[[resultArray valueForKey:@"rideStatus"]objectAtIndex:indexPath.row] stringValue]  isEqual: @"3"])
         {
             cell.cratedLabel.text = @"Order on it's way!!";
             cell.cratedLabel.textColor = [UIColor redColor];
             cell.totalLabel.text = [[resultArray valueForKey:@"RequestPickupAddress"]objectAtIndex:indexPath.row];;
             
         }
        if([[[[resultArray valueForKey:@"rideStatus"]objectAtIndex:indexPath.row] stringValue]  isEqual: @"6"])
        {
            cell.cratedLabel.text = @"Pending";
            cell.cratedLabel.textColor = [UIColor redColor];
            cell.totalLabel.text = [[resultArray valueForKey:@"RequestPickupAddress"]objectAtIndex:indexPath.row];;
            
        }
        if([[[[resultArray valueForKey:@"rideStatus"]objectAtIndex:indexPath.row] stringValue]  isEqual: @"8"])
        {
            cell.cratedLabel.text = @"Confirmed";
            cell.cratedLabel.textColor = [UIColor redColor];
            cell.totalLabel.text = [[resultArray valueForKey:@"RequestPickupAddress"]objectAtIndex:indexPath.row];;
            
        }
        return cell;
        
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView == self.serviceTableView)
    {
        NSString *str =[[[resultArray valueForKey:@"status"]objectAtIndex:indexPath.row] stringValue];
        
        if ([str isEqual:@"6"])
        {
            BookingConfirmedViewController * settingsViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"BookingConfirmedViewController");
            settingsViewController.dict = [resultArray objectAtIndex:indexPath.row];
            UINavigationController * navigationController = [Alert navigationControllerWithVC:settingsViewController];
            PERSENT_VIEW_CONTOLLER(navigationController, YES);
        }
        else if ([str isEqual:@"2"])
        {
            CompleteMemberViewController * settingsViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"CompleteMemberViewController");
            settingsViewController.dict = [resultArray objectAtIndex:indexPath.row];
            UINavigationController * navigationController = [Alert navigationControllerWithVC:settingsViewController];
            PERSENT_VIEW_CONTOLLER(navigationController, YES);
        }
        else
        {
            
            ChoreDetailViewController * settingsViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"ChoreDetailViewController");
            settingsViewController.dict = [resultArray objectAtIndex:indexPath.row];
            UINavigationController * navigationController = [Alert navigationControllerWithVC:settingsViewController];
            PERSENT_VIEW_CONTOLLER(navigationController, YES);
        }
    }
    else{
       

        if([[[[resultArray objectAtIndex:indexPath.row] valueForKey:@"rideStatus"] stringValue]  isEqual: @"1"] )
        {
            WaitingPassengerViewController *waitingPassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"WaitingPassengerViewController");
            NSMutableArray *array =[[NSMutableArray alloc]init];
            
            [array addObject:[resultArray objectAtIndex:indexPath.row]];
           
            waitingPassengerViewController.dictionaryNotification = [resultArray objectAtIndex:indexPath.row];
            UINavigationController * navigationController = [Alert navigationControllerWithVC:waitingPassengerViewController];
            PERSENT_VIEW_CONTOLLER(navigationController, YES);
           
        }
       else if([[[[resultArray objectAtIndex:indexPath.row] valueForKey:@"rideStatus"] stringValue]  isEqual: @"2"] )
        {
            WaitingPassengerViewController *waitingPassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"WaitingPassengerViewController");
            NSMutableArray *array =[[NSMutableArray alloc]init];
            
            [array addObject:[resultArray objectAtIndex:indexPath.row]];
           
            waitingPassengerViewController.dictionaryNotification = [resultArray objectAtIndex:indexPath.row];
            UINavigationController * navigationController = [Alert navigationControllerWithVC:waitingPassengerViewController];
            PERSENT_VIEW_CONTOLLER(navigationController, YES);
           
        }
       else if([[[[resultArray objectAtIndex:indexPath.row] valueForKey:@"rideStatus"] stringValue]  isEqual: @"3"] )
        {
            WaitingPassengerViewController *waitingPassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"WaitingPassengerViewController");
            NSMutableArray *array =[[NSMutableArray alloc]init];
            
            [array addObject:[resultArray objectAtIndex:indexPath.row]];
           
            waitingPassengerViewController.dictionaryNotification = [resultArray objectAtIndex:indexPath.row];
            UINavigationController * navigationController = [Alert navigationControllerWithVC:waitingPassengerViewController];
            PERSENT_VIEW_CONTOLLER(navigationController, YES);
           
        }
       else if([[[[resultArray objectAtIndex:indexPath.row] valueForKey:@"rideStatus"] stringValue]  isEqual: @"11"] )
        {
            WaitingPassengerViewController *waitingPassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"WaitingPassengerViewController");
            NSMutableArray *array =[[NSMutableArray alloc]init];
            
            [array addObject:[resultArray objectAtIndex:indexPath.row]];
           
            waitingPassengerViewController.dictionaryNotification = [resultArray objectAtIndex:indexPath.row];
            UINavigationController * navigationController = [Alert navigationControllerWithVC:waitingPassengerViewController];
            PERSENT_VIEW_CONTOLLER(navigationController, YES);
           
        }
       else if([[[[resultArray objectAtIndex:indexPath.row] valueForKey:@"rideStatus"] stringValue]  isEqual: @"10"] )
        {
            WaitingPassengerViewController *waitingPassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"WaitingPassengerViewController");
            NSMutableArray *array =[[NSMutableArray alloc]init];
            
            [array addObject:[resultArray objectAtIndex:indexPath.row]];
           
            waitingPassengerViewController.dictionaryNotification = [resultArray objectAtIndex:indexPath.row];
            UINavigationController * navigationController = [Alert navigationControllerWithVC:waitingPassengerViewController];
            PERSENT_VIEW_CONTOLLER(navigationController, YES);
           
        }
       else if([[[[resultArray objectAtIndex:indexPath.row] valueForKey:@"rideStatus"] stringValue]  isEqual: @"8"] )
        {
            WaitingPassengerViewController *waitingPassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"WaitingPassengerViewController");
            NSMutableArray *array =[[NSMutableArray alloc]init];
            
            [array addObject:[resultArray objectAtIndex:indexPath.row]];
           
            waitingPassengerViewController.dictionaryNotification = [resultArray objectAtIndex:indexPath.row];
            UINavigationController * navigationController = [Alert navigationControllerWithVC:waitingPassengerViewController];
            PERSENT_VIEW_CONTOLLER(navigationController, YES);
           
        }
        else  if([[[[resultArray objectAtIndex:indexPath.row] valueForKey:@"rideStatus"] stringValue]  isEqual: @"4"] )
         {
            PendingPaymentViewController * settingsViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"PendingPaymentViewController");
            NSMutableArray *array =[[NSMutableArray alloc]init];
           
            [array addObject:[resultArray objectAtIndex:indexPath.row]];
            settingsViewController.detailArray = array;
            UINavigationController * navigationController = [Alert navigationControllerWithVC:settingsViewController];
            PERSENT_VIEW_CONTOLLER(navigationController, YES);
        }
        else  if([[[[resultArray objectAtIndex:indexPath.row] valueForKey:@"rideStatus"] stringValue]  isEqual: @"5"] )
         {
            PendingPaymentViewController * settingsViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"PendingPaymentViewController");
            NSMutableArray *array =[[NSMutableArray alloc]init];
           [array addObject:[resultArray objectAtIndex:indexPath.row]];
            settingsViewController.detailArray = array;
            UINavigationController * navigationController = [Alert navigationControllerWithVC:settingsViewController];
            PERSENT_VIEW_CONTOLLER(navigationController, YES);
        }
    }
    
    
}
- (void)tableView:(UITableView *)tableView willDisplayCell: (UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.serviceTableView)
    {
        if (indexPath.row == [resultArray count] - 1 )
        {
            if (arr.count==0)
            {
                
            }
            else{
                startidx += 1;
                [self serviceConfirmedList];
            }
        }
    }
    else{
        if (indexPath.row == [resultArray count] - 1 )
        {
            if (arr.count==0)
            {
                
            }
            else{
                startidx += 1;
                [self serviceCarBookingList];
            }
        }
    }
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
