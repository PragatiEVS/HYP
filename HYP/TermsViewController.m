//
//  TermsViewController.m
//  CHORETHING
//
//  Created by santosh kumar singh on 27/03/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import "TermsViewController.h"
#import "RegisterInfoViewController.h"
#import "DashboardViewController.h"
#import "MenuViewController.h"
#import "ViewController.h"
#import "DriverRegisterViewController.h"


@interface TermsViewController ()<WebServiceDelegate,MDTabBarDelegate>
{
    AppDelegate * appDelegate;
}
@end

@implementation TermsViewController

- (void)viewDidLoad
{
   
    [super viewDidLoad];
    if ([self.matchFrom isEqualToString:@"bank"])
    {
        self.buttonCheck.hidden = YES;
         self.title= @"PRIVACY POLICY";
    }
    else{
        self.buttonCheck.hidden = NO;
         self.title= @"TERMS OF SERVICE";
    }
    
    [self navigationBarConfigure];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    [self serviceTermsAndConditions];
}


-(void)serviceTermsAndConditions
{
    [Alert svProgress:@"Please Wait..."];
    
    
    // SERVER CONNCETION
    WebService * connection = [[WebService alloc]init];
    connection.delegate = self;
    
    NSMutableDictionary * dict                                  = [NSMutableDictionary dictionary];
    
    if ([self.matchFrom isEqualToString:@"bank"])
    {
        [dict setObject:@"privacypolicy"                        forKey:kAPI_ACTION];
        
    }
    else{
        [dict setObject:kAPI_METHOD_TERM_COND                        forKey:kAPI_ACTION];
    }
    [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:kAPI_METHOD_TERM_COND];
    
    
}



-(void)getDataFormWebService:(NSDictionary *)jsonResults urlStr:(NSString *)yourUR methodName:(NSString *)methodName{
    
    [SVProgressHUD dismiss];
    
    SWITCH (methodName) {
        CASE (kAPI_METHOD_TERM_COND)
        {
            NSLog(@"%@",jsonResults);
            NSString * strTerm = [jsonResults objectForKey:@"msg"];
            
            NSDictionary *options = @{ NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType };
            
            NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithData:[strTerm dataUsingEncoding:NSUTF8StringEncoding] options:options documentAttributes:nil error:nil];
            
            _textViewTerm.attributedText  = attrString;
        }
        
        break;
        
        DEFAULT
        {
            break;
        }
    }
    
}
#pragma mark - navigation Bar Configure
-(void)navigationBarConfigure
{
    
    self.navigationController.navigationBarHidden = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    UIButton * menuButton  = [UIButton buttonWithType:UIButtonTypeSystem];
    menuButton.frame = CGRectMake(8, 18, 24, 24);
    [menuButton setBackgroundImage:[UIImage imageNamed:IMG_BACK] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(backButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *accountBarItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    self.navigationItem.leftBarButtonItem = accountBarItem;
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor],
    }];
    
    
}

#pragma mark - button Back Click
-(void)backButtonClick:(id)sender
{
  
    if ([self.matchFrom isEqualToString:@"bank"])
    {
        DISMISS_VIEW_CONTROLLER;
    }
    else{
      ViewController *reqSubmitViewController = GET_VIEW_CONTROLLER(@"ViewController");
        [self.navigationController pushViewController:reqSubmitViewController animated:YES];
        
    }
    // Go back (pop view controller)
    
}


-(IBAction)buttonCheckClick:(id)sender
{
    
    [self.buttonCheck setImage:SET_IMAGE(@"whitecheckbox") forState:UIControlStateNormal];
    
    NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"ProfileDetail"];
    
    
    NSString * str = [dictLogin valueForKey:@"role"];
    
    if ([str isEqualToString:@"Member"])
    {
        
        appDelegate = UIAppDelegate;
        DashboardViewController * homePassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBID_ARRIVED_VC);
        UIAppDelegate.isCall  = YES;
        MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
        [appDelegate manageDrawerRear:menuViewController front:homePassengerViewController];
        
        
    }
    else  if ([str isEqualToString:@"Driver"])
        {
            DriverRegisterViewController *forgotPasswordViewController =  GET_VIEW_CONTROLLER(@"DriverRegisterViewController");
            UINavigationController * navigationController = [Alert navigationControllerWithVC:forgotPasswordViewController];
            
            PERSENT_VIEW_CONTOLLER(navigationController, YES);
           
        }
    else
    {
        
        RegisterInfoViewController *forgotPasswordViewController =  GET_VIEW_CONTROLLER(@"RegisterInfoViewController");
        UINavigationController * navigationController = [Alert navigationControllerWithVC:forgotPasswordViewController];
        
        PERSENT_VIEW_CONTOLLER(navigationController, YES);
        
    }
    
    
}
// WEB SERVER ERROR MSG LOCAL SERVER (APPLICATION SERVER)
-(void) webServiceFailWithApplicationServerMSG:(NSString *)msg
{
    
    [Alert svError:msg];
    
}

// WEB SERVICE FAIL WITH REGION TCP/IP
-(void) webServiceFail:(NSError *)error{
    
    
    [Alert svError:[error localizedDescription]];
    
    
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
