//
//  BookingProgressViewController.h
//  HYP
//
//  Created by santosh kumar singh on 26/09/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface BookingProgressViewController : UIViewController<GMSMapViewDelegate>
{
     CLLocationManager *loctionManager;
    CLLocation *myOrigin;
    CLLocation *myDestination ;
     GMSCameraPosition *camera;
     GMSMapView *mapView_;
     GMSMarker *locationMarker_;
     float latitude , latSearch;
     float longitude, lngSearch;
}

@property (strong, nonatomic)IBOutlet UIView * userDetailView;
@property (strong, nonatomic)IBOutlet UIView * addressView;

@property(nonatomic,strong)  GMSGeocoder     *geocoder;
@property(nonatomic,retain) CLLocationManager *locationManager;
@property (nonatomic, strong) IBOutlet UIView * mapView;

@property (strong, nonatomic) NSDictionary * dict;


@property (strong, nonatomic)IBOutlet UILabel * username;
@property (strong, nonatomic)IBOutlet UILabel * serviceAddressLabel;

@property (strong, nonatomic)IBOutlet UILabel * currentAddressLabel;


@end


