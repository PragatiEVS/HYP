//
//  AddMoneyViewController.m
//  GetSum
//
//  Created by santosh kumar singh on 28/04/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import "AddMoneyViewController.h"
#import "BankAccountViewController.h"
#import "CompleteMemberViewController.h"
#import "DashboardViewController.h"
#import "MenuViewController.h"
#import "AppDelegate.h"

@interface AddMoneyViewController ()
{
    
    AppDelegate * appDelegate;
    
}
@end

@implementation AddMoneyViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    if ([self.matchString isEqualToString:@"Services"])
    {
        self.title = [NSString stringWithFormat:@"Total Payable Amount: $%@",self.priceString];
        self.amountLable.text = [NSString stringWithFormat:@"$%@",self.priceString];
        self.nameLable.text=[NSString stringWithFormat:@"%@",self.servicename];
        
    }
   else if ([self.matchString isEqualToString:@"Driver"])
    {
        self.title = [NSString stringWithFormat:@"Total Payable Amount: $%@",self.priceString];
        self.amountLable.text = [NSString stringWithFormat:@"$%@",self.priceString];
        self.nameLable.text=[NSString stringWithFormat:@"%@",self.servicename];
        
    }
   else if ([self.matchString isEqualToString:@"Tip"])
    {
        self.title = [NSString stringWithFormat:@"Total Payable Tip Amount: $%@",self.tipString];
        
        self.amountLable.text = [NSString stringWithFormat:@"Total Payable Tip Amount: $%@",self.tipString];
        
        self.nameLable.text=[NSString stringWithFormat:@"Service Name: %@",self.servicename];
        
    }
    else
    {
        
        self.title = @"CREDIT & DEBIT ACCEPTED";
        self.nameLable.text=[NSString stringWithFormat:@"Pay $%@ for HYP MEMBERSHIP",self.priceString];
        
    }
    
    txtamount.layer.cornerRadius = 12.0f;
    txtamount.clipsToBounds = YES;
    
    UITapGestureRecognizer *gr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleGesture:)];
    [self.view addGestureRecognizer:gr];
    
    [Alert setKeyBoardToolbar:@[txtCardNo,txtCvv]];
    
    picker = [[NTMonthYearPicker alloc] init];
    [picker addTarget:self action:@selector(onDatePicked:) forControlEvents:UIControlEventValueChanged];
    
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    NSCalendar *cal = [NSCalendar currentCalendar];
    
    // Set mode to month + year
    // This is optional; default is month + year
    picker.datePickerMode = NTMonthYearPickerModeMonthAndYear;
    
    picker.backgroundColor=[UIColor whiteColor];
    
    // Set minimum date to January 2000
    // This is optional; default is no min date
    [comps setDay:1];
    [comps setMonth:1];
    [comps setYear:2000];
    picker.minimumDate = [NSDate date];
    
    // Set maximum date to next month
    // This is optional; default is no max date
    [comps setDay:0];
    [comps setMonth:12];
    [comps setYear:20];
    picker.maximumDate = [cal dateByAddingComponents:comps toDate:[NSDate date] options:0];
    
    // Set initial date to last month
    // This is optional; default is current month/year
    [comps setDay:0];
    [comps setMonth:-1];
    [comps setYear:0];
    picker.date = [cal dateByAddingComponents:comps toDate:[NSDate date] options:0];
    
    // Initialize UI label and mode selector
    [self updateLabel];
    [picker setHidden:true];
    
    [self SetTextFieldBorder: txtCardNo];
    [self SetTextFieldBorder:txtCvv];
    [self SetTextFieldBorder:txtYear];
    [self SetTextFieldBorder:txtMonth];
    
    [self navigationBarConfiguration];
    
    //  self.view.backgroundColor = [Alert colorWithPatternImage2:kBG_IMAGE];
    // Do any additional setup after loading the view.
}

-(void)navigationBarConfiguration
{
    
    self.navigationController.navigationBarHidden = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    UIButton * menuButton  = [UIButton buttonWithType:UIButtonTypeSystem];
    menuButton.frame = CGRectMake(8, 18, 24, 24);
    [menuButton setBackgroundImage:[UIImage imageNamed:IMG_BACK] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *accountBarItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    self.navigationItem.leftBarButtonItem = accountBarItem;
    
    NSMutableArray *arrRightBarItems = [[NSMutableArray alloc] init];
       UIButton *btnSetting = [UIButton buttonWithType:UIButtonTypeCustom];
       [btnSetting setImage:[UIImage imageNamed:@"Frequency"] forState:UIControlStateNormal];
       btnSetting.frame = CGRectMake(0, 0, 50, 50);
       btnSetting.showsTouchWhenHighlighted=YES;
       [btnSetting addTarget:self action:@selector(onSettings:) forControlEvents:UIControlEventTouchUpInside];
       UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSetting];
       
       UIButton *btnLib = [UIButton buttonWithType:UIButtonTypeCustom];
       [btnLib setImage:[UIImage imageNamed:@"logo1.png"] forState:UIControlStateNormal];
       btnLib.frame = CGRectMake(0, 0, 50, 40);
       btnLib.showsTouchWhenHighlighted=YES;
       [btnLib addTarget:self action:@selector(onMyLibrary:) forControlEvents:UIControlEventTouchUpInside];
       UIBarButtonItem *barButtonItem2 = [[UIBarButtonItem alloc] initWithCustomView:btnLib];
       [arrRightBarItems addObject:barButtonItem2];
       [arrRightBarItems addObject:barButtonItem];
       self.navigationItem.rightBarButtonItems=arrRightBarItems;
       
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName :[UIColor whiteColor]}];
}

-(void)back:(id)sender
{
    DISMISS_VIEW_CONTROLLER;
}

-(void)onMyLibrary:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}

-(void)onSettings:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/hyptunesradio";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}

#pragma mark - Drawer class
-(void)drawer:(id)sender
{
    
    KYDrawerController *elDrawer = (KYDrawerController*)self.navigationController.parentViewController;
    [elDrawer setDrawerState:KYDrawerControllerDrawerStateOpened animated:YES];
    
    
}
- (void)handleGesture:(UIGestureRecognizer *)gestureRecognizer {
    [picker setHidden:true];
}

- (void)updateLabel {
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    
    [df setDateFormat:@"MM"];
    
    _month = [df stringFromDate:picker.date];
    //txtMonth.text = dateStr;
    NSDateFormatter *df1 = [[NSDateFormatter alloc] init];
    
    [df1 setDateFormat:@"yyyy"];
    _year = [df1 stringFromDate:picker.date];
    
    txtMonth.text = [NSString stringWithFormat:@"%@/%@",_month,_year];
    
    expiry.text = [NSString stringWithFormat:@"%@/%@",_month,_year];
    
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    picker.backgroundColor = [UIColor whiteColor];
    CGSize pickerSize = [picker sizeThatFits:CGSizeZero];
    if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ) {
        // iPhone: show picker at the bottom of the screen
        if( ![picker isDescendantOfView:self.view] ) {
            picker.frame = CGRectMake( 0, [[UIScreen mainScreen] bounds].size.height - 300, [[UIScreen mainScreen] bounds].size.width, 300);
            [self.view addSubview:picker];
        }
    } else {
        // iPad: show picker in a popover
        if( !popupCtrl.isPopoverVisible ) {
            UIView *container = [[UIView alloc] init];
            [container addSubview:picker];
            
            UIViewController* popupVC = [[UIViewController alloc] init];
            popupVC.view = container;
            
            popupCtrl = [[UIPopoverController alloc] initWithContentViewController:popupVC];
            [popupCtrl setPopoverContentSize:picker.frame.size animated:NO];
            
            
        }
    }
}

#pragma mark - <UITextFieldDelegate>

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField == txtCardNo)
    {
        cardnumberLabel.text =txtCardNo.text;
        if (textField.text.length >= 16 && range.length == 0)
        {
            return NO; // return NO to not change text
        }
        else
        {
            return YES;
        }
    }
    else if(textField == txtCvv)
    {
        if (textField.text.length >= 3 && range.length == 0)
        {
            return NO; // return NO to not change text
        }
        else
        {
            return YES;
        }
    }
    
    else
    {
        return YES;
        
    }
}


-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [picker setHidden:true];
    
    
    if(textField == txtMonth || textField == txtYear)
    {
        [textField resignFirstResponder];
        [picker setHidden:false];
        
    }
}

-(void)SetTextFieldBorder :(UITextField *)textField
{
    
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 1.0;
    border.borderColor = [UIColor blackColor].CGColor;
    border.frame = CGRectMake(0, textField.frame.size.height - borderWidth, textField.frame.size.width, textField.frame.size.height);
    border.borderWidth = borderWidth;
    [textField.layer addSublayer:border];
    textField.layer.masksToBounds = YES;
    
}
- (void)onDatePicked:(UITapGestureRecognizer *)gestureRecognizer {
    [self updateLabel];
}

#pragma mark - service Register by Email
#pragma mark - done Button
- (IBAction)buttonSave:(id)sender
{
    
    [Alert svProgress:@"Please wait...."];
    
    cardParams = [[STPCardParams alloc] init];
    cardParams.number = txtCardNo.text;
    cardParams.expMonth = _month.integerValue ;
    cardParams.expYear = _year.integerValue;
    cardParams.cvc = txtCvv.text;
    
    
    [[STPAPIClient sharedClient] createTokenWithCard:cardParams completion:^(STPToken *token, NSError *error) {
        if (token == nil || error != nil) {
            // Present error to user...
            
            [SVProgressHUD dismiss];
            [Alert alertViewDefalultWithTitleString:kAPPICATION_TITLE
                                             forMsg:[error localizedDescription] forOK:kOK];
            return;
        }
        else
        {
            //            [self registerDriverAPI];
            
            if ([self.matchString isEqualToString:@"Services"])
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"HYP"
                                                                message:[NSString stringWithFormat:@"You will $%@ Pay for Service.",self.priceString]
                                                               delegate:self
                                                      cancelButtonTitle:@"Cancel"
                                                      otherButtonTitles:@"OK", nil];
                [alert setTag:11];
                [alert show];
            }
            
            else  if ([self.matchString isEqualToString:@"Driver"])
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"HYP"
                                                                message:[NSString stringWithFormat:@"You will $%@ Pay for Driver Booking.",self.priceString]
                                                               delegate:self
                                                      cancelButtonTitle:@"Cancel"
                                                      otherButtonTitles:@"OK", nil];
                [alert setTag:11];
                [alert show];
            }
            else if ([self.matchString isEqualToString:@"Tip"])
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"HYP"
                                                                message:[NSString stringWithFormat:@"You will Pay $%@ for tip %@ Service.",self.tipString, self.servicename]
                                                               delegate:self
                                                      cancelButtonTitle:@"Cancel"
                                                      otherButtonTitles:@"OK", nil];
                [alert setTag:11];
                [alert show];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"HYP"
                                                                message:[NSString stringWithFormat:@"You will be Pay $%@ for HYP Membership.",self.priceString]
                                                               delegate:self
                                                      cancelButtonTitle:@"Cancel"
                                                      otherButtonTitles:@"OK", nil];
                [alert setTag:11];
                [alert show];
            }
            
            
            
            
        }
        
        
    }];
    
    // }
}


-(void)StripeCharg
{
    
    [[STPAPIClient sharedClient] createTokenWithCard:cardParams
                                          completion:^(STPToken *token, NSError *error)
     {
        if (error)
        {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [Alert svError:[error localizedDescription]];
                
                
            });
            
        }
        else
        {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if ([self.matchString isEqualToString:@"Services"])
                {
                    [self chargeService:token.tokenId];
                }
                else if ([self.matchString isEqualToString:@"Driver"])
                {
                    [self chargeService:token.tokenId];
                }
                else  if ([self.matchString isEqualToString:@"Tip"])
                {
                    [self chargeTipService:token.tokenId];
                }
                else
                {
                    [self chargeService:token.tokenId];
                }
            
            });
        }
        
    }];
}

-(void)chargeTipService:(NSString*)strToken
{
    [Alert svProgress:@"Please Wait..."];
    
    NSDictionary * dictLogin  = [[NSUserDefaults standardUserDefaults]  objectForKey:kLOGIN];
    //NSDictionary * dictLogin = [NSKeyedUnarchiver unarchiveObjectWithData:data1];
    
    NSString * user_id ;
    if ([dictLogin valueForKey:@"id"] == nil)
    {
        user_id  = [dictLogin valueForKey:@"userId"];
        
    }
    else
    {
        user_id  = [dictLogin valueForKey:@"id"];
    }
    
    int num  = [self.priceString intValue]+[self.tipString intValue];
    
    WebService * connection = [[WebService alloc]init];
    connection.delegate = self;
    
    NSMutableDictionary * dict       = [NSMutableDictionary dictionary];
    [dict setObject:@"updatepayment"                    forKey:kAPI_ACTION];
    [dict setObject:user_id                    forKey:kUSER_ID];
    [dict setObject:txtCardNo.text                    forKey:@"cardNo"];
    [dict setObject:self.groupId                    forKey:@"groupId"];
    [dict setObject:[NSString stringWithFormat:@"%d",num]    forKey:@"totalAmount"];
    [dict setObject:self.tipString           forKey:@"TIP"];
    [dict setObject:@""                      forKey:@"serviceType"];
    [dict setObject:self.priceString                 forKey:@"serviceAmount"];
    [dict setObject:strToken                 forKey:@"transactionId"];
    
    [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:@"updatepayment"];
    
}


-(void)chargeService:(NSString*)strToken
{
    [Alert svProgress:@"Please Wait..."];
    
    NSDictionary * dictLogin  = [[NSUserDefaults standardUserDefaults]  objectForKey:kLOGIN];
    
    NSString * user_id ;
    if ([dictLogin valueForKey:@"id"] == nil)
    {
        user_id  = [dictLogin valueForKey:@"userId"];
        
    }
    else
    {
        user_id  = [dictLogin valueForKey:@"id"];
    }
    
    float num  = [self.priceString floatValue];
    
    WebService * connection = [[WebService alloc]init];
    connection.delegate = self;
    
    NSMutableDictionary * dict       = [NSMutableDictionary dictionary];
    [dict setObject:@"chargeramount"                    forKey:kAPI_ACTION];
    [dict setObject:user_id                    forKey:kUSER_ID];
    [dict setObject: [NSString stringWithFormat:@"%.f",([self.priceString floatValue]* 100)]
             forKey:@"amount"];
    [dict setObject:strToken                 forKey:@"tokenID"];
    
    [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_STRIPE_CHARGE1 methodName:@"chargeramount"];
    
}

-(void)updateMemberService:(NSString*)strToken
{
    [Alert svProgress:@"Please Wait..."];
    
    NSDictionary * dictLogin  = [[NSUserDefaults standardUserDefaults]  objectForKey:kLOGIN];
    //  NSDictionary * dictLogin = [NSKeyedUnarchiver unarchiveObjectWithData:data1];
    
    NSString * user_id ;
    if ([dictLogin valueForKey:@"id"] == nil)
    {
        user_id  = [dictLogin valueForKey:@"userId"];
        
    }
    else
    {
        user_id  = [dictLogin valueForKey:@"id"];
    }
    
    
    WebService * connection = [[WebService alloc]init];
    connection.delegate = self;
    
    NSMutableDictionary * dict       = [NSMutableDictionary dictionary];
    [dict setObject:@"updatesubscription"                    forKey:kAPI_ACTION];
    [dict setObject:user_id                    forKey:kUSER_ID];
    [dict setObject:self.priceString         forKey:@"amount"];
    [dict setObject:strToken                 forKey:@"transactionId"];
    
    [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:@"updatesubscription"];
    
}

-(void)updatePaymentService:(NSString*)strToken
{
    [Alert svProgress:@"Please Wait..."];
    
    NSDictionary * dictLogin  = [[NSUserDefaults standardUserDefaults]  objectForKey:kLOGIN];
    //  NSDictionary * dictLogin = [NSKeyedUnarchiver unarchiveObjectWithData:data1];
    
    NSString * user_id ;
    if ([dictLogin valueForKey:@"id"] == nil)
    {
        user_id  = [dictLogin valueForKey:@"userId"];
        
    }
    else
    {
        user_id  = [dictLogin valueForKey:@"id"];
    }
    
    
    WebService * connection = [[WebService alloc]init];
    connection.delegate = self;
    
    NSMutableDictionary * dict       = [NSMutableDictionary dictionary];
    [dict setObject:@"updatepayment"                    forKey:kAPI_ACTION];
    [dict setObject:user_id                    forKey:kUSER_ID];
    [dict setObject:txtCardNo.text                    forKey:@"cardNo"];
    [dict setObject:self.groupId                    forKey:@"groupId"];
    [dict setObject:self.priceString         forKey:@"totalAmount"];
    [dict setObject:self.invoiceId                forKey:@"invoiceId"];
    [dict setObject:@"0"                forKey:@"TIP"];
    [dict setObject:strToken                 forKey:@"transactionId"];
    
    [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:@"updatepayment"];
    
}

-(void)updateDriverService:(NSString*)strToken
{
    [Alert svProgress:@"Please Wait..."];
    
    NSDictionary * dictLogin  = [[NSUserDefaults standardUserDefaults]  objectForKey:kLOGIN];
    //  NSDictionary * dictLogin = [NSKeyedUnarchiver unarchiveObjectWithData:data1];
    
    NSString * user_id ;
    if ([dictLogin valueForKey:@"id"] == nil)
    {
        user_id  = [dictLogin valueForKey:@"userId"];
        
    }
    else
    {
        user_id  = [dictLogin valueForKey:@"id"];
    }
    
    
    WebService * connection = [[WebService alloc]init];
    connection.delegate = self;
    
    NSMutableDictionary * dict       = [NSMutableDictionary dictionary];
    [dict setObject:@"carupdatepayment"                    forKey:kAPI_ACTION];
    [dict setObject:user_id                    forKey:kUSER_ID];
//    [dict setObject:txtCardNo.text                    forKey:@"cardNo"];
    [dict setObject:self.groupId                    forKey:@"bookingId"];
    [dict setObject:self.priceString         forKey:@"totalAmount"];
    [dict setObject:@""              forKey:@"paymentMethod"];
//    [dict setObject:@"0"                forKey:@"TIP"];
    [dict setObject:strToken                 forKey:@"transactionId"];
    
    [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:@"carupdatepayment"];
    
}
-(void)chargeAmount :(NSString*)strToken
{
    NSArray *listItems = [txtamount.text componentsSeparatedByString:@"$"];
    NSString *str=[listItems objectAtIndex:1];
    
    NSArray *list = [str componentsSeparatedByString:@"."];
    
    [Alert svProgress:@"Please wait...."];
    
    WebService * connection = [[WebService alloc]init];
    connection.delegate = self;
    NSDictionary * dictLogin  = [[NSUserDefaults standardUserDefaults]  objectForKey:kLOGIN];
    //  NSDictionary * dictLogin = [NSKeyedUnarchiver unarchiveObjectWithData:data1];
    
    
    NSString * user_id ;
    if ([dictLogin valueForKey:@"id"] == nil)
    {
        user_id  = [dictLogin valueForKey:@"userId"];
        
    }
    else
    {
        user_id  = [dictLogin valueForKey:@"id"];
    }
    
    NSMutableDictionary * dict = [NSMutableDictionary dictionary];
    [dict setObject:[list objectAtIndex:0] forKey:@"amount"];
    [dict setObject:strToken forKey:@"transactionId"];
    [dict setObject:user_id forKey:@"userId"];
    [dict setObject:@"addmoney" forKey:kAPI_ACTION];
    
    [connection webServicePostString:[Alert jsonStringWithDictionary:dict] urlString:kURL_BASE methodName:@"addmoney"];
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1009) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            DISMISS_VIEW_CONTROLLER;
        });
    }
    else
    {
        if (buttonIndex == [alertView cancelButtonIndex]){
            //cancel clicked ...do your action
            [SVProgressHUD dismiss];
        }
        else{
            
            [self StripeCharg];
        }
    }
}


#pragma mark - Sevice delgate methods

-(void)getDataFormWebService:(NSDictionary *)jsonResults urlStr:(NSString *)yourUR methodName:(NSString *)methodName{
    
    [SVProgressHUD dismiss];
    
    SWITCH (methodName) {
        CASE (@"chargeramount")
        {
            
            if ([self.matchString isEqualToString:@"Services"])
            {
                [self updatePaymentService:[jsonResults objectForKey:@"transactionID"]];
                
            }
            if ([self.matchString isEqualToString:@"Driver"])
            {
                [self updatePaymentService:[jsonResults objectForKey:@"transactionID"]];
                
            }
            else
            {
                [self updateMemberService:[jsonResults objectForKey:@"transactionID"]];
                
            }
            
            
        }
        CASE (@"updatesubscription")
        {
            
            BankAccountViewController * bookingHistoryViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"BankAccountViewController");
            UINavigationController * navigationController = [Alert navigationControllerWithVC:bookingHistoryViewController];
            PERSENT_VIEW_CONTOLLER(navigationController, YES);
        }
        CASE (@"carupdatepayment")
        {
            
            appDelegate = UIAppDelegate;
            DashboardViewController * homePassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBID_ARRIVED_VC);
            UIAppDelegate.isCall  = YES;
            MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
            [appDelegate manageDrawerRear:menuViewController front:homePassengerViewController];
       
        }
        CASE (@"updatepayment")
        {
            if ([self.matchString isEqualToString:@"Tip"])
            {
                
                appDelegate = UIAppDelegate;
                DashboardViewController * homePassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBID_ARRIVED_VC);
                UIAppDelegate.isCall  = YES;
                MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
                [appDelegate manageDrawerRear:menuViewController front:homePassengerViewController];
                
            }
            else
            {
                
                appDelegate = UIAppDelegate;
                DashboardViewController * homePassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBID_ARRIVED_VC);
                UIAppDelegate.isCall  = YES;
                MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
                [appDelegate manageDrawerRear:menuViewController front:homePassengerViewController];
                
                //               CompleteMemberViewController * settingsViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"CompleteMemberViewController");
                //                           settingsViewController.dict = _dict;
                //                           UINavigationController * navigationController = [Alert navigationControllerWithVC:settingsViewController];
                //                           PERSENT_VIEW_CONTOLLER(navigationController, YES);
                
                
                //                BookingCompletedViewController * booking= GET_VIEW_CONTROLLER_STORYBOARD(@"BookingCompletedViewController");
                //                booking.dateString = self.dateString;
                //                booking.groupid = self.groupId;
                //                booking.timeString = self.timeString;
                //                booking.dict = self.dict;
                //                booking.servicename = self.servicename;
                //                booking.totalAmount = [NSString stringWithFormat:@"%@",self.priceString];
                //                UINavigationController * navigationController = [Alert navigationControllerWithVC:booking];
                //                PERSENT_VIEW_CONTOLLER(navigationController, YES);
                
            }
        }
        
        
        break;
        
        DEFAULT
        {
            break;
        }
    }
    
}
-(void) webServiceFailWithApplicationServerMSG:(NSString *)msg
{
    
    [Alert svError:msg];
    
}

// WEB SERVICE FAIL WITH REGION TCP/IP
-(void) webServiceFail:(NSError *)error{
    
    
    [Alert svError:[error localizedDescription]];
    
    
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
