//
//  SendRequestViewController.h
//  HYP
//
//  Created by VA pvt ltd on 14/09/21.
//  Copyright © 2021 Pragati Porwal. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface SendRequestViewController : UIViewController
{
    NSData *imageData;
}


@property (nonatomic, strong) UIPopoverController *popOver;
@property (nonatomic, strong) IBOutlet UIImageView * vehicleImageView;
@property (nonatomic, strong) IBOutlet UIImageView * invoiceImageView;
@property (nonatomic, strong) IBOutlet UILabel * DeliveryTypeLabel;
@property (nonatomic, strong) IBOutlet UILabel * vehicleNameLabel;
@property (nonatomic, strong) IBOutlet UILabel * welcomUserLabel;
@property (nonatomic, strong) IBOutlet UIButton * uploadBtn;
@property (nonatomic, strong) IBOutlet UIButton * pickupLocation;
@property (nonatomic, strong) IBOutlet UIButton * dropLocation;
@property (nonatomic, strong) IBOutlet UITextField * invoiceTextfield;

@property(nonatomic,weak)  NSString * deleviryType;
@property(nonatomic,weak)  NSString * vehicleId;
@property(nonatomic,weak)  NSString * vehicleName;
@property(nonatomic,weak)  NSString * vehicleImagee;
@property(nonatomic,weak)  NSString * locationType;

@property(nonatomic,strong)  NSString * pickupLat;
@property(nonatomic,strong)  NSString * pickupLong;

@property(nonatomic,strong) NSString *droplat;
@property(nonatomic,strong) NSString *droplongi;
@end


