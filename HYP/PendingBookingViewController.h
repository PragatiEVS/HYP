//
//  PendingBookingViewController.h
//  CHORETHING
//
//  Created by santosh kumar singh on 07/04/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface PendingBookingViewController : UIViewController
{
     NSMutableArray *resultArray;
     int startidx;
     NSArray *arr;
}

@property (weak, nonatomic) IBOutlet UILabel * noListAvalible;
@property(weak, nonatomic) IBOutlet UITableView *completedTableView;
@end


