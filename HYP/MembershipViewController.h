//
//  MembershipViewController.h
//  HYP
//
//  Created by santosh kumar singh on 23/09/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface MembershipViewController : UIViewController
{
     NSMutableString *serviceTypeArr;
}

@property (weak, nonatomic) IBOutlet UITextView * textViewTerm;
@property (weak, nonatomic) IBOutlet UIButton   * buttonCheck;
@property (weak, nonatomic) IBOutlet UIButton   * buttonTerm;

@end


