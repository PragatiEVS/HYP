//
//  BookingCompletedViewController.m
//  Q-Workerbee
//
//  Created by santosh kumar singh on 04/07/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import "BookingCompletedViewController.h"
#import "MenuViewController.h"
#import "AppDelegate.h"
#import "DashboardViewController.h"
#import "MenuViewController.h"
#import "UserDetailViewController.h"


@interface BookingCompletedViewController ()<WebServiceDelegate,CLLocationManagerDelegate>
{
    
    AppDelegate * appDelegate;
    
}

@end

@implementation BookingCompletedViewController

-(CLLocationCoordinate2D) getLocationFromAddressString: (NSString*) addressStr
{
    double latitude = 0, longitude = 0;
    
    CLLocationCoordinate2D center;
    center.latitude=latitude;
    center.longitude = longitude;
    NSLog(@"View Controller get Location Logitute : %f",center.latitude);
    NSLog(@"View Controller get Location Latitute : %f",center.longitude);
    return center;
    
}

-(IBAction)profileBtn:(id)sender
{
    
    UserDetailViewController *forgotPasswordViewController =  GET_VIEW_CONTROLLER(@"UserDetailViewController");
    
    if (self.dict[@"vendorID"])
    {
        forgotPasswordViewController.vendorId=  [self.dict valueForKey:@"vendorID"];
    }
    else if (self.dict[@"vendorId"])
    {
        forgotPasswordViewController.vendorId=  [self.dict valueForKey:@"vendorId"];
    }
    
    else
    {
        forgotPasswordViewController.vendorId=  [self.dict valueForKey:@"userId"];
        
    }
    UINavigationController * navigationController = [Alert navigationControllerWithVC:forgotPasswordViewController];
    PERSENT_VIEW_CONTOLLER(navigationController, YES);
    
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    
    
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder geocodeAddressString:[_dict valueForKey:@"address"]
                 completionHandler:^(NSArray* placemarks, NSError* error){
        if (placemarks && placemarks.count > 0) {
            CLPlacemark *topResult = [placemarks objectAtIndex:0];
            MKPlacemark *placemark = [[MKPlacemark alloc] initWithPlacemark:topResult];
            
            self->latSearch =  placemark.coordinate.latitude;
            self->lngSearch = placemark.coordinate.longitude;
            
            self.geocoder = [[GMSGeocoder alloc]init];
            
            [self showGoogleMap];
            
            [self showRoute];
            
        }
    }
     ];
    
    CLLocationCoordinate2D coordinate = [self getLocation];
    
    latitude = coordinate.latitude;
    longitude = coordinate.longitude;
    
    
    self.bokkingDateLabel.layer.borderColor = [UIColor blackColor].CGColor;
    self.bokkingDateLabel.layer.borderWidth = 2.0;
    self.bokkingDateLabel.layer.cornerRadius = 5.0;
    self.bokkingDateLabel.clipsToBounds = YES;
    [self.bokkingDateLabel setText:[NSString stringWithFormat:@" %@ | %@",[self.dict valueForKey:@"bookingDate"],[self.dict valueForKey:@"slote"]]];
    
    self.detailView.layer.cornerRadius = 10;
    self.detailView.clipsToBounds=YES;
    
    self.title = @"TRACK YOUR ORDER";
    
    [self.userImage setImageURL:[NSURL URLWithString:[self.dict valueForKey:@"vendorImage"]]];
    self.userImage.layer.cornerRadius = 35;
    self.userImage.clipsToBounds=YES;
    
    self.rateview.starSize = 15;
    
    self.rateview.starFillColor = [Alert colorFromHexString:@"#F4CC47"];
    self.rateview.starNormalColor = [UIColor grayColor];
    
    
    if (!self.dict[@"vendorName"])
    {
        self.username.text= [self.dict valueForKey:@"vendorFullName"];
    }
    else
    {
        self.username.text= [self.dict valueForKey:@"vendorName"];
    }
    
    if (!self.dict[@"image"])
    {
        if ([[self.dict valueForKey:@"vendorImage"] isEqualToString:@""])
        {
            NSString *userName = [self.dict valueForKey:@"vendorFullName"];
            
            [self.userImage setImageWithString:userName color:nil circular:YES];
            
        }
        else
        {
            [self.userImage setImageURL:[NSURL URLWithString:[self.dict valueForKey:@"vendorImage"]]];
        }
    }
    else
    {
        if ([[self.dict valueForKey:@"image"] isEqualToString:@""])
        {
            NSString *userName = [self.dict valueForKey:@"vendorName"];
            
            [self.userImage setImageWithString:userName color:nil circular:YES];
            
        }
        else
        {
            [self.userImage setImageURL:[NSURL URLWithString:[self.dict valueForKey:@"image"]]];
        }
    }
    self.rateview.rating= [[self.dict valueForKey:@"AVGRating"]floatValue];
    
    
    self.serviceAddressLabel.numberOfLines = 0;
    self.currentAddressLabel.numberOfLines = 0;
    self.serviceAddressLabel.text = [self.dict valueForKey:@"address"];
    
    NSInteger borderThickness = 1;
    UIView *leftBorder = [UIView new];
    leftBorder.backgroundColor = [UIColor blackColor];
    leftBorder.frame = CGRectMake(0, 0, borderThickness, self.cancelBtn.frame.size.height);
    [self.cancelBtn addSubview:leftBorder];
    
    
    
    
    self.serviceNameLabel.text = [NSString stringWithFormat:@"Service Name: %@",self.servicename];
    
    self.toatlAmountLabel.text = [NSString stringWithFormat:@"Service Charge: $%@",self.totalAmount];
    [super viewDidLoad];
    
    
    UIView *topBorder = [UIView new];
    topBorder.backgroundColor = [UIColor blackColor];
    topBorder.frame = CGRectMake(0, 0, self.userDetailView.frame.size.width, borderThickness);
    [self.userDetailView addSubview:topBorder];
    
    UIView *bottomBorder = [UIView new];
    bottomBorder.backgroundColor = [UIColor blackColor];
    bottomBorder.frame = CGRectMake(0, self.userDetailView.frame.size.height - borderThickness, self.userDetailView.frame.size.width, borderThickness);
    [self.userDetailView addSubview:bottomBorder];
    
    
    [self navigationBarConfiguration];
    
    // Do any additional setup after loading the view.
}

-(void)showRoute
{
    
    CLLocation * locPicOff       = [[CLLocation alloc]initWithLatitude:latitude longitude:longitude];
    
    CLLocation * locDrop       = [[CLLocation alloc]initWithLatitude:latSearch longitude:lngSearch];
    
    
    GMSMarker *picoff = [[GMSMarker alloc]init];
    picoff.position   = CLLocationCoordinate2DMake(latitude, longitude);
    picoff.icon = [UIImage imageNamed:kIMG_DOT_CIRCLE_MARKER];
    picoff.map = mapView_;
    
    GMSMarker *dropoff = [[GMSMarker alloc]init];
    dropoff.position = CLLocationCoordinate2DMake(latSearch, lngSearch);
    dropoff.icon = [UIImage imageNamed:kIMG_USER_DESTI_MARKER];
    dropoff.map = mapView_;
    
    dispatch_async(dispatch_get_main_queue(),^{
        
        [self drawRouteOrigin:locPicOff destination:locDrop];
        
    });
}


- (void)drawRouteOrigin:(CLLocation *)myOrigin destination:(CLLocation *)myDestination
{
    [self fetchPolylineWithOrigin:myOrigin destination:myDestination completionHandler:^(GMSPolyline *polyline)
     {
        
        if(polyline)
        {
            GMSStrokeStyle *greenToRed = [GMSStrokeStyle solidColor:kCOLOR_BLACK];
            polyline.strokeWidth = 5.0;
            polyline.spans = @[[GMSStyleSpan spanWithStyle:greenToRed]];
            polyline.map = mapView_;
        }
        
        
    }];
}

- (void)fetchPolylineWithOrigin:(CLLocation *)origin destination:(CLLocation *)destination completionHandler:(void (^)(GMSPolyline *))completionHandler
{
    dispatch_async(dispatch_get_main_queue(), ^{ // Second main
        
        NSString *originString = [NSString stringWithFormat:@"%f,%f", origin.coordinate.latitude, origin.coordinate.longitude];
        NSString *destinationString = [NSString stringWithFormat:@"%f,%f", destination.coordinate.latitude, destination.coordinate.longitude];
        NSString *directionsAPI = @"https://maps.googleapis.com/maps/api/directions/json?";
        NSString *directionsUrlString = [NSString stringWithFormat:@"%@&origin=%@&destination=%@&mode=driving&key=%@", directionsAPI, originString, destinationString,kKEY_GOOGLE_MAP];
        NSURL *directionsUrl = [NSURL URLWithString:directionsUrlString];
        NSURLSessionDataTask *fetchDirectionsTask = [[NSURLSession sharedSession] dataTaskWithURL:directionsUrl completionHandler:
                                                     ^(NSData *data, NSURLResponse *response, NSError *error)
                                                     {
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            if(error)
            {
                if(completionHandler)
                    completionHandler(nil);
                return;
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                //Your code...
                NSArray *routesArray = [json objectForKey:@"routes"];
                GMSPolyline *polyline = nil;
                if ([routesArray count] > 0)
                {
                    NSDictionary *routeDict = [routesArray objectAtIndex:0];
                    NSDictionary *routeOverviewPolyline = [routeDict objectForKey:@"overview_polyline"];
                    NSString *points = [routeOverviewPolyline objectForKey:@"points"];
                    GMSPath *path = [GMSPath pathFromEncodedPath:points];
                    polyline = [GMSPolyline polylineWithPath:path];
                    polyline.strokeWidth = 3.f;
                    polyline.strokeColor = [UIColor blueColor];
                    polyline.map = self->mapView_;
                    
                }
            });
        }];
        [fetchDirectionsTask resume];
    });
}

-(CLLocationCoordinate2D) getLocation
{
    CLLocationManager *locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager startUpdatingLocation];
    CLLocation *location = [locationManager location];
    CLLocationCoordinate2D coordinate = [location coordinate];
    
    return coordinate;
}

#pragma mark --- : GOOGLE MAP :----
-(void)showGoogleMap
{
    
    CLLocation * upadetLocation = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
    
    CGRect frame = CGRectMake(0, 0, self.mapView.frame.size.width,self.mapView.frame.size.height);
    camera = [GMSCameraPosition cameraWithLatitude:latitude longitude:longitude zoom:15];
    mapView_ = [GMSMapView mapWithFrame:frame camera:camera];
    mapView_.delegate = self;
    
    [mapView_ setCamera:camera];
    
    [self reverGeoCodingUsingGoogle:upadetLocation];
    
    self->mapView_.myLocationEnabled              = YES;
    self->mapView_.indoorEnabled                  = YES;
    self->mapView_.settings.myLocationButton      = NO;
    self->mapView_.settings.scrollGestures        = YES;
    
    [_mapView addSubview:mapView_];
    
    locationMarker_ = [[GMSMarker alloc] init];
    locationMarker_.position = CLLocationCoordinate2DMake(latitude, longitude);
    locationMarker_.map = mapView_;
    
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    
    [self.locationManager stopUpdatingLocation];
    //  CLLocation *mostRecentLocation = [locations objectAtIndex:0];
    
    
}

-(void)reverGeoCodingUsingGoogle:(CLLocation *)location
{
    [[GMSGeocoder geocoder] reverseGeocodeCoordinate:CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude) completionHandler:^(GMSReverseGeocodeResponse* response, NSError* error)
     {
        
        GMSReverseGeocodeResult *result = response.firstResult;
        
        NSArray   * arrayAdd = result.lines;
        
        NSString  * address1;
        NSString  * address2;
        
        if (arrayAdd.count>1)
        {
            address1 = result.lines[0];
            address2 = result.subLocality;
        }
        else
        {
            address1 = result.lines[0];
        }
        
        self.currentAddressLabel.text = address1;
    }];
}


-(void)back:(id)sender
{
    DISMISS_VIEW_CONTROLLER;
}


#pragma mark - Configure Navigation Controller

-(void)navigationBarConfiguration
{
    
    self.navigationController.navigationBarHidden = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    UIButton * menuButton  = [UIButton buttonWithType:UIButtonTypeSystem];
    menuButton.frame = CGRectMake(8, 18, 24, 24);
    [menuButton setBackgroundImage:[UIImage imageNamed:IMG_BACK] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *accountBarItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    self.navigationItem.leftBarButtonItem = accountBarItem;
    
    NSMutableArray *arrRightBarItems = [[NSMutableArray alloc] init];
    UIButton *btnSetting = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnSetting setImage:[UIImage imageNamed:@"Frequency"] forState:UIControlStateNormal];
    btnSetting.frame = CGRectMake(0, 0, 40, 40);
    btnSetting.showsTouchWhenHighlighted=YES;
    [btnSetting addTarget:self action:@selector(onSettings:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSetting];
    
    UIButton *btnLib = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnLib setImage:[UIImage imageNamed:@"logo1.png"] forState:UIControlStateNormal];
    btnLib.frame = CGRectMake(0, 0, 40, 40);
    btnLib.showsTouchWhenHighlighted=YES;
    [btnLib addTarget:self action:@selector(onMyLibrary:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButtonItem2 = [[UIBarButtonItem alloc] initWithCustomView:btnLib];
    [arrRightBarItems addObject:barButtonItem2];
    [arrRightBarItems addObject:barButtonItem];
    self.navigationItem.rightBarButtonItems=arrRightBarItems;
    
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor],
    }];
    
}
-(void)onMyLibrary:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}
-(void)onSettings:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/hyptunesradio";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}

#pragma mark - Sevice delgate methods

-(void)getDataFormWebService:(NSDictionary *)jsonResults urlStr:(NSString *)yourUR methodName:(NSString *)methodName{
    
    [SVProgressHUD dismiss];
    
    SWITCH (methodName) {
        CASE (@"canclerequest")
        {
            NSLog(@"jsonResults =%@",jsonResults);
            appDelegate = UIAppDelegate;
            DashboardViewController * homePassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBID_ARRIVED_VC);
            UIAppDelegate.isCall  = YES;
            MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
            [appDelegate manageDrawerRear:menuViewController front:homePassengerViewController];
        }
        
        
        break;
        
        DEFAULT
        {
            break;
        }
    }
    
}
// WEB SERVER ERROR MSG LOCAL SERVER (APPLICATION SERVER)
-(void) webServiceFailWithApplicationServerMSG:(NSString *)msg
{
    
    [Alert svError:msg];
    
}

// WEB SERVICE FAIL WITH REGION TCP/IP
-(void) webServiceFail:(NSError *)error{
    
    
    [Alert svError:[error localizedDescription]];
    
}

-(IBAction)cancelBtn:(id)sender
{
    
    [Alert svProgress:@"Please Wait..."];
    NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults]  objectForKey:kLOGIN];
    
    
    WebService * connection = [[WebService alloc]init];
    connection.delegate = self;
    NSMutableDictionary * dict                              = [NSMutableDictionary dictionary];
    
    [dict setObject:@"canclerequest"                     forKey:kAPI_ACTION];
    if (self.dict[@"bookingId"])
    {
            
       [dict setObject:[self.dict valueForKey:@"bookingId"]                     forKey:@"groupId"];
                 
    }
    else
    {
        [dict setObject:[self.dict valueForKey:@"bookingID"]                     forKey:@"groupId"];
        
    }
    
    [dict setObject:[dictLogin valueForKey:@"userId"]                   forKey:kUSER_ID];
    
    NSLog(@"dict is ---%@",dict);
    [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:@"canclerequest"];
    
}

-(IBAction)callBtn:(id)sender
{
    if (TARGET_OS_SIMULATOR)
    {
        [Alert alertControllerTitle:kAPPICATION_TITLE msg:kDEVICE_NOT_SUPPORT ok:kOK controller:self.navigationController];
    }
    else
    {
        if (!self.dict[@"vendorPhone"])
        {
            [Alert makePhoneCall:[self.dict valueForKey:@"contactNumber"]];
        }
        else
        {
            [Alert makePhoneCall:[self.dict valueForKey:@"vendorPhone"]];
        }
      
    }
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
