//
//  ManageServicesViewController.m
//  Q-Workerbee
//
//  Created by santosh kumar singh on 17/06/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import "ManageServicesViewController.h"
#import "ManageTableViewCell.h"
#import "ChoreDetailViewController.h"


@interface ManageServicesViewController ()<WebServiceDelegate,UITableViewDelegate,UITableViewDataSource,UIPickerViewDelegate,UIPickerViewDataSource,UITextViewDelegate>

@end

@implementation ManageServicesViewController

- (void)viewDidLoad
{
    self.removeService.layer.cornerRadius = 8;
    self.addService.layer.cornerRadius = 8;
    
    self.totalItemLabel.text= [NSString stringWithFormat:@"Total Item: %ld", self.PriceMutable.count];
    
    self.totalPriceLabel.text= [NSString stringWithFormat:@"Total Amount: %d", sum];
    
    
    self.title= @"CREATE INVOICE";
    self.view.backgroundColor = [Alert colorWithPatternImage2:kBG_IMAGE];
    self.PriceMutable = [[NSMutableArray alloc]init];
    self.PriceDicionary = [[NSMutableDictionary alloc]init];
    [self navigationBarConfiguration];
    [self serviceCategoryList];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
#pragma mark - Configure Navigation Controller

-(void)navigationBarConfiguration
{
    
    self.navigationController.navigationBarHidden = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    UIButton * menuButton  = [UIButton buttonWithType:UIButtonTypeSystem];
    menuButton.frame = CGRectMake(8, 18, 24, 24);
    [menuButton setBackgroundImage:[UIImage imageNamed:IMG_BACK] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *accountBarItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    self.navigationItem.leftBarButtonItem = accountBarItem;
    
    NSMutableArray *arrRightBarItems = [[NSMutableArray alloc] init];
    UIButton *btnSetting = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnSetting setImage:[UIImage imageNamed:@"Frequency"] forState:UIControlStateNormal];
    btnSetting.frame = CGRectMake(0, 0, 40, 40);
    btnSetting.showsTouchWhenHighlighted=YES;
    [btnSetting addTarget:self action:@selector(onSettings:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSetting];
    
    UIButton *btnLib = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnLib setImage:[UIImage imageNamed:@"logo1.png"] forState:UIControlStateNormal];
    btnLib.frame = CGRectMake(0, 0, 40, 40);
    btnLib.showsTouchWhenHighlighted=YES;
     [btnLib addTarget:self action:@selector(onMyLibrary:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButtonItem2 = [[UIBarButtonItem alloc] initWithCustomView:btnLib];
    [arrRightBarItems addObject:barButtonItem2];
    [arrRightBarItems addObject:barButtonItem];
    self.navigationItem.rightBarButtonItems=arrRightBarItems;
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName :[UIColor whiteColor]}];
    
}

-(void)onMyLibrary:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}

-(void)onSettings:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/hyptunesradio";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}

-(void)back:(id)sender
{
    DISMISS_VIEW_CONTROLLER;
}

-(void)serviceCategoryList{
    
    // SERVER CONNCETION
    [Alert svProgress:@"Please Wait..."];
    
    WebService * connection = [[WebService alloc]init];
    connection.delegate = self;
    NSMutableDictionary * dict                              = [NSMutableDictionary dictionary];
    
    [dict setObject:kAPI_METHOD_CATERGORY                     forKey:kAPI_ACTION];
    
    NSLog(@"dict is ---%@",dict);
    [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:kAPI_METHOD_CATERGORY];
    
}
#pragma mark - Sevice delgate methods

-(void)getDataFormWebService:(NSDictionary *)jsonResults urlStr:(NSString *)yourUR methodName:(NSString *)methodName{
    
    [SVProgressHUD dismiss];
    
    SWITCH (methodName) {
        
        CASE (@"sendinvoice")
        {
            
           // [Alert svSuccess:@"Payment Link sent to Customer. Stand By for Payment Confirmation..."];
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@"Payment Link sent to Customer. Stand By for Payment Confirmation..."
                                         message:nil
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* yesButton = [UIAlertAction
                                        actionWithTitle:@"Ok"
                                        style:UIAlertActionStyleDestructive
                                        handler:^(UIAlertAction * action)
                                        {
                [self yesButton];
            }];
            
            
            [alert addAction:yesButton];
            
            [self presentViewController:alert animated:YES completion:nil];
            
            
            
        }
        CASE (kAPI_METHOD_CATERGORY)
        {
            NSMutableArray *dict1 = [[NSMutableArray alloc]init];
            
            resultArray = [[NSMutableArray alloc]init];
            
            dict1 = [jsonResults valueForKey:@"data"];
            
            for(int i=0; i<=dict1.count-1; i++)
            {
                NSMutableArray *dictnew = [[NSMutableArray alloc]init];
                dictnew = [[dict1 objectAtIndex:i] valueForKey:@"SubCat"];
                
                for(int j=0; j<=dictnew.count-1; j++)
                {
                    [resultArray addObject:[dictnew objectAtIndex:j]];
                }
                
            }
            
            
            tblView.delegate= self;
            tblView.dataSource= self;
            [tblView reloadData];
            
            
        }
        break;
        DEFAULT
        {
            break;
        }
    }
    
}

-(void)yesButton
{
    
    ChoreDetailViewController * settingsViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"ChoreDetailViewController");
        if (self.dict[@"bookingId"])
           {
           [AppDelegate appDelegate].bookingId = self.dict;
           }
           else
           {
               [AppDelegate appDelegate].bookingId =self.dict;
               
           }
    settingsViewController.dict = [AppDelegate appDelegate].bookingId;
    
    UINavigationController * navigationController = [Alert navigationControllerWithVC:settingsViewController];
    PERSENT_VIEW_CONTOLLER(navigationController, YES);
    
    
    
}

-(void) webServiceFailWithApplicationServerMSG:(NSString *)msg
{
    
    [Alert svError:msg];
    
}

// WEB SERVICE FAIL WITH REGION TCP/IP
-(void) webServiceFail:(NSError *)error{
    
    
    [Alert svError:[error localizedDescription]];
    
    
}


-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
    ManageTableViewCell *cell = (ManageTableViewCell *)[tblView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:self.selectedIndex inSection:0]];
    
    cell.serviceLabel.text = [[resultArray valueForKey:@"name"]objectAtIndex:row];
    
    NSString *price = [[[resultArray valueForKey:@"price"]objectAtIndex:row] stringValue];
    
    cell.pricetextField.text= price;
    
    [self.PriceDicionary setValue:cell.serviceLabel.text forKey:@"itemName"];
    [self.PriceDicionary setValue:cell.pricetextField.text forKey:@"price"];
    [self.PriceDicionary setValue:cell.disctextView.text forKey:@"description"];
    
    sum = [cell.pricetextField.text intValue];
    if (self.PriceMutable.count>0)
    {
        
        for (int i=0; i<=self.PriceMutable.count-1; i++)
        {
            int value=[[[self.PriceMutable objectAtIndex:i]valueForKey:@"price"] intValue];
            sum=sum+value;
        }
        
    }
    
    self.totalItemLabel.text= [NSString stringWithFormat:@"Total Item: %ld", self.PriceMutable.count+1];
    
    self.totalPriceLabel.text= [NSString stringWithFormat:@"Total Amount: %d", sum];
    
    
}

#pragma mark - UIPickerViewDataSource
-(IBAction)done:(id)sender
{
    [self.dienstPicker setHidden:YES];
    [self.donePicker setHidden:YES];
}


#pragma mark - UIPickerViewDataSource


-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// #4
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return  resultArray.count;
    
}

#pragma mark - UIPickerViewDelegate

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    
    return  [[resultArray valueForKey:@"name"]objectAtIndex:row];
    
}

- (void)showPickerView:(UIButton*)sender
{
    self.selectedIndex = sender.tag;
    
    [self.dienstPicker setHidden:NO];
    [self.donePicker setHidden:NO];
    
    self.dienstPicker.delegate = self;
    self.dienstPicker.dataSource = self;
    
}

#pragma mark - UITextViewDelegate

- (BOOL)textViewShouldReturn:(UITextView *)textField {
    [textField resignFirstResponder];
    return YES;
}

-(void)textViewDidBeginEditing:(UITextView *)textField {
    
    if ([textField.text isEqualToString:@"Sorunun cevabını buraya yazınız!"]) {
        textField.text = @"";
    }
}


- (void)textViewDidEndEditing:(UITextView *)textField {
    
    [self.PriceDicionary setValue:textField.text forKey:@"description"];
    
}

#pragma mark - TableView methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (self.PriceMutable.count == 0)
    {
        return 1;
    }
    else
    {
        return self.PriceMutable.count+1;
    }
    
}
- (UITableViewCell *)tableView:(UITableView *)table cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ManageTableViewCell *cell = [table dequeueReusableCellWithIdentifier:@"ManageTableViewCell"];
    if (cell ==nil)
    {
        [tblView registerClass:[ManageTableViewCell class] forCellReuseIdentifier:@"ManageTableViewCell"];
        
        cell = [tblView dequeueReusableCellWithIdentifier:@"ManageTableViewCell"];
    }
    
    
    cell.serviceLabel.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 30);
    cell.serviceLabel.layer.cornerRadius = 5.0f;
    cell.serviceLabel.clipsToBounds = YES;
    //cell.serviceLabel.text = [[resultArray valueForKey:@"name"]objectAtIndex:indexPath.row];
    
    cell.disctextView.layer.cornerRadius = 5.0f;
    cell.disctextView.clipsToBounds = YES;
    cell.disctextView.delegate = self;
    
    cell.pricetextField.layer.cornerRadius = 5.0f;
    cell.pricetextField.clipsToBounds = YES;
    
    cell.serviceBtn.tag = indexPath.row;
    [cell.serviceBtn addTarget:self action:@selector(showPickerView:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
    
}

- (IBAction)removeMore:(UIButton*)btn
{
    [self.PriceMutable removeLastObject];
    tblView.delegate= self;
    tblView.dataSource= self;
    [tblView reloadData];
    
}

- (IBAction)addMore:(UIButton*)btn
{
    
    if (!self.PriceDicionary[@"itemName"])
    {
        [Alert svError:@"Please select service first"];
    }
    else if (!self.PriceDicionary[@"description"])
    {
        [Alert svError:@"Please enter service Price"];
    }
    else
    {
        [self.PriceMutable addObject:self.PriceDicionary];
        self.PriceDicionary= [[NSMutableDictionary alloc] init];
        
        tblView.delegate= self;
        tblView.dataSource= self;
        [tblView reloadData];
        
    }
    
}

- (IBAction)submitInvoice:(UIButton*)btn
{
    if (self.PriceDicionary.count ==0)
    {
        [Alert svError:@"Please add service first"];
    }
    else
    {
        [self.PriceMutable addObject:self.PriceDicionary];
        NSData *jsonData1 = [NSJSONSerialization dataWithJSONObject:self.PriceMutable options:0 error:nil];
        self.treetrimmingString = [[NSString alloc] initWithData:jsonData1 encoding:NSUTF8StringEncoding];
        
        self.treetrimmingString = [NSString stringWithFormat:@"%s",[self.treetrimmingString UTF8String]];
        
        self.treetrimmingString =[self.treetrimmingString stringByReplacingOccurrencesOfString:@"/\\/g" withString:@""] ;
        
        
        NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults]  objectForKey:kLOGIN];
        
        
        NSString * user_id ;
        if ([dictLogin valueForKey:@"id"] == nil)
        {
            user_id  = [dictLogin valueForKey:@"userId"];
            
        }
        else
        {
            user_id  = [dictLogin valueForKey:@"id"];
        }
        [Alert svProgress:@"Please Wait..."];
        
        WebService * connection = [[WebService alloc]init];
        connection.delegate = self;
        NSMutableDictionary * dict                              = [NSMutableDictionary dictionary];
        [dict setObject:user_id forKey:@"sendFrom"];
        if (self.dict[@"bookingId"])
        {
        [dict setObject:[self.dict valueForKey:@"bookingId"] forKey:@"sendTo"];
        }
        else
        {
            [dict setObject:[self.dict valueForKey:@"bookingID"] forKey:@"sendTo"];
            
        }
        [dict setObject:@"sendinvoice"                     forKey:kAPI_ACTION];
        [dict setObject:[NSString stringWithFormat:@"%d", sum]                    forKey:@"totalPrice"];
        [dict setObject:self.treetrimmingString                     forKey:@"jsonCode"];
        
        
        NSLog(@"dict is ---%@",dict);
        [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:@"sendinvoice"];
    }
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
