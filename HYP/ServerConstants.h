//
//  ServerConstants.h
//  ROLUX
//
//  Created by Nitin Kumar on 06/12/16.
//  Copyright © 2016 Nitin Kumar. All rights reserved.
//

#ifndef ServerConstants_h
#define ServerConstants_h

//******************************** Base urls *********************************


static NSString *const  kURL_BASE  = @"http://107.180.107.200/hyp/site/services/index/";


static NSString *const  kURL_STRIPE_CHARGE  = @"http://35.184.135.73/~xyzchore/webroot/authriz/autopayment.php";


static NSString *const  kURL_STRIPE_CHARGE1  = @"http://107.180.107.200/hyp/site/strip_master/charge.php";


#define Server_url_GOOGLE                                   @"https://www.google.co.in/maps/dir/?"


#define Server_url_GOOGLE_DISTANCE_DURATION                 @"https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins="


#define service_urlDest @"&destinations"
#define ANDkey @"&key="




/*
 https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=Washington,DC&destinations=New+York+City,NY&key=YOUR_API_KEY

*/
//********************************** Other Urls******************************

static NSString *const kDEVICE_TOKEN                        = @"devicetoken";



static NSString *const kAPI_ACTION                          = @"action";
static NSString *const kMSG_KEY                             = @"msg";
// LOGIN 
static NSString *const kAPI_USER_EMAIL                      = @"email";
static NSString *const kAPI_USER_address                   = @"address";
static NSString *const kAPI_USER_DEVICE                     = @"device";
static NSString *const kAPI_PASSWORD                    = @"password";


static NSString *const kAPI_USER_LATITUDE                   = @"latitude";
static NSString *const kAPI_USER_LONGITUDE                  = @"logitude";


static NSString *const kAPI_PICKUP_LATITUDE_LONGITUDE       = @"pickuplatLong";
static NSString *const kAPI_contactNumber     = @"contactNumber";
static NSString *const kAPI_PICKUP_LOCATION                 = @"pickupLocation";
static NSString *const kAPI_DROPOFF_LOCATION                = @"dropLocation";
static NSString *const kLOGIN_STATUS                        = @"loginSatatus";

//lat


// RESITER 

static NSString *const kAPI_USER_FULLNAME                      = @"fullName";
static NSString *const kAPI_USER_Gender                      = @"gender";
static NSString *const kAPI_USER_OTHER                      = @"other";
static NSString *const kAPI_USER_DOB                  = @"dob";
static NSString *const kAPI_USER_STATE                  = @"state";
static NSString *const kAPI_USER_TYPE                       = @"role";


// CHANGE PASSOWRD

static NSString *const k_USER_OLDPASSWD                     = @"oldPassword";
static NSString *const k_USER_NEWPASSWD                     = @"newPassword";



// PAYMENT

static NSString *const kPAGE_NO                        = @"pageNo";
static NSString *const kPAYMENT_AMT                         = @"amount";
static NSString *const kPAYMENT_TIPS                        = @"tips";
static NSString *const kVENDOR_ID                           = @"venderId";



// Predefine Methods

static NSString *const kAPI_METHOD_LOGIN                    = @"login";
static NSString *const kAPI_METHOD_REGISTER                 = @"registration";
static NSString *const kAPI_METHOD_CATERGORY             = @"category";
static NSString *const kAPI_METHOD_EDIT_PROFILE             = @"editprofile";
static NSString *const kAPI_METHOD_CNG_PSSWD                = @"changePassword";
static NSString *const kAPI_METHOD_UPDATE_LAT_LNG           = @"updateLatLong";
static NSString *const kAPI_METHOD_TERM_COND                = @"termAndConditions";
static NSString *const kAPI_METHOD_findvendor                    = @"findvendor";
static NSString *const kAPI_METHOD_USER_PROFILE             = @"profile";
static NSString *const kAPI_METHOD_SHOPPINGID          = @"shopingId";
static NSString *const kAPI_METHOD_businesstypelist               = @"businesstypelist";
static NSString *const kAPI_METHOD_SHOPPING_LIST          = @"shopinglist";
static NSString *const kAPI_METHOD_REQUEST_ACCEPTED         = @"acceptServiceReq";
static NSString *const kAPI_METHOD_CREATELIST               = @"createshopping";

;
static NSString *const kAPI_METHOD_PRODUCTLIST                    = @"productlist";
static NSString *const kAPI_METHOD_ADDPRODUCT                 = @"addshopinglist";
static NSString *const kAPI_METHOD_MAP_DRAW_LIST            = @"drawMapdataList";

static NSString *const kAPI_METHOD_START_REQ                = @"startReqestServices";

static NSString *const kAPI_METHOD_END_REQ                  = @"aboutus";
static NSString *const kAPI_METHOD_PAYMENT                  = @"payment";

static NSString *const kAPI_METHOD_DRIVEROFF                  = @"Driveroff";

static NSString *const kAPI_METHOD_DRIVEONN                = @"Driveron";

static NSString *const kAPI_METHOD_GET_ESTIMATED_INFO                = @"getstemaitPrice";

static NSString *const kAPI_METHOD_GET_LATLONG                = @"getlatlong";

static NSString *const kAPI_METHOD_CANCEL_REQ               = @"canclerequest";

//  RESPONSE KEYWORDS CONST 

static NSString *const kUSER_ID                             = @"userId";



//********************************* Method and Key Constants*******************

#endif /* ServerConstants_h */
