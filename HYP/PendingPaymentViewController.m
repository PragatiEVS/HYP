//
//  PendingPaymentViewController.m
//  HYP
//
//  Created by VA pvt ltd on 23/09/21.
//  Copyright © 2021 Pragati Porwal. All rights reserved.
//

#import "PendingPaymentViewController.h"
#import "RateSubmitViewController.h"
#import "PaymentViewController.h"


@interface PendingPaymentViewController ()<WebServiceDelegate>

@end

@implementation PendingPaymentViewController

@synthesize detailArray;
@synthesize indextag;
#pragma mark - ViewController Life Cycle
- (void)viewDidLoad
{
    
    
    NSLog(@"detail Array is ----%@",detailArray);
    [_picLoc setBackgroundColor:[UIColor clearColor]];
    [_dropoffLoc setBackgroundColor:[UIColor clearColor]];
    
    [_picLoc setText:nil];
    [_dropoffLoc setText:nil];
    
    if (self.bookingDetail.count>0)
    {
         self.buttonCompleted.hidden = YES;
         self.buttoninvite.hidden = NO;
        _picLoc.text = [self.bookingDetail objectForKey:@"RequestPickupAddress"];
        _dropoffLoc.text = [self.bookingDetail objectForKey:@"RequestDropAddress"];
        
        
    }
    else{
        
        NSString *strPic = [[detailArray objectAtIndex:0]objectForKey:@"RequestPickupAddress"];
        
        if ([strPic length]>0)
        {
            [_locNameView setHidden:NO];
            
            _picLoc.text = [NSString stringWithFormat:@"%@",[[detailArray objectAtIndex:0]objectForKey:@"RequestPickupAddress"]];
            
            _dropoffLoc.text =[NSString stringWithFormat:@"%@",[[detailArray objectAtIndex:0]objectForKey:@"RequestDropAddress"]];
            
            
        }
        else
        {
            [_locNameView setHidden:YES];
        }
        
        [super viewDidLoad];
        
        
        
        if ([[[detailArray objectAtIndex:0]objectForKey:@"transactionId"] isEqualToString:@""]
            )
        {
           self.buttonCompleted.hidden = YES;
            self.buttoninvite.hidden = NO;
        }
        else
        {
                 self.buttonCompleted.hidden = NO;
            self.buttoninvite.hidden = NO;
        }
    }
    
    _viewPayment.layer.cornerRadius = 5;
    
    // border
    [_viewPayment.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [_viewPayment.layer setBorderWidth:1.5f];
    
    // drop shadow
    [_viewPayment.layer setShadowColor:[UIColor grayColor].CGColor];
    [_viewPayment.layer setShadowOpacity:0.8];
    [_viewPayment.layer setShadowRadius:3.0];
    [_viewPayment.layer setShadowOffset:CGSizeMake(1.0, 1.0)];
    
    
    //       self.buttoninvite.hidden = YES;
    self.buttonCompleted.layer.cornerRadius = 10;
   
    self.buttoninvite.layer.cornerRadius = 10;
    _viewDistance.layer.cornerRadius = 5;
    
    // border
    [_viewDistance.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [_viewDistance.layer setBorderWidth:1.5f];
    
    // drop shadow
    [_viewDistance.layer setShadowColor:[UIColor grayColor].CGColor];
    [_viewDistance.layer setShadowOpacity:0.8];
    [_viewDistance.layer setShadowRadius:3.0];
    [_viewDistance.layer setShadowOffset:CGSizeMake(1.0, 1.0)];
    
    
    self.title = @"INVOICES";
    
    // Do any additional setup after loading the view.
    
    // configure view controller
    
    [self configure];
    // show map
    
    // show Google Map
    
}



-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //  [_viewBottom setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.7]];
    //  [_viewTopView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.7]];
    
    //    [self starRatingToDriver:5.0];
    
}

#pragma mark - configure view controller
-(void)configure
{
    // navigation bar Configure
    
    [self navigationBarConfiguration];
    NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults]  objectForKey:kLOGIN];
    
    
    NSString * user_id ;
    if ([dictLogin valueForKey:@"id"] == nil)
    {
        user_id  = [dictLogin valueForKey:@"userId"];
        
    }
    else
    {
        user_id  = [dictLogin valueForKey:@"id"];
    }
    
    // SERVER CONNCETION
    
    WebService * connection = [[WebService alloc]init];
    connection.delegate = self;
    
    NSMutableDictionary * dict       = [NSMutableDictionary dictionary];
    [dict setObject:@"carbookingdetail"                    forKey:kAPI_ACTION];
    [dict setObject:user_id                     forKey:kUSER_ID];
    if (self.bookingDetail.count>0)
    {
        [dict setObject:[self.bookingDetail objectForKey:@"bookingId"]      forKey:@"bookingId"];
        
    }
    else{
        
        [dict setObject:[[self.detailArray objectAtIndex:0] objectForKey:@"bookingId"]      forKey:@"bookingId"];
        
    }
    
    [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:@"bookingdetail"];
    
}
-(void)getDataFormWebService:(NSDictionary *)jsonResults urlStr:(NSString *)yourUR methodName:(NSString *)methodName{
    
    
    SWITCH (methodName) {
        
        CASE (@"bookingdetail")
        {
            
            self.detailArray = [jsonResults objectForKey:@"data"];
            
            self.pickupAddressLabel.text = [[jsonResults  valueForKey:@"data"]objectForKey:@"RequestPickupAddress"];
            
            self.dropAddressLabel.text = [[jsonResults  valueForKey:@"data"]objectForKey:@"RequestDropAddress"];
            
            self.totalLabel.text = [NSString stringWithFormat:@"$ %@",[[jsonResults  valueForKey:@"data"]objectForKey:@"FinalFare"]];
            
            self.toolLabel.text = [NSString stringWithFormat:@"$ %@",[[jsonResults  valueForKey:@"data"]objectForKey:@"FinalFare"]];
            
            self.discountLabel.text =[NSString stringWithFormat:@"%@",[[jsonResults  valueForKey:@"data"]objectForKey:@"serviceName"]];
            
            self.outstandingLabel.text = [NSString stringWithFormat:@"%@",[[jsonResults  valueForKey:@"data"]objectForKey:@"vehicleName"]];
            
            self.tripLabel.text = [NSString stringWithFormat:@"$ %@",[[jsonResults  valueForKey:@"data"]objectForKey:@"totalAmount"]];
            
            self.lblDuration.text = [NSString stringWithFormat:@"$ %@",[[jsonResults  valueForKey:@"data"]objectForKey:@"FinalFare"]];
            
            self.lblDistance.text = [NSString stringWithFormat:@"%@ %@",[[jsonResults  valueForKey:@"data"]objectForKey:@"totalDistance"], @"Miles"];
            
            if ([[[jsonResults valueForKey:@"data"]objectForKey:@"reviewed"] boolValue] == YES)
            {
                self.buttonReview.hidden =  YES;
            }
            else{
                self.buttonReview.hidden =  NO;
            }
        }
        
        break;
        
        DEFAULT
        {
            break;
        }
    }
    
}

// WEB SERVER ERROR MSG LOCAL SERVER (APPLICATION SERVER)
-(void) webServiceFailWithApplicationServerMSG:(NSString *)msg
{
    
    [Alert svError:msg];
    
}

// WEB SERVICE FAIL WITH REGION TCP/IP
-(void) webServiceFail:(NSError *)error{
    
    
    [Alert svError:[error localizedDescription]];
    
    
}

#pragma mark - Navigation bar configure
-(void)navigationBarConfiguration
{
    
    self.navigationController.navigationBarHidden = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    UIButton * menuButton  = [UIButton buttonWithType:UIButtonTypeSystem];
    menuButton.frame = CGRectMake(8, 18, 24, 24);
    [menuButton setBackgroundImage:[UIImage imageNamed:IMG_BACK] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(backButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *accountBarItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    self.navigationItem.leftBarButtonItem = accountBarItem;
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
    self.navigationController.navigationBar.tintColor = [Alert colorFromHexString:kCOLOR_NAV];
    
}

//#pragma mark - Update Location Button
//-(UIBarButtonItem*)buttonUpdateLocation
//{
//
//   UIButton *buttonUpdate = [UIButton buttonWithType:UIButtonTypeCustom];
//    buttonUpdate.frame=CGRectMake(0,0,50,30);
//    [buttonUpdate setTitle:@"Update location" forState:UIControlStateNormal];
//
//
//    [Alert buttonWithLayerColorAndWidth:kCOLOR_WHITE button:buttonUpdate radius:4.0 border:1.5];
//    [buttonUpdate addTarget:self action:@selector(buttonUpdateLocationClick:) forControlEvents:UIControlEventTouchUpInside];
//
//    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:buttonUpdate];
//
//    return backButton;
//}


-(IBAction)buttonUpdateLocationClick:(id)sender
{
    PaymentViewController * bookingHistoryViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"PaymentViewController");
    if (_bookingDetail)
    {
        bookingHistoryViewController.dict = self.bookingDetail;
        bookingHistoryViewController.groupId = [self.bookingDetail valueForKey:@"bookingId"];
        bookingHistoryViewController.invoiceId = [self.bookingDetail valueForKey:@"invoiceId"];
        bookingHistoryViewController.matchfrom = @"Driver";
        bookingHistoryViewController.ammount = [self.bookingDetail valueForKey:@"FinalFare"];
        
    }
    else
    {
        bookingHistoryViewController.dict = self.detailArray;
        bookingHistoryViewController.groupId = [self.detailArray valueForKey:@"bookingId"];
        bookingHistoryViewController.invoiceId = [self.detailArray valueForKey:@"invoiceId"];
        bookingHistoryViewController.matchfrom = @"Driver";
        bookingHistoryViewController.ammount = [self.detailArray valueForKey:@"FinalFare"];
        
    }
    
    UINavigationController * navigationController    = [Alert navigationControllerWithVC:bookingHistoryViewController];
    PERSENT_VIEW_CONTOLLER(navigationController, YES);
    
}

#pragma mark - button Back Click
-(void)backButtonClick:(id)sender
{
    // Go back (pop view controller)
    DISMISS_VIEW_CONTROLLER;
}

#pragma mark - button Back Click
-(IBAction)backuttonClick:(id)sender
{
    // Go back (pop view controller)
    DISMISS_VIEW_CONTROLLER;
}


-(IBAction)buttonReviewClick:(id)sender
{
    RateSubmitViewController * bookingDetailsViewController = GET_VIEW_CONTROLLER(@"RateSubmitViewController");
    bookingDetailsViewController.bookingDetail = self.detailArray;
    MOVE_VIEW_CONTROLLER(bookingDetailsViewController, YES);
    
}

@end
