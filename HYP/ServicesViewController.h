//
//  ServicesViewController.h
//  CHORETHING
//
//  Created by santosh kumar singh on 24/03/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ServicesViewController : UIViewController
{
    NSMutableArray *resultArray;
    NSMutableString *serviceTypeArr;
    NSIndexPath* selection;
    NSMutableArray *searchArray1;
    NSMutableArray *searchArray;
    NSString *searchTextString;
    NSArray *myWords;
    int i;
}


@property (nonatomic, strong) NSMutableDictionary *headerDict;
@property (nonatomic, strong) NSMutableArray *headerMulable;

@property (weak, nonatomic) IBOutlet UITextField *searchTextField;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (nonatomic, strong) NSMutableArray *selectedIndexDictionary;
@property (nonatomic, strong) NSMutableArray *selectedIndexNameDictionary;
@property (nonatomic, strong) IBOutlet UIButton *continuebtn;
@property(nonatomic,strong) NSString *matchConation;
@property(nonatomic,strong) NSString *match;
@property(nonatomic,strong) NSString *serviceTypeArr1;
@property (nonatomic, strong) NSString *selecteditem;

@property (nonatomic, strong) IBOutlet UIButton * referBtnView;
@property (nonatomic, strong) IBOutlet UIButton * linkBtnView;

@property (nonatomic, strong) NSDictionary *dictonartSelct;

@end


