//
//  WalletListTableViewCell.h
//  GetSum
//
//  Created by santosh kumar singh on 25/04/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface WalletListTableViewCell : UITableViewCell

@property(nonatomic,strong)  IBOutlet UIImageView *userThumb;
@property(nonatomic,strong)  IBOutlet UILabel *teamLabel;
@property(nonatomic,strong)  IBOutlet UILabel *delegateTypeLabel;
@property(nonatomic,strong)  IBOutlet UILabel *delegatewaitingLabel;
@property(nonatomic,strong)  IBOutlet UIButton *waitingLabel;
@property(nonatomic,strong)  IBOutlet UIButton *approvedLabel;
@property(nonatomic,strong)  IBOutlet UIButton *rejectLabel;
@property(nonatomic,strong)  IBOutlet UIButton *completedLabel;

@property(nonatomic,strong)  IBOutlet UIButton *adminLabel;
@property(nonatomic,strong)  IBOutlet UIButton *claimLabel;
@end


