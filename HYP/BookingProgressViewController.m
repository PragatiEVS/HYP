//
//  BookingProgressViewController.m
//  HYP
//
//  Created by santosh kumar singh on 26/09/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import "BookingProgressViewController.h"
#import "ManageServicesViewController.h"


@interface BookingProgressViewController ()<WebServiceDelegate,CLLocationManagerDelegate>
{
    AppDelegate * appDelegate;
}

@end

@implementation BookingProgressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
      CLGeocoder *geocoder = [[CLGeocoder alloc] init];
      [geocoder geocodeAddressString:[_dict valueForKey:@"address"]
                  completionHandler:^(NSArray* placemarks, NSError* error){
                          if (placemarks && placemarks.count > 0) {
                              CLPlacemark *topResult = [placemarks objectAtIndex:0];
                              MKPlacemark *placemark = [[MKPlacemark alloc] initWithPlacemark:topResult];
                              
                              self->latSearch =  placemark.coordinate.latitude;
                              self->lngSearch = placemark.coordinate.longitude;
                              
                               self.geocoder = [[GMSGeocoder alloc]init];
                              
                                 [self showGoogleMap];
                                
                                 [self showRoute];
                  
                          }
                      }
       ];
      
      CLLocationCoordinate2D coordinate = [self getLocation];
      
      latitude = coordinate.latitude;
      longitude = coordinate.longitude;
      
  
      self.title = @"IN PROGRESS";
       
   if (!self.dict[@"userFullName"])
    {
        self.username.text= [self.dict valueForKey:@"userName"];
            
    }
   else{
        self.username.text= [self.dict valueForKey:@"userFullName"];
   }
      
  
      self.serviceAddressLabel.numberOfLines = 0;
      self.currentAddressLabel.numberOfLines = 0;
      self.serviceAddressLabel.text = [self.dict valueForKey:@"address"];
      
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.addressView.bounds byRoundingCorners:( UIRectCornerBottomLeft | UIRectCornerBottomRight) cornerRadii:CGSizeMake(7.0, 7.0)];
            
            CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
            maskLayer.frame = self.view.bounds;
            maskLayer.path  = maskPath.CGPath;
       self.addressView.layer.mask = maskLayer;
    
    
    UIBezierPath *maskPath1 = [UIBezierPath bezierPathWithRoundedRect:self.userDetailView.bounds byRoundingCorners:( UIRectCornerTopLeft | UIRectCornerTopRight) cornerRadii:CGSizeMake(7.0, 7.0)];
              
              CAShapeLayer *maskLayer1 = [[CAShapeLayer alloc] init];
              maskLayer1.frame = self.view.bounds;
              maskLayer1.path  = maskPath1.CGPath;
    self.userDetailView.layer.mask = maskLayer1;
      
    
      [super viewDidLoad];
      [self navigationBarConfiguration];
    // Do any additional setup after loading the view.
}



-(void)showRoute
{
   
    CLLocation * locPicOff       = [[CLLocation alloc]initWithLatitude:latitude longitude:longitude];
    
    CLLocation * locDrop       = [[CLLocation alloc]initWithLatitude:latSearch longitude:lngSearch];
    
    
    GMSMarker *picoff = [[GMSMarker alloc]init];
    picoff.position   = CLLocationCoordinate2DMake(latitude, longitude);
    picoff.icon = [UIImage imageNamed:kIMG_DOT_CIRCLE_MARKER];
    picoff.map = mapView_;
    
    GMSMarker *dropoff = [[GMSMarker alloc]init];
    dropoff.position = CLLocationCoordinate2DMake(latSearch, lngSearch);
    dropoff.icon = [UIImage imageNamed:kIMG_USER_DESTI_MARKER];
    dropoff.map = mapView_;
    
    dispatch_async(dispatch_get_main_queue(),^{
      
        [self drawRouteOrigin:locPicOff destination:locDrop];
        
    });
}


- (void)drawRouteOrigin:(CLLocation *)myOrigin destination:(CLLocation *)myDestination
{
    [self fetchPolylineWithOrigin:myOrigin destination:myDestination completionHandler:^(GMSPolyline *polyline)
     {
        
         if(polyline)
         {
             GMSStrokeStyle *greenToRed = [GMSStrokeStyle solidColor:kCOLOR_BLACK];
             polyline.strokeWidth = 5.0;
             polyline.spans = @[[GMSStyleSpan spanWithStyle:greenToRed]];
             polyline.map = mapView_;
         }
         
         
     }];
}

- (void)fetchPolylineWithOrigin:(CLLocation *)origin destination:(CLLocation *)destination completionHandler:(void (^)(GMSPolyline *))completionHandler
{
  dispatch_async(dispatch_get_main_queue(), ^{ // Second main
    
    NSString *originString = [NSString stringWithFormat:@"%f,%f", origin.coordinate.latitude, origin.coordinate.longitude];
    NSString *destinationString = [NSString stringWithFormat:@"%f,%f", destination.coordinate.latitude, destination.coordinate.longitude];
    NSString *directionsAPI = @"https://maps.googleapis.com/maps/api/directions/json?";
    NSString *directionsUrlString = [NSString stringWithFormat:@"%@&origin=%@&destination=%@&mode=driving&key=%@", directionsAPI, originString, destinationString,kKEY_GOOGLE_MAP];
    NSURL *directionsUrl = [NSURL URLWithString:directionsUrlString];
    NSURLSessionDataTask *fetchDirectionsTask = [[NSURLSession sharedSession] dataTaskWithURL:directionsUrl completionHandler:
                                                 ^(NSData *data, NSURLResponse *response, NSError *error)
                                                 {
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        if(error)
        {
            if(completionHandler)
                completionHandler(nil);
            return;
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            //Your code...
            NSArray *routesArray = [json objectForKey:@"routes"];
            GMSPolyline *polyline = nil;
            if ([routesArray count] > 0)
            {
                NSDictionary *routeDict = [routesArray objectAtIndex:0];
                NSDictionary *routeOverviewPolyline = [routeDict objectForKey:@"overview_polyline"];
                NSString *points = [routeOverviewPolyline objectForKey:@"points"];
                GMSPath *path = [GMSPath pathFromEncodedPath:points];
                polyline = [GMSPolyline polylineWithPath:path];
                polyline.strokeWidth = 3.f;
                polyline.strokeColor = [UIColor blueColor];
                polyline.map = self->mapView_;
                
            }
        });
    }];
    [fetchDirectionsTask resume];
    });
}

-(CLLocationCoordinate2D) getLocation
{
    CLLocationManager *locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager startUpdatingLocation];
    CLLocation *location = [locationManager location];
    CLLocationCoordinate2D coordinate = [location coordinate];
    
    return coordinate;
}

#pragma mark --- : GOOGLE MAP :----
-(void)showGoogleMap
{
  
    CLLocation * upadetLocation = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
    
    CGRect frame = CGRectMake(0, 0, self.mapView.frame.size.width,self.mapView.frame.size.height);
    camera = [GMSCameraPosition cameraWithLatitude:latitude longitude:longitude zoom:15];
    mapView_ = [GMSMapView mapWithFrame:frame camera:camera];
    mapView_.delegate = self;
    
    [mapView_ setCamera:camera];
    
    [self reverGeoCodingUsingGoogle:upadetLocation];
    
    self->mapView_.myLocationEnabled              = YES;
    self->mapView_.indoorEnabled                  = YES;
    self->mapView_.settings.myLocationButton      = NO;
    self->mapView_.settings.scrollGestures        = YES;
    
    [_mapView addSubview:mapView_];
   
    locationMarker_ = [[GMSMarker alloc] init];
    locationMarker_.position = CLLocationCoordinate2DMake(latitude, longitude);
    locationMarker_.map = mapView_;
    
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    
    [self.locationManager stopUpdatingLocation];
    //  CLLocation *mostRecentLocation = [locations objectAtIndex:0];
   
   
}

-(void)reverGeoCodingUsingGoogle:(CLLocation *)location
{
    [[GMSGeocoder geocoder] reverseGeocodeCoordinate:CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude) completionHandler:^(GMSReverseGeocodeResponse* response, NSError* error)
     {
        
        GMSReverseGeocodeResult *result = response.firstResult;
        
        NSArray   * arrayAdd = result.lines;
        
        NSString  * address1;
        NSString  * address2;
        
        if (arrayAdd.count>1)
        {
            address1 = result.lines[0];
            address2 = result.subLocality;
        }
        else
        {
            address1 = result.lines[0];
        }
        
        self.currentAddressLabel.text = address1;
    }];
}


-(void)back:(id)sender
{
    DISMISS_VIEW_CONTROLLER;
}


#pragma mark - Configure Navigation Controller

-(void)navigationBarConfiguration
{
    
    self.navigationController.navigationBarHidden = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    UIButton * menuButton  = [UIButton buttonWithType:UIButtonTypeSystem];
    menuButton.frame = CGRectMake(8, 18, 24, 24);
    [menuButton setBackgroundImage:[UIImage imageNamed:IMG_BACK] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *accountBarItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    self.navigationItem.leftBarButtonItem = accountBarItem;
    
    NSMutableArray *arrRightBarItems = [[NSMutableArray alloc] init];
    UIButton *btnSetting = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnSetting setImage:[UIImage imageNamed:@"Frequency"] forState:UIControlStateNormal];
    btnSetting.frame = CGRectMake(0, 0, 40, 40);
    btnSetting.showsTouchWhenHighlighted=YES;
    [btnSetting addTarget:self action:@selector(onSettings:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSetting];
    
    UIButton *btnLib = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnLib setImage:[UIImage imageNamed:@"logo1.png"] forState:UIControlStateNormal];
    btnLib.frame = CGRectMake(0, 0, 40, 40);
    btnLib.showsTouchWhenHighlighted=YES;
     [btnLib addTarget:self action:@selector(onMyLibrary:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButtonItem2 = [[UIBarButtonItem alloc] initWithCustomView:btnLib];
    [arrRightBarItems addObject:barButtonItem2];
    [arrRightBarItems addObject:barButtonItem];
    self.navigationItem.rightBarButtonItems=arrRightBarItems;
    
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor],
    }];
    
}
-(void)onMyLibrary:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}

-(void)onSettings:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}


-(IBAction)callBtn:(id)sender
{
    if (TARGET_OS_SIMULATOR)
    {
        [Alert alertControllerTitle:kAPPICATION_TITLE msg:kDEVICE_NOT_SUPPORT ok:kOK controller:self.navigationController];
    }
    else
    {
        if (!self.dict[@"userContactNumber"])
           {
                [Alert makePhoneCall:[self.dict valueForKey:@"userPhone"]];
                   
           }
          else{
               [Alert makePhoneCall:[self.dict valueForKey:@"userContactNumber"]];
          }
       
    }
}

-(IBAction)buttonInvoiceClick:(id)sender
{
    ManageServicesViewController * bookingHistoryViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"ManageServicesViewController");
    bookingHistoryViewController.dict = _dict;
    UINavigationController * navigationController    = [Alert navigationControllerWithVC:bookingHistoryViewController];
     PERSENT_VIEW_CONTOLLER(navigationController, YES);
     
}
-(IBAction)buttonDirectionClick:(id)sender
{
   
    NSString * currentLat,*currentLong;
    
    currentLat = [NSString stringWithFormat:@"%.8f",latitude];
    currentLong = [NSString stringWithFormat:@"%.8f",longitude];
    
    NSMutableString* current = [NSMutableString stringWithString:currentLat];
    [current appendString:[NSString stringWithFormat:@",%@",currentLong]];
  
    NSString * destLat,*destLong;
    
    destLat = [NSString stringWithFormat:@"%.8f",latSearch];
     destLong = [NSString stringWithFormat:@"%.8f",lngSearch];
       
    
    NSMutableString* Dest = [NSMutableString stringWithString:destLat];
       [Dest appendString:[NSString stringWithFormat:@",%@",lngSearch]];
      
    NSString *string = [NSString stringWithFormat:@"%@saddr=%@&daddr=%@",Server_url_GOOGLE,current,Dest];
    NSLog(@"string=%@",string);
  
    NSString *encodedString=[string stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    if ([[UIApplication sharedApplication] canOpenURL:
         [NSURL URLWithString:@"comgooglemaps://"]]) {
        [[UIApplication sharedApplication] openURL:
         [NSURL URLWithString:encodedString]];
    } else {
        
      
        NSArray *item2          = [Dest componentsSeparatedByString:@","];
        
        NSString * strLatPic    = [item2 objectAtIndex:0];
        NSString * strLongPic   = [item2 objectAtIndex:1];
        
        NSString* directionsURL = [NSString stringWithFormat:@"http://maps.apple.com/?saddr=%f,%f&daddr=%f,%f",latitude, longitude, [strLatPic floatValue], [strLongPic floatValue]];
        [[UIApplication sharedApplication] openURL: [NSURL URLWithString: directionsURL]];
  
  }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
