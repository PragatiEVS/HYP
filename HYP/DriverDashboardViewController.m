//
//  DriverDashboardViewController.m
//  HYP
//
//  Created by VA pvt ltd on 13/09/21.
//  Copyright © 2021 Pragati Porwal. All rights reserved.
//

#import "DriverDashboardViewController.h"
#import "ArrivingViewController.h"


@interface DriverDashboardViewController ()<MKMapViewDelegate,CLLocationManagerDelegate,GMSMapViewDelegate,WebServiceDelegate>
{
    CLLocationManager *locationManager;
   
}

@end

@implementation DriverDashboardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.lblTimeLeft.layer.cornerRadius = 5;
    self.lblTimeLeft.clipsToBounds = YES;
    
    [self updateDriverLocation];
    
    NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"ProfileDetail"];
    
    //  NSString * userType  = [dict valueForKey:kAPI_USER_TYPE];
    
    self.driverName.text=[NSString stringWithFormat:@"Welcome HYP Driver, %@",[dictLogin valueForKey:@"fullName"]];

    
    if ([[dictLogin valueForKey:@"image"] isEqualToString:@""])
    {
        
        NSString *userName = [dictLogin valueForKey:@"fullName"];
        [self.driverImageView setImageWithString:userName color:nil circular:YES];
                     
    }
    else
    {
        
        [self.driverImageView setImageURL:[NSURL URLWithString:[dictLogin objectForKey:@"image"]]];
    }
    
    CLLocationCoordinate2D coordinate = [self getLocation];
    
    [AppDelegate appDelegate].latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
    [AppDelegate appDelegate].longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
    
    latitude = [[AppDelegate appDelegate].latitude floatValue];
    longitude = [[AppDelegate appDelegate].longitude floatValue];
    
    self.geocoder = [[GMSGeocoder alloc]init];
    
    [self showGoogleMap];
    
    if (self.notificationDict)
    {
        [self.addressDeleviry setText:[_notificationDict objectForKey:@"address"]];
      
        Time = 59;
        self.lblTimeLeft.text = [NSString stringWithFormat:@"00:%d",Time];
       
        countDown =  [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(countDown) userInfo:nil repeats:true];
        
        timer1 =  [NSTimer scheduledTimerWithTimeInterval:59 target:self selector:@selector(declineAPI) userInfo:nil repeats:false];
        
        
        self.notifiactionView.hidden= NO;
        self.addressDeleviry.text = [_notificationDict objectForKey:@"RequestDropAddress"];
        self.dateTimenotif.text =[NSString stringWithFormat:@"%@", [_notificationDict objectForKey:@"bookingDate"]];
        self.durationType.text = [_notificationDict objectForKey:@"totalTime"];
        self.invoiceIdNoti.text = [_notificationDict objectForKey:@"invoiceId"];
        self.addressType.text = [_notificationDict objectForKey:@"RequestPickupAddress"];
        self.vehicleTypeNoti.text = [_notificationDict objectForKey:@"carName"];
        self.bookingType.text = [_notificationDict objectForKey:@"serviceName"];
        self.earningNoti.text = [NSString stringWithFormat:@"$ %@",[_notificationDict objectForKey:@"estimatedPrice"]];
        [self.invoiceImage setImageURL:[NSURL URLWithString:[_notificationDict objectForKey:@"invoiceImage"]]];
        
    }
    else
    {
       
        self.notifiactionView.hidden= YES;
    }
  
    NSString *struserStatus =[[dictLogin valueForKey:@"availableStatus"]stringValue];
    
    if ([struserStatus isEqualToString:@"1"])
    {
        mdSwitch.on =YES;
        self.statusLabel.text = @"SIGN IN";
        
        
    }
    else{
        
        mdSwitch.on = NO;
        self.statusLabel.text = @"SIGN OFF";
    }
    
    [mdSwitch addTarget:self
                 action:@selector(updateState:)
       forControlEvents:UIControlEventValueChanged];
    
    self.title = @"DASHBOARD";
    
    [self navigationBarConfiguration];
    
    // Do any additional setup after loading the view.
}

-(void)countDown
{
    if(Time > 0)
    {
        Time -= 1;
        self.lblTimeLeft.text = [NSString stringWithFormat:@"00:%d",Time];
       
    }
}
-(void)declineAPI
{
    self.notifiactionView.hidden= YES;
    [timer1 invalidate];
}

-(IBAction)btnTapped:(UIButton*)sender
{  // SERVER CONNCETION
    
    
    [Alert svProgress:@"Please Wait..."];
    
    NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults] dictionaryForKey:kLOGIN];
    NSString * user_id ;
    if ([dictLogin valueForKey:@"id"] == nil)
    {
        user_id  = [dictLogin valueForKey:@"userId"];
        
    }
    else
    {
        user_id  = [dictLogin valueForKey:@"id"];
    }
    WebService * connection = [[WebService alloc]init];
    connection.delegate = self;
    NSMutableDictionary * dict                              = [NSMutableDictionary dictionary];
    
    [dict setObject:@"driverconfirm"                     forKey:kAPI_ACTION];
    [dict setObject:user_id                     forKey:@"driverId"];
    [dict setObject:[_notificationDict objectForKey:@"bookingId"]                     forKey:@"bookingId"];
   
    NSLog(@"dict is ---%@",dict);
    [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:@"changebookingstatus"];
    
}

-(IBAction)buttonRejectClick:(id)sender;
{
    self.notifiactionView.hidden = YES;
   [timer1 invalidate];
    //    [self declineAPI];
    
}

-(IBAction)cancelBtn:(id)sender
{  // SERVER CONNCETION
    
    self.notifiactionView.hidden= YES;
    [timer1 invalidate];
}

-(CLLocationCoordinate2D) getLocation
{
    CLLocationManager *locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager startUpdatingLocation];
    CLLocation *location = [locationManager location];
    CLLocationCoordinate2D coordinate = [location coordinate];
    
    return coordinate;
}

-(void)showGoogleMap
{
    
    CLLocation * upadetLocation = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
    
    CGRect frame = CGRectMake(0, 0, kSCREEN_WIDTH,kSCREEN_HEIGHT);
    camera = [GMSCameraPosition cameraWithLatitude:latitude longitude:longitude zoom:15];
    mapView_ = [GMSMapView mapWithFrame:frame camera:camera];
    mapView_.delegate = self;
    
    [mapView_ setCamera:camera];
    
    // satelliteFlyover
    [self reverGeoCodingUsingGoogle:upadetLocation];
    
    self->mapView_.myLocationEnabled              = YES;
    self->mapView_.indoorEnabled                  = YES;
    self->mapView_.settings.myLocationButton      = NO;
    self->mapView_.settings.scrollGestures        = YES;
    
    [_mapView addSubview:mapView_];
    NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"ProfileDetail"];
    
    NSString * str = [dictLogin valueForKey:@"role"];
    
    if ([str isEqualToString:@"Member"])
    {
        locationMarker_ = [[GMSMarker alloc] init];
        locationMarker_.position = CLLocationCoordinate2DMake(latitude, longitude);
        locationMarker_.icon = [UIImage imageNamed:@"marker"];
        locationMarker_.map = mapView_;
    }
    else{
        locationMarker_ = [[GMSMarker alloc] init];
        locationMarker_.position = CLLocationCoordinate2DMake(latitude, longitude);
        locationMarker_.icon = [UIImage imageNamed:@"car1.png"];
        locationMarker_.map = mapView_;
    }
    
    
}
- (void)updateState:(MDSwitch *)switchmd
{
    // MDSwitch *mdSwitch = (MDSwitch *)sender;
    
    [Alert svProgress:@"Please Wait..."];
    
    NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults] dictionaryForKey:kLOGIN];
    NSString *struserStatus =[[dictLogin valueForKey:@"availableStatus"]stringValue];
    
    NSString * user_id ;
    if ([dictLogin valueForKey:@"id"] == nil)
    {
        user_id  = [dictLogin valueForKey:@"userId"];
        
    }
    else
    {
        user_id  = [dictLogin valueForKey:@"id"];
    }
    
    WebService * connection = [[WebService alloc]init];
    connection.delegate = self;
    NSMutableDictionary * dict                              = [NSMutableDictionary dictionary];
    
    [dict setObject:@"onoff"                     forKey:kAPI_ACTION];
    [dict setObject:user_id                     forKey:kUSER_ID];
    
    
    if ([struserStatus isEqualToString:@"1"])
    {
        [dict setObject:@"0"                     forKey:@"availableStatus"];
        self.matchstatus = @"0";
        self.statusLabel.text = @"SIGN OFF";
    }
    else
    {
        [dict setObject:@"1"                     forKey:@"availableStatus"];
        self.matchstatus = @"1";
        self.statusLabel.text = @"SIGN IN";
    }
    
    NSLog(@"dict is ---%@",dict);
    [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:@"onoff"];
    
    
}

-(void)updateDriverLocation
{
    NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults]  objectForKey:kLOGIN];
    
    NSString * user_id ;
    if ([dictLogin valueForKey:@"id"] == nil)
    {
        user_id  = [dictLogin valueForKey:@"userId"];
        
    }
    else
    {
        user_id  = [dictLogin valueForKey:@"id"];
    }
    
    [Alert svProgress:@"Please Wait..."];
    
    WebService * connection = [[WebService alloc]init];
    connection.delegate = self;
    NSMutableDictionary * dict                              = [NSMutableDictionary dictionary];
    
    [dict setObject:@"editprofile"                     forKey:kAPI_ACTION];
    [dict setObject:user_id                     forKey:kUSER_ID];
    [dict setObject:[AppDelegate appDelegate].latitude                     forKey:@"BusinessLatitude"];
    [dict setObject:[AppDelegate appDelegate].longitude                     forKey:@"BusinessLongitude"];
    
    NSLog(@"dict is ---%@",dict);
    [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:@"findvendor" ];
    
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    
    [self.locationManager stopUpdatingLocation];
    //  CLLocation *mostRecentLocation = [locations objectAtIndex:0];
    
}

-(void)reverGeoCodingUsingGoogle:(CLLocation *)location
{
    [[GMSGeocoder geocoder] reverseGeocodeCoordinate:CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude) completionHandler:^(GMSReverseGeocodeResponse* response, NSError* error)
     {
        
        GMSReverseGeocodeResult *result = response.firstResult;
        
        NSArray   * arrayAdd = result.lines;
        
        NSString  * address1;
        NSString  * address2;
        
        if (arrayAdd.count>1)
        {
            address1 = result.lines[0];
            address2 = result.subLocality;
        }
        else
        {
            address1 = result.lines[0];
        }
        
    }];
    
    
}
-(void)navigationBarConfiguration
{
    
    self.navigationController.navigationBarHidden = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    UIButton * menuButton  = [UIButton buttonWithType:UIButtonTypeSystem];
    menuButton.frame = CGRectMake(8, 18, 24, 24);
    [menuButton setBackgroundImage:[UIImage imageNamed:NAV_SLIDER_IMAGE] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(drawer:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *accountBarItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    self.navigationItem.leftBarButtonItem = accountBarItem;
    
    NSMutableArray *arrRightBarItems = [[NSMutableArray alloc] init];
    UIButton *btnSetting = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnSetting setImage:[UIImage imageNamed:@"Frequency"] forState:UIControlStateNormal];
    btnSetting.frame = CGRectMake(0, 0, 50, 50);
    btnSetting.showsTouchWhenHighlighted=YES;
    [btnSetting addTarget:self action:@selector(onSettings:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSetting];
    
    UIButton *btnLib = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnLib setImage:[UIImage imageNamed:@"logo1.png"] forState:UIControlStateNormal];
    btnLib.frame = CGRectMake(0, 0, 50, 40);
    btnLib.showsTouchWhenHighlighted=YES;
    [btnLib addTarget:self action:@selector(onMyLibrary:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButtonItem2 = [[UIBarButtonItem alloc] initWithCustomView:btnLib];
    [arrRightBarItems addObject:barButtonItem2];
    [arrRightBarItems addObject:barButtonItem];
    self.navigationItem.rightBarButtonItems=arrRightBarItems;
    
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName :[UIColor whiteColor]}];
}

-(void)onMyLibrary:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}

-(void)onSettings:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/hyptunesradio";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}

#pragma mark - Drawer class
-(void)drawer:(id)sender
{
    
    KYDrawerController *elDrawer = (KYDrawerController*)self.navigationController.parentViewController;
    [elDrawer setDrawerState:KYDrawerControllerDrawerStateOpened animated:YES];
    
    
}
-(void)getDataFormWebService:(NSDictionary *)jsonResults urlStr:(NSString *)yourUR methodName:(NSString *)methodName{
    
    [SVProgressHUD dismiss];
    
    SWITCH (methodName) {
    CASE (@"onoff")
    {
        NSLog(@"jsonResults =%@",jsonResults);
        
        if ([[jsonResults valueForKey:@"status"] isEqualToString:@"success"])
        {
            [Alert svSuccess:[jsonResults valueForKey:@"msg"]];
            
            NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"ProfileDetail"];
            
            NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
            
            [newDict addEntriesFromDictionary:dictLogin];
            [newDict setObject:self.matchstatus forKey:@"availableStatus"];
            
            NSString *struserStatus =[[newDict valueForKey:@"availableStatus"]stringValue];
            
            [[NSUserDefaults standardUserDefaults] setObject:newDict forKey:@"ProfileDetail"];
            [[NSUserDefaults standardUserDefaults] setObject:newDict forKey:kLOGIN];
           
        }
    }
        CASE (@"findvendor")
        {
            NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"ProfileDetail"];
            
//            NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
//
//            [newDict addEntriesFromDictionary:dictLogin];
//            [newDict setObject:self.matchstatus forKey:@"availableStatus"];
//
//            NSString *struserStatus =[[newDict valueForKey:@"availableStatus"]stringValue];
//
            [[NSUserDefaults standardUserDefaults] setObject:dictLogin forKey:@"ProfileDetail"];
            [[NSUserDefaults standardUserDefaults] setObject:dictLogin forKey:kLOGIN];
           
        }
        CASE (@"changebookingstatus")
        {
            [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"rideStatus"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            ArrivingViewController * arrivingViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"ArrivingViewController");
            arrivingViewController.userName =  [jsonResults objectForKey:@"UserName"];
            arrivingViewController.userImage =  [jsonResults objectForKey:@"userImage"];
            arrivingViewController.uuserPhone = [jsonResults objectForKey:@"UserContactNumber"];
            arrivingViewController.dictArrived = self.notificationDict;
            
            UINavigationController * navigation = [Alert navigationControllerWithVC:arrivingViewController];
            
            PERSENT_VIEW_CONTOLLER(navigation, YES);
            
        }
    break;
    
    DEFAULT
    {
        break;
    }
}

}


-(void) webServiceFailWithApplicationServerMSG:(NSString *)msg
{
    
    [Alert svError:msg];
    
}

// WEB SERVICE FAIL WITH REGION TCP/IP
-(void) webServiceFail:(NSError *)error{
    
    
    [Alert svError:[error localizedDescription]];
    
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
