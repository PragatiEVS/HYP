//
//  AffiliateViewController.h
//  HYP
//
//  Created by santosh kumar singh on 05/09/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface AffiliateViewController : UIViewController
{
     int valrible;
}

@property (strong, nonatomic) NSString *professionId;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UITextField *firstNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *addressTextField;
@property (weak, nonatomic) IBOutlet UITextField *sendGiftTextField;
@property (weak, nonatomic) IBOutlet UITextField *professionTextField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTextField;


@property (weak, nonatomic) IBOutlet UIPickerView *dienstPicker;
@property (weak, nonatomic) IBOutlet UIButton *donePicker;

@property (weak, nonatomic) IBOutlet  UIButton *btnEditProfile;
@end


