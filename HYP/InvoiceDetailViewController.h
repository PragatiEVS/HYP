//
//  InvoiceDetailViewController.h
//  HYP
//
//  Created by santosh kumar singh on 10/09/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface InvoiceDetailViewController : UIViewController
{
    NSArray *invoiceArr;
}

@property (weak, nonatomic)  NSString * matchFrom;
@property(nonatomic,weak) IBOutlet UIView * serviceView;
@property(nonatomic,weak) IBOutlet UIView * jobView;
@property(nonatomic,weak) IBOutlet UILabel * datetimeLabel;
@property(nonatomic,weak) IBOutlet UILabel * username;
@property(nonatomic,weak) IBOutlet UILabel * address;
@property(nonatomic,weak) IBOutlet UILabel * phone;
@property(nonatomic,weak) IBOutlet UIImageView * userBg;
@property(nonatomic,weak) IBOutlet UILabel * toatalAmmount;
@property(nonatomic,weak) IBOutlet UIButton * donebtn;
@property(nonatomic,weak) IBOutlet UIButton * amountbtn;
@property (strong, nonatomic) NSDictionary * dict;

@property (strong, nonatomic) NSDictionary * notificationDict;

@end


