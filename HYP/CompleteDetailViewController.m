//
//  CompleteDetailViewController.m
//  HYP
//
//  Created by santosh kumar singh on 18/10/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import "CompleteDetailViewController.h"
#import "RateListViewController.h"
#import "InvoiceDetailViewController.h"
#import "DashboardViewController.h"
#import "MenuViewController.h"
#import "AffiliateViewController.h"

@interface CompleteDetailViewController ()<WebServiceDelegate>
{
  AppDelegate * appDelegate;

}
@end

@implementation CompleteDetailViewController

- (void)viewDidLoad
{
    self.title = @"INVOICE DETAILS";
    _viewaddClick.hidden=YES;
    
    self.viewaddClick.clipsToBounds = YES;
    self.viewaddClick.layer.cornerRadius = 5.0f;
    
    self.msgtextfiled.clipsToBounds = YES;
    self.msgtextfiled.layer.cornerRadius = 5.0f;
    
    self.submitbtn.clipsToBounds = YES;
    self.submitbtn.layer.cornerRadius = 5.0f;
    
    [self navigationBarConfiguration];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(void)onSettings:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}


-(void)back:(id)sender
{
    appDelegate = UIAppDelegate;
    DashboardViewController * homePassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBID_ARRIVED_VC);
    UIAppDelegate.isCall  = YES;
    MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
    [appDelegate manageDrawerRear:menuViewController front:homePassengerViewController];
    
}

#pragma mark - Configure Navigation Controller

-(void)navigationBarConfiguration
{
    
    self.navigationController.navigationBarHidden = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    UIButton * menuButton  = [UIButton buttonWithType:UIButtonTypeSystem];
    menuButton.frame = CGRectMake(8, 18, 24, 24);
    [menuButton setBackgroundImage:[UIImage imageNamed:IMG_BACK] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *accountBarItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    self.navigationItem.leftBarButtonItem = accountBarItem;
    
    
    NSMutableArray *arrRightBarItems = [[NSMutableArray alloc] init];
    UIButton *btnSetting = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnSetting setImage:[UIImage imageNamed:@"Frequency"] forState:UIControlStateNormal];
    btnSetting.frame = CGRectMake(0, 0, 40, 40);
    btnSetting.showsTouchWhenHighlighted=YES;
    [btnSetting addTarget:self action:@selector(onSettings:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSetting];
    
    UIButton *btnLib = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnLib setImage:[UIImage imageNamed:@"logo1.png"] forState:UIControlStateNormal];
    btnLib.frame = CGRectMake(0, 0, 40, 40);
    btnLib.showsTouchWhenHighlighted=YES;
    [btnLib addTarget:self action:@selector(onMyLibrary:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButtonItem2 = [[UIBarButtonItem alloc] initWithCustomView:btnLib];
    [arrRightBarItems addObject:barButtonItem2];
    [arrRightBarItems addObject:barButtonItem];
    self.navigationItem.rightBarButtonItems=arrRightBarItems;
    
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor],
    }];
    
}

-(void)onMyLibrary:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}
-(IBAction)reviewList:(id)sender
{
    
    RateListViewController * bookingHistoryViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"RateListViewController");
    
    NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults]  objectForKey:kLOGIN];
    
    NSString * str = [dictLogin valueForKey:@"role"];
    
    if ([str isEqualToString:@"Member"])
    {
        bookingHistoryViewController.userId = [_dict valueForKey:@"vendorId"];
    }
    else
    {
        bookingHistoryViewController.userId = [_dict valueForKey:@"userId"];
    }
    
    UINavigationController * navigationController    = [Alert navigationControllerWithVC:bookingHistoryViewController];
    PERSENT_VIEW_CONTOLLER(navigationController, YES);
    
    
}


-(IBAction)addReviewBtn:(id)sender
{  // SERVER CONNCETION
    
    self.viewaddClick.layer.masksToBounds = NO;
    self.viewaddClick.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    self.viewaddClick.layer.shadowOffset = CGSizeMake(-5.0f, -5.0f);
    self.viewaddClick.layer.shadowOpacity = 1.0f;
    
    self.viewaddClick.hidden= NO;
    //_ratingViewClick.hidden=YES;
    self.rateUpdate.starSize = 25;
    self.rateUpdate.rating = 2.5f;
    self.rateUpdate.canRate = YES;
    self.rateUpdate.starFillColor = [Alert colorFromHexString:@"#F4CC47"];
    
}

-(void)rateView:(RateView*)rateView didUpdateRating:(float)rating
{
    self.rateingString = [NSString stringWithFormat:@"%f", rating];
    NSLog(@"rateViewDidUpdateRating: %@",  self.rateingString);
}

-(IBAction)cancelBtn:(id)sender
{  // SERVER CONNCETION
    
    self.viewaddClick.hidden= YES;
    
}


-(IBAction)showInvoiceBtn:(id)sender
{  // SERVER CONNCETION
    
    
    InvoiceDetailViewController * bookingHistoryViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"InvoiceDetailViewController");
    bookingHistoryViewController.dict = _dict;
    UINavigationController * navigationController    = [Alert navigationControllerWithVC:bookingHistoryViewController];
    PERSENT_VIEW_CONTOLLER(navigationController, YES);
    
    
}

-(IBAction)clickHomeBtn:(id)sender
{  // SERVER CONNCETION
    
    appDelegate = UIAppDelegate;
                   DashboardViewController * homePassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBID_ARRIVED_VC);
                   UIAppDelegate.isCall  = YES;
                   MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
                   [appDelegate manageDrawerRear:menuViewController front:homePassengerViewController];
       
           
    
}

-(IBAction)btnReferClick:(UIButton*)sender
{
    AffiliateViewController * termConditionViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"AffiliateViewController");
    UINavigationController * navigationController = [Alert navigationControllerWithVC:termConditionViewController];
    
    PERSENT_VIEW_CONTOLLER(navigationController, YES);
    
    
}

- (IBAction)buttonsubmited:(id)sender
{
    if (self.msgtextfiled.text.length>0)
    {
        [Alert svProgress:@"Please Wait..."];
        
        NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults]  objectForKey:kLOGIN];
        
        
        NSString * user_id ;
        if ([dictLogin valueForKey:@"id"] == nil)
        {
            user_id  = [dictLogin valueForKey:@"userId"];
            
        }
        else
        {
            user_id  = [dictLogin valueForKey:@"id"];
        }
        WebService * connection = [[WebService alloc]init];
        connection.delegate = self;
        NSMutableDictionary * dict                              = [NSMutableDictionary dictionary];
        
        [dict setObject:@"submitreview"                     forKey:kAPI_ACTION];
        [dict setObject:user_id                     forKey:@"reviewFrom"];
        [dict setObject:[_dict objectForKey:@"userId"]                     forKey:@"reviewTo"];
        [dict setObject:[NSString stringWithFormat:@"%f", self.rateUpdate.rating]                     forKey:@"star"];
        [dict setObject:self.msgtextfiled.text                     forKey:@"message"];
        
        
        NSLog(@"dict is ---%@",dict);
        [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:@"submitreview"];
        
        
    }
    
    else
    {
        
        [Alert svError:@"Enter Review"];
        
        
    }
    
    
}

#pragma mark - Sevice delgate methods

-(void)getDataFormWebService:(NSDictionary *)jsonResults urlStr:(NSString *)yourUR methodName:(NSString *)methodName{
    
    [SVProgressHUD dismiss];
    
    SWITCH (methodName) {
        CASE (@"submitreview")
        {
             self.viewaddClick.hidden= YES;
            NSLog(@"jsonResults =%@",jsonResults);
            [Alert svSuccess:[jsonResults objectForKey:@"msg"]];
        }
        
        
        break;
        
        DEFAULT
        {
            break;
        }
    }
    
}
// WEB SERVER ERROR MSG LOCAL SERVER (APPLICATION SERVER)
-(void) webServiceFailWithApplicationServerMSG:(NSString *)msg
{
    
    [Alert svError:msg];
    
}

// WEB SERVICE FAIL WITH REGION TCP/IP
-(void) webServiceFail:(NSError *)error{
    
    
    [Alert svError:[error localizedDescription]];
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
