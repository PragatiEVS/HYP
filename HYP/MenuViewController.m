//
//  MenuViewController.m
//  Boccia
//
//  Created by IOSdev on 12/31/19.
//  Copyright © 2019 Pragati Porwal. All rights reserved.
//

#import "MenuViewController.h"
#import "ViewController.h"
#import "EditViewController.h"
#import "ChangePasswordViewController.h"
#import "HelpViewController.h"
#import "PendingBookingViewController.h"
#import "BankAccountViewController.h"
#import "EarningViewController.h"
#import "RateListPartenerViewController.h"
#import "DashboardViewController.h"
#import "FAQViewController.h"
#import "ChoreHistoryViewController.h"
#import "NewRequestViewController.h"
#import "AffiliateViewController.h"
#import "TermsViewController.h"
#import "DriverBookingViewController.h"
#import "DriverDashboardViewController.h"
#import "DriverEarningViewController.h"
#import "UserBookingViewController.h"



@interface MenuViewController ()<WebServiceDelegate>

@end

@implementation MenuViewController

- (void)viewDidLoad {
    [self config];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}



#pragma mark - Config
-(void)config
{
    NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"ProfileDetail"];
    
    //  NSString * userType  = [dict valueForKey:kAPI_USER_TYPE];
    
    self.nameLabel.text=[dictLogin valueForKey:@"fullName"];
    
    
    if ([[dictLogin valueForKey:@"image"] isEqualToString:@""])
    {
        
        NSString *userName = [dictLogin valueForKey:@"fullName"];
        [self.userbgLabel setImageWithString:userName color:nil circular:YES];
        
    }
    else
    {
        
        [self.userbgLabel setImageURL:[NSURL URLWithString:[dictLogin objectForKey:@"image"]]];
    }
    
    
    NSString * str = [dictLogin valueForKey:@"role"];
    
    if ([str isEqualToString:@"Member"])
    {
        
        _arrayMenu = [[NSArray alloc]initWithObjects:@"Home",@"Booking History",@"Edit Profile",@"Change Password",@"FAQ",@"Help",@"Sign Out",nil];
        
        _imageMenu= [[NSArray alloc]initWithObjects:@"Home",@"edit_info",@"Edit",@"Lock",@"Help",@"Help",@"Logout", nil];
        
        
    }
    
    else if ([str isEqualToString:@"Driver"])
    {
        
        _arrayMenu = [[NSArray alloc]initWithObjects:@"Home",@"Booking History",@"Edit Profile",@"Reviews & Rating",@"My Earning",@"Change Password",@"FAQ",@"Help",@"Sign Out",nil];
        
        _imageMenu= [[NSArray alloc]initWithObjects:@"Home",@"edit_info",@"Edit",@"Rating",@"Earning",@"Lock",@"Help",@"Help",@"Logout", nil];
    }
    else
    {
        _arrayMenu = [[NSArray alloc]initWithObjects:@"Home",@"Pending Request",@"Booking History",@"Edit Profile",@"Reviews & Rating",@"My Earning",@"Change Password",@"FAQ",@"Help",@"Sign Out",nil];
        
        _imageMenu= [[NSArray alloc]initWithObjects:@"Home",@"edit_info",@"edit_info",@"Edit",@"Rating",@"Earning",@"Lock",@"Help",@"Help",@"Logout", nil];
        
    }
}

-(IBAction)buttonEmailClick:(id)sender
{
    EditViewController * settingsViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBID_CANCEL_RIDE_VC);
    
    [self push:settingsViewController];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return _arrayMenu.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell)
        cell = [[UITableViewCell alloc] initWithStyle: UITableViewCellStyleDefault reuseIdentifier: CellIdentifier];
    NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults] dictionaryForKey:kLOGIN];
    
    cell.imageView.image =[UIImage imageNamed:[_imageMenu objectAtIndex:indexPath.row]];
    
    cell.textLabel.text  = (NSString *)[_arrayMenu objectAtIndex:indexPath.row];
    cell.textLabel.font  = [UIFont fontWithName:kFONT size:18];
    
    //[UIColor whiteColor];
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle  = UITableViewCellSelectionStyleNone;
    cell.textLabel.textColor = [UIColor whiteColor];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults] dictionaryForKey:kLOGIN];
    
    NSString * str = [dictLogin valueForKey:@"role"];
    
    if ([str isEqualToString:@"Member"])
    {
        
        switch (indexPath.row) {
            case 0:
            {
                
                DashboardViewController * bookingHistoryViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBID_ARRIVED_VC);
                [self push:bookingHistoryViewController];
                
            }
                break;
            case 1:
            {
                UserBookingViewController * bookingHistoryViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"UserBookingViewController");
                [self push:bookingHistoryViewController];
                
            }
                break;
                
            case 2:
            {
                
                EditViewController * settingsViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBID_CANCEL_RIDE_VC);
                [self push:settingsViewController];
            }
                break;
            case 3:
            {
                
                ChangePasswordViewController *helpViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDChangePasswordVC);
                [self push:helpViewController];
                
            }
                break;
            case 4:
            {
                
                
                FAQViewController * settingsViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"FAQViewController");
                [self push:settingsViewController];
                
            }
                break;
                
            case 5:
            {
                HelpViewController *helpViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDHelpVC);
                
                [self push:helpViewController];
            }
                break;
            case 6:
            {
                
                UIAlertController * alert = [UIAlertController
                                             alertControllerWithTitle:kLOGOUT_CONFIRMATION
                                             message:nil
                                             preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* yesButton = [UIAlertAction
                                            actionWithTitle:@"Sign Out"
                                            style:UIAlertActionStyleDestructive
                                            handler:^(UIAlertAction * action)
                                            {
                    [self yesButton];
                }];
                
                UIAlertAction* noButton = [UIAlertAction
                                           actionWithTitle:@"Cancel"
                                           style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction * action) {
                    //Handle no, thanks button
                }];
                
                [alert addAction:noButton];
                [alert addAction:yesButton];
                
                [self presentViewController:alert animated:YES completion:nil];
                
            }
                break;
            default:
                break;
        }
    }
    else if([str isEqualToString:@"Driver"])
    {
        
        switch (indexPath.row)
        {
                
            case 0:
            {
                DriverDashboardViewController * bookingHistoryViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"DriverDashboardViewController");
                [self push:bookingHistoryViewController];
                
                
            }
                break;
            case 1:
            {
                
                DriverBookingViewController * settingsViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"DriverBookingViewController");
                [self push:settingsViewController];
                
            }
                break;
            case 2:
            {
                EditViewController * settingsViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBID_CANCEL_RIDE_VC);
                [self push:settingsViewController];
                
            }
                break;
            case 4:
            {
                
                DriverEarningViewController * settingsViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"DriverEarningViewController");
                [self push:settingsViewController];
                
            }
                break;
            case 3:
            {
                
                RateListPartenerViewController * settingsViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"RateListPartenerViewController");
                [self push:settingsViewController];
            }
                
                break;
                
                
                
            case 5:
            {
                
                ChangePasswordViewController *helpViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDChangePasswordVC);
                [self push:helpViewController];
                
            }
                break;
                
            case 6:
            {
                
                FAQViewController * settingsViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"FAQViewController");
                [self push:settingsViewController];
                
                
            }
                
                break;
            case 7:
            {
                HelpViewController *helpViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDHelpVC);
                
                [self push:helpViewController];
            }
                break;
            case 8:
            {
                UIAlertController * alert = [UIAlertController
                                             alertControllerWithTitle:kLOGOUT_CONFIRMATION
                                             message:nil
                                             preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* yesButton = [UIAlertAction
                                            actionWithTitle:@"Sign Out"
                                            style:UIAlertActionStyleDestructive
                                            handler:^(UIAlertAction * action)
                                            {
                    [self yesButton];
                }];
                
                UIAlertAction* noButton = [UIAlertAction
                                           actionWithTitle:@"Cancel"
                                           style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction * action) {
                    //Handle no, thanks button
                }];
                
                [alert addAction:noButton];
                [alert addAction:yesButton];
                
                [self presentViewController:alert animated:YES completion:nil];
                
            }
                break;
            default:
                break;
        }
    }
else{
    
    
    switch (indexPath.row)
    {
            
        case 0:
        {
            DashboardViewController * bookingHistoryViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBID_ARRIVED_VC);
            [self push:bookingHistoryViewController];
            
            
        }
            break;
        case 1:
        {
            
            NewRequestViewController * settingsViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"NewRequestViewController");
            [self push:settingsViewController];
            
            
        }
            break;
        case 2:
        {
            ChoreHistoryViewController * settingsViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDWaitingPassengerVC);
            [self push:settingsViewController];
            
        }
            break;
        case 3:
        {
            EditViewController * settingsViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBID_CANCEL_RIDE_VC);
            [self push:settingsViewController];
            
        }
            break;
        case 5:
        {
            
            EarningViewController * settingsViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"EarningViewController");
            [self push:settingsViewController];
        }
            break;
        case 4:
        {
            
            RateListPartenerViewController * settingsViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"RateListPartenerViewController");
            [self push:settingsViewController];
        }
            
            break;
            
            
            
        case 6:
        {
            
            ChangePasswordViewController *helpViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDChangePasswordVC);
            [self push:helpViewController];
            
        }
            break;
            
        case 7:
        {
            
            FAQViewController * settingsViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"FAQViewController");
            [self push:settingsViewController];
            
            
        }
            
            break;
        case 8:
        {
            HelpViewController *helpViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDHelpVC);
            
            [self push:helpViewController];
        }
            break;
        case 9:
        {
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:kLOGOUT_CONFIRMATION
                                         message:nil
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* yesButton = [UIAlertAction
                                        actionWithTitle:@"Sign Out"
                                        style:UIAlertActionStyleDestructive
                                        handler:^(UIAlertAction * action)
                                        {
                [self yesButton];
            }];
            
            UIAlertAction* noButton = [UIAlertAction
                                       actionWithTitle:@"Cancel"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {
                //Handle no, thanks button
            }];
            
            [alert addAction:noButton];
            [alert addAction:yesButton];
            
            [self presentViewController:alert animated:YES completion:nil];
            
        }
            break;
        default:
            break;
    }
}
}


-(void)yesButton
{
    // SERVER CONNCETION
    [Alert svProgress:@"Sign Out..."];
    // Dictionary Login
    NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults] dictionaryForKey:kLOGIN];
    NSString *user_id;
    
    if ([dictLogin valueForKey:@"id"] == nil)
    {
        user_id  = [dictLogin valueForKey:@"userId"];
        
    }
    else
    {
        user_id  = [dictLogin valueForKey:@"id"];
    }
    
    
    // service
    WebService * connection = [[WebService alloc]init];
    connection.delegate = self;
    
    NSMutableDictionary * dict                              = [NSMutableDictionary dictionary];
    [dict setObject:@"logout" forKey:kAPI_ACTION];
    [dict setObject: user_id                       forKey:@"userId"];
    
    [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:@"logout"];
}

#pragma mark - <WebServiveDelegate>
-(void)getDataFormWebService:(NSDictionary *)jsonResults urlStr:(NSString *)yourUR methodName:(NSString *)methodName
{
    [Alert removeView:self.view];
    NSLog(@"jsonResults is ---%@",jsonResults);
    if ([methodName isEqualToString:@"logout"])
    {
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:kLOGIN];
        AppDelegate* appDelegate = UIAppDelegate;
        ViewController *loginViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"ViewController");
        [appDelegate setInitialViewController:loginViewController];
        
    }
    
}


// WEB SERVER ERROR MSG LOCAL SERVER (APPLICATION SERVER)
-(void) webServiceFailWithApplicationServerMSG:(NSString *)msg
{
    [Alert removeView:self.view];
    [Alert alertViewDefalultWithTitleString:kAPPICATION_TITLE
                                     forMsg:msg forOK:kOK];
}

// WEB SERVICE FAIL WITH REGION TCP/IP
-(void) webServiceFail:(NSError *)error
{
    [Alert removeView:self.view];
    [Alert alertViewDefalultWithTitleString:
     kAPPICATION_TITLE forMsg:[error localizedDescription] forOK:kOK];
}


#pragma mark - Push Using SWRevealViewController
-(void)push:(UIViewController *)controller
{
    
    BOOL network = [Alert networkStatus];
    
    if (network) {
        KYDrawerController *elDrawer = (KYDrawerController*)self.parentViewController;
        
        UINavigationController *navController=[[UINavigationController alloc]initWithRootViewController:controller];
        elDrawer.mainViewController=navController;
        
        [elDrawer setDrawerState:KYDrawerControllerDrawerStateClosed animated:YES];
    }
    else
        kNETWORK_PROBLEM;
    
    
}




/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
