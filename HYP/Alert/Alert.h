//
//  Alert.h
//  Q-municate
//
//  Created by Sandeep Kumar on 18/07/15.
//  Copyright (c) 2015 Quickblox. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "PNTToolbar.h"
@interface Alert : NSObject
+ (void)alertWithMessage:(NSString*)message navigation:(UINavigationController*)navigation gotoBack:(BOOL)goBack animation:(BOOL)animation;
+ (void)alertWithMessage:(NSString*)message navigation:(UINavigationController*)navigation gotoBack:(BOOL)goBack animation:(BOOL)animation sencond:(int)second;
+ (void)alertViewDefalultWithTitleString:(NSString *)title forMsg:(NSString*)message forOK:(NSString *)Ok;
+ (void)alertWithMessageNew:(NSString *)title forMsg:(NSString*)message forOK:(NSString *)Ok navigation:(UINavigationController*)navigation;
+ (void)globalAlertViewtitle:(NSString *)title forMsg:(NSString*)message forOK:(NSString *)Ok navigation:(UINavigationController*)navigation;
+(void)alertControllerTitle:(NSString *)title msg:(NSString*)msg ok:(NSString *)ok controller:(UINavigationController *)contoller;
+ (void)performBlockWithInterval:(double)interval completion:(void(^)(void))completion;
+ (BOOL)validationName:(NSString *)checkString;
+ (BOOL)validationEmail:(NSString *)checkString;
+ (BOOL)validateMobileNumber:(NSString*)number;
+ (BOOL)validateNumber:(NSString*)number;
+ (BOOL)validatePinCode:(NSString*)number;
+ (BOOL)ValidateTextFieldValidate:(UITextField *)textField andLenght:(int)textlenght;
+(BOOL)validateTextView:(UITextView *)textView andLenght:(int)textlenght;
+(CGSize)findHeightForText:(NSString *)text havingWidth:(CGFloat)widthValue andFont:(UIFont *)font;
+(UIImage*) drawText:(NSString*)text inImage:(UIImage*)image atPoint:(CGPoint)point;

+(NSString *)getCurrentDate;
+(NSString *)getCurrentTime;
+(BOOL) checkIfUsernameValidation:(NSString *)text;
+ (BOOL)isPasswordMatch:(NSString *)pwd withConfirmPwd:(NSString *)cnfPwd;
+ (BOOL)isStateSelectOrNot:(NSString *)string andyourStr:(NSString *)yourStr;
+ (UIColor *)colorFromHexString:(NSString *)hexString ;
+(void)addIndicatorView:(UIView*)view color:(UIColor *)color;
+(void)removeView:(UIView*)yourView;
+ (UIImage *)scaleAndRotateImage:(UIImage *)image;
+ (BOOL)networkStatus;
+ (NSString*)getFirstName:(NSString*)name;
+ (NSDateFormatter*)getDateFormatWithString:(NSString*)string;
+ (NSDate*)getDateWithDateString:(NSString*)dateString setFormat:(NSString*)format;
+ (NSString*)getDateWithString:(NSString*)string getFormat:(NSString*)format1 setFormat:(NSString*)format2;
+ (NSDictionary*)getAllCountryNameWithCodeList;
+ (NSArray*)getLanguageNamelist;
+ (NSMutableArray *)removeViewControllFromNavArray:(int)number navigation:(UINavigationController*)class;
+ (void)viewButtonCALayer:(UIColor *)yourColor viewButton:(UIButton *)yourButton;
+ (void)viewCALayer:(UIColor *)yourColor viewButton:(UIView *)yourView;
+ (NSArray *)getCounrtyName;
+ (NSString*) bv_jsonStringWithDictionary:(NSDictionary*)dictionary;
+ (NSString*)jsonStringWithDictionary:(NSDictionary*)data;
+ (NSDictionary*)getDictionaryWithJsonString:(NSString*)string;
+ (NSString*)getDeviceToken:(NSData*)deviceToken;
+ (UIImage *)imageFromColor:(UIColor *)color;
+ (void)changeImageColor:(NSString *)imageName imageView:(UIImageView *)imageView andColor:(UIColor *)yourColor;
+ (void)SetToolBarONKeyBoard:(UITableView *)view andFields:(NSArray *)arrayFields;
+(void)SetToolBarONKeyBoardScrollView:(UIScrollView *)view andFields:(NSArray *)arrayFields;
+ (void)setKeyBoardToolbar:(NSArray*)arrayFields;
+ (void)viewWithLayerColorAndWidth:(UIColor *)yourColor view:(UIView *)yourView radius:(CGFloat)radius border:(CGFloat)border;
+ (void)buttonWithLayerColorAndWidth:(UIColor *)color button:(UIButton *)button radius:(CGFloat)radius border:(CGFloat)border;
#pragma mark - Device Name
+ (NSString*)deviceName;
+(NSString *)getFullServerUrlStr:(NSString*)baseUrl api:(NSString *)api;
#pragma mark - Shadow on UIButton
+(void)shadowOnButtons:(UIButton*)sender;
+(BOOL)isValidPassword:(NSString *)passwordString;
+(BOOL)validate:(NSString *)string;
+(NSString *)date;
#pragma mark - name and version get
+(NSString *)appNameAndVersionNumberDisplayString;
#pragma remove Null object
+(NSMutableDictionary *)removeNSNullClass:(NSMutableDictionary *)dict;
#pragma mark -  add uilabel on TableView BG
+(void)addLabelBGTableView:(UITableView *)tableView msg:(NSString*)msg color:(UIColor *)color;
+(void)moveUpView:(UIView *)view;
+(void)moveDownView:(UIView *)view;
+(void)makePhoneCall:(NSString *) phoneNumber;
#pragma mark - date format
//+(NSString *)relativeDateStringForDate:(NSDate *)date;
+(void)placeHoderColor:(UITextField *)txtField color:(UIColor *)color;
+(NSString*)getYoutubeVideoThumbnail:(NSString*)youTubeUrl;
+(UIColor *)colorWithPatternImage2:(UIImage *)image;
+(void) setLeftPaddingTextField:(UITextField *)textField paddingValue:(int) paddingValue image:(UIImage *)image;
+(void) setRightPaddingTextField:(UITextField *)textField paddingValue:(int) paddingValue image:(UIImage *)image;

+(UINavigationController *)navigationControllerWithVC:(UIViewController *)vc;
#pragma mark - Empty Str.
+(BOOL)isEmptyStr:(NSString *)str;
+(UIImage *)coloredImage:(UIImage *)firstImage withColor:(UIColor *)color;
+(void)changeImageColorURL:(NSURL *)urlImage imageView:(UIImageView *)imageView andColor:(UIColor *)yourColor;
+(void)makeLineLayer:(CALayer *)layer lineFromPointA:(CGPoint)pointA toPointB:(CGPoint)pointB;

// shadow on UILabel
+(void)setUILabelShadow:(UILabel *)label shadowColor:(UIColor*)shadowColor shadowOpacity:(float)shadowOpacity shadowRadius:(float)shadowRadius masksToBounds:(BOOL)masksToBounds;


+(void)drawLineUsingFrameOnview:(UIView *)view strokeColor:(UIColor *)strokeColor fillColor:(UIColor *)fillColor frame:(CGRect)frame;
+(void)drawLineUsingPointOnview:(UIView *)view strokeColor:(UIColor *)strokeColor pointStart:(CGPoint)pointStart pointEnd:(CGPoint)pointEnd;
+(void)drawRect:(CGRect)rect color:(UIColor *)color ;

#pragma mark - MULTIPLE COLOR STRING ON UILABEL
+(void)setColoredLabel:(UILabel*)label  name:(NSString *)name phone:(NSString *)phone colorStr1:(UIColor *)colorStr1 colorStr2:(UIColor *)colorStr2;

#pragma mark - string of commas 
+(NSArray *)stringOfCommas:(NSString *)str;
//+(CLPlacemark *)locationAddress:(CLLocation *)location;
+ (void)setCustomBackgroundImage:(UIImage *)image view:(UIView *)view;
+(void)svError :(NSString *)str;
+(void)svSuccess :(NSString *)str;
+(void)svProgress :(NSString *)str;




@end
