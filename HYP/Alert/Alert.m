//
//  Alert.m
//  Q-municate
//
//  Created by Sandeep Kumar on 18/07/15.
//  Copyright (c) 2015 Quickblox. All rights reserved.
//

#import "Alert.h"
#import <UIKit/UIKit.h>
#include <sys/utsname.h>
#import <AVFoundation/AVFoundation.h>
#import "Reachability.h"
#import <CoreGraphics/CoreGraphics.h>


@implementation Alert

#pragma mark get height of text
#pragma mark Keyboard Up & Down


+(UIImage*) drawText:(NSString*) text
             inImage:(UIImage*)  image
             atPoint:(CGPoint)   point
{

    UIFont *font = [UIFont boldSystemFontOfSize:12];
    UIGraphicsBeginImageContext(image.size);
    [image drawInRect:CGRectMake(0,0,image.size.width,image.size.height)];
    CGRect rect = CGRectMake(point.x, point.y, image.size.width, image.size.height);
    [[UIColor whiteColor] set];
    [text drawInRect:CGRectIntegral(rect) withFont:font];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return newImage;
}

+(void)moveUpView:(UIView *)view{
    
    
    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        
        CGRect frame = view.frame;
        if (kSCREEN_HEIGHT == 1024) {
            frame.origin.y = (-265);
        }
        else
            if (kSCREEN_HEIGHT == 568) {
                frame.origin.y = (-220);
            }
            else{
                frame.origin.y = (-215);
            }
        frame.origin.x =0;
        view.frame = frame;
        
    }completion:^(BOOL finished){
        
        
    }];
    
}



+(void)moveDownView:(UIView *)view{
    
    [UIView animateWithDuration:0.0 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        
        
        CGRect frame = view.frame;
        frame.origin.y = 0;
        frame.origin.x =0;
        view.frame = frame;
        
    }completion:^(BOOL finished){
        
         
    }];
    
}

+(CGSize)findHeightForText:(NSString *)text havingWidth:(CGFloat)widthValue andFont:(UIFont *)font {
    CGSize size = CGSizeZero;
    if (text) {
        //iOS 7
        CGRect frame = [text boundingRectWithSize:CGSizeMake(widthValue, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{ NSFontAttributeName:font } context:nil];
        size = CGSizeMake(frame.size.width, frame.size.height + 1);
    }
    return size;
}

#pragma mark Current Date & Time

+(NSString *)getCurrentDate{
    
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    NSLog(@"%@",[dateFormatter stringFromDate:[NSDate date]]);
    
    return [dateFormatter stringFromDate:[NSDate date]];
    
}
+(NSString *)getCurrentTime{
    
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"hh:mm a"];
    NSLog(@"%@",[dateFormatter stringFromDate:[NSDate date]]);
    
    
    return [dateFormatter stringFromDate:[NSDate date]];
    
}
#pragma mark - sv progress
+(void)svError :(NSString *)str
{
    [SVProgressHUD showErrorWithStatus:str maskType:SVProgressHUDMaskTypeBlack];
}

+(void)svSuccess :(NSString *)str
{
   // [SVProgressHUD showErrorWithStatus:str];
    
    [SVProgressHUD showSuccessWithStatus:str maskType:SVProgressHUDMaskTypeBlack];
}

+(void)svProgress :(NSString *)str
{
    [SVProgressHUD showWithStatus:str maskType:SVProgressHUDMaskTypeBlack];
}


#pragma mark - Alert With Message With Go Back Or not
+(void)alertWithMessage:(NSString*)message navigation:(UINavigationController*)navigation gotoBack:(BOOL)goBack animation:(BOOL)animation{
    UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@""
                                                          message:message
                                                         delegate:nil
                                                cancelButtonTitle:nil
                                                otherButtonTitles: nil];
    
    [myAlertView show];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [myAlertView dismissWithClickedButtonIndex:0 animated:animation];
        goBack ? [navigation popViewControllerAnimated:YES]  :nil;
    });
}
#pragma mark - Alert With Message With Go Back Or not With Time
+(void)alertWithMessage:(NSString*)message navigation:(UINavigationController*)navigation gotoBack:(BOOL)goBack animation:(BOOL)animation sencond:(int)second{
    UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@""
                                                          message:message
                                                         delegate:nil
                                                cancelButtonTitle:nil
                                                otherButtonTitles: nil];
    
    [myAlertView show];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(second * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [myAlertView dismissWithClickedButtonIndex:0 animated:animation];
        goBack ? [navigation popViewControllerAnimated:YES]  :nil;
    });
}
#pragma Alert global Alert View title
+(void)globalAlertViewtitle:(NSString *)title forMsg:(NSString*)message forOK:(NSString *)Ok navigation:(UINavigationController*)navigation
{
    UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:title
                                                          message:message
                                                         delegate:nil
                                                cancelButtonTitle:Ok
                                                otherButtonTitles: nil];
    
    [myAlertView show];
}
#pragma mark - Alert View Default
+(void)alertViewDefalultWithTitleString:(NSString *)title forMsg:(NSString*)message forOK:(NSString *)Ok{
    
    UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:title
                                                          message:message
                                                         delegate:nil
                                                cancelButtonTitle:Ok
                                                otherButtonTitles: nil];
    
    [myAlertView show];
}
#pragma mark - alertWithMessageNew
+(void)alertWithMessageNew:(NSString *)title forMsg:(NSString*)message forOK:(NSString *)Ok navigation:(UINavigationController*)navigation{

UIAlertController * alert=   [UIAlertController
                              alertControllerWithTitle:title
                              message:message
                              preferredStyle:UIAlertControllerStyleAlert];


UIAlertAction* noButton = [UIAlertAction
                           actionWithTitle:Ok
                           style:UIAlertActionStyleDefault
                           handler:^(UIAlertAction * action)
                           {
                               
                               [alert dismissViewControllerAnimated:YES completion:nil];
                               
                           }];


[alert addAction:noButton];

[navigation presentViewController:alert animated:YES completion:nil];

    
}
#pragma mark - Alert Contoller
+(void)alertControllerTitle:(NSString *)title msg:(NSString*)msg ok:(NSString *)ok controller:(UINavigationController *)contoller
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:msg
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *actionOk = [UIAlertAction actionWithTitle:ok style:UIAlertActionStyleDefault handler:nil];
    
    [alertController addAction:actionOk];
    [contoller presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - performBlockWithInterval
+(void)performBlockWithInterval:(double)interval completion:(void(^)(void))completion{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(interval * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        completion();
        
    });
}


#pragma mark - validationName
+(BOOL)validationName:(NSString *)checkString{
    
    NSString *_username = checkString;
    
    NSCharacterSet * characterSetFromTextField = [NSCharacterSet characterSetWithCharactersInString: _username];
    if([[NSCharacterSet alphanumericCharacterSet] isSupersetOfSet: characterSetFromTextField] == NO)
    {
        //NSLog( @"there are bogus characters here, throw up a UIAlert at this point");
        return NO;
    }
    return YES;

   
}
#pragma mark - validationEmail
+(BOOL)validationEmail:(NSString *)checkString{
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
#pragma mark - validateMobileNumber
+(BOOL)validateMobileNumber:(NSString*)number{
    NSString *numberRegEx = @"[0-9]{10}";
    NSPredicate *numberTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", numberRegEx];
    if ([numberTest evaluateWithObject:number] == YES)
        return TRUE;
    else
        return FALSE;
}
#pragma mark - validatePinCode
+(BOOL)validatePinCode:(NSString*)number{
    NSString *numberRegEx = @"[0-9]{5}";
    NSPredicate *numberTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", numberRegEx];
    if ([numberTest evaluateWithObject:number] == YES)
        return TRUE;
    else
        return FALSE;
}
#pragma mark - validateNumber
+(BOOL)validateNumber:(NSString*)number{
    NSScanner *scanner = [NSScanner scannerWithString:number];
    BOOL isNumeric = [scanner scanInteger:NULL] && [scanner isAtEnd];
    return isNumeric;
}
+(void)makePhoneCall:(NSString *) phoneNumber
{
    NSURL *dialingURL = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"telprompt:%@",phoneNumber]];
    [[UIApplication sharedApplication] openURL:dialingURL];
}
#pragma mark - ValidateTextFieldValidate
+(BOOL)ValidateTextFieldValidate:(UITextField *)textField andLenght:(int)textlenght
{
    if ([textField.text length] > textlenght)
    {
        //do your work
        
        return YES;
    }
    else
    {
        //through error
        return NO;
    }
}
#pragma mark - ValidateTextFieldValidate
+(BOOL)validateTextView:(UITextView *)textView andLenght:(int)textlenght
{
    if ([textView.text length] > textlenght)
    {
        //do your work
        
        return YES;
    }
    else
    {
        //through error
        return NO;
    }
}
#pragma mark - isPasswordMatch or not
+(BOOL)isPasswordMatch:(NSString *)pwd withConfirmPwd:(NSString *)cnfPwd
{
    //asume pwd and cnfPwd has not whitespace
    if([pwd length]>0 && [cnfPwd length]>0){
        if([pwd isEqualToString:cnfPwd]){
            NSLog(@"Hurray! Password matches ");
            return YES;
        }else{
            NSLog(@"Oops! Password does not matches");
            return NO;
        }
    }else{
        NSLog(@"Password field can not be empty ");
        return NO;
    }
    return NO;
}
#pragma mark - isStateSelectOrNot
+(BOOL)isStateSelectOrNot:(NSString *)string andyourStr:(NSString *)yourStr
{
    if(![string isEqualToString:yourStr])
    {
        // passwords are equal
        return YES;
    }
    else
    {
        return NO;
    }
}
+(BOOL) checkIfUsernameValidation:(NSString *)text
{
    NSString *_username = text;
    
    NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@".!#$%&'*+-/=?^_`{|}~@,;"] invertedSet];
    
    if ([_username rangeOfCharacterFromSet:set].location == NSNotFound)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}
#pragma mark - colorFromHexString
// Assumes input like "#00FF00" (#RRGGBB).
+ (UIColor *)colorFromHexString:(NSString *)hexString
{
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}
#pragma mark - setProgessView
+(void)addIndicatorView:(UIView*)view color:(UIColor *)color
{

    UIActivityIndicatorView *indicatorView=[[UIActivityIndicatorView alloc] init];
    indicatorView.activityIndicatorViewStyle=UIActivityIndicatorViewStyleWhite;
    [indicatorView startAnimating];
    indicatorView.frame=CGRectMake(0,0, 40, 40);
    indicatorView.backgroundColor = [UIColor clearColor];
    indicatorView.color = color;
    indicatorView.autoresizesSubviews = YES;
    indicatorView.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin |
                                      UIViewAutoresizingFlexibleRightMargin |
                                      UIViewAutoresizingFlexibleTopMargin |
                                      UIViewAutoresizingFlexibleBottomMargin);
    
    
    
    UIView *v=[[UIView alloc] init];
    v.frame=view.bounds;
    v.tag=-2000;
   // v.backgroundColor= [Alert colorWithPatternImage2:kBG_IMAGE]; //[UIColor clearColor];
    v.backgroundColor=[UIColor clearColor];
    v.autoresizesSubviews = YES;
    v.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    indicatorView.center = v.center;

    [v addSubview:indicatorView];

    [view bringSubviewToFront:v];
    [view addSubview:v];
    
}
#pragma mark - CloseProgress
+(void)removeView:(UIView*)yourView
{
    for (UIView *view in [yourView subviews] )
    {
        if (view.tag==-2000)
        {
            [view removeFromSuperview];
        }
    }
}
#pragma mark image Scaling
+(UIImage *)scaleAndRotateImage:(UIImage *)image
{
    int kMaxResolution = 375; // Or whatever
    
    CGImageRef imgRef = image.CGImage;
    
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    if (width > kMaxResolution || height > kMaxResolution) {
        CGFloat ratio = width/height;
        if (ratio > 1) {
            bounds.size.width = kMaxResolution;
            bounds.size.height = bounds.size.width / ratio;
        }
        else {
            bounds.size.height = kMaxResolution;
            bounds.size.width = bounds.size.height * ratio;
        }
    }
    
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
    switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }
    
    UIGraphicsBeginImageContext(bounds.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageCopy;
}
#pragma mark - reachability .......
+(BOOL)networkStatus
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    
    return networkStatus;
}
+(NSString*)getFirstName:(NSString*)name
{
    
    NSString *firstWord = [[name componentsSeparatedByString:@" "] objectAtIndex:0];
    return firstWord;
}
+(NSDateFormatter*)getDateFormatWithString:(NSString*)string
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:string];
    return df;
    
}
+(NSDate*)getDateWithDateString:(NSString*)dateString setFormat:(NSString*)format
{
    
    NSDate* date= [[self getDateFormatWithString:format] dateFromString:dateString];
    return date;
}
+(NSString*)getDateWithString:(NSString*)string getFormat:(NSString*)format1 setFormat:(NSString*)format2
{
    
    NSDate* date= [[self getDateFormatWithString:format1] dateFromString:string];
    NSString *dateString = [[self getDateFormatWithString:format2] stringFromDate:date];
    
    return dateString;
    
}
#pragma mark - Get Counrty List
+(NSArray *)getCounrtyName
{
    NSArray * arrayCountry = [[NSArray alloc]initWithObjects:@"Afghanistan",
                              @"Albania",
                              @"Algeria",
                              @"Andorra",
                              @"Angola",
                              @"Antigua and Barbuda",
                              @"Argentina",
                              @"Armenia",
                              @"Australia",
                              @"Austria",
                              @"Azerbaijan",
                              @"Bahamas",
                              @"Bahrain",
                              @"Bangladesh",
                              @"Barbados",
                              @"Belarus",
                              @"Belgium",
                              @"Belize",
                              @"Benin",
                              @"Bhutan",
                              @"Bolivia",
                              @"Bosnia and Herzegovina",
                              @"Botswana",
                              @"Brazil",
                              @"Brunei",
                              @"Bulgaria",
                              @"Burkina Faso",
                              @"Burundi",
                              @"Cabo Verde",
                              @"Cambodia",
                              @"Cameroon",
                              @"Canada",
                              @"Central African Republic",
                              @"Chad",
                              @"Chile",
                              @"China",
                              @"Colombia",
                              @"Comoros",
                              @"Congo, Republic of the",
                              @"Congo, Democratic Republic of the",
                              @"Costa Rica",
                              @"Cote d'Ivoire",
                              @"Croatia",
                              @"Cuba",
                              @"Cyprus",
                              @"Czech Republic",
                              @"Denmark",
                              @"Djibouti",
                              @"Dominica",
                              @"Dominican Republic",
                              @"Ecuador",
                              @"Egypt",
                              @"El Salvador",
                              @"Equatorial Guinea",
                              @"Eritrea",
                              @"Estonia",
                              @"Ethiopia",
                              @"Fiji",
                              @"Finland",
                              @"France",
                              @"Gabon",
                              @"Gambia",
                              @"Georgia",
                              @"Germany",
                              @"Ghana",
                              @"Greece",
                              @"Grenada",
                              @"Guatemala",
                              @"Guinea",
                              @"Guinea-Bissau",
                              @"Guyana",
                              @"Haiti",
                              @"Honduras",
                              @"Hungary",
                              @"Iceland",
                              @"India",
                              @"Indonesia",
                              @"Iran",
                              @"Iraq",
                              @"Ireland",
                              @"Israel",
                              @"Italy",
                              @"Jamaica",
                              @"Japan",
                              @"Jordan",
                              @"Kazakhstan",
                              @"Kenya",
                              @"Kiribati",
                              @"Kosovo",
                              @"Kuwait",
                              @"Kyrgyzstan",
                              @"Laos",
                              @"Latvia",
                              @"Lebanon",
                              @"Lesotho",
                              @"Liberia",
                              @"Libya",
                              @"Liechtenstein",
                              @"Lithuania",
                              @"Luxembourg",
                              @"Macedonia",
                              @"Madagascar",
                              @"Malawi",
                              @"Malaysia",
                              @"Maldives",
                              @"Mali",
                              @"Malta",
                              @"Marshall Islands",
                              @"Mauritania",
                              @"Mauritius",
                              @"Mexico",
                              @"Micronesia",
                              @"Moldova",
                              @"Monaco",
                              @"Mongolia",
                              @"Montenegro",
                              @"Morocco",
                              @"Mozambique",
                              @"Myanmar (Burma)",
                              @"Namibia",
                              @"Nauru",
                              @"Nepal",
                              @"Netherlands",
                              @"New Zealand",
                              @"Nicaragua",
                              @"Niger",
                              @"Nigeria",
                              @"North Korea",
                              @"Norway",
                              @"Oman",
                              @"Pakistan",
                              @"Palau",
                              @"Palestine",
                              @"Panama",
                              @"Papua New Guinea",
                              @"Paraguay",
                              @"Peru",
                              @"Philippines",
                              @"Poland",
                              @"Portugal",
                              @"Qatar",
                              @"Romania",
                              @"Russia",
                              @"Rwanda",
                              @"St. Kitts and Nevis",
                              @"St. Lucia",
                              @"St. Vincent and The Grenadines",
                              @"Samoa",
                              @"San Marino",
                              @"Sao Tome and Principe",
                              @"Saudi Arabia",
                              @"Senegal",
                              @"Serbia",
                              @"Seychelles",
                              @"Sierra Leone",
                              @"Singapore",
                              @"Slovakia",
                              @"Slovenia",
                              @"Solomon Islands",
                              @"Somalia",
                              @"South Africa",
                              @"South Korea",
                              @"South Sudan",
                              @"Spain",
                              @"Sri Lanka"
                              @"Sudan",
                              @"Suriname",
                              @"Swaziland",
                              @"Sweden",
                              @"Switzerland",
                              @"Syria",
                              @"Taiwan",
                              @"Tajikistan",
                              @"Tanzania",
                              @"Thailand",
                              @"Timor-Leste",
                              @"Togo",
                              @"Tonga",
                              @"Trinidad and Tobago",
                              @"Tunisia",
                              @"Turkey",
                              @"Turkmenistan",
                              @"Tuvalu",
                              @"Uganda",
                              @"Ukraine",
                              @"United Arab Emirates",
                              @"UK (United Kingdom)",
                              @"USA (United States of America)",
                              @"Uruguay",
                              @"Uzbekistan",
                              @"Vanuatu",
                              @"Vatican City (Holy See)",
                              @"Venezuela",
                              @"Vietnam",
                              @"Yemen",
                              @"Zambia",
                              @"Zimbabwe",
                              
                              nil];
    
    return arrayCountry;
}
#pragma mark - get All Country Name With Code List
+(NSDictionary*)getAllCountryNameWithCodeList
{
    // Country code
    NSDictionary *codes = @{
                            @"Canada"                                       : @"+1",
                            @"China"                                        : @"+86",
                            @"France"                                       : @"+33",
                            @"Germany"                                      : @"+49",
                            @"India"                                        : @"+91",
                            @"Japan"                                        : @"+81",
                            @"Pakistan"                                     : @"+92",
                            @"United Kingdom"                               : @"+44",
                            @"United States"                                : @"+1",
                            @"Abkhazia"                                     : @"+7 840",
                            @"Abkhazia"                                     : @"+7 940",
                            @"Afghanistan"                                  : @"+93",
                            @"Albania"                                      : @"+355",
                            @"Algeria"                                      : @"+213",
                            @"American Samoa"                               : @"+1 684",
                            @"Andorra"                                      : @"+376",
                            @"Angola"                                       : @"+244",
                            @"Anguilla"                                     : @"+1 264",
                            @"Antigua and Barbuda"                          : @"+1 268",
                            @"Argentina"                                    : @"+54",
                            @"Armenia"                                      : @"+374",
                            @"Aruba"                                        : @"+297",
                            @"Ascension"                                    : @"+247",
                            @"Australia"                                    : @"+61",
                            @"Australian External Territories"              : @"+672",
                            @"Austria"                                      : @"+43",
                            @"Azerbaijan"                                   : @"+994",
                            @"Bahamas"                                      : @"+1 242",
                            @"Bahrain"                                      : @"+973",
                            @"Bangladesh"                                   : @"+880",
                            @"Barbados"                                     : @"+1 246",
                            @"Barbuda"                                      : @"+1 268",
                            @"Belarus"                                      : @"+375",
                            @"Belgium"                                      : @"+32",
                            @"Belize"                                       : @"+501",
                            @"Benin"                                        : @"+229",
                            @"Bermuda"                                      : @"+1 441",
                            @"Bhutan"                                       : @"+975",
                            @"Bolivia"                                      : @"+591",
                            @"Bosnia and Herzegovina"                       : @"+387",
                            @"Botswana"                                     : @"+267",
                            @"Brazil"                                       : @"+55",
                            @"British Indian Ocean Territory"               : @"+246",
                            @"British Virgin Islands"                       : @"+1 284",
                            @"Brunei"                                       : @"+673",
                            @"Bulgaria"                                     : @"+359",
                            @"Burkina Faso"                                 : @"+226",
                            @"Burundi"                                      : @"+257",
                            @"Cambodia"                                     : @"+855",
                            @"Cameroon"                                     : @"+237",
                            @"Canada"                                       : @"+1",
                            @"Cape Verde"                                   : @"+238",
                            @"Cayman Islands"                               : @"+ 345",
                            @"Central African Republic"                     : @"+236",
                            @"Chad"                                         : @"+235",
                            @"Chile"                                        : @"+56",
                            @"China"                                        : @"+86",
                            @"Christmas Island"                             : @"+61",
                            @"Cocos-Keeling Islands"                        : @"+61",
                            @"Colombia"                                     : @"+57",
                            @"Comoros"                                      : @"+269",
                            @"Congo"                                        : @"+242",
                            @"Congo, Dem. Rep. of (Zaire)"                  : @"+243",
                            @"Cook Islands"                                 : @"+682",
                            @"Costa Rica"                                   : @"+506",
                            @"Ivory Coast"                                  : @"+225",
                            @"Croatia"                                      : @"+385",
                            @"Cuba"                                         : @"+53",
                            @"Curacao"                                      : @"+599",
                            @"Cyprus"                                       : @"+537",
                            @"Czech Republic"                               : @"+420",
                            @"Denmark"                                      : @"+45",
                            @"Diego Garcia"                                 : @"+246",
                            @"Djibouti"                                     : @"+253",
                            @"Dominica"                                     : @"+1 767",
                            @"Dominican Republic"                           : @"+1 809",
                            @"Dominican Republic"                           : @"+1 829",
                            @"Dominican Republic"                           : @"+1 849",
                            @"East Timor"                                   : @"+670",
                            @"Easter Island"                                : @"+56",
                            @"Ecuador"                                      : @"+593",
                            @"Egypt"                                        : @"+20",
                            @"El Salvador"                                  : @"+503",
                            @"Equatorial Guinea"                            : @"+240",
                            @"Eritrea"                                      : @"+291",
                            @"Estonia"                                      : @"+372",
                            @"Ethiopia"                                     : @"+251",
                            @"Falkland Islands"                             : @"+500",
                            @"Faroe Islands"                                : @"+298",
                            @"Fiji"                                         : @"+679",
                            @"Finland"                                      : @"+358",
                            @"France"                                       : @"+33",
                            @"French Antilles"                              : @"+596",
                            @"French Guiana"                                : @"+594",
                            @"French Polynesia"                             : @"+689",
                            @"Gabon"                                        : @"+241",
                            @"Gambia"                                       : @"+220",
                            @"Georgia"                                      : @"+995",
                            @"Germany"                                      : @"+49",
                            @"Ghana"                                        : @"+233",
                            @"Gibraltar"                                    : @"+350",
                            @"Greece"                                       : @"+30",
                            @"Greenland"                                    : @"+299",
                            @"Grenada"                                      : @"+1 473",
                            @"Guadeloupe"                                   : @"+590",
                            @"Guam"                                         : @"+1 671",
                            @"Guatemala"                                    : @"+502",
                            @"Guinea"                                       : @"+224",
                            @"Guinea-Bissau"                                : @"+245",
                            @"Guyana"                                       : @"+595",
                            @"Haiti"                                        : @"+509",
                            @"Honduras"                                     : @"+504",
                            @"Hong Kong SAR China"                          : @"+852",
                            @"Hungary"                                      : @"+36",
                            @"Iceland"                                      : @"+354",
                            @"India"                                        : @"+91",
                            @"Indonesia"                                    : @"+62",
                            @"Iran"                                         : @"+98",
                            @"Iraq"                                         : @"+964",
                            @"Ireland"                                      : @"+353",
                            @"Israel"                                       : @"+972",
                            @"Italy"                                        : @"+39",
                            @"Jamaica"                                      : @"+1 876",
                            @"Japan"                                        : @"+81",
                            @"Jordan"                                       : @"+962",
                            @"Kazakhstan"                                   : @"+7 7",
                            @"Kenya"                                        : @"+254",
                            @"Kiribati"                                     : @"+686",
                            @"North Korea"                                  : @"+850",
                            @"South Korea"                                  : @"+82",
                            @"Kuwait"                                       : @"+965",
                            @"Kyrgyzstan"                                   : @"+996",
                            @"Laos"                                         : @"+856",
                            @"Latvia"                                       : @"+371",
                            @"Lebanon"                                      : @"+961",
                            @"Lesotho"                                      : @"+266",
                            @"Liberia"                                      : @"+231",
                            @"Libya"                                        : @"+218",
                            @"Liechtenstein"                                : @"+423",
                            @"Lithuania"                                    : @"+370",
                            @"Luxembourg"                                   : @"+352",
                            @"Macau SAR China"                              : @"+853",
                            @"Macedonia"                                    : @"+389",
                            @"Madagascar"                                   : @"+261",
                            @"Malawi"                                       : @"+265",
                            @"Malaysia"                                     : @"+60",
                            @"Maldives"                                     : @"+960",
                            @"Mali"                                         : @"+223",
                            @"Malta"                                        : @"+356",
                            @"Marshall Islands"                             : @"+692",
                            @"Martinique"                                   : @"+596",
                            @"Mauritania"                                   : @"+222",
                            @"Mauritius"                                    : @"+230",
                            @"Mayotte"                                      : @"+262",
                            @"Mexico"                                       : @"+52",
                            @"Micronesia"                                   : @"+691",
                            @"Midway Island"                                : @"+1 808",
                            @"Micronesia"                                   : @"+691",
                            @"Moldova"                                      : @"+373",
                            @"Monaco"                                       : @"+377",
                            @"Mongolia"                                     : @"+976",
                            @"Montenegro"                                   : @"+382",
                            @"Montserrat"                                   : @"+1664",
                            @"Morocco"                                      : @"+212",
                            @"Myanmar"                                      : @"+95",
                            @"Namibia"                                      : @"+264",
                            @"Nauru"                                        : @"+674",
                            @"Nepal"                                        : @"+977",
                            @"Netherlands"                                  : @"+31",
                            @"Netherlands Antilles"                         : @"+599",
                            @"Nevis"                                        : @"+1 869",
                            @"New Caledonia"                                : @"+687",
                            @"New Zealand"                                  : @"+64",
                            @"Nicaragua"                                    : @"+505",
                            @"Niger"                                        : @"+227",
                            @"Nigeria"                                      : @"+234",
                            @"Niue"                                         : @"+683",
                            @"Norfolk Island"                               : @"+672",
                            @"Northern Mariana Islands"                     : @"+1 670",
                            @"Norway"                                       : @"+47",
                            @"Oman"                                         : @"+968",
                            @"Pakistan"                                     : @"+92",
                            @"Palau"                                        : @"+680",
                            @"Palestinian Territory"                        : @"+970",
                            @"Panama"                                       : @"+507",
                            @"Papua New Guinea"                             : @"+675",
                            @"Paraguay"                                     : @"+595",
                            @"Peru"                                         : @"+51",
                            @"Philippines"                                  : @"+63",
                            @"Poland"                                       : @"+48",
                            @"Portugal"                                     : @"+351",
                            @"Puerto Rico"                                  : @"+1 787",
                            @"Puerto Rico"                                  : @"+1 939",
                            @"Qatar"                                        : @"+974",
                            @"Reunion"                                      : @"+262",
                            @"Romania"                                      : @"+40",
                            @"Russia"                                       : @"+7",
                            @"Rwanda"                                       : @"+250",
                            @"Samoa"                                        : @"+685",
                            @"San Marino"                                   : @"+378",
                            @"Saudi Arabia"                                 : @"+966",
                            @"Senegal"                                      : @"+221",
                            @"Serbia"                                       : @"+381",
                            @"Seychelles"                                   : @"+248",
                            @"Sierra Leone"                                 : @"+232",
                            @"Singapore"                                    : @"+65",
                            @"Slovakia"                                     : @"+421",
                            @"Slovenia"                                     : @"+386",
                            @"Solomon Islands"                              : @"+677",
                            @"South Africa"                                 : @"+27",
                            @"South Georgia and the South Sandwich Islands" : @"+500",
                            @"Spain"                                        : @"+34",
                            @"Sri Lanka"                                    : @"+94",
                            @"Sudan"                                        : @"+249",
                            @"Suriname"                                     : @"+597",
                            @"Swaziland"                                    : @"+268",
                            @"Sweden"                                       : @"+46",
                            @"Switzerland"                                  : @"+41",
                            @"Syria"                                        : @"+963",
                            @"Taiwan"                                       : @"+886",
                            @"Tajikistan"                                   : @"+992",
                            @"Tanzania"                                     : @"+255",
                            @"Thailand"                                     : @"+66",
                            @"Timor Leste"                                  : @"+670",
                            @"Togo"                                         : @"+228",
                            @"Tokelau"                                      : @"+690",
                            @"Tonga"                                        : @"+676",
                            @"Trinidad and Tobago"                          : @"+1 868",
                            @"Tunisia"                                      : @"+216",
                            @"Turkey"                                       : @"+90",
                            @"Turkmenistan"                                 : @"+993",
                            @"Turks and Caicos Islands"                     : @"+1 649",
                            @"Tuvalu"                                       : @"+688",
                            @"Uganda"                                       : @"+256",
                            @"Ukraine"                                      : @"+380",
                            @"United Arab Emirates"                         : @"+971",
                            @"United Kingdom"                               : @"+44",
                            @"United States"                                : @"+1",
                            @"Uruguay"                                      : @"+598",
                            @"U.S. Virgin Islands"                          : @"+1 340",
                            @"Uzbekistan"                                   : @"+998",
                            @"Vanuatu"                                      : @"+678",
                            @"Venezuela"                                    : @"+58",
                            @"Vietnam"                                      : @"+84",
                            @"Wake Island"                                  : @"+1 808",
                            @"Wallis and Futuna"                            : @"+681",
                            @"Yemen"                                        : @"+967",
                            @"Zambia"                                       : @"+260",
                            @"Zanzibar"                                     : @"+255",
                            @"Zimbabwe"                                     : @"+263"
                            };
    
    return codes;
}
#pragma mark - Get Language Name list
+(NSArray*)getLanguageNamelist
{
    NSArray* language=[[NSArray alloc]initWithObjects:
                       @"English (U.S.)",
                       @"English (UK)",
                       @"French (France)",
                       @"German",
                       @"Traditional Chinese",
                       @"Simplified Chinese",
                       @"Dutch",
                       @"Italian",
                       @"Spanish",
                       @"Portuguese (Brazil)",
                       @"Portuguese (Portugal)",
                       @"Danish",
                       @"Swedish",
                       @"Finnish",
                       @"Norwegian",
                       @"Korean",
                       @"Japanese",
                       @"Russian",
                       @"Polish",
                       @"Turkish",
                       @"Ukrainian",
                       @"Hungarian",
                       @"Arabic",
                       @"Thai",
                       @"Czech",
                       @"Greek",
                       @"Hebrew",
                       @"Indonesi",
                       @"Malay",
                       @"Romanian",
                       @"Slovak",
                       @"Croatian",
                       @"Catalan",
                       @"Vietnamese"
                       , nil];
    return language;
}
#pragma mark - removeViewControllFromNavArray
+(NSMutableArray *)removeViewControllFromNavArray:(int)number navigation:(UINavigationController*)class
{
    
    NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: class.viewControllers];
    int count=0;
    if(number<navigationArray.count)
        for (int i= (int)navigationArray.count; i>=1; i--) {
            
            if(count==number)   break;
            
            [navigationArray removeLastObject];
            
            count++;
        }
    return navigationArray;
    
}
#pragma mark - View With CLLayer
+(void)viewCALayer:(UIColor *)yourColor viewButton:(UIView *)yourView
{
    yourView.layer.cornerRadius= 4.0F;
    yourView.layer.masksToBounds=YES;
    yourView.layer.borderColor=[yourColor CGColor];
    yourView.layer.borderWidth= 1.0f;
    
    
}
#pragma mark - UIView With Layer Color And Width
+(void)viewWithLayerColorAndWidth:(UIColor *)yourColor view:(UIView *)yourView radius:(CGFloat)radius border:(CGFloat)border
{
    yourView.layer.cornerRadius= radius;
    yourView.layer.masksToBounds=YES;
    yourView.layer.borderColor=[yourColor CGColor];
    yourView.layer.borderWidth= border;
    
}
#pragma mark - UIButton With Layer Color And Width
+(void)buttonWithLayerColorAndWidth:(UIColor *)color button:(UIButton *)button radius:(CGFloat)radius border:(CGFloat)border
{
    button.layer.cornerRadius= radius;
    button.layer.masksToBounds=YES;
    button.layer.borderColor=[color CGColor];
    button.layer.borderWidth= border;
    
}


#pragma mark - CL Layar With Color On UIButton
+(void)viewButtonCALayer:(UIColor *)yourColor viewButton:(UIButton *)yourButton
{
    yourButton.layer.cornerRadius= 4.0F;
    yourButton.layer.masksToBounds=YES;
    yourButton.layer.borderColor=[yourColor CGColor];
    yourButton.layer.borderWidth= 1.0f;

}
#pragma mark - bv_jsonStringWithDictionary
+(NSString*) bv_jsonStringWithDictionary:(NSDictionary*)dictionary
{
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictionary
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    
    if (! jsonData) {
        NSLog(@"bv_jsonStringWithPrettyPrint: error: %@", error.localizedDescription);
        return @"{}";
    } else {
        return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
}
#pragma mark - Get String From Dictionary
+(NSString*)jsonStringWithDictionary:(NSDictionary*)data{
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:data
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    if (! jsonData)
    {
        NSLog(@"Got an error: %@", error);
        return nil;
    } else
    {
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        NSLog(@"jsonString->%@",jsonString);
        return jsonString;
    }
}
#pragma mark - Get Dictionary With Json String
+(NSDictionary*)getDictionaryWithJsonString:(NSString*)string{
    NSError *error;
    
    NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data
                                                                 options:kNilOptions
                                                                   error:&error];
    
    return jsonResponse;
}
#pragma mark - Get Token From devices
+(NSString*)getDeviceToken:(NSData*)deviceToken{
    
    NSString *token = [[deviceToken description] stringByTrimmingCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSLog(@"*------------------------------*");
    NSLog(@"Device Token---%@", token);
    NSLog(@"*------------------------------*");
    return token;
}
#pragma mark - Get Image Form Color
+ (UIImage *)imageFromColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}
#pragma mark -  Change Image Color Which is Randring on View.
+(void)changeImageColor:(NSString *)imageName imageView:(UIImageView *)imageView andColor:(UIColor *)yourColor
{
    UIImage *image = [UIImage imageNamed:imageName];
    imageView.image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [imageView setTintColor: yourColor];
}

#pragma mark -  Change Image Color Which is Randring on View.
+(void)changeImageColorURL:(NSURL *)urlImage imageView:(UIImageView *)imageView andColor:(UIColor *)yourColor
{

    dispatch_async(dispatch_get_global_queue(0,0), ^{
        NSData * data = [[NSData alloc] initWithContentsOfURL:urlImage];
        if ( data == nil )
            return;
        dispatch_async(dispatch_get_main_queue(), ^{
            // WARNING: is the cell still using the same data by this point??
            UIImage * image  = [UIImage imageWithData: data];
            
            imageView.image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
            [imageView setTintColor: yourColor];
        });
    });
}
#pragma mark - Set ToolBar On KeyBoard TableView
+(void)SetToolBarONKeyBoard:(UITableView *)view andFields:(NSArray *)arrayFields
{
    PNTToolbar *toolbar = [PNTToolbar defaultToolbar];
    toolbar.mainScrollView = view;
    toolbar.inputFields = arrayFields;
}

#pragma mark - Set ToolBar On KeyBoard TableView
+(void)SetToolBarONKeyBoardScrollView:(UIScrollView *)view andFields:(NSArray *)arrayFields
{
    PNTToolbar *toolbar = [PNTToolbar defaultToolbar];
    toolbar.mainScrollView = view;
    toolbar.inputFields = arrayFields;
}
#pragma mark - Set ToolBar On KeyBoard
+(void)setKeyBoardToolbar:(NSArray*)arrayFields;
{
    PNTToolbar *toolbar = [PNTToolbar defaultToolbar];
    toolbar.inputFields = arrayFields;
}
#pragma mark - Device type
+ (NSString*) deviceName
{
    struct utsname systemInfo;
    
    uname(&systemInfo);
    
    NSString* code = [NSString stringWithCString:systemInfo.machine
                                        encoding:NSUTF8StringEncoding];
    
    static NSDictionary* deviceNamesByCode = nil;
    
    if (!deviceNamesByCode) {
        
        deviceNamesByCode = @{@"i386"      :@"Simulator",
                              @"x86_64"    :@"Simulator",
                              @"iPod1,1"   :@"iPod Touch",      // (Original)
                              @"iPod2,1"   :@"iPod Touch",      // (Second Generation)
                              @"iPod3,1"   :@"iPod Touch",      // (Third Generation)
                              @"iPod4,1"   :@"iPod Touch",      // (Fourth Generation)
                              @"iPod7,1"   :@"iPod Touch",      // (6th Generation)
                              @"iPhone1,1" :@"iPhone",          // (Original)
                              @"iPhone1,2" :@"iPhone",          // (3G)
                              @"iPhone2,1" :@"iPhone",          // (3GS)
                              @"iPad1,1"   :@"iPad",            // (Original)
                              @"iPad2,1"   :@"iPad 2",          //
                              @"iPad3,1"   :@"iPad",            // (3rd Generation)
                              @"iPhone3,1" :@"iPhone 4",        // (GSM)
                              @"iPhone3,3" :@"iPhone 4",        // (CDMA/Verizon/Sprint)
                              @"iPhone4,1" :@"iPhone 4S",       //
                              @"iPhone5,1" :@"iPhone 5",        // (model A1428, AT&T/Canada)
                              @"iPhone5,2" :@"iPhone 5",        // (model A1429, everything else)
                              @"iPad3,4"   :@"iPad",            // (4th Generation)
                              @"iPad2,5"   :@"iPad Mini",       // (Original)
                              @"iPhone5,3" :@"iPhone 5c",       // (model A1456, A1532 | GSM)
                              @"iPhone5,4" :@"iPhone 5c",       // (model A1507, A1516, A1526 (China), A1529 | Global)
                              @"iPhone6,1" :@"iPhone 5s",       // (model A1433, A1533 | GSM)
                              @"iPhone6,2" :@"iPhone 5s",       // (model A1457, A1518, A1528 (China), A1530 | Global)
                              @"iPhone7,1" :@"iPhone 6 Plus",   //
                              @"iPhone7,2" :@"iPhone 6",        //
                              @"iPhone8,1" :@"iPhone 6S",       //
                              @"iPhone8,2" :@"iPhone 6S Plus",  //
                              @"iPad4,1"   :@"iPad Air",        // 5th Generation iPad (iPad Air) - Wifi
                              @"iPad4,2"   :@"iPad Air",        // 5th Generation iPad (iPad Air) - Cellular
                              @"iPad4,4"   :@"iPad Mini",       // (2nd Generation iPad Mini - Wifi)
                              @"iPad4,5"   :@"iPad Mini",       // (2nd Generation iPad Mini - Cellular)
                              @"iPad4,7"   :@"iPad Mini"        // (3rd Generation iPad Mini - Wifi (model A1599))
                              };
    }
    
    NSString* deviceName = [deviceNamesByCode objectForKey:code];
    
    if (!deviceName) {
        // Not found on database. At least guess main device type from string contents:
        
        if ([code rangeOfString:@"iPod"].location != NSNotFound) {
            deviceName = @"iPod Touch";
        }
        else if([code rangeOfString:@"iPad"].location != NSNotFound) {
            deviceName = @"iPad";
        }
        else if([code rangeOfString:@"iPhone"].location != NSNotFound){
            deviceName = @"iPhone";
        }
        else {
            deviceName = @"Unknown";
        }
    }
    
    return deviceName;
}
#pragma mark - Get full Path URl
+(NSString *)getFullServerUrlStr:(NSString*)baseUrl api:(NSString *)api
{
    NSString*url = [baseUrl stringByAppendingString:api];
    
    return url;
}
#pragma mark - Shadow on UIButton
+(void)shadowOnButtons:(UIButton*)sender{
    
    UIView* view;
    
    view = [[UIView alloc] initWithFrame:sender.bounds];
    view.backgroundColor = [UIColor clearColor];
    
    view.layer.shadowPath = [UIBezierPath bezierPathWithRoundedRect:sender.frame
                                                       cornerRadius:sender.frame.size.height/2].CGPath;
    view.layer.shadowColor = [UIColor blackColor].CGColor;
    view.layer.shadowRadius = 3;
    view.layer.shadowOffset = CGSizeMake(0, 5);
    view.layer.shadowOpacity = .25;
    view.layer.cornerRadius = sender.frame.size.height/2;
    
    [sender.superview insertSubview:view belowSubview:sender];
    
    
}
+(BOOL)isValidPassword:(NSString *)passwordString
{
    NSString *stricterFilterString = @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*?&])[A-Za-z\\d$@$!%*?&]{10,}";
    NSPredicate *passwordTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", stricterFilterString];
    return [passwordTest evaluateWithObject:passwordString];
}
+ (BOOL)validate:(NSString *)string
{
    NSError *error             = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:
                                  @"[a-zA-Z ]" options:0 error:&error];
    
    NSUInteger numberOfMatches = [regex numberOfMatchesInString:string options:0 range:NSMakeRange(0, [string length])];
    
    return numberOfMatches == string.length;
}
#pragma mark - date
+(NSString *)date
{
    NSString *dateTimeStamp = [NSString stringWithFormat:@"%lu",(unsigned long)([[NSDate date] timeIntervalSince1970]*10.0)];
    
    return dateTimeStamp;
}
#pragma mark - app Name And VersionNumber DisplayString
+(NSString *)appNameAndVersionNumberDisplayString {
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *appDisplayName = [infoDictionary objectForKey:@"CFBundleDisplayName"];
    NSString *majorVersion = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    NSString *minorVersion = [infoDictionary objectForKey:@"CFBundleVersion"];
    
    return [NSString stringWithFormat:@"%@ \n Version %@ (%@)",
            appDisplayName, majorVersion, minorVersion];
}


#pragma mark- remove null objects
+(NSMutableDictionary *)removeNSNullClass:(NSMutableDictionary *)dict
{
    NSMutableDictionary *mutableDict = [dict mutableCopy];
    for (NSString *key in [dict allKeys]) {
        if ([dict[key] isEqual:[NSNull null]])
        {
            mutableDict[key] = @"";//or [NSNull null] or whatever value you want to change it to
        }
    }
    dict = [mutableDict copy];
    return dict;
}
#pragma mark -  add uilabel on TableView BG
+(void)addLabelBGTableView:(UITableView *)tableView msg:(NSString*)msg color:(UIColor *)color
{
    UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, tableView.bounds.size.height)];
    
    messageLabel.text = msg;
    messageLabel.textColor = color;
    messageLabel.numberOfLines = 0;
    messageLabel.textAlignment = NSTextAlignmentCenter;
    messageLabel.font = [UIFont fontWithName:@"Palatino-Italic" size:18];
    [messageLabel sizeToFit];
    tableView.backgroundView = messageLabel;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

/*
#pragma mark - date deff...
+(NSString *)relativeDateStringForDate:(NSDate *)date
{
    
    NSCalendarUnit units = NSDayCalendarUnit | NSWeekOfYearCalendarUnit |
    NSMonthCalendarUnit | NSYearCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit |NSSecondCalendarUnit ;
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *components1 = [cal components:(NSCalendarUnitEra|NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSHourCalendarUnit|NSMinuteCalendarUnit|NSSecondCalendarUnit) fromDate:[NSDate date]];
    NSDate *today = [cal dateFromComponents:components1];
    
    components1 = [cal components:(NSCalendarUnitEra|NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSHourCalendarUnit|NSMinuteCalendarUnit|NSSecondCalendarUnit) fromDate:date];
    NSDate *thatdate = [cal dateFromComponents:components1];
    
    // if `date` is before "now" (i.e. in the past) then the components will be positive
    NSDateComponents *components = [[NSCalendar currentCalendar] components:units
                                                                   fromDate:thatdate
                                                                     toDate:today
                                                                    options:0];
    
    if (components.year > 0) {
        return [NSString stringWithFormat:@"%ld years ago", (long)components.year];
    } else if (components.month > 0) {
        return [NSString stringWithFormat:@"%ld months ago", (long)components.month];
    } else if (components.weekOfYear > 0) {
        return [NSString stringWithFormat:@"%ld weeks ago", (long)components.weekOfYear];
    } else if (components.day > 0) {
        if (components.day > 1) {
            return [NSString stringWithFormat:@"%ld days ago", (long)components.day];
        } else {
            return @"Yesterday";
        }
    }
    else if(components.hour >0)
    {
        return [NSString stringWithFormat:@"%ld hours ago", (long)components.hour];
    }
    else if (components.minute >0)
    {
        return [NSString stringWithFormat:@"%ld min ago", (long)components.minute];
    }
    else
    {
        return [NSString stringWithFormat:@"%ld sec ago", (long)components.second];
    }
}
 
 */

+(void)placeHoderColor:(UITextField *)txtField color:(UIColor *)color
{
    
    [txtField setValue:color
            forKeyPath:@"placeholderLabel.textColor"];
}
#pragma mark -  "Vedio Thoumnail Image from Youtube"
+(NSString*)getYoutubeVideoThumbnail:(NSString*)youTubeUrl
{
    NSString* video_id = @"";
    
    if (youTubeUrl.length > 0)
    {
        NSError *error = NULL;
        NSRegularExpression *regex =
        [NSRegularExpression regularExpressionWithPattern:@"(?<=watch\\?v=|/videos/|embed\\/)[^#\\&\\?]*"
                                                  options:NSRegularExpressionCaseInsensitive
                                                    error:&error];
        NSTextCheckingResult *match = [regex firstMatchInString:youTubeUrl
                                                        options:0
                                                          range:NSMakeRange(0, [youTubeUrl length])];
        if (match)
        {
            NSRange videoIDRange = [match rangeAtIndex:0];
            video_id = [youTubeUrl substringWithRange:videoIDRange];
            
            NSLog(@"%@",video_id);
        }
    }
    
    NSString* thumbImageUrl = [NSString stringWithFormat:@"http://img.youtube.com/vi/%@/default.jpg",video_id];
    
    return thumbImageUrl;
}
+(UIColor *)colorWithPatternImage2:(UIImage *)image
{
    UIColor * color =  [UIColor colorWithPatternImage:image];
    
    return color;
}

#pragma padding left and right
+(void) setLeftPaddingTextField:(UITextField *)textField paddingValue:(int) paddingValue image:(UIImage *)image
{
    UIView *paddingView     = [[UIView alloc] initWithFrame:CGRectMake(0, 0, paddingValue, textField.frame.size.height)];
    
    UIImageView * imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 24, 24)];
    imageView.center      = paddingView.center;
    imageView.image       = image;
    [paddingView addSubview:imageView];
    textField.leftView      = paddingView;
    
    
    textField.leftViewMode  = UITextFieldViewModeAlways;
}
+(void) setRightPaddingTextField:(UITextField *)textField paddingValue:(int) paddingValue image:(UIImage *)image
{
    UIView *paddingView     = [[UIView alloc] initWithFrame:CGRectMake(0, 0, paddingValue, textField.frame.size.height)];
    
    UIImageView * imageView = [[UIImageView alloc]initWithFrame:CGRectMake(-10, 0, 24, 24)];
    imageView.center      = paddingView.center;
    imageView.image       = image;
    imageView.userInteractionEnabled = NO;
    [paddingView addSubview:imageView];
    textField.rightView = paddingView;
    textField.rightViewMode = UITextFieldViewModeAlways;
}


+(UINavigationController *)navigationControllerWithVC:(UIViewController *)vc
{
    UINavigationController * navigationController  = [[UINavigationController alloc]initWithRootViewController:vc];
    navigationController.modalPresentationStyle= UIModalPresentationFullScreen;
     
    return navigationController;
}


+(BOOL)validateName:(NSString *)text
{
    NSString *Regex = @"[a-zA-Z][a-zA-Z ]*";
    NSPredicate *TestResult = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",Regex];
    
    if ([TestResult evaluateWithObject:text] == true)
    {
        
        // validation passed
        return YES;
    }
    else
    {
        // invalid name
        return NO;
    }
}

+(BOOL)isEmptyStr:(NSString *)str
{
    if(str.length==0 || [str isKindOfClass:[NSNull class]] || [str isEqualToString:@""]||[str  isEqualToString:@"NULL"]||[str isEqualToString:@"(null)"]||str==nil || [str isEqualToString:@"<null>"]){
        return YES;
    }
    return NO;
}


#pragma mark - color image 
+(UIImage *)coloredImage:(UIImage *)firstImage withColor:(UIColor *)color {
    UIGraphicsBeginImageContext(firstImage.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    [color setFill];
    
    CGContextTranslateCTM(context, 0, firstImage.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    
    CGContextSetBlendMode(context, kCGBlendModeCopy);
    CGRect rect = CGRectMake(0, 0, firstImage.size.width, firstImage.size.height);
    CGContextDrawImage(context, rect, firstImage.CGImage);
    
    CGContextClipToMask(context, rect, firstImage.CGImage);
    CGContextAddRect(context, rect);
    CGContextDrawPath(context,kCGPathElementMoveToPoint);
    
    UIImage *coloredImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return coloredImg;
}



+(void)makeLineLayer:(CALayer *)layer lineFromPointA:(CGPoint)pointA toPointB:(CGPoint)pointB
{
    CAShapeLayer *line = [CAShapeLayer layer];
    UIBezierPath *linePath=[UIBezierPath bezierPath];
    [linePath moveToPoint: pointA];
    [linePath addLineToPoint:pointB];
    line.path=linePath.CGPath;
    line.fillColor = nil;
    line.opacity = 1.0;
    line.strokeColor = [UIColor redColor].CGColor;
    [layer addSublayer:line];
 
}

+(void)setUILabelShadow:(UILabel *)label shadowColor:(UIColor*)shadowColor shadowOpacity:(float)shadowOpacity shadowRadius:(float)shadowRadius masksToBounds:(BOOL)masksToBounds
{
    
    label.layer.shadowColor     = shadowColor.CGColor;
    label.layer.shadowOffset    = CGSizeMake(0.0, 2.0f);
    label.layer.shadowOpacity   = shadowOpacity;
    label.layer.shadowRadius    = shadowRadius;
    label.layer.cornerRadius    = 5.0;
    label.clipsToBounds         = YES;
    label.layer.masksToBounds   = masksToBounds;
}



#pragma mark - Draw line.........
+(void)drawLineUsingFrameOnview:(UIView *)view strokeColor:(UIColor *)strokeColor fillColor:(UIColor *)fillColor frame:(CGRect)frame
{
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    shapeLayer.path = [UIBezierPath bezierPathWithRect:frame].CGPath;
    shapeLayer.strokeColor = strokeColor.CGColor;
    shapeLayer.fillColor = fillColor.CGColor;
    shapeLayer.lineWidth = 1;
    [view.layer addSublayer:shapeLayer];
    
}

#pragma mark - Draw line
+(void)drawLineUsingPointOnview:(UIView *)view strokeColor:(UIColor *)strokeColor pointStart:(CGPoint)pointStart pointEnd:(CGPoint)pointEnd
{

CAShapeLayer *shapeLayer = [CAShapeLayer layer];
UIBezierPath *path = [UIBezierPath bezierPath];
[path moveToPoint:pointStart];
[path addLineToPoint:pointEnd];
shapeLayer.path = path.CGPath;
shapeLayer.strokeColor = strokeColor.CGColor;
[view.layer addSublayer:shapeLayer];
}

#pragma mark- Draw Rect
+(void)drawRect:(CGRect)rect color:(UIColor *)color {
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(context, 4.0);
    CGContextSetStrokeColorWithColor(context,
                                     color.CGColor);
    CGRect rectangle = rect;
    CGContextAddRect(context, rectangle);
    CGContextStrokePath(context);
}

#pragma mark - color string
+(void)setColoredLabel:(UILabel*)label  name:(NSString *)name phone:(NSString *)phone colorStr1:(UIColor *)colorStr1 colorStr2:(UIColor *)colorStr2
{
    NSString *strComplete = [NSString stringWithFormat:@"%@ %@",name,phone];
    
    NSMutableAttributedString *attributedString =[[NSMutableAttributedString alloc] initWithString:strComplete];
    
    NSDictionary *baseAttributes = @{
                                     NSForegroundColorAttributeName:[UIColor blackColor],
                                     NSFontAttributeName:[UIFont systemFontOfSize:15.0f]
                                     };
    
    [attributedString addAttribute:NSForegroundColorAttributeName
                             value: colorStr1
                             range:[strComplete rangeOfString:name]];
    
    [attributedString addAttribute:NSForegroundColorAttributeName
                             value:colorStr2
                             range:[strComplete rangeOfString:phone]];
    label.attributedText = attributedString;
}


#pragma mark - string by commas...
+(NSArray *)stringOfCommas:(NSString *)str
{
    NSArray *items = [str componentsSeparatedByString:@","];
    
    return items;
}



+ (void)setCustomBackgroundImage:(UIImage *)image view:(UIView *)view
{
    UIImageView *bgImage = [[UIImageView alloc]initWithImage:image];
    bgImage.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    bgImage.frame = view.frame;
    [view addSubview:bgImage];
}









@end
