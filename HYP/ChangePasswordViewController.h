//
//  ChangePasswordViewController.h
//  Boccia
//
//  Created by IOSdev on 12/31/19.
//  Copyright © 2019 Pragati Porwal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangePasswordViewController : UIViewController

@property(nonatomic,weak) IBOutlet UIButton     * buttonChangePasswd;
@property(nonatomic,weak) IBOutlet UITextField * textFieldOldPasswd;
@property(nonatomic,weak) IBOutlet UITextField * textFieldNewPasswd;
@property(nonatomic,weak) IBOutlet UITextField * textFieldConfirmPasswd;

@end


