//
//  DriverDashboardViewController.h
//  HYP
//
//  Created by VA pvt ltd on 13/09/21.
//  Copyright © 2021 Pragati Porwal. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface DriverDashboardViewController : UIViewController
{
   CLLocationManager *loctionManager;
   GMSCameraPosition *camera;
   GMSMapView *mapView_;
  IBOutlet MDSwitch *mdSwitch;
    int Time;
    NSTimer *timer1;
    NSTimer *countDown;
   GMSMarker *locationMarker_;
   float latitude , latSearch;
   float longitude, lngSearch;
  
}

@property (nonatomic, strong) IBOutlet UILabel * statusLabel;
@property (nonatomic, strong) IBOutlet UIView * statusView;
@property(nonatomic,weak)  NSString * matchstatus;
@property(nonatomic,strong)  GMSGeocoder     *geocoder;
@property(nonatomic,retain) CLLocationManager *locationManager;
@property (nonatomic, strong) IBOutlet UIView * mapView;
@property (nonatomic, strong) NSString *matchFrom;
@property (nonatomic, strong) IBOutlet UIView * mapView1;
@property (nonatomic, strong) IBOutlet UIImageView * driverImageView;
@property (nonatomic, strong) IBOutlet UILabel * driverName;


@property (nonatomic, strong) IBOutlet UIView *notifiactionView;
@property (nonatomic,strong) NSDictionary * notificationDict;
@property(nonatomic,weak) IBOutlet UIButton * acceptbtn;
@property(nonatomic,weak) IBOutlet UIButton * rejectbtn;
@property(nonatomic,weak) IBOutlet UILabel * addressDeleviry;
@property(nonatomic,weak) IBOutlet UILabel * serviceNoti;
@property(nonatomic,weak) IBOutlet UILabel * dateTimenotif;
@property(nonatomic,weak) IBOutlet UILabel * vehicleTypeNoti;
@property(nonatomic,weak) IBOutlet UILabel * bookingType;
@property(nonatomic,weak) IBOutlet UILabel * earningNoti;
@property(nonatomic,weak) IBOutlet UILabel * durationType;
@property(nonatomic,weak) IBOutlet UILabel * invoiceIdNoti;
@property(nonatomic,weak) IBOutlet UILabel * addressType;
@property(nonatomic,weak) IBOutlet UILabel * lblTimeLeft;
@property(nonatomic,weak) IBOutlet UIImageView * invoiceImage;

@end


