//
//  PendingPaymentViewController.h
//  HYP
//
//  Created by VA pvt ltd on 23/09/21.
//  Copyright © 2021 Pragati Porwal. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface PendingPaymentViewController : UIViewController
{
    
    float latitude , latSearch;
    float longitude, lngSearch;
    CLLocationCoordinate2D coordinate;
    GMSCameraPosition *camera;
    GMSMapView *mapView_;
    GMSMarker *locationMarker_;
    AppDelegate * appDelegate;
    
    
    
}

@property (nonatomic,strong) NSDictionary * bookingDetail;
@property (nonatomic, retain) CLLocationManager *locationManager;
@property (nonatomic, strong) IBOutlet UIView * mapView;
@property (nonatomic, strong) IBOutlet UIButton * buttonLocation;
@property (nonatomic, strong) IBOutlet UIView * viewTopView;
@property (nonatomic, strong) IBOutlet UIView * viewBottom;
@property (nonatomic, strong) IBOutlet UIView * viewBottomInfo;
@property (nonatomic, strong) IBOutlet UIView * viewBottomPriceRating;

@property(nonatomic, assign)NSInteger indextag;

@property (weak, nonatomic) IBOutlet UILabel *lblfaretotal;

@property (weak, nonatomic) IBOutlet UILabel *lbldistacetotal;

@property (weak, nonatomic) IBOutlet UILabel *lblTripFare;

@property (weak, nonatomic) IBOutlet UILabel *lblDuration;

@property (weak, nonatomic) IBOutlet UILabel *lblDistance;

@property (weak, nonatomic) IBOutlet UILabel *lblTool;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalprice;
@property (weak, nonatomic) IBOutlet UILabel *lblOutstasnding;

@property (weak, nonatomic) IBOutlet UILabel *lbldiscount;

@property (weak, nonatomic) IBOutlet UIView *locNameView;

@property(nonatomic,strong)NSMutableArray *detailArray;
-(IBAction)buttonLocationClick:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *picLoc;
@property (weak, nonatomic) IBOutlet UILabel *dropoffLoc;


@property (weak, nonatomic) IBOutlet UILabel *discountLabel;
@property (weak, nonatomic) IBOutlet UILabel *toolLabel;
@property (weak, nonatomic) IBOutlet UILabel *outstandingLabel;
@property (weak, nonatomic) IBOutlet UILabel *tripLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalLabel;
@property (weak, nonatomic) IBOutlet UILabel *pickupAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *dropAddressLabel;

@property (nonatomic, strong) IBOutlet UIView * viewPayment;
@property (nonatomic, strong) IBOutlet UIView * viewDistance;

@property (nonatomic, strong) IBOutlet UIButton * buttoninvite;
@property (nonatomic, strong) IBOutlet UIButton * buttonReview;
@property (nonatomic, strong) IBOutlet UIButton * buttonCompleted;


@property (strong, nonatomic) NSDictionary * notificationDict;

@end


