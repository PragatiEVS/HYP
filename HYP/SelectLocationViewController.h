//
//  SelectLocationViewController.h
//  CHORETHING
//
//  Created by santosh kumar singh on 25/03/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectLocationViewController : UIViewController<GMSMapViewDelegate,CLLocationManagerDelegate,GMSAutocompleteViewControllerDelegate>
{
      CLLocationCoordinate2D coordinate;
      CLLocationCoordinate2D startPoint;
      GMSCameraPosition *camera;
      GMSMapView *mapView_;
      float latitude , latSearch;
      float longitude, lngSearch;
      GMSMarker *locationMarker_;
}
@property(nonatomic,retain) CLLocationManager *locationManager;
@property (nonatomic, strong) IBOutlet UIView * mapView;
@property (strong, nonatomic) IBOutlet UILabel *lblName;

@property (strong, nonatomic) NSString * priceString;
@property (strong, nonatomic) NSString * dateString;
@property (strong, nonatomic) NSString * timeString;
@property (weak, nonatomic)  NSString * serviceId;

@property (weak, nonatomic)  NSString * servicename;
@property (weak, nonatomic)  NSString * vendorId;
@property (nonatomic, retain) NSString* state;
@property (nonatomic, retain) NSString* latitudeString;
@property (nonatomic, retain) NSString* longitudeString;

@property (strong, nonatomic) IBOutlet AsyncImageView *imageview;
@property (strong, nonatomic) IBOutlet UIButton *addressTextfield;
@property(nonatomic,strong)  GMSGeocoder     *geocoder;
@property (strong, nonatomic) NSString * addCurrent;
@property (strong, nonatomic) NSDictionary * dict;

@end


