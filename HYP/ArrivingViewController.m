//
//  ArrivingViewController.m
//  ROLUX
//
//  Created by Nitin Kumar on 06/07/17.
//  Copyright © 2017 Nitin Kumar. All rights reserved.
//

#import "ArrivingViewController.h"
#import "AppDelegate.h"
#import "MenuViewController.h"
#import <Firebase/Firebase.h>
#import "DriverDashboardViewController.h"
#import "MenuViewController.h"


@interface ArrivingViewController ()<GMSMapViewDelegate,CLLocationManagerDelegate,WebServiceDelegate>
{
    AppDelegate *appDelegate;
    float latitude,longitude;
    GMSMarker * markerCurrent, *markerPickoff,*markerDest;
    CLLocation * locPicOff ;
    FIRDatabaseReference *FIRDB, *FIRDB1322;
    FIRDatabaseHandle _refHandle;
}


@property (strong, nonatomic) NSString * latDropOrder;
@property (strong, nonatomic) NSString * longiDropOrder;
@property (strong, nonatomic) NSString * latUser;
@property (strong, nonatomic) NSString * longiUser;
@property (strong, nonatomic) FIRDatabaseReference *ref;
@property (copy, nonatomic) CLLocation * driverLocation;
@end

@implementation ArrivingViewController



-(void)loadRoute
{
    
    FIRDB = [FIRDatabase database].reference;

    NSDictionary * dictLogin  = [[NSUserDefaults standardUserDefaults] dictionaryForKey:kLOGIN];
    
    NSString * user_id;
    if ([dictLogin valueForKey:@"id"] == nil)
    {
        user_id  = [dictLogin valueForKey:@"userId"];
    }
    else
    {
        user_id  = [dictLogin valueForKey:@"id"];
    }
    
    FIRDatabaseReference *usersRef =  [[FIRDB child:@"liveTracking"]child:[NSString stringWithFormat:@"%@@%@",[_dictArrived objectForKey:@"bookingId"], user_id]];
    
    [usersRef observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot *snapshot)
     {
        
        [usersRef observeEventType:FIRDataEventTypeChildAdded withBlock:^(FIRDataSnapshot *snapshot)
         {
            
            NSDictionary *  Dictionary;
            
            self.firebaseDictinary = @{
                @"longs": snapshot.value [@"longs"],
                @"lat": snapshot.value [@"lat"],
                @"adress": snapshot.value [@"adress"],
                @"bookingId": snapshot.value [@"bookingId"],
                @"driverId": snapshot.value [@"driverId"]
            };
            
            Dictionary = @{
                @"longs": snapshot.value[@"longs"],
                @"lat": snapshot.value [@"lat"]
            };
            
            longitude = [snapshot.value[@"longs"]floatValue];
            latitude = [snapshot.value[@"lat"]floatValue];
            self.dropaddress = snapshot.value [@"adress"];
            _bookingid  = snapshot.value [@"bookingId"];
            _driverId  = snapshot.value [@"driverId"];
          
            self.driverLocation  = [[CLLocation alloc]initWithLatitude:latitude longitude:longitude];
            
            [mapView_ clear];
            
            GMSMarker*  marker = [[GMSMarker alloc] init];
            
            marker.icon = [UIImage imageNamed:@"car1.png"];
            marker.position =  _driverLocation.coordinate;
            camera =   [GMSCameraPosition cameraWithTarget:_driverLocation.coordinate zoom:ZOOM];
            [mapView_ animateToCameraPosition:camera];
            marker.map = mapView_;
            
            GMSMarker*  markerDestView = [[GMSMarker alloc] init];
            markerDestView.map  = nil;
            markerDestView.position = locPicOff.coordinate;
            markerDestView.icon = [UIImage imageNamed:@"EndMark"];
            markerDestView.map = mapView_;
           
            _dropoffLoc.text =[NSString stringWithFormat:@"Driver: %@",self.dropaddress];
           
            [self drawRouteOrigin:_driverLocation destination:locPicOff];
            _locationManager.delegate = self;
          
            CLLocationDistance distance2 = [_driverLocation distanceFromLocation:locPicOff] * 0.000621371;
            NSLog(@"%f",distance2);

            [self.labelDistance setText:[NSString stringWithFormat:@"%.2f Miles", distance2]];

//          [self getDistancebyVehicleStart:self.dropaddress endPoint:self.pickupFinal ];
            
        }];
        
    }];
    
    
}

-(void)getDistancebyVehicleStart:(NSString *)startPoint endPoint:(NSString *)endPoint
{
    endPoint = [endPoint stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    startPoint = [startPoint stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
 
    NSString *str = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/directions/json?origin=%@&destination=%@&key=%@",startPoint,endPoint,kKEY_GOOGLE_MAP];
    
    NSURL *url= [NSURL URLWithString:[NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/directions/json?origin=%@&destination=%@&key=%@",startPoint,endPoint,kKEY_GOOGLE_MAP]];
    NSData* data = [NSData dataWithContentsOfURL:
                    url];
    
    NSError* error;
    
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:data //1
                          
                          options:kNilOptions
                          error:&error];
    //NSLog(@"json = %@",json);
    NSArray* arr_elements = (NSArray*)[json objectForKey:@"routes"]; //2
    
    
    if (arr_elements.count>0) {
        NSDictionary *first=(NSDictionary*)[arr_elements objectAtIndex:0];
        //NSLog(@"nitin first : %@",first.description);
        
        NSArray *element=(NSArray*)[first valueForKey:@"legs"];
        //NSLog(@"nitin element : %@",element.description);
        
        
        NSDictionary *dist=(NSDictionary*)[((NSDictionary*)[element objectAtIndex:0]) objectForKey:@"distance"];
        NSDictionary *time=(NSDictionary*)[((NSDictionary*)[element objectAtIndex:0]) objectForKey:@"duration"];
        //NSLog(@"nitin distance :%@",dist.description);
        
        
        //NSLog(@"nitin text :%@",(NSString*)[dist objectForKey:@"text"]);
        //  lbl_Drive.text=[NSString stringWithFormat:@"Drive : %@",[time objectForKey:@"text"]];
//        if ([[time objectForKey:@"text"] isEqualToString:@"(null)"])
//        {
//            self.labelDistance.text=@"0 min";
//        }
//        else
//        {
//            self.labelDistance.text=[NSString stringWithFormat:@"%@",[time objectForKey:@"text"]];
//        }
//
        if ([[dist objectForKey:@"text"] isEqualToString:@"(null)"])
        {
            self.labelDistance.text=@"0 MI";
        }
        else
        {
            self.labelDistance.text=[NSString stringWithFormat:@"%@",[dist objectForKey:@"text"]];
        }
    }
}
- (void)viewDidLoad
{
    FIRDB = [FIRDatabase database].reference;
    [super viewDidLoad];
    self.firebaseDictinary = [[NSDictionary alloc]init];
    [self getAuthorizationForLocation];
    [self showGoogleMap];
    
    [self configView];
    
  
}

-(IBAction)buttonPhnemergencyClick:(id)sender;
{
    NSString *phoneURLString = [NSString stringWithFormat:@"tel:%@", @"911"];
    NSURL *phoneURL = [NSURL URLWithString:phoneURLString];
    [[UIApplication sharedApplication] openURL:phoneURL];
}

-(IBAction)buttonokClick:(id)sender;
{
    DriverDashboardViewController *homeDriverViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"DriverDashboardViewController");
    MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
    [UIAppDelegate manageDrawerRear:menuViewController front:homeDriverViewController];
}

#pragma mark - Config
-(void)configView
{
    
    [Alert viewWithLayerColorAndWidth:kCOLOR_WHITE view:_viewPayment radius:5.0 border:2.0];
    [Alert viewWithLayerColorAndWidth:kCOLOR_CLEAR view:_buttonPaymentOk radius:5.0 border:1.0];
    
    _viewTopAddressesInfo.layer.cornerRadius = 5;
    [_viewTopAddressesInfo.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [_viewTopAddressesInfo.layer setBorderWidth:1.5f];
    [_viewTopAddressesInfo.layer setShadowColor:[UIColor grayColor].CGColor];
    [_viewTopAddressesInfo.layer setShadowOpacity:0.8];
    [_viewTopAddressesInfo.layer setShadowRadius:3.0];
    [_viewTopAddressesInfo.layer setShadowOffset:CGSizeMake(1.0, 1.0)];
    
    _viewBottomAddressesInfo.layer.cornerRadius = 5;
    [_viewBottomAddressesInfo.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [_viewBottomAddressesInfo.layer setBorderWidth:1.5f];
    [_viewBottomAddressesInfo.layer setShadowColor:[UIColor grayColor].CGColor];
    [_viewBottomAddressesInfo.layer setShadowOpacity:0.8];
    [_viewBottomAddressesInfo.layer setShadowRadius:3.0];
    [_viewBottomAddressesInfo.layer setShadowOffset:CGSizeMake(1.0, 1.0)];
    
    
    _viewPayment.hidden = YES;
    
    // Navigation bar
    [self navigationBarConfiguration];
    
    
    //    _viewPayment.hidden = NO;
    _buttonOptions.enabled = YES;
    _buttonDirection.hidden = NO;
    self.buttonOptions.layer.cornerRadius = 10;
    self.buttonOptions.clipsToBounds=YES;
   
     
    
    NSString * price = [_dictArrived valueForKey:@"totalPrice"];
    
    _labelPaymentAmount.text = [NSString stringWithFormat:@"Amount $%@",price];
    
    NSString * strreqId = [_dictArrived valueForKey:@"invoiceId"];
    
    self.labelBookingID.text = [NSString stringWithFormat:@"%@ %@",@"Invoice Id:",strreqId];
        
   
    self.labelocation.text = [_dictArrived valueForKey:@"RequestDropAddress"];
    
    NSString * email = [_dictArrived valueForKey:@"email"];
    NSString *status = [[NSUserDefaults standardUserDefaults] stringForKey:@"rideStatus"];

  
     if ([status isEqualToString:@"3"])
    {
        [[NSUserDefaults standardUserDefaults] setValue:@"3" forKey:@"rideStatus"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [_buttonOptions setTitle:@"DESTINATION ARRIVAL" forState:UIControlStateNormal];
//        _buttonOptions.backgroundColor = [Alert colorFromHexString:@"#FFA249"];
        _picLoc.text = [NSString stringWithFormat:@"Drop: %@",[_dictArrived objectForKey:@"RequestDropAddress"]];
        self.pickupFinal = [_dictArrived objectForKey:@"RequestDropAddress"];
        NSString * pickuplatLong = [_dictArrived valueForKey:@"RequestDropLatLong"];
        
        NSArray *item2 = [pickuplatLong componentsSeparatedByString:@","];
        
        NSString * strLatPic    = [item2 objectAtIndex:0];
        NSString * strLongPic   = [item2 objectAtIndex:1];
        
        self.latDropOrder = strLatPic;
        self.longiDropOrder = strLongPic;
        locPicOff  = [[CLLocation alloc]initWithLatitude:[strLatPic floatValue] longitude:[strLongPic floatValue]];
      
         self.title = @"BECOME A HYPSTER";
    }
    else if ([status isEqualToString:@"11"])
    {
        [[NSUserDefaults standardUserDefaults] setValue:@"11" forKey:@"rideStatus"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [_buttonOptions setTitle:@"ORDER DELIVERED" forState:UIControlStateNormal];
//        _buttonOptions.backgroundColor = [Alert colorFromHexString:@"#FFA249"];
        _picLoc.text = [NSString stringWithFormat:@"Drop: %@",[_dictArrived objectForKey:@"RequestDropAddress"]];
        self.pickupFinal = [_dictArrived objectForKey:@"RequestDropAddress"];
        NSString * pickuplatLong = [_dictArrived valueForKey:@"RequestDropLatLong"];
        
        NSArray *item2 = [pickuplatLong componentsSeparatedByString:@","];
        
        NSString * strLatPic    = [item2 objectAtIndex:0];
        NSString * strLongPic   = [item2 objectAtIndex:1];
        self.latDropOrder = strLatPic;
        self.longiDropOrder = strLongPic;
        locPicOff  = [[CLLocation alloc]initWithLatitude:[strLatPic floatValue] longitude:[strLongPic floatValue]];
      
         self.title = @"BECOME A HYPSTER";
    }
    else if ([status isEqualToString:@"10"])
    {
        [[NSUserDefaults standardUserDefaults] setValue:@"10" forKey:@"rideStatus"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [_buttonOptions setTitle:@"ARRIVED AT STORE" forState:UIControlStateNormal];
//        _buttonOptions.backgroundColor = [Alert colorFromHexString:@"#FFA249"];
        _picLoc.text = [NSString stringWithFormat:@"Pickup: %@",[_dictArrived objectForKey:@"RequestPickupAddress"]];
        self.pickupFinal = [_dictArrived objectForKey:@"RequestPickupAddress"];
        NSString * pickuplatLong = [_dictArrived valueForKey:@"RequestPickupLatLong"];
        
        NSArray *item2 = [pickuplatLong componentsSeparatedByString:@","];
        
        NSString * strLatPic    = [item2 objectAtIndex:0];
        NSString * strLongPic   = [item2 objectAtIndex:1];
        self.latDropOrder = strLatPic;
        self.longiDropOrder = strLongPic;
        locPicOff  = [[CLLocation alloc]initWithLatitude:[strLatPic floatValue] longitude:[strLongPic floatValue]];
      
         self.title = @"BECOME A HYPSTER";
    }
    else if ([status isEqualToString:@"8"])
    {
        [[NSUserDefaults standardUserDefaults] setValue:@"8" forKey:@"rideStatus"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [_buttonOptions setTitle:@"CONFIRM IN ROUTE TO DROP" forState:UIControlStateNormal];
//        _buttonOptions.backgroundColor = [Alert colorFromHexString:@"#FFA249"];
        _picLoc.text = [NSString stringWithFormat:@"Drop: %@",[_dictArrived objectForKey:@"RequestDropAddress"]];
        self.pickupFinal = [_dictArrived objectForKey:@"RequestDropAddress"];
        NSString * pickuplatLong = [_dictArrived valueForKey:@"RequestDropLatLong"];
        
        NSArray *item2 = [pickuplatLong componentsSeparatedByString:@","];
        
        NSString * strLatPic    = [item2 objectAtIndex:0];
        NSString * strLongPic   = [item2 objectAtIndex:1];
        self.latDropOrder = strLatPic;
        self.longiDropOrder = strLongPic;
        locPicOff  = [[CLLocation alloc]initWithLatitude:[strLatPic floatValue] longitude:[strLongPic floatValue]];
        
         self.title = @"BECOME A HYPSTER";
    }
    else if ([status isEqualToString:@"1"])
    {
        [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"rideStatus"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [_buttonOptions setTitle:@"CONFIRM YOUR IN ROUTE" forState:UIControlStateNormal];
//        _buttonOptions.backgroundColor = [Alert colorFromHexString:@"#FFA249"];
        _picLoc.text = [NSString stringWithFormat:@"Pickup: %@",[_dictArrived objectForKey:@"RequestPickupAddress"]];
        self.pickupFinal = [_dictArrived objectForKey:@"RequestPickupAddress"];
       
        NSString * pickuplatLong = [_dictArrived valueForKey:@"RequestPickupLatLong"];
        
        NSArray *item2 = [pickuplatLong componentsSeparatedByString:@","];
        
        NSString * strLatPic    = [item2 objectAtIndex:0];
        NSString * strLongPic   = [item2 objectAtIndex:1];
        self.latDropOrder = strLatPic;
        self.longiDropOrder = strLongPic;
        locPicOff  = [[CLLocation alloc]initWithLatitude:[strLatPic floatValue] longitude:[strLongPic floatValue]];
        
         self.title = @"BECOME A HYPSTER";
    }
    else{
       
        [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"rideStatus"];
        [[NSUserDefaults standardUserDefaults] synchronize];
       
        [_buttonOptions setTitle:@"CONFIRM YOUR IN ROUTE" forState:UIControlStateNormal];
        _picLoc.text = [NSString stringWithFormat:@"Pickup: %@",[_dictArrived objectForKey:@"RequestPickupAddress"]];
        NSString * pickuplatLong = [_dictArrived valueForKey:@"RequestPickupLatLong"];
        
        NSArray *item2 = [pickuplatLong componentsSeparatedByString:@","];
        
        NSString * strLatPic    = [item2 objectAtIndex:0];
        NSString * strLongPic   = [item2 objectAtIndex:1];
        self.latDropOrder = strLatPic;
        self.longiDropOrder = strLongPic;
        locPicOff  = [[CLLocation alloc]initWithLatitude:[strLatPic floatValue] longitude:[strLongPic floatValue]];
        
         self.title = @"BECOME A HYPSTER";
    }
    
    NSString * UserName = [_dictArrived valueForKey:@"fullName"];
    if (UserName == nil)
    {
        self.labelName.text = self.userName;
    }
    else
    {
        self.labelName.text = UserName;
    }
    
    NSString * UserPhone = [_dictArrived valueForKey:@"contactNumber"];
   
    if (UserPhone == nil)
    {
        self.labelPhone.text = self.uuserPhone;
    }
    else
    {
        self.labelPhone.text = UserPhone;
    }
    
    
    NSString * driverimage = [_dictArrived valueForKey:@"image"];
    self.imageViewUser.layer.cornerRadius = self.imageViewUser.frame.size.height/2;
    self.imageViewUser.clipsToBounds=YES;
    
    if (driverimage == nil)
    {
//        [self.imageViewUser sd_setImageWithURL:[NSURL URLWithString:self.userImage]];
        [self.imageViewUser sd_setImageWithURL:[NSURL URLWithString:self.userImage] placeholderImage:[UIImage imageNamed:@"user12.png"]];
    }
    else
    {
//     /   [self.imageViewUser sd_setImageWithURL:[NSURL URLWithString:driverimage]];
        [self.imageViewUser sd_setImageWithURL:[NSURL URLWithString:driverimage] placeholderImage:[UIImage imageNamed:@"user12.png"]];
    }
    
    NSString * invoiceImage = [_dictArrived valueForKey:@"invoiceImage"];
   
    if (invoiceImage == nil)
    {

    }
    else
    {
//     /   [self.imageViewUser sd_setImageWithURL:[NSURL URLWithString:driverimage]];
        [self.invoiceImage sd_setImageWithURL:[NSURL URLWithString:invoiceImage] placeholderImage:nil];
    }
   
    NSLog(@"email is =====%@",email);
    
   
    [self viewInfoLoad];
    
}

#pragma mark - Configure Navigation Controller

-(void)navigationBarConfiguration
{

    self.navigationController.navigationBarHidden = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    UIButton * menuButton  = [UIButton buttonWithType:UIButtonTypeSystem];
    menuButton.frame = CGRectMake(8, 18, 24, 26);
    [menuButton setBackgroundImage:[UIImage imageNamed:IMG_BACK] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(drawer:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *accountBarItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    self.navigationItem.leftBarButtonItem = accountBarItem;
    
    NSMutableArray *arrRightBarItems = [[NSMutableArray alloc] init];
    UIButton *btnSetting = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnSetting setImage:[UIImage imageNamed:@"Frequency"] forState:UIControlStateNormal];
    btnSetting.frame = CGRectMake(0, 0, 50, 50);
    btnSetting.showsTouchWhenHighlighted=YES;
    [btnSetting addTarget:self action:@selector(onSettings:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSetting];
    
    UIButton *btnLib = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnLib setImage:[UIImage imageNamed:@"logo1.png"] forState:UIControlStateNormal];
    btnLib.frame = CGRectMake(0, 0, 50, 40);
    btnLib.showsTouchWhenHighlighted=YES;
    [btnLib addTarget:self action:@selector(onMyLibrary:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButtonItem2 = [[UIBarButtonItem alloc] initWithCustomView:btnLib];
    [arrRightBarItems addObject:barButtonItem2];
    [arrRightBarItems addObject:barButtonItem];
    self.navigationItem.rightBarButtonItems=arrRightBarItems;
    
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.navigationController.navigationBar.tintColor = [Alert colorFromHexString:kCOLOR_NAV];
    
}
-(void)onMyLibrary:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}

-(void)onSettings:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/hyptunesradio";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}
#pragma mark - Drawer class
-(void)drawer:(id)sender
{
    
    NSDictionary * dict = [[NSUserDefaults standardUserDefaults] dictionaryForKey:kLOGIN];
    NSString * userType = [dict valueForKey:kAPI_USER_TYPE];
    
    DriverDashboardViewController *homeDriverViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"DriverDashboardViewController");
    MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
    [UIAppDelegate manageDrawerRear:menuViewController front:homeDriverViewController];
    
}


#pragma mark --- : GOOGLE MAP :----
-(void)showGoogleMap
{
//    float tempLat = appDelegate.locationGlobal.coordinate.latitude;
//    if (tempLat > 0)
//    {
//        latitude = appDelegate.locationGlobal.coordinate.latitude;
//        longitude = appDelegate.locationGlobal.coordinate.longitude;
//        // _str_lat_longStart = [NSString stringWithFormat:@"%@,%@",strLat,strLong];
//        appDelegate.locationGlobal = nil;
//    }
//    else
//    {
        coordinate = [self getLocation];
        latitude = coordinate.latitude;
        longitude = coordinate.longitude;
        // _str_lat_longStart = [NSString stringWithFormat:@"%@,%@",strLat,strLong];
//    }
    CGRect frame = CGRectMake(0, 0, kSCREEN_WIDTH,kSCREEN_HEIGHT);
    camera = [GMSCameraPosition cameraWithLatitude:latitude longitude:longitude zoom:ZOOM];
    mapView_ = [GMSMapView mapWithFrame:frame camera:camera];
    mapView_.delegate = self;
    locationMarker_ = [[GMSMarker alloc] init];
    [mapView_ setCamera:camera];
    dispatch_async(dispatch_get_main_queue(),^{
        mapView_.myLocationEnabled              = YES;
        mapView_.indoorEnabled                  = YES;
        mapView_.settings.compassButton         = YES;
        mapView_.settings.myLocationButton      = NO;
        mapView_.settings.zoomGestures          = YES;
        mapView_.settings.indoorPicker          = YES;
        mapView_.settings.scrollGestures        = YES;
        mapView_.trafficEnabled                 = YES;
        
    });
    [self.viewMap addSubview:mapView_];
}


#pragma mark ----: GET Coordinate2D :----
-(CLLocationCoordinate2D) getLocation
{
    CLLocation *location = [_locationManager location];
    coordinate = [location coordinate];
    return coordinate;
}

#pragma mark - Get My Current Location
-(void)getMyCurrentLocation
{
    coordinate = [self getLocation];
    mapView_.myLocationEnabled = YES;
    camera = [GMSCameraPosition cameraWithLatitude:coordinate.latitude longitude:coordinate.longitude zoom:ZOOM];
    [mapView_ animateToCameraPosition:camera];
}

#pragma mark - 

-(void)getAuthorizationForLocation
{
    
    if ([CLLocationManager locationServicesEnabled])
    {
        
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        _locationManager.delegate = self;
        if (IS_iOS_8_OR_LATER)
        {
            [_locationManager requestWhenInUseAuthorization];
            //  [_locationManager requestAlwaysAuthorization];
            [_locationManager startUpdatingLocation];
        }
        else
        {
            //_locationManager = nil;
            [_locationManager startUpdatingLocation];
        }
    }
}



#pragma mark - CLLocation Delegate methods

- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    
    //    [manager stopUpdatingLocation];
    NSLog(@"how many time location update");
    CLLocation * currentLocation = [locations lastObject];
    
    _locationManager = manager;
    latitude = currentLocation.coordinate.latitude;
    longitude = currentLocation.coordinate.longitude;
    
          _latUser    = [NSString stringWithFormat:@"%.8f",latitude];
          _longiUser    = [NSString stringWithFormat:@"%.8f",longitude];
    
  
         locationMarker_ = [GMSMarker markerWithPosition:CLLocationCoordinate2DMake(latitude, longitude)];
         locationMarker_.icon = SET_IMAGE(kIMG_CAR_MARKER);
         locationMarker_.title = @"Driver";
           locationMarker_.map = mapView_;
            
        CLLocation * currentLoc = [[CLLocation alloc]initWithLatitude:latitude longitude:longitude];
        [self reverGeoCodingUsingGoogle:currentLoc];
        [self drawRouteOrigin:currentLoc destination:locPicOff];
}


-(void)reverGeoCodingUsingGoogle:(CLLocation *)location
{
    [[GMSGeocoder geocoder] reverseGeocodeCoordinate:CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude) completionHandler:^(GMSReverseGeocodeResponse* response, NSError* error)
     {
        
        GMSReverseGeocodeResult *result = response.firstResult;
        
        NSArray   * arrayAdd = result.lines;
        
        NSString  * address1;
        NSString  * address2;
        
        if (arrayAdd.count>1)
        {
            address1 = result.lines[0];
            address2 = result.subLocality;
        }
        else
        {
            address1 = result.lines[0];
        }
        
        
        self.address = address1;
        NSDictionary * dictLogin  = [[NSUserDefaults standardUserDefaults] dictionaryForKey:kLOGIN];
        
        NSString * user_id;
        if ([dictLogin valueForKey:@"id"] == nil)
        {
            user_id  = [dictLogin valueForKey:@"userId"];
        }
        else
        {
            user_id  = [dictLogin valueForKey:@"id"];
        }
      
        FIRDatabaseReference *usersRef = [[FIRDB child:@"liveTracking"]child:[NSString stringWithFormat:@"%@@%@",[self.dictArrived objectForKey:@"bookingId"], user_id]];
        
        [usersRef observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot *snapshot)
        {
         
            NSDictionary *post1;
            
            if([snapshot.value isEqual:[NSNull null]])
            {
                
                post1 = @{
                    @"longs":self.longiUser,
                    @"lat": self.latUser,
                    @"adress":address1,
                    @"bookingId":[NSString stringWithFormat:@"%@",[self.dictArrived objectForKey:@"bookingId"]],
                    @"driverId":[NSString stringWithFormat:@"%@",user_id],
                };
                
                
                FIRDatabaseReference *post1Ref = [usersRef childByAutoId];
                [post1Ref setValue: post1];
                [self loadRoute];
            }
            else{
               
                post1 = @{
                    @"longs":self.longiUser,
                    @"lat": self.latUser,
                    @"adress":address1,
                    @"bookingId":[NSString stringWithFormat:@"%@",[self.dictArrived objectForKey:@"bookingId"]],
                    @"driverId":[NSString stringWithFormat:@"%@",user_id],
                };
                
                
                FIRDatabaseReference *post1Ref = [usersRef childByAutoId];
                [post1Ref setValue: post1];
                [self loadRoute];
            }
        }];
    }];
    
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    NSLog(@"error = %@",error.localizedDescription);
}

-(void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading
{
    
}

-(void)viewInfoLoad
{
    
    // [self.buttonOptions setTitle:@"Reaching Source" forState:UIControlStateNormal];
    // [self.buttonOptions setBackgroundColor:[Alert colorFromHexString:kCOLOR_BUTTON]];
    
    [Alert viewWithLayerColorAndWidth:kCOLOR_CLEAR view:_buttonDirection radius:5.0 border:1.0];
    
//    NSString * strreqId = [_dictArrived valueForKey:@"invoiceId"];
//
//        self.labelBookingID.text = [NSString stringWithFormat:@"%@ %@",@"Invoice Id: ",strreqId];
    self.labelocation.text = [_dictArrived valueForKey:@"RequestDropAddress"];
    
    NSString * email = [_dictArrived objectForKey:@"userEmail"];
    
    NSLog(@"email is =====%@",email);
    
    NSString * pickuplatLong = [_dictArrived objectForKey:@"RequestDropLatLong"];
    
    NSArray *item2 = [pickuplatLong componentsSeparatedByString:@","];
    
    NSString * strLatPic    = [item2 objectAtIndex:0];
    NSString * strLongPic   = [item2 objectAtIndex:1];
    
//    locPicOff  = [[CLLocation alloc]initWithLatitude:[strLatPic floatValue] longitude:[strLongPic floatValue]];
    
    
    GMSMarker*  markerDestView = [[GMSMarker alloc] init];
    markerDestView.position = CLLocationCoordinate2DMake([strLatPic floatValue], [strLongPic floatValue]);
    markerDestView.icon = [UIImage imageNamed:kIMG_USER_DESTI_MARKER];
    markerDestView.title = @"PickOff Location";
    markerDestView.map = mapView_;
    
    CLLocation * currentLoc = [[CLLocation alloc]initWithLatitude:latitude longitude:longitude];
    
    CLLocationCoordinate2D position1 = CLLocationCoordinate2DMake(latitude, longitude);
    markerCurrent = [GMSMarker markerWithPosition:position1];
    
    markerCurrent.icon   = SET_IMAGE(@"car_driverHome");
    markerCurrent.map    = mapView_;
    
    CLLocationCoordinate2D position2 = CLLocationCoordinate2DMake([strLatPic floatValue], [strLongPic floatValue]);
    markerPickoff = [GMSMarker markerWithPosition:position2];
    
    markerPickoff.icon   = SET_IMAGE(@"dashboardapp");
    markerPickoff.map    = mapView_;
    
//    [self drawRouteOrigin:currentLoc destination:locPicOff];
    
   
}


- (void)drawRouteOrigin:(CLLocation *)myOrigin destination:(CLLocation *)myDestination
{
    
    
    [self fetchPolylineWithOrigin:myOrigin destination:myDestination completionHandler:^(GMSPolyline *polyline)
     {
        if(polyline)
        {
            GMSStrokeStyle *greenToRed = [GMSStrokeStyle solidColor:kCOLOR_BLACK];
            polyline.strokeWidth = 2.5;
            polyline.spans = @[[GMSStyleSpan spanWithStyle:greenToRed]];
            polyline.map = mapView_;
            
        }
        
    }];
    
}

- (void)fetchPolylineWithOrigin:(CLLocation *)origin destination:(CLLocation *)destination completionHandler:(void (^)(GMSPolyline *))completionHandler
{
    NSString *originString = [NSString stringWithFormat:@"%f,%f", origin.coordinate.latitude, origin.coordinate.longitude];
    NSString *destinationString = [NSString stringWithFormat:@"%f,%f", destination.coordinate.latitude, destination.coordinate.longitude];
    NSString *directionsAPI = @"https://maps.googleapis.com/maps/api/directions/json?";
    NSString *directionsUrlString = [NSString stringWithFormat:@"%@&origin=%@&destination=%@&mode=driving&key=%@", directionsAPI, originString, destinationString, kKEY_GOOGLE_MAP];
    NSURL *directionsUrl = [NSURL URLWithString:directionsUrlString];
    
    
    NSURLSessionDataTask *fetchDirectionsTask = [[NSURLSession sharedSession] dataTaskWithURL:directionsUrl completionHandler:
                                                 ^(NSData *data, NSURLResponse *response, NSError *error)
                                                 {
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        if(error)
        {
            if(completionHandler)
                completionHandler(nil);
            return;
        }
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            NSArray *routesArray = [json objectForKey:@"routes"];
            
            GMSPolyline *polyline = nil;
            if ([routesArray count] > 0)
            {
                NSDictionary *routeDict = [routesArray objectAtIndex:0];
                NSDictionary *routeOverviewPolyline = [routeDict objectForKey:@"overview_polyline"];
                NSString *points = [routeOverviewPolyline objectForKey:@"points"];
                GMSPath *path = [GMSPath pathFromEncodedPath:points];
                polyline = [GMSPolyline polylineWithPath:path];
                
                
                
            }
            
            if(completionHandler)
                completionHandler(polyline);
            
        });
        
    }];
    [fetchDirectionsTask resume];
}




-(IBAction)buttonPhnClick:(id)sender;
{
    NSString *strPhone =[_dictArrived objectForKey:@"UserPhone"];
    
    NSString *phoneURLString = [NSString stringWithFormat:@"tel:%@", strPhone];
    NSURL *phoneURL = [NSURL URLWithString:phoneURLString];
    [[UIApplication sharedApplication] openURL:phoneURL];
}


-(IBAction)buttonOptionsClick:(id)sender;
{
    
    if ([self.buttonOptions.titleLabel.text isEqualToString:@"DESTINATION ARRIVAL"]) {
        
        BOOL network = [Alert networkStatus];
        if (network) {
        [self serviceArrived];
        }
        else
        {
            kNETWORK_PROBLEM;
        }
    }
    else if ([self.buttonOptions.titleLabel.text isEqualToString:@"CONFIRM YOUR IN ROUTE"]) {

        BOOL network = [Alert networkStatus];
        
        if (network) {
            
            [self servicePickup];
        }
        else
        {
            kNETWORK_PROBLEM;
        }
        

    }
    else if ([self.buttonOptions.titleLabel.text isEqualToString:@"ARRIVED AT STORE"]) {

       [self servicepickConfirm];

    }
    else if ([self.buttonOptions.titleLabel.text isEqualToString:@"CONFIRM IN ROUTE TO DROP"])
    {
        
        [[NSUserDefaults standardUserDefaults] setValue:@"3" forKey:@"rideStatus"];
         [[NSUserDefaults standardUserDefaults] synchronize];
       
        self.title = @"RIDE STARTED";
        BOOL network = [Alert networkStatus];
        
        if (network) {
            
            [self serviceStartRide];
        }
        else
        {
            kNETWORK_PROBLEM;
        }
        
        
        NSString * droplatLong      = [_dictArrived valueForKey:@"RequestDropLatLong"];
        
        NSArray *item1 = [droplatLong componentsSeparatedByString:@","];
        
        NSString * strLatDrop = [item1 objectAtIndex:0];
        NSString * strLongDrop = [item1 objectAtIndex:1];
        
        CLLocation * locDrop       = [[CLLocation alloc]initWithLatitude:[strLatDrop floatValue] longitude:[strLongDrop floatValue]];
        
        NSString * pickuplatLong      = [_dictArrived valueForKey:@"RequestDropLatLong"];
        
        NSArray *item2          = [pickuplatLong componentsSeparatedByString:@","];
        
        NSString * strLatPic    = [item2 objectAtIndex:0];
        NSString * strLongPic   = [item2 objectAtIndex:1];
        
        
        locPicOff       = [[CLLocation alloc]initWithLatitude:[strLatPic floatValue] longitude:[strLongPic floatValue]];
        
        
        [mapView_ clear];
        
        CLLocationCoordinate2D position1 = CLLocationCoordinate2DMake([strLatPic floatValue], [strLongPic floatValue]);
        markerPickoff = [GMSMarker markerWithPosition:position1];
        
        markerPickoff.icon   = SET_IMAGE(kIMG_CAR_MARKER);
        markerPickoff.map    = mapView_;
        
        CLLocationCoordinate2D position2 = CLLocationCoordinate2DMake([strLatDrop floatValue], [strLongDrop floatValue]);
        markerDest = [GMSMarker markerWithPosition:position2];
        
        markerDest.icon   = SET_IMAGE(kIMG_USER_DESTI_MARKER);
        markerDest.map    = mapView_;
        
        
        [self drawRouteOrigin:locPicOff destination:locDrop];
        
    }
    
    else if([self.buttonOptions.titleLabel.text isEqualToString:@"ORDER DELIVERED"])
    {
        BOOL network = [Alert networkStatus];
        if (network) {
            [self serviceENDRide];
        }
        else
        {
            kNETWORK_PROBLEM;
        }
    }
    
    else if([self.buttonOptions.titleLabel.text isEqualToString:@"WAITING FOR PAYMENT"])
    {
        self.title = @"WAITING FOR PAYMENT";
        
        BOOL network = [Alert networkStatus];
        if (network)
        {
            [self serviceENDRide];
        }
        else
        {
            kNETWORK_PROBLEM;
        }
        
        
    }
    
}

-(IBAction)buttonPaymentOkClick:(id)sender
{
    _viewPayment.hidden = YES;
    _buttonOptions.enabled = YES;
    _buttonDirection.enabled = YES;
}

-(IBAction)buttonDirectionClick:(id)sender
{

    
    NSString* directionsURL = [NSString stringWithFormat:@"http://maps.apple.com/?saddr=%f,%f&daddr=%f,%f",latitude, longitude, [self.latDropOrder  floatValue], [self.longiDropOrder  floatValue]];
 
        NSString *directionsURL1=[directionsURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

    if ([[UIApplication sharedApplication] respondsToSelector:@selector(openURL:options:completionHandler:)]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: directionsURL1] options:@{} completionHandler:^(BOOL success) {}];
    } else {
            NSString* directionsURL = [NSString stringWithFormat:@"http://maps.apple.com/?saddr=%@&daddr=%@",_driverLocation, locPicOff];
            [[UIApplication sharedApplication] openURL: [NSURL URLWithString: directionsURL1]];
        }
   
}



#pragma mark - SERVICE START RIDE
-(void)serviceStartRide
{
    [Alert svProgress:@"Please Wait..."];
  
    WebService * connection = [[WebService alloc]init];
    connection.delegate = self;
    NSString * languageSaved = [[NSUserDefaults standardUserDefaults] objectForKey:@"kSaveLanguageDefaultKey"];
    NSArray * arrayExt = [languageSaved componentsSeparatedByString:@"_"];
   
    NSString * pickuplatLong    = [_dictArrived valueForKey:@"pickuplatLong"];
    NSString * strreqId            = [_dictArrived valueForKey:@"bookingId"];
    
    // Dictionary Login
    NSDictionary * dictLogin           = [[NSUserDefaults standardUserDefaults] dictionaryForKey:kLOGIN];
    
    NSString * user_id;
    if ([dictLogin valueForKey:@"id"] == nil)
    {
        user_id  = [dictLogin valueForKey:@"userId"];
    }
    else
    {
        user_id  = [dictLogin valueForKey:@"id"];
    }
    
    NSMutableDictionary * dict                              = [NSMutableDictionary dictionary];
    [dict setObject:@"ridestart"                 forKey:kAPI_ACTION];
    
    [dict setObject:[NSString stringWithFormat:@"%f,%f",latitude,longitude]                   forKey:@"Actual_Pickup_Lat_Long"];
    [dict setObject:strreqId                       forKey:@"bookingId"];
    [dict setObject:user_id                       forKey:@"driverId"];
    [dict setObject:self.address                       forKey:@"Actual_PickupAddress"];
    
    [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:kAPI_METHOD_START_REQ];
}


-(void)serviceENDRide
{
    [Alert svProgress:@"Please Wait..."];
  
    WebService * connection = [[WebService alloc]init];
    connection.delegate = self;
    // Dictionary Login
    NSDictionary * dictLogin           = [[NSUserDefaults standardUserDefaults] dictionaryForKey:kLOGIN];
    NSString * strreqId            = [_dictArrived valueForKey:@"bookingId"];
   
    NSString * user_id;
    if ([dictLogin valueForKey:@"id"] == nil)
    {
        user_id  = [dictLogin valueForKey:@"userId"];
    }
    else
    {
        user_id  = [dictLogin valueForKey:@"id"];
    }
    NSMutableDictionary * dict                              = [NSMutableDictionary dictionary];
    [dict setObject:@"rideend"                 forKey:kAPI_ACTION];
    [dict setObject:[NSString stringWithFormat:@"%f,%f",latitude,longitude]                 forKey:@"Actual_Drop_Lat_Long"];
    [dict setObject:strreqId                     forKey:@"bookingId"];
    [dict setObject:user_id                       forKey:@"driverId"];
    [dict setObject:self.dropaddress             forKey:@"Actual_Drop_Address"];
   
    [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:kAPI_METHOD_END_REQ];
    
}
-(void)serviceArrived
{
    [Alert svProgress:@"Please Wait..."];
  
    WebService * connection = [[WebService alloc]init];
    connection.delegate = self;
    // Dictionary Login
    NSDictionary * dictLogin           = [[NSUserDefaults standardUserDefaults] dictionaryForKey:kLOGIN];
    
    NSString * user_id;
    if ([dictLogin valueForKey:@"id"] == nil)
    {
        user_id  = [dictLogin valueForKey:@"userId"];
    }
    else
    {
        user_id  = [dictLogin valueForKey:@"id"];
    }
    NSMutableDictionary * dict                              = [NSMutableDictionary dictionary];
    [dict setObject:@"arrived"                 forKey:kAPI_ACTION];
     [dict setObject:[_dictArrived valueForKey:@"bookingId"]   forKey:@"bookingId"];
    [dict setObject:user_id                       forKey:@"driverId"];
    [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:@"driverarrived"];
    
}
-(void)servicepickConfirm
{
    [Alert svProgress:@"Please Wait..."];
  
    WebService * connection = [[WebService alloc]init];
    connection.delegate = self;
    // Dictionary Login
    NSDictionary * dictLogin           = [[NSUserDefaults standardUserDefaults] dictionaryForKey:kLOGIN];
    
    NSString * user_id;
    if ([dictLogin valueForKey:@"id"] == nil)
    {
        user_id  = [dictLogin valueForKey:@"userId"];
    }
    else
    {
        user_id  = [dictLogin valueForKey:@"id"];
    }
    NSMutableDictionary * dict                              = [NSMutableDictionary dictionary];
    [dict setObject:@"orderpicked"                 forKey:kAPI_ACTION];
     [dict setObject:[_dictArrived valueForKey:@"bookingId"]   forKey:@"bookingId"];
    [dict setObject:user_id                       forKey:@"driverId"];
    [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:@"orderpicked"];
    
}
-(void)servicePickup
{
    [Alert svProgress:@"Please Wait..."];
  
    WebService * connection = [[WebService alloc]init];
    connection.delegate = self;
    // Dictionary Login
    NSDictionary * dictLogin           = [[NSUserDefaults standardUserDefaults] dictionaryForKey:kLOGIN];
    
    NSString * user_id;
    if ([dictLogin valueForKey:@"id"] == nil)
    {
        user_id  = [dictLogin valueForKey:@"userId"];
    }
    else
    {
        user_id  = [dictLogin valueForKey:@"id"];
    }
    NSMutableDictionary * dict                              = [NSMutableDictionary dictionary];
    [dict setObject:@"pikuplocation"                 forKey:kAPI_ACTION];
     [dict setObject:[_dictArrived valueForKey:@"bookingId"]   forKey:@"bookingId"];
    [dict setObject:user_id                       forKey:@"driverId"];
    [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:@"pikuplocation"];
    
}

#pragma mark -  Service
-(void)getDataFormWebService:(NSDictionary *)jsonResults urlStr:(NSString *)yourUR methodName:(NSString *)methodName{
    
    [SVProgressHUD dismiss];
    
    NSLog(@"methodName is ----%@",methodName);
    if ([methodName isEqualToString:kAPI_METHOD_UPDATE_LAT_LNG]) {
        
        //        [Alert alertControllerTitle:kAPPICATION_TITLE msg:[jsonResults valueForKey:kMSG_KEY] ok:kOK controller:self.navigationController];
    }
  
    if ([methodName isEqualToString:@"pikuplocation"])
    {
       
        [[NSUserDefaults standardUserDefaults] setValue:@"10" forKey:@"rideStatus"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [_buttonOptions setTitle:@"ARRIVED AT STORE" forState:UIControlStateNormal];
        [self configView];
    }
    if ([methodName isEqualToString:@"orderpicked"])
    {
       
        [[NSUserDefaults standardUserDefaults] setValue:@"8" forKey:@"rideStatus"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [_buttonOptions setTitle:@"CONFIRM IN ROUTE TO DROP" forState:UIControlStateNormal];
        [self configView];
       
    }
    if ([methodName isEqualToString:@"driverarrived"])
    {
       
        [[NSUserDefaults standardUserDefaults] setValue:@"11" forKey:@"rideStatus"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [_buttonOptions setTitle:@"ORDER DELIVERED" forState:UIControlStateNormal];
        [self configView];
       
    }
    else if ([methodName isEqualToString:kAPI_METHOD_START_REQ])
    {
        NSLog(@"jsonResults =%@",jsonResults);
        
        [self.buttonOptions setTitle:@"END RIDE" forState:UIControlStateNormal];
        [Alert alertControllerTitle:kAPPICATION_TITLE msg:[jsonResults valueForKey:@"msg"] ok:kOK controller:self.navigationController];
        [self configView];
         
    }
    
    else if ([methodName isEqualToString:kAPI_METHOD_END_REQ])
    {
        
        NSLog(@"jsonResults =%@",jsonResults);
        
        _viewPayment.hidden = NO;
        _buttonOptions.enabled = YES;
        _buttonDirection.enabled = NO;
        
        NSString * price = [jsonResults valueForKey:@"FinalFare"];
        
        _labelPaymentAmount.text = [NSString stringWithFormat:@"%@\n%@ %@\n%@ %@\n%@ $%@",[jsonResults objectForKey:@"msg"],@"Total Distance:",[jsonResults objectForKey:@"totalDistance"],@"Duration Time:",[jsonResults objectForKey:@"totalTime"],@"Total Amount:",price];
        
    }
    else if ([methodName isEqualToString:kAPI_METHOD_UPDATE_LAT_LNG])
    {
        [self.locationManager startUpdatingHeading];
    }
}
// WEB SERVER ERROR MSG LOCAL SERVER (APPLICATION SERVER)
-(void) webServiceFailWithApplicationServerMSG:(NSString *)msg
{
    
    
    [Alert removeView:self.view];
    [Alert alertViewDefalultWithTitleString:kAPPICATION_TITLE
                                     forMsg:msg forOK:kOK];
}

// WEB SERVICE FAIL WITH REGION TCP/IP
-(void) webServiceFail:(NSError *)error{
    [Alert removeView:self.view];
    [Alert alertViewDefalultWithTitleString:
     kAPPICATION_TITLE forMsg:[error localizedDescription] forOK:kOK];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
