//
//  PaymentViewController.h
//  Q-Workerbee
//
//  Created by santosh kumar singh on 09/05/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface PaymentViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *dummyImage;
@property(nonatomic, strong, readwrite) NSString *environment;
@property(nonatomic, strong, readwrite) NSString *resultText;


@property (strong, nonatomic) NSString * matchfrom;
@property(nonatomic, strong, readwrite) NSString *dateString;
@property(nonatomic, strong, readwrite) NSString *timeString;
@property (weak, nonatomic)  NSString * servicename;
@property(nonatomic, strong, readwrite) NSString *ammount;
@property(nonatomic, strong, readwrite) IBOutlet UIView *successView;
@property (strong, nonatomic) NSString * groupId;
@property (strong, nonatomic) NSString * bookingdateTime;
@property (strong, nonatomic) NSDictionary * dict;

@property (strong, nonatomic) NSString * invoiceId;
@property (strong, nonatomic) NSString * tipString;

@end


