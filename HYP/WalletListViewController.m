//
//  WalletListViewController.m
//  GetSum
//
//  Created by santosh kumar singh on 25/04/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import "WalletListViewController.h"
#import "WalletListTableViewCell.h"

@interface WalletListViewController ()<WebServiceDelegate,UITableViewDelegate,UITableViewDataSource>

@end

@implementation WalletListViewController

- (void)viewDidLoad
{
   // [self.view setBackgroundColor:[Alert colorWithPatternImage2:kBG_IMAGE]];
    
    self.title = @"CASHOUT HISTORY";
    
    
    self.challengeDetal.layer.cornerRadius = 20;
    self.challengeDetal.clipsToBounds=YES;
    self.challengeDetal.hidden= YES;
    self.challengeDetal.layer.borderWidth =5.0f;
    self.challengeDetal.layer.borderColor = [[Alert colorFromHexString:kCOLOR_NAV] CGColor];
    
    self.closebtn.clipsToBounds=YES;
    self.closebtn.layer.cornerRadius = 20;
    
    _noListAvalible.hidden=YES;
    startidx = 1;
    _completedTableView.hidden=YES;
    resultArray =[[NSMutableArray alloc]init];
    
  
    [self navigationBarConfiguration];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)navigationBarConfiguration
{
    
    self.navigationController.navigationBarHidden = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    UIButton * menuButton  = [UIButton buttonWithType:UIButtonTypeSystem];
    menuButton.frame = CGRectMake(8, 18, 24, 24);
    [menuButton setImage:[UIImage imageNamed:IMG_BACK] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *accountBarItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    self.navigationItem.leftBarButtonItem = accountBarItem;
    
    NSMutableArray *arrRightBarItems = [[NSMutableArray alloc] init];
          UIButton *btnSetting = [UIButton buttonWithType:UIButtonTypeCustom];
          [btnSetting setImage:[UIImage imageNamed:@"Frequency"] forState:UIControlStateNormal];
          btnSetting.frame = CGRectMake(0, 0, 40, 40);
          btnSetting.showsTouchWhenHighlighted=YES;
         [btnSetting addTarget:self action:@selector(onSettings:) forControlEvents:UIControlEventTouchUpInside];
          UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSetting];
        
          UIButton *btnLib = [UIButton buttonWithType:UIButtonTypeCustom];
          [btnLib setImage:[UIImage imageNamed:@"logo1.png"] forState:UIControlStateNormal];
          btnLib.frame = CGRectMake(0, 0, 40, 40);
          btnLib.showsTouchWhenHighlighted=YES;
    [btnLib addTarget:self action:@selector(onMyLibrary:) forControlEvents:UIControlEventTouchUpInside];
          UIBarButtonItem *barButtonItem2 = [[UIBarButtonItem alloc] initWithCustomView:btnLib];
          [arrRightBarItems addObject:barButtonItem2];
          [arrRightBarItems addObject:barButtonItem];
          self.navigationItem.rightBarButtonItems=arrRightBarItems;
       
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName :[UIColor whiteColor]}];
}
-(void)onMyLibrary:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}
-(void)onSettings:(id)sender
{
  
    NSString*myurl=@"https://www.hypdemand.com/";
     NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
       [[UIApplication sharedApplication] openURL:url];
    }
}

-(void)back:(id)sender
{
    DISMISS_VIEW_CONTROLLER;
}

-(void)viewWillAppear:(BOOL)animated
{
    [self serviceCompletedList];
}

-(void)serviceCompletedList{
    
    // SERVER CONNCETION
    [Alert svProgress:@"Please Wait..."];
 NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults]  objectForKey:kLOGIN];
      
   // NSDictionary * dictLogin = [NSKeyedUnarchiver unarchiveObjectWithData:data1];
       
    
    NSString * user_id ;
    if ([dictLogin valueForKey:@"id"] == nil)
    {
        user_id  = [dictLogin valueForKey:@"userId"];
        
    }
    else
    {
        user_id  = [dictLogin valueForKey:@"id"];
    }
    
    WebService * connection = [[WebService alloc]init];
    connection.delegate = self;
    NSMutableDictionary * dict                              = [NSMutableDictionary dictionary];
    
    [dict setObject:@"cashoutlist"                     forKey:kAPI_ACTION];
    [dict setObject:user_id                     forKey:kUSER_ID];
    [dict setObject:[NSString stringWithFormat:@"%d", startidx]                     forKey:@"pageNo"];
    
    NSLog(@"dict is ---%@",dict);
    [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:@"cashoutlist"];
    
}


#pragma mark - Sevice delgate methods

-(void)getDataFormWebService:(NSDictionary *)jsonResults urlStr:(NSString *)yourUR methodName:(NSString *)methodName{
    
    [SVProgressHUD dismiss];
    
    SWITCH (methodName) {
        CASE (@"cashoutlist")
        {
            NSLog(@"jsonResults =%@",jsonResults);
            
            arr=[jsonResults valueForKey:@"data"];
            
            
            for ( NSDictionary *dict in [jsonResults valueForKey:@"data"])
            {
                [resultArray addObject:dict];
            }
            
            if (arr.count==0)
            {
                
                if (arr.count ==0 && resultArray.count ==0)
                {
                    _noListAvalible.hidden=NO;
                    self.completedTableView.hidden=YES;
                }
                
            }
            else
            {
                _noListAvalible.hidden=YES;
                _completedTableView.hidden=NO;
                self.completedTableView.delegate=self;
                self.completedTableView.dataSource=self;
                [self.completedTableView reloadData];
                
            }
        }
       
        break;
        
        DEFAULT
        {
            break;
        }
    }
    
}
// WEB SERVER ERROR MSG LOCAL SERVER (APPLICATION SERVER)
-(void) webServiceFailWithApplicationServerMSG:(NSString *)msg
{
    
    [Alert svError:msg];
    
}

// WEB SERVICE FAIL WITH REGION TCP/IP
-(void) webServiceFail:(NSError *)error{
    
    
    [Alert svError:[error localizedDescription]];
    
    
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
    
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    
    return resultArray.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    static  NSString *identifierDriver = @"WalletListTableViewCell";
    
    WalletListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifierDriver forIndexPath:indexPath];
      
    NSString * UserName = [[resultArray valueForKey:@"created"]objectAtIndex:indexPath.row];
    
    NSString * UserPhone = [NSString stringWithFormat:@"Amount: $%@",[[resultArray valueForKey:@"requestAmount"]objectAtIndex:indexPath.row]];
      
     cell.delegatewaitingLabel.text= UserPhone;
    cell.teamLabel.text = UserName;
    
    cell.waitingLabel.layer.cornerRadius = 8;
    cell.approvedLabel.layer.cornerRadius = 8;
    
    if ([[[resultArray valueForKey:@"sendDate"]objectAtIndex:indexPath.row] isEqualToString:@""])
    {
        cell.approvedLabel.hidden= YES;
        cell.waitingLabel.hidden= NO;
    }
    
    else
    {
        cell.approvedLabel.hidden= NO;
        cell.waitingLabel.hidden= YES;
    }
    
    
    
    return cell;
    
}



- (void)tableView:(UITableView *)tableView willDisplayCell: (UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if (indexPath.row == [resultArray count] - 1 )
    {
        if (arr.count==0)
        {
            
        }
        else{
            
            startidx += 1;
            [self serviceCompletedList];
            
        }
        
        
    }
    
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
