//
//  RateSubmitViewController.h
//  UpDriver
//
//  Created by santosh kumar singh on 21/03/21.
//  Copyright © 2021 Nitin Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface RateSubmitViewController : UIViewController

@property (nonatomic,strong) NSMutableArray * bookingDetail;

@property(nonatomic,weak)  NSString * rateingString;
@property (weak, nonatomic) IBOutlet UILabel *lblDuration;

@property (weak, nonatomic) IBOutlet UILabel *lblTotal;
@property (nonatomic, strong) IBOutlet UIView * viewTopView;

@property (weak, nonatomic) IBOutlet UILabel *lblBookingDate;

@property(nonatomic,weak) IBOutlet UITextField * msgtextfiled;
@property(nonatomic,weak) IBOutlet UIButton * submitbtn;
@property(nonatomic,weak) IBOutlet RateView * rateUpdate;

@property (weak, nonatomic) IBOutlet UILabel *pickupAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *dropAddressLabel;

@property (nonatomic, strong) IBOutlet UIView * viewPayment;
@property (nonatomic, strong) IBOutlet UIView * viewDistance;
@property(nonatomic,strong)NSMutableArray *detailArray;
@end


