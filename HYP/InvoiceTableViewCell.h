//
//  InvoiceTableViewCell.h
//  HYP
//
//  Created by santosh kumar singh on 10/09/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface InvoiceTableViewCell : UITableViewCell

@property(nonatomic,weak) IBOutlet UILabel * servicesName;
@property(nonatomic,weak) IBOutlet UILabel * servicesAmount;
@property(nonatomic,weak) IBOutlet UILabel * invoiceDiscription;
@end


