//
//  DashboardViewController.m
//  HYP
//
//  Created by santosh kumar singh on 31/08/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import "DashboardViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "NewRequestViewController.h"
#import "UserDetailViewController.h"
#import "ViewControllerCell.h"
#import "ViewControllerCellHeader.h"
#import "SelectLocationViewController.h"
#import <MessageUI/MessageUI.h>
#import "SendRequestViewController.h"
#import "BookingProgressViewController.h"
#import "PendingPaymentViewController.h"



#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

@interface DashboardViewController ()<MKMapViewDelegate,CLLocationManagerDelegate,WebServiceDelegate,UITableViewDataSource, UITableViewDelegate,GMSMapViewDelegate,MFMailComposeViewControllerDelegate>
{
    CLLocationManager *locationManager;
    BOOL isMultipleExpansionAllowed;
    int count ;
    
}
@end

@implementation DashboardViewController

#pragma mark --- : GOOGLE MAP :----
-(void)showGoogleMap
{
    
    CLLocation * upadetLocation = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
    
    CGRect frame = CGRectMake(0, 0, kSCREEN_WIDTH,kSCREEN_HEIGHT);
    camera = [GMSCameraPosition cameraWithLatitude:latitude longitude:longitude zoom:15];
    mapView_ = [GMSMapView mapWithFrame:frame camera:camera];
    mapView_.delegate = self;
    
    [mapView_ setCamera:camera];
    
    // satelliteFlyover
    [self reverGeoCodingUsingGoogle:upadetLocation];
    
    self->mapView_.myLocationEnabled              = YES;
    self->mapView_.indoorEnabled                  = YES;
    self->mapView_.settings.myLocationButton      = NO;
    self->mapView_.settings.scrollGestures        = YES;
    
    [_mapView addSubview:mapView_];
    NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"ProfileDetail"];
    
    NSString * str = [dictLogin valueForKey:@"role"];
    
    if ([str isEqualToString:@"Member"])
    {
        locationMarker_ = [[GMSMarker alloc] init];
        locationMarker_.position = CLLocationCoordinate2DMake(latitude, longitude);
        locationMarker_.icon = [UIImage imageNamed:@"marker"];
        locationMarker_.map = mapView_;
    }
    else{
        locationMarker_ = [[GMSMarker alloc] init];
        locationMarker_.position = CLLocationCoordinate2DMake(latitude, longitude);
        locationMarker_.icon = [UIImage imageNamed:@"car1.png"];
        locationMarker_.map = mapView_;
    }
    
    
}

- (void)updateState:(MDSwitch *)switchmd
{
    // MDSwitch *mdSwitch = (MDSwitch *)sender;
    
    [Alert svProgress:@"Please Wait..."];
    
    NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults] dictionaryForKey:kLOGIN];
    NSString *struserStatus =[[dictLogin valueForKey:@"availableStatus"]stringValue];
    
    NSString * user_id ;
    if ([dictLogin valueForKey:@"id"] == nil)
    {
        user_id  = [dictLogin valueForKey:@"userId"];
        
    }
    else
    {
        user_id  = [dictLogin valueForKey:@"id"];
    }
    
    WebService * connection = [[WebService alloc]init];
    connection.delegate = self;
    NSMutableDictionary * dict                              = [NSMutableDictionary dictionary];
    
    [dict setObject:@"onoff"                     forKey:kAPI_ACTION];
    [dict setObject:user_id                     forKey:kUSER_ID];
    
    
    if ([struserStatus isEqualToString:@"1"])
    {
        [dict setObject:@"0"                     forKey:@"availableStatus"];
        self.matchstatus = @"0";
        self.statusLabel.text = @"SIGN OFF";
    }
    else
    {
        [dict setObject:@"1"                     forKey:@"availableStatus"];
        self.matchstatus = @"1";
        self.statusLabel.text = @"SIGN IN";
    }
    
    NSLog(@"dict is ---%@",dict);
    [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:@"onoff"];
    
    
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    
    [self.locationManager stopUpdatingLocation];
    //  CLLocation *mostRecentLocation = [locations objectAtIndex:0];
    
}

-(void)reverGeoCodingUsingGoogle:(CLLocation *)location
{
    [[GMSGeocoder geocoder] reverseGeocodeCoordinate:CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude) completionHandler:^(GMSReverseGeocodeResponse* response, NSError* error)
     {
        
        GMSReverseGeocodeResult *result = response.firstResult;
        
        NSArray   * arrayAdd = result.lines;
        
        NSString  * address1;
        NSString  * address2;
        
        if (arrayAdd.count>1)
        {
            address1 = result.lines[0];
            address2 = result.subLocality;
        }
        else
        {
            address1 = result.lines[0];
        }
        
    }];
    
    
}

- (void)viewDidLoad {
    
    NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"ProfileDetail"];
    
    //  NSString * userType  = [dict valueForKey:kAPI_USER_TYPE];
    
    self.driverName.text = [dictLogin valueForKey:@"fullName"];
    
    self.welcomLabel.text = [NSString stringWithFormat:@"Welcome!!!\n%@",@"You are logged into the HYP Matrix"];

    self.welcomUserLabel.text = [NSString stringWithFormat:@"Welcome %@, %@",[dictLogin valueForKey:@"fullName"], @"How can we help today?"];

    if ([[dictLogin valueForKey:@"image"] isEqualToString:@""])
    {
        
        NSString *userName = [dictLogin valueForKey:@"fullName"];
        [self.driverImageView setImageWithString:userName color:nil circular:YES];
                     
    }
    else
    {
        
        [self.driverImageView setImageURL:[NSURL URLWithString:[dictLogin objectForKey:@"image"]]];
    }
    
    
    self.cartView.layer.cornerRadius = 8;
    self.cartView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.cartView.layer.borderWidth = 1.0f;
    self.cartView.clipsToBounds= YES;
    
    self.shoppingView.hidden = YES;
    self.categoryView.hidden = YES;
    
    self.emailbtn.titleLabel.numberOfLines = 2;
    CLLocationCoordinate2D coordinate = [self getLocation];
    
    [AppDelegate appDelegate].latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
    [AppDelegate appDelegate].longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
    
    latitude = [[AppDelegate appDelegate].latitude floatValue];
    longitude = [[AppDelegate appDelegate].longitude floatValue];
    
    self.geocoder = [[GMSGeocoder alloc]init];
    
    [self showGoogleMap];
    
    NSString * str = [dictLogin valueForKey:@"role"];
    
    if ([str isEqualToString:@"Member"])
    {
    
        self.notifiactionView.hidden= YES;
        self.doneBtn.backgroundColor = [Alert colorFromHexString:@"#F35FF3"];
        [self.doneBtn setTitle:@"POST A JOB REQUEST" forState:UIControlStateNormal];
        latitlong = [[NSMutableArray alloc]init];
        [self updateSearchArray];
        self.statusView.hidden = YES;
        self.providerView.hidden =  YES;
        self.userView.hidden =  NO;
        if ([[_notificationDict objectForKey:@"type"] isEqualToString:@"rideend"]) {
          
            self.viewPayment.hidden= NO;
            NSString * price = [_notificationDict valueForKey:@"FinalFare"];
            
            _labelPaymentAmount.text = @"Your order has been delivered successfully, Please check your invoice below";
            
        }
        else{
            self.viewPayment.hidden= YES;
        }
        
    }
    else
    {
        
        if (self.notificationDict)
        {
         
            [self.addressNotification setText:[_notificationDict objectForKey:@"address"]];
            
            self.notifiactionView.hidden= NO;
                self.viewPayment.hidden= YES;
            self.serviceNoti.text = [_notificationDict objectForKey:@"categoryName"];
            self.dateTimenotif.text =[NSString stringWithFormat:@"%@", [_notificationDict objectForKey:@"bookingDate"]];
            NSString *location = [_notificationDict objectForKey:@"address"];
            CLGeocoder *geocoder = [[CLGeocoder alloc] init];
            [geocoder geocodeAddressString:location
                         completionHandler:^(NSArray* placemarks, NSError* error){
                if (placemarks && placemarks.count > 0) {
                    CLPlacemark *topResult = [placemarks objectAtIndex:0];
                    MKPlacemark *placemark = [[MKPlacemark alloc] initWithPlacemark:topResult];
                    
                    latitude =  placemark.coordinate.latitude;
                    longitude = placemark.coordinate.longitude;
                    CLLocation * upadetLocation = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
                    
                    CGRect frame = CGRectMake(0, 0, _mapView1.frame.size.width,_mapView1.frame.size.height);
                    camera = [GMSCameraPosition cameraWithLatitude:latitude longitude:longitude zoom:15];
                    mapView1_ = [GMSMapView mapWithFrame:frame camera:camera];
                    mapView1_.delegate = self;
                    
                    [mapView1_ setCamera:camera];
                    mapView1_.clipsToBounds= YES;
                    [_mapView1 addSubview:mapView1_];
                    
                    locationMarker_ = [[GMSMarker alloc] init];
                    locationMarker_.position = CLLocationCoordinate2DMake(latitude, longitude);
                    locationMarker_.icon = [UIImage imageNamed:@"marker"];
                    locationMarker_.map = mapView1_;
                    
                }
            }
             ];
            }
       
        else
        {
            self.notifiactionView.hidden= YES;
            self.viewPayment.hidden= YES;
        }
        self.statusView.hidden = NO;
        [self.doneBtn setTitle:@"PENDING REQUEST" forState:UIControlStateNormal];
        self.doneBtn.hidden =  YES;
        self.providerView.hidden =  NO;
        self.userView.hidden =  YES;
        
    }
    
    
    NSString *struserStatus =[[dictLogin valueForKey:@"availableStatus"]stringValue];
    
    if ([struserStatus isEqualToString:@"1"])
    {
        mdSwitch.on =YES;
        self.statusLabel.text = @"SIGN IN";
        
        
    }
    else{
        
        mdSwitch.on = NO;
        self.statusLabel.text = @"SIGN OFF";
    }
    
    [mdSwitch addTarget:self
                 action:@selector(updateState:)
       forControlEvents:UIControlEventValueChanged];
    
    self.title = @"DASHBOARD";
    
    count= 0;
    [self serviceCategoryList];
    isMultipleExpansionAllowed = NO;
    
    arrSelectedSectionIndex = [[NSMutableArray alloc] init];
    
    [self navigationBarConfiguration];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(IBAction)btnOkTapped:(UIButton*)sender
{
    PendingPaymentViewController * bookingDetailsViewController = GET_VIEW_CONTROLLER(@"PendingPaymentViewController");
   bookingDetailsViewController.bookingDetail = self.notificationDict;
   MOVE_VIEW_CONTROLLER(bookingDetailsViewController, YES);
}

-(IBAction)btnTapped:(UIButton*)sender
{  // SERVER CONNCETION
    
    
    [Alert svProgress:@"Please Wait..."];
    
    NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults] dictionaryForKey:kLOGIN];
    NSString * user_id ;
    if ([dictLogin valueForKey:@"id"] == nil)
    {
        user_id  = [dictLogin valueForKey:@"userId"];
        
    }
    else
    {
        user_id  = [dictLogin valueForKey:@"id"];
    }
    WebService * connection = [[WebService alloc]init];
    connection.delegate = self;
    NSMutableDictionary * dict                              = [NSMutableDictionary dictionary];
    
    [dict setObject:@"changebookingstatus"                     forKey:kAPI_ACTION];
    [dict setObject:user_id                     forKey:@"driverId"];
    [dict setObject:[_notificationDict objectForKey:@"bookingId"]                     forKey:@"bookingId"];
    [dict setObject:@""                    forKey:@"groupId"];
    if ([[self.notificationDict valueForKey:@"today"] isEqualToString:@"today"])
    {
         [dict setObject:@"CURRENT"                     forKey:@"bookingTYPE"];
    }
    
    if (sender.tag == 0 )
    {
        
        [dict setObject:@"1"                     forKey:@"status"];
        
    }
    else
    {
        [dict setObject:@"3"                     forKey:@"status"];
    }
    
    NSLog(@"dict is ---%@",dict);
    [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:@"changebookingstatus"];
    
}


-(IBAction)cancelBtn:(id)sender
{  // SERVER CONNCETION
    
    self.notifiactionView.hidden= YES;
    
}

-(IBAction)btnContinue:(UIButton*)sender
{
   self.categoryView.hidden = NO;
   
}


-(IBAction)btnShopClick:(UIButton*)sender
{
    
    shoptblView.delegate= self;
    shoptblView.dataSource= self;
    [shoptblView reloadData];
    
     self.shoppingView.hidden = NO;
}


-(IBAction)btnDoneTapped:(UIButton*)sender
{
    if (!_selecteditem)
    {
        [Alert svError:@"Please Select Services"];
    }
    else
    {
        SelectLocationViewController *forgotPasswordViewController =  GET_VIEW_CONTROLLER(kSBIDHomePassengerVC);
        forgotPasswordViewController.dict=  self.dictonartSelct;
        UINavigationController * navigationController = [Alert navigationControllerWithVC:forgotPasswordViewController];
        PERSENT_VIEW_CONTOLLER(navigationController, YES);
    }
}

-(CLLocationCoordinate2D) getLocation
{
    CLLocationManager *locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager startUpdatingLocation];
    CLLocation *location = [locationManager location];
    CLLocationCoordinate2D coordinate = [location coordinate];
    
    return coordinate;
}

#pragma mark - Configure Navigation Controller

-(void)navigationBarConfiguration
{
    
    self.navigationController.navigationBarHidden = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    UIButton * menuButton  = [UIButton buttonWithType:UIButtonTypeSystem];
    menuButton.frame = CGRectMake(8, 18, 24, 24);
    [menuButton setBackgroundImage:[UIImage imageNamed:NAV_SLIDER_IMAGE] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(drawer:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *accountBarItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    self.navigationItem.leftBarButtonItem = accountBarItem;
    
    NSMutableArray *arrRightBarItems = [[NSMutableArray alloc] init];
    UIButton *btnSetting = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnSetting setImage:[UIImage imageNamed:@"Frequency"] forState:UIControlStateNormal];
    btnSetting.frame = CGRectMake(0, 0, 50, 50);
    btnSetting.showsTouchWhenHighlighted=YES;
    [btnSetting addTarget:self action:@selector(onSettings:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSetting];
    
    UIButton *btnLib = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnLib setImage:[UIImage imageNamed:@"logo1.png"] forState:UIControlStateNormal];
    btnLib.frame = CGRectMake(0, 0, 50, 40);
    btnLib.showsTouchWhenHighlighted=YES;
    [btnLib addTarget:self action:@selector(onMyLibrary:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButtonItem2 = [[UIBarButtonItem alloc] initWithCustomView:btnLib];
    [arrRightBarItems addObject:barButtonItem2];
    [arrRightBarItems addObject:barButtonItem];
    self.navigationItem.rightBarButtonItems=arrRightBarItems;
    
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName :[UIColor whiteColor]}];
}

-(void)onMyLibrary:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}

-(void)onSettings:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/hyptunesradio";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}


-(IBAction)onLink:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}



-(IBAction)onclickCall:(id)sender
{
    NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults] dictionaryForKey:kLOGIN];
   
    if (TARGET_OS_SIMULATOR)
    {
        [Alert alertControllerTitle:kAPPICATION_TITLE msg:kDEVICE_NOT_SUPPORT ok:kOK controller:self.navigationController];
    }
    else
    {
        
        [Alert makePhoneCall:@"5713367738"];
    }
}

-(IBAction)onclickDeliver:(id)sender
{
    [pickupBtn setImage:[UIImage imageNamed:@"whiteuncheckbox"] forState:UIControlStateNormal];
    [deliverBtn setImage:[UIImage imageNamed:@"whitecheckbox"] forState:UIControlStateNormal];
    self.matchDeliveryType = @"#1 Shop & Deliver this Order";
}

-(IBAction)onclickPickUp:(id)sender
{
    [pickupBtn setImage:[UIImage imageNamed:@"whitecheckbox"] forState:UIControlStateNormal];
    [deliverBtn setImage:[UIImage imageNamed:@"whiteuncheckbox"] forState:UIControlStateNormal];
    self.matchDeliveryType = @"#2 I need Pick Up & Delivery";
}

-(IBAction)btnContinueShop:(UIButton*)sender
{
    if (self.matchDeliveryType.length==nil)
    {
        [Alert svError:@"Please Select Service"];
    }
    else if (self.selecteditem.length== nil)
    {
        [Alert svError:@"Please Select Vehicle"];
    }
    
  else
  {
      SendRequestViewController *forgotPasswordViewController =  GET_VIEW_CONTROLLER(@"SendRequestViewController");
      forgotPasswordViewController.deleviryType=  self.matchDeliveryType;
      forgotPasswordViewController.vehicleId=  self.selecteditem;
      forgotPasswordViewController.vehicleName=  self.matchVehicleName;
      forgotPasswordViewController.vehicleImagee=  self.matchVehicleImage;
      UINavigationController * navigationController = [Alert navigationControllerWithVC:forgotPasswordViewController];
      PERSENT_VIEW_CONTOLLER(navigationController, YES);
    }
}

#pragma mark - Drawer class
-(void)drawer:(id)sender
{
    
    KYDrawerController *elDrawer = (KYDrawerController*)self.navigationController.parentViewController;
    [elDrawer setDrawerState:KYDrawerControllerDrawerStateOpened animated:YES];
    
    
}

//update seach method where the textfield acts as seach bar
-(void)updateSearchArray
{
    NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults]  objectForKey:kLOGIN];
    
    NSString * user_id ;
    if ([dictLogin valueForKey:@"id"] == nil)
    {
        user_id  = [dictLogin valueForKey:@"userId"];
        
    }
    else
    {
        user_id  = [dictLogin valueForKey:@"id"];
    }
    
    [Alert svProgress:@"Please Wait..."];
    
    WebService * connection = [[WebService alloc]init];
    connection.delegate = self;
    NSMutableDictionary * dict                              = [NSMutableDictionary dictionary];
    
    [dict setObject:@"findvendor"                     forKey:kAPI_ACTION];
    [dict setObject:user_id                     forKey:kUSER_ID];
    [dict setObject:[AppDelegate appDelegate].latitude                     forKey:kAPI_USER_LATITUDE];
    [dict setObject:[AppDelegate appDelegate].longitude                     forKey:kAPI_USER_LONGITUDE];
    
    NSLog(@"dict is ---%@",dict);
    [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:@"findvendor" ];
    
}
#pragma mark - Sevice delgate methods

-(void)getDataFormWebService:(NSDictionary *)jsonResults urlStr:(NSString *)yourUR methodName:(NSString *)methodName{
    
    [SVProgressHUD dismiss];
    
    SWITCH (methodName) {
        CASE (@"findvendor")
        {
            imagedistance = [jsonResults objectForKey:@"data"];
            int i;
            for (i=0; i<imagedistance.count; i++)
            {
                
                locationMarker_ = [[GMSMarker alloc] init];
                locationMarker_.position = CLLocationCoordinate2DMake([[[imagedistance objectAtIndex:i]objectForKey:@"latitude"] floatValue], [[[imagedistance objectAtIndex:i]objectForKey:@"longitude"] floatValue]);
                locationMarker_.title =[[imagedistance objectAtIndex:i]objectForKey:@"fullName"];
                locationMarker_.userData=[[imagedistance objectAtIndex:i]objectForKey:@"id"];
                locationMarker_.icon = [UIImage imageNamed:@"car1.png"];
                locationMarker_.map = mapView_;
               
            }
        
        }
        CASE (@"onoff")
        {
            NSLog(@"jsonResults =%@",jsonResults);
            
            if ([[jsonResults valueForKey:@"status"] isEqualToString:@"success"])
            {
                [Alert svSuccess:[jsonResults valueForKey:@"msg"]];
                
                NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"ProfileDetail"];
                
                NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
                
                [newDict addEntriesFromDictionary:dictLogin];
                [newDict setObject:self.matchstatus forKey:@"availableStatus"];
                
                NSString *struserStatus =[[newDict valueForKey:@"availableStatus"]stringValue];
                
                [[NSUserDefaults standardUserDefaults] setObject:newDict forKey:@"ProfileDetail"];
                
                
                self.notifiactionView.hidden= YES;
                
            }
        }
        CASE (kAPI_METHOD_CATERGORY)
        {
            
            
            resultArray = [[NSMutableArray alloc]init];
            resultArray=[jsonResults valueForKey:@"data"];
            NSMutableDictionary *mDictionary = [[NSMutableDictionary alloc]init];
            NSMutableArray *dict1 = [[NSMutableArray alloc]init];
            
            
            NSMutableDictionary *item = [NSMutableDictionary dictionary];
            [item setValue:@"https://www.hypdemand.com/locations" forKey:@"Link"];
            [dict1 addObject:item];
            
            [mDictionary setValue:dict1 forKey:@"SubCat"];
            NSString *html = @"Shop from these Locations";
            [mDictionary setValue:html forKey:@"name"];
            
            [resultArray insertObject:mDictionary atIndex:resultArray.count];
            
            tblView.delegate= self;
            tblView.dataSource= self;
            [tblView reloadData];
            
            
            
        }
        
        CASE (@"changebookingstatus")
        {
            NSLog(@"jsonResults =%@",jsonResults);
            if ([[self.notificationDict valueForKey:@"today"] isEqualToString:@"today"])
            {
                
        
            if (([[jsonResults objectForKey:@"msg"] isEqualToString:@"Booking accepted successfully."]))
            {
                [Alert svSuccess:[jsonResults valueForKey:@"msg"]];
                BookingProgressViewController * bookingHistoryViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"BookingProgressViewController");
                bookingHistoryViewController.dict = _notificationDict;
                UINavigationController * navigationController    = [Alert navigationControllerWithVC:bookingHistoryViewController];
                PERSENT_VIEW_CONTOLLER(navigationController, YES);
            }
            else{
                
                [Alert svSuccess:[jsonResults valueForKey:@"msg"]];
                self.notifiactionView.hidden= YES;
            }
            }
            else{
                [Alert svSuccess:[jsonResults valueForKey:@"msg"]];
                               self.notifiactionView.hidden= YES;
            }
            
        }
        
        
        break;
        
        DEFAULT
        {
            break;
        }
    }
    
}

-(void)serviceCategoryList{
    
    // SERVER CONNCETION
    [Alert svProgress:@"Please Wait..."];
    
    WebService * connection = [[WebService alloc]init];
    connection.delegate = self;
    NSMutableDictionary * dict                              = [NSMutableDictionary dictionary];
    
    [dict setObject:kAPI_METHOD_CATERGORY                     forKey:kAPI_ACTION];
    
    NSLog(@"dict is ---%@",dict);
    [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:kAPI_METHOD_CATERGORY];
    
}

- (void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker {
    // your code
    
//    UserDetailViewController *forgotPasswordViewController =  GET_VIEW_CONTROLLER(@"UserDetailViewController");
//    
//    forgotPasswordViewController.vendorId=  marker.userData;
//    UINavigationController * navigationController = [Alert navigationControllerWithVC:forgotPasswordViewController];
//    PERSENT_VIEW_CONTOLLER(navigationController, YES);
}


-(void) webServiceFailWithApplicationServerMSG:(NSString *)msg
{
    
    [Alert svError:msg];
    
}

// WEB SERVICE FAIL WITH REGION TCP/IP
-(void) webServiceFail:(NSError *)error{
    
    
    [Alert svError:[error localizedDescription]];
    
    
}

#pragma mark - TableView methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(tableView == shoptblView)
    {
        return 1;
    }
    else
    {
        return resultArray.count;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView == shoptblView)
    {

        return [AppDelegate appDelegate].vehicleArr.count+1;
    }
    else
    {
    if ([arrSelectedSectionIndex containsObject:[NSNumber numberWithInteger:section]])
    {
        NSDictionary *section1 = resultArray[section];
        NSArray *rows = section1[@"SubCat"];
        return rows.count;
    }
    else
    {
        return 0;
    }
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == shoptblView)
    {
        ViewControllerCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ViewControllerCell"];
        
        if (cell ==nil)
        {
            [tblView registerClass:[ViewControllerCell class] forCellReuseIdentifier:@"ViewControllerCell"];
            cell = [tblView dequeueReusableCellWithIdentifier:@"ViewControllerCell"];
        }
       
        
        if (indexPath.row == [AppDelegate appDelegate].vehicleArr.count)
        {
           
            NSDictionary *underlineAttribute = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
            cell.lblName.attributedText = [[NSAttributedString alloc] initWithString:@"Shop Locations"
                                                                     attributes:underlineAttribute];
            [cell.imageview setImage:[UIImage  imageNamed:@"shoping_trolly.jpg"]];
            cell.btnShowHide.hidden =  YES;
            
        }
        else
        {
           
            cell.lblName.text = [[AppDelegate appDelegate].vehicleArr[indexPath.row]valueForKey:@"name"];
           
            [cell.imageview setImageURL:[NSURL URLWithString:[[AppDelegate appDelegate].vehicleArr[indexPath.row]valueForKey:@"image"]]];
           
            cell.btnShowHide.hidden =  NO;
            
            if ([[NSString stringWithFormat:@"%@",[[AppDelegate appDelegate].vehicleArr[indexPath.row]valueForKey:@"id"]] isEqualToString:_selecteditem])
            {
                
                [[cell btnShowHide] setImage:[UIImage imageNamed:@"checkBlack"] forState:UIControlStateNormal];
                
            }
            else
            {
                [[cell btnShowHide] setImage:[UIImage imageNamed:@"UnCheckBlack"] forState:UIControlStateNormal];
            }
       
        }
        return cell;
    }
    else{
        
    ViewControllerCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ViewControllerCell"];
    
    if (cell ==nil)
    {
        [tblView registerClass:[ViewControllerCell class] forCellReuseIdentifier:@"ViewControllerCell"];
        cell = [tblView dequeueReusableCellWithIdentifier:@"ViewControllerCell"];
    }
    
    NSDictionary *section = resultArray[indexPath.section];
    NSArray *rows = section[@"SubCat"];
    
    i = indexPath.section;
    
    NSDictionary *row = rows[indexPath.row];
    
    NSString *title = row[@"name"];
    
    if (i == resultArray.count-1)
    {
        
        NSURL *url = [NSURL URLWithString:@"https://www.hypdemand.com/locations"];
        if ([[UIApplication sharedApplication] canOpenURL:url])
        {
            [[UIApplication sharedApplication] openURL:url];
        }
    
    }
    else
    {
        if ([[row[@"id"] stringValue] isEqualToString:@"118"])
        {
            cell.lblName.text = @"AisleRunz Shop/Pick-Up/ Delivery";
        }
        else
        {
            cell.lblName.text = title;
        }
        
        if ([[NSString stringWithFormat:@"%@",row[@"id"]] isEqualToString:_selecteditem])
        {
            
            [[cell btnShowHide] setImage:[UIImage imageNamed:@"Uncheck1"] forState:UIControlStateNormal];
            
        }
        else
        {
            [[cell btnShowHide] setImage:nil forState:UIControlStateNormal];
        }
        
        
        [[cell btnShowHide] setTag:indexPath.row];
    }
//     [[cell btnShowHide] addTarget:self action:@selector(btnTapSection:) forControlEvents:UIControlEventTouchUpInside];
    
    
    return cell;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView == shoptblView)
    {
        return 1;
    }
    else
    {
       return 45;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (tableView == shoptblView)
    {
        return nil;
    }
     else
     {
      
    ViewControllerCellHeader *headerView = [tableView dequeueReusableCellWithIdentifier:@"ViewControllerCellHeader"];
    
    if (headerView ==nil)
    {
        [tblView registerClass:[ViewControllerCellHeader class] forCellReuseIdentifier:@"ViewControllerCellHeader"];
        
        headerView = [tableView dequeueReusableCellWithIdentifier:@"ViewControllerCellHeader"];
    }
      if ([[[resultArray valueForKey:@"name"]objectAtIndex:section] isEqualToString:@"AisleRunz Driver"])
    {
        headerView.lbTitle.text = @"Shop & Delivery";
      
    }
    else
    {
        headerView.lbTitle.text = [[resultArray valueForKey:@"name"]objectAtIndex:section];
    }
    if ([arrSelectedSectionIndex containsObject:[NSNumber numberWithInteger:section]])
    {
        headerView.btnShowHide.selected = YES;
        
    }
    else
    {
   
    [[headerView btnShowHide] setTag:section];
    
    [[headerView btnShowHide] addTarget:self action:@selector(btnTapShowHideSection:) forControlEvents:UIControlEventTouchUpInside];
    }
    return headerView.contentView;
    }
}


-(IBAction)btnTapShowHideSection:(UIButton*)sender
{
    if (!sender.selected)
    {
        if (!isMultipleExpansionAllowed)
        {
            if (!arrSelectedSectionIndex)
            {
                
                [arrSelectedSectionIndex addObject:[NSNumber numberWithInteger:sender.tag]];
            }
            else
            {
                [arrSelectedSectionIndex removeAllObjects];
                [arrSelectedSectionIndex addObject:[NSNumber numberWithInteger:sender.tag]];
                
            }
        }
        else {
            
            [arrSelectedSectionIndex addObject:[NSNumber numberWithInteger:sender.tag]];
        }
        
        sender.selected = YES;
    }else
    {
        sender.selected = NO;
        
        if ([arrSelectedSectionIndex containsObject:[NSNumber numberWithInteger:sender.tag]])
        {
            [arrSelectedSectionIndex removeObject:[NSNumber numberWithInteger:sender.tag]];
        }
    }
    
    if (!isMultipleExpansionAllowed) {
        [tblView reloadData];
    }
    else
    {
        [tblView reloadSections:[NSIndexSet indexSetWithIndex:sender.tag] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //  NSNumber *section = [NSNumber numberWithInt:indexPath.section];
   
    if (tableView == shoptblView)
    {
        if (indexPath.row ==  [AppDelegate appDelegate].vehicleArr.count)
        {
            NSURL *url = [NSURL URLWithString:@"https://www.hypdemand.com/hypdelivers"];
            if ([[UIApplication sharedApplication] canOpenURL:url])
            {
                [[UIApplication sharedApplication] openURL:url];
            }
        }
        else
        {
        NSDictionary *row = [AppDelegate appDelegate].vehicleArr[indexPath.row];
        NSString *title =[NSString stringWithFormat:@"%@", row[@"id"]];
       
        if(selection)
        {
            _selecteditem = nil;
        }
        if([selection isEqual:indexPath])
        {
            selection = nil;
        }
        else
        {
           _selecteditem  = title;
            self.matchVehicleImage = row[@"image"];
            self.matchVehicleName = row[@"name"];
          
            selection = indexPath;
            self.dictonartSelct = row;
        }
        NSLog(@"_selecteditem =%@",_selecteditem);
        
        [shoptblView reloadData];
        
        }
    }
     else
     {
         
        NSDictionary *section = resultArray[i];
      
        NSArray *rows = section[@"SubCat"];
        
        NSDictionary *row = rows[indexPath.row];
        
        NSString *title =[NSString stringWithFormat:@"%@", row[@"id"]];
        
        if(selection)
        {
            _selecteditem = nil;
        }
        if([selection isEqual:indexPath])
        {
            selection = nil;
        }
        else
        {
            _selecteditem =title;
            selection = indexPath;
            self.dictonartSelct = row;
        }
        NSLog(@"_selecteditem =%@",_selecteditem);
        
        [tblView reloadData];
 
     }
}
-(IBAction)buttonClose:(id)sender
{
    self.categoryView.hidden = YES;
    self.shoppingView.hidden = YES;
}


-(IBAction)buttonCallClick:(id)sender
{
    
    if (TARGET_OS_SIMULATOR)
    {
        [Alert alertControllerTitle:kAPPICATION_TITLE msg:kDEVICE_NOT_SUPPORT ok:kOK controller:self.navigationController];
    }
    else
    {
        
        [Alert makePhoneCall:@"571-3367738‬"];
    }
    
}

-(IBAction)buttonEmailClick:(id)sender
{
    if (TARGET_OS_SIMULATOR) {
        
        [Alert alertControllerTitle:kAPPICATION_TITLE msg:kDEVICE_NOT_SUPPORT ok:kOK controller:self.navigationController];
    }
    else
    {
        MFMailComposeViewController *comp=[[MFMailComposeViewController alloc]init];
        [comp setMailComposeDelegate:self];
        if([MFMailComposeViewController canSendMail])
        {
            
            [comp setToRecipients:[NSArray arrayWithObjects:@"HYPCARTS@GMAIL.COM", nil]];
            
            [comp setSubject:@""];
            [comp setMessageBody:@"" isHTML:NO];
            [comp setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
            [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentModalViewController:comp animated:YES];
            
        }
        else{
            [Alert alertControllerTitle:kAPPICATION_TITLE msg:kDEVICE_NOT_SUPPORT ok:kOK controller:self.navigationController];
            
        }
    }
    
    
}

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result
                       error:(NSError *)error
{
    
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    switch (result) {
        case MFMailComposeResultCancelled:
        {
            [Alert alertControllerTitle:kAPPICATION_TITLE msg:@"Mail Cancelled" ok:kOK controller:self.navigationController];
        }
            break;
        case MFMailComposeResultSaved:
        {
            [Alert alertControllerTitle:kAPPICATION_TITLE msg:@"Mail Saved" ok:kOK controller:self.navigationController];
        }
            break;
        case MFMailComposeResultSent:
        {
            [Alert alertControllerTitle:kAPPICATION_TITLE msg:@"Mail Sent Successfully!" ok:kOK controller:self.navigationController];
        }
            break;
        case MFMailComposeResultFailed:
        {
            [Alert alertControllerTitle:kAPPICATION_TITLE msg:@"Unable to Send Email?" ok:kOK controller:self.navigationController];
        }
            break;
            
        default:
            break;
    }
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
