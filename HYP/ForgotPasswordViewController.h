//
//  ForgotPasswordViewController.h
//  CHORETHING
//
//  Created by santosh kumar singh on 09/03/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface ForgotPasswordViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField     * textFieldusername;
@property (weak, nonatomic) IBOutlet UITextField     * textFieldEmail;
@property(nonatomic,weak) IBOutlet UIButton    * requestButton;

@end


