//
//  ForgotPasswordViewController.m
//  CHORETHING
//
//  Created by santosh kumar singh on 09/03/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import "ForgotPasswordViewController.h"

@interface ForgotPasswordViewController ()<UITextFieldDelegate,WebServiceDelegate>

@end

@implementation ForgotPasswordViewController

- (void)viewDidLoad {
    [self configure];
       
       self.title = @"FORGOT PASSWORD";
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

#pragma mark - configure screen
-(void)configure
{
    self.textFieldEmail.text = nil;
    // Set Delegate of UItextField
    
    [self.view setBackgroundColor:[Alert colorWithPatternImage2:kBG_IMAGE]];
    
    
    self.requestButton.layer.cornerRadius = 5.0f;
    self.requestButton.clipsToBounds = YES;
    
  
     [Alert setLeftPaddingTextField:self.textFieldEmail paddingValue:36 image:[UIImage  imageNamed:kIMG_EMAIL]];
    
   
    self.textFieldEmail.layer.cornerRadius = 5.0f;
    self.textFieldEmail.layer.borderWidth=0.5f;
    self.textFieldEmail.layer.borderColor=[UIColor lightGrayColor].CGColor;
    self.textFieldEmail.clipsToBounds = YES;

     
    self.textFieldEmail.delegate   = self;
    //  TextField PlaceHolder
   
  //  [Alert setKeyBoardToolbar:@[self.textFieldEmail]];
    
    // navigation bar configure
    
    [self navigationBarConfiguration];
}
#pragma mark - Configure Navigation Controller

-(void)navigationBarConfiguration
{
    
    self.navigationController.navigationBarHidden = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    UIButton * menuButton  = [UIButton buttonWithType:UIButtonTypeSystem];
    menuButton.frame = CGRectMake(8, 18, 24, 24);
    [menuButton setBackgroundImage:[UIImage imageNamed:IMG_BACK] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *accountBarItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    self.navigationItem.leftBarButtonItem = accountBarItem;
    
    NSMutableArray *arrRightBarItems = [[NSMutableArray alloc] init];
       UIButton *btnSetting = [UIButton buttonWithType:UIButtonTypeCustom];
       [btnSetting setImage:[UIImage imageNamed:@"Frequency"] forState:UIControlStateNormal];
       btnSetting.frame = CGRectMake(0, 0, 50, 50);
       btnSetting.showsTouchWhenHighlighted=YES;
       [btnSetting addTarget:self action:@selector(onSettings:) forControlEvents:UIControlEventTouchUpInside];
       UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSetting];
       
       UIButton *btnLib = [UIButton buttonWithType:UIButtonTypeCustom];
       [btnLib setImage:[UIImage imageNamed:@"logo1.png"] forState:UIControlStateNormal];
       btnLib.frame = CGRectMake(0, 0, 50, 40);
       btnLib.showsTouchWhenHighlighted=YES;
       [btnLib addTarget:self action:@selector(onMyLibrary:) forControlEvents:UIControlEventTouchUpInside];
       UIBarButtonItem *barButtonItem2 = [[UIBarButtonItem alloc] initWithCustomView:btnLib];
       [arrRightBarItems addObject:barButtonItem2];
       [arrRightBarItems addObject:barButtonItem];
       self.navigationItem.rightBarButtonItems=arrRightBarItems;
       
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName :[UIColor whiteColor]
    }];
    
    
}
-(void)onMyLibrary:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}

-(void)onSettings:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/hyptunesradio";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}


-(void)back:(id)sender
{
    DISMISS_VIEW_CONTROLLER;
}

- (IBAction)buttonReqPasswd:(id)sender
{
    
    if ([self.textFieldusername.text isEqualToString:@""])
    {

       [Alert svError:@"Please Enter Username"];
      
    }
   else if ([self.textFieldEmail.text isEqualToString:@""])
    {

       [Alert svError:@"Please Enter Email Address"];
      
    }
    else if(![self validateEmailWithString: self.textFieldEmail.text])
    {
           [Alert svError:@"Please Enter Valid Email Address"];
       
    }
    else
    {
        [self serviceChangePassword];
    }
    
}

-(void)serviceChangePassword
{
     
     [Alert svProgress:@"Please Wait..."];
    
   // service
    WebService * connection = [[WebService alloc]init];
    connection.delegate = self;
    
    NSMutableDictionary * dict                              = [NSMutableDictionary dictionary];
    [dict setObject:@"forgotpassword"                     forKey:kAPI_ACTION];
    [dict setObject:self.textFieldEmail.text                        forKey:@"email"];
   
    [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:@"forgotpassword"];
}

#pragma mark - <WebServiveDelegate>
-(void)getDataFormWebService:(NSDictionary *)jsonResults urlStr:(NSString *)yourUR methodName:(NSString *)methodName{
    
    [Alert svSuccess:[jsonResults objectForKey:@"msg"]];
      
         DISMISS_VIEW_CONTROLLER
             
        }

// WEB SERVER ERROR MSG LOCAL SERVER (APPLICATION SERVER)
-(void) webServiceFailWithApplicationServerMSG:(NSString *)msg
{
    
    [Alert svError:msg];
    
}

// WEB SERVICE FAIL WITH REGION TCP/IP
-(void) webServiceFail:(NSError *)error{
    
    
    [Alert svError:[error localizedDescription]];
    
    
}

- (BOOL)validateEmailWithString:(NSString*)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
