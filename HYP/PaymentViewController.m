//
//  PaymentViewController.m
//  Q-Workerbee
//
//  Created by santosh kumar singh on 09/05/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import "PaymentViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "AddMoneyViewController.h"
#import "CompleteMemberViewController.h"
#import "DashboardViewController.h"
#import "MenuViewController.h"
#import "RateSubmitViewController.h"



@interface PaymentViewController ()<WebServiceDelegate>
{
    
    AppDelegate * appDelegate;
    
}
//@property(nonatomic, strong, readwrite) PayPalConfiguration *payPalConfig;
@end

@implementation PaymentViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
   
    self.title = @"PAYMENT METHOD";
     
    self.dummyImage.layer.cornerRadius = 12.0f;
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                action:@selector(singleTapGestureCaptured:)];
    [self.dummyImage addGestureRecognizer:singleTap];
    [self.dummyImage setMultipleTouchEnabled:YES];
    [self.dummyImage setUserInteractionEnabled:YES];
    
    [self navigationBarConfiguration];
    
}

-(void)singleTapGestureCaptured:(UITapGestureRecognizer *)gestureRecognizer
{
    NSURL *url = [NSURL URLWithString:@"https://www.hypdemand.com/hyptunesradio"];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
    
}


-(void)navigationBarConfiguration
{
    
    self.navigationController.navigationBarHidden = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.navigationController.navigationBarHidden = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    UIButton * menuButton  = [UIButton buttonWithType:UIButtonTypeSystem];
    menuButton.frame = CGRectMake(8, 18, 24, 26);
    [menuButton setImage:[UIImage imageNamed:IMG_BACK] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *accountBarItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    self.navigationItem.leftBarButtonItem = accountBarItem;
   
    NSMutableArray *arrRightBarItems = [[NSMutableArray alloc] init];
       UIButton *btnSetting = [UIButton buttonWithType:UIButtonTypeCustom];
       [btnSetting setImage:[UIImage imageNamed:@"Frequency"] forState:UIControlStateNormal];
       btnSetting.frame = CGRectMake(0, 0, 50, 50);
       btnSetting.showsTouchWhenHighlighted=YES;
       [btnSetting addTarget:self action:@selector(onSettings:) forControlEvents:UIControlEventTouchUpInside];
       UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSetting];
       
       UIButton *btnLib = [UIButton buttonWithType:UIButtonTypeCustom];
       [btnLib setImage:[UIImage imageNamed:@"logo1.png"] forState:UIControlStateNormal];
       btnLib.frame = CGRectMake(0, 0, 50, 40);
       btnLib.showsTouchWhenHighlighted=YES;
       [btnLib addTarget:self action:@selector(onMyLibrary:) forControlEvents:UIControlEventTouchUpInside];
       UIBarButtonItem *barButtonItem2 = [[UIBarButtonItem alloc] initWithCustomView:btnLib];
       [arrRightBarItems addObject:barButtonItem2];
       [arrRightBarItems addObject:barButtonItem];
       self.navigationItem.rightBarButtonItems=arrRightBarItems;
       
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor],
    }];
    
}


-(void)onMyLibrary:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}

-(void)onSettings:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/hyptunesradio";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}
-(void)back:(id)sender
{
    DISMISS_VIEW_CONTROLLER;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    // Preconnect to PayPal early
    //[self setPayPalEnvironment:self.environment];
}

-(IBAction)paymentBnttap:(id)sender
{
    
    
    AddMoneyViewController * booking= GET_VIEW_CONTROLLER_STORYBOARD(@"AddMoneyViewController");
    
    if ([self.matchfrom isEqualToString:@"Driver"])
    {
        booking.matchString = @"Driver";
    }
    else{
        booking.matchString = @"Services";
    }
    booking.dict = self.dict;
    booking.servicename = [self.dict valueForKey:@"serviceName"];
    booking.groupId = self.groupId;
    booking.invoiceId = self.invoiceId;
    booking.tipString =@"";
    booking.priceString = self.ammount;
    UINavigationController * navigationController    = [Alert navigationControllerWithVC:booking];
    PERSENT_VIEW_CONTOLLER(navigationController, YES);
    
}

-(IBAction)ChaqueBtnTapped:(UIButton*)sender
{
    
    if ([self.matchfrom isEqualToString:@"Driver"])
    {
        [Alert svProgress:@"Please Wait..."];
        NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults] dictionaryForKey:kLOGIN];
        NSString * user_id ;
        if ([dictLogin valueForKey:@"id"] == nil)
        {
            user_id  = [dictLogin valueForKey:@"userId"];
            
        }
        else
        {
            user_id  = [dictLogin valueForKey:@"id"];
        }
        
        WebService * connection = [[WebService alloc]init];
        connection.delegate = self;
        
        NSMutableDictionary * dict       = [NSMutableDictionary dictionary];
        [dict setObject:@"carupdatepayment"                    forKey:kAPI_ACTION];
        [dict setObject:user_id                    forKey:kUSER_ID];
        [dict setObject:_groupId                 forKey:@"bookingId"];
//        [dict setObject:_invoiceId                forKey:@"invoiceId"];
        [dict setObject:@"Checks"                 forKey:@"paymentMethod"];
        [dict setObject:self.ammount                  forKey:@"totalAmount"];
        [dict setObject:@"checkues"                    forKey:@"transactionId"];
        
        [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:@"carupdatepayment"];
    }
    else
    {
    
        [Alert svProgress:@"Please Wait..."];
    NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults] dictionaryForKey:kLOGIN];
    NSString * user_id ;
    if ([dictLogin valueForKey:@"id"] == nil)
    {
        user_id  = [dictLogin valueForKey:@"userId"];
        
    }
    else
    {
        user_id  = [dictLogin valueForKey:@"id"];
    }
    
    WebService * connection = [[WebService alloc]init];
    connection.delegate = self;
    
    NSMutableDictionary * dict       = [NSMutableDictionary dictionary];
    [dict setObject:@"updatepayment"                    forKey:kAPI_ACTION];
    [dict setObject:user_id                    forKey:kUSER_ID];
    [dict setObject:_groupId                 forKey:@"groupId"];
    [dict setObject:_invoiceId                forKey:@"invoiceId"];
    [dict setObject:@"Checks"                 forKey:@"cardNo"];
    [dict setObject:self.ammount                  forKey:@"totalAmount"];
    [dict setObject:@""                    forKey:@"transactionId"];
    
    [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:@"updatepayment"];
    }
}

-(IBAction)CashBtnTapped:(UIButton*)sender
{
    if ([self.matchfrom isEqualToString:@"Driver"])
    {
        [Alert svProgress:@"Please Wait..."];
        NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults] dictionaryForKey:kLOGIN];
        NSString * user_id ;
        if ([dictLogin valueForKey:@"id"] == nil)
        {
            user_id  = [dictLogin valueForKey:@"userId"];
            
        }
        else
        {
            user_id  = [dictLogin valueForKey:@"id"];
        }
        
        WebService * connection = [[WebService alloc]init];
        connection.delegate = self;
        
        NSMutableDictionary * dict       = [NSMutableDictionary dictionary];
        [dict setObject:@"carupdatepayment"                    forKey:kAPI_ACTION];
        [dict setObject:user_id                    forKey:kUSER_ID];
        [dict setObject:_groupId                 forKey:@"bookingId"];
//        [dict setObject:_invoiceId                forKey:@"invoiceId"];
        [dict setObject:@"Cash"                 forKey:@"paymentMethod"];
        [dict setObject:self.ammount                  forKey:@"totalAmount"];
        [dict setObject:@"CashPayment"                    forKey:@"transactionId"];
        
        [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:@"carupdatepayment"];
    }
    else
    {
    
    [Alert svProgress:@"Please Wait..."];
    NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults] dictionaryForKey:kLOGIN];
    NSString * user_id ;
    if ([dictLogin valueForKey:@"id"] == nil)
    {
        user_id  = [dictLogin valueForKey:@"userId"];
        
    }
    else
    {
        user_id  = [dictLogin valueForKey:@"id"];
    }
    
    WebService * connection = [[WebService alloc]init];
    connection.delegate = self;
    
    NSMutableDictionary * dict       = [NSMutableDictionary dictionary];
    [dict setObject:@"updatepayment"                    forKey:kAPI_ACTION];
    [dict setObject:user_id                    forKey:kUSER_ID];
    [dict setObject:_groupId                 forKey:@"groupId"];
    [dict setObject:_invoiceId                forKey:@"invoiceId"];
    [dict setObject:@"Cash"                 forKey:@"cardNo"];
    [dict setObject:self.ammount                 forKey:@"totalAmount"];
    [dict setObject:@""                    forKey:@"transactionId"];
    
    [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:@"updatepayment"];
    }
}

#pragma mark - <WebServiveDelegate>
-(void)getDataFormWebService:(NSDictionary *)jsonResults urlStr:(NSString *)yourUR methodName:(NSString *)methodName{
    
    [Alert svSuccess:@"Request Successfully Submitted"];
    
    SWITCH (methodName) {
        CASE (@"updatepayment")
        {
            appDelegate = UIAppDelegate;
            DashboardViewController * homePassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBID_ARRIVED_VC);
            UIAppDelegate.isCall  = YES;
            MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
            [appDelegate manageDrawerRear:menuViewController front:homePassengerViewController];
            
            
        }
        CASE (@"carupdatepayment")
        {
            RateSubmitViewController * bookingDetailsViewController = GET_VIEW_CONTROLLER(@"RateSubmitViewController");
            bookingDetailsViewController.bookingDetail = self.dict;
            MOVE_VIEW_CONTROLLER(bookingDetailsViewController, YES);

            
        }
        break;
        
        DEFAULT
        {
            break;
        }
    }
}
// WEB SERVER ERROR MSG LOCAL SERVER (APPLICATION SERVER)
-(void) webServiceFailWithApplicationServerMSG:(NSString *)msg
{
    
    [Alert svError:msg];
    
}

// WEB SERVICE FAIL WITH REGION TCP/IP
-(void) webServiceFail:(NSError *)error{
    
    
    [Alert svError:[error localizedDescription]];
    
    
}

#pragma mark - Flipside View Controller

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"pushSettings"]) {
        [[segue destinationViewController] setDelegate:(id)self];
    }
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
