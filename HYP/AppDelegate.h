//
//  AppDelegate.h
//  HYP
//
//  Created by santosh kumar singh on 29/08/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;
@property (strong, nonatomic)  NSString * latitude;
@property (strong, nonatomic)  NSString * longitude;
@property (strong, nonatomic)  NSString * addressString;
@property (strong, nonatomic)  NSString * firebaseId;

@property(nonatomic) BOOL cellDefaltSeleted;
@property (strong, nonatomic) UIWindow *window;
+ (AppDelegate *)appDelegate;
@property(nonatomic) BOOL isCall;
@property(nonatomic,retain)NSArray* professionArr;
@property(nonatomic,retain)NSArray* vehicleArr;

@property(nonatomic,strong)NSDictionary* bookingId;

@property(nonatomic,strong)NSArray* benefitArr;
@property(nonatomic,strong)NSArray* stateArr;
@property(nonatomic,strong)NSArray* yesNoArr;
@property(nonatomic,strong)NSArray* sendArr;
@property(nonatomic,strong)NSArray* genderArr;
-(void)setInitialViewController:(UIViewController *)controller;
-(void)manageDrawerRear:(UIViewController *)rear front:(UIViewController *)front;

@end

