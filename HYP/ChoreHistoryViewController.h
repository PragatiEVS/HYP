//
//  ChoreHistoryViewController.h
//  CHORETHING
//
//  Created by santosh kumar singh on 02/04/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface ChoreHistoryViewController : UIViewController
{
     NSMutableArray *resultArray;
    int startidx;
    NSArray *arr;
}
@property (weak, nonatomic) IBOutlet UILabel * noListAvalible;
@property(weak, nonatomic) IBOutlet MDTabBar *tabBar;
@property(weak, nonatomic)  NSString *matchRequest;
@property(weak, nonatomic) IBOutlet UITableView *completedTableView;
@property(weak, nonatomic) IBOutlet UITableView *confirmedTableView;
@property(nonatomic,strong) NSString *matchFrom;
@end


