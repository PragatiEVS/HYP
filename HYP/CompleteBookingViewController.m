//
//  CompleteBookingViewController.m
//  Q-Workerbee
//
//  Created by santosh kumar singh on 15/06/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import "CompleteBookingViewController.h"
#import "ConfirmedTableViewCell.h"


@interface CompleteBookingViewController ()<WebServiceDelegate,UITableViewDelegate,UITableViewDataSource>

@end

@implementation CompleteBookingViewController

- (void)viewDidLoad
{
    _noListAvalible.hidden=YES;
    
  //  self.view.backgroundColor = [Alert colorWithPatternImage2:kBG_IMAGE];
    
    self.title =@"COMPLETED BOOKING";
    startidx = 1;
    resultArray =[[NSMutableArray alloc]init];
    [self serviceCompletedList];
    [self navigationBarConfiguration];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)navigationBarConfiguration
{
    self.navigationController.navigationBarHidden = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    
    UIButton * menuButton  = [UIButton buttonWithType:UIButtonTypeSystem];
    menuButton.frame = CGRectMake(8, 18, 24, 26);
    [menuButton setImage:[UIImage imageNamed:IMG_BACK] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *accountBarItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    self.navigationItem.leftBarButtonItem = accountBarItem;
    
    NSMutableArray *arrRightBarItems = [[NSMutableArray alloc] init];
         UIButton *btnSetting = [UIButton buttonWithType:UIButtonTypeCustom];
         [btnSetting setImage:[UIImage imageNamed:@"Frequency"] forState:UIControlStateNormal];
         btnSetting.frame = CGRectMake(0, 0, 40, 40);
         btnSetting.showsTouchWhenHighlighted=YES;
         [btnSetting addTarget:self action:@selector(onSettings:) forControlEvents:UIControlEventTouchUpInside];
         UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSetting];
         
         UIButton *btnLib = [UIButton buttonWithType:UIButtonTypeCustom];
         [btnLib setImage:[UIImage imageNamed:@"logo1.png"] forState:UIControlStateNormal];
         btnLib.frame = CGRectMake(0, 0, 40, 40);
         btnLib.showsTouchWhenHighlighted=YES;
         UIBarButtonItem *barButtonItem2 = [[UIBarButtonItem alloc] initWithCustomView:btnLib];
         [arrRightBarItems addObject:barButtonItem2];
         [arrRightBarItems addObject:barButtonItem];
         self.navigationItem.rightBarButtonItems=arrRightBarItems;
         
      
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor],
    }];
    
}

-(void)onMyLibrary:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}

-(void)onSettings:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/hyptunesradio";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}

-(void)back:(id)sender
{
    DISMISS_VIEW_CONTROLLER;
}

-(void)serviceCompletedList{
    
    // SERVER CONNCETION
    [Alert svProgress:@"Please Wait..."];
    
    WebService * connection = [[WebService alloc]init];
    connection.delegate = self;
    NSMutableDictionary * dict  = [NSMutableDictionary dictionary];
    
    NSString * str = [self.dicttionary objectForKey:@"role"];
    
    NSString * user_id ;
    if ([self.dicttionary objectForKey:@"id"] == nil)
    {
        user_id  = [self.dicttionary objectForKey:@"userId"];
        
    }
    else
    {
        user_id  = [self.dicttionary objectForKey:@"id"];
    }
    [dict setObject:@"bookinglist"                     forKey:kAPI_ACTION];
    [dict setObject:user_id                     forKey:kUSER_ID];
    [dict setObject:str                     forKey:@"usertype"];
    [dict setObject:@"2"                     forKey:@"status"];
    [dict setObject:[NSString stringWithFormat:@"%d", startidx]                     forKey:@"pageNo"];
    
    NSLog(@"dict is ---%@",dict);
    [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:@"bookinglist"];
    
}

#pragma mark - Sevice delgate methods

-(void)getDataFormWebService:(NSDictionary *)jsonResults urlStr:(NSString *)yourUR methodName:(NSString *)methodName{
    
    [SVProgressHUD dismiss];
    
    SWITCH (methodName) {
        CASE (@"bookinglist")
        {
            NSLog(@"jsonResults =%@",jsonResults);
            arr=[jsonResults valueForKey:@"data"];
            
            
            for ( NSDictionary *dict in [jsonResults valueForKey:@"data"])
            {
                [resultArray addObject:dict];
            }
            
            if (arr.count==0)
            {
                
                if (arr.count ==0 && resultArray.count ==0)
                {
                    _noListAvalible.hidden=NO;
                    self.completedTableView.hidden=YES;
                }
            }
            else
            {
                _noListAvalible.hidden=YES;
                _completedTableView.hidden=NO;
                self.completedTableView.delegate=self;
                self.completedTableView.dataSource=self;
                [self.completedTableView reloadData];
                
            }
        }
        
        
        break;
        
        DEFAULT
        {
            break;
        }
    }
    
}
// WEB SERVER ERROR MSG LOCAL SERVER (APPLICATION SERVER)
-(void) webServiceFailWithApplicationServerMSG:(NSString *)msg
{
    
    [Alert svError:msg];
    
}

// WEB SERVICE FAIL WITH REGION TCP/IP
-(void) webServiceFail:(NSError *)error{
    
    
    [Alert svError:[error localizedDescription]];
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell: (UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if (indexPath.row == [resultArray count] - 1 )
    {
        if (arr.count==0)
        {
            
        }
        else{
            
            startidx += 1;
            [self serviceCompletedList];
            
        }
        
        
    }
    
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return resultArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static  NSString *identifierDriver = @"ConfirmedTableViewCell";
    
    ConfirmedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifierDriver forIndexPath:indexPath];
    
    if ([[[resultArray valueForKey:@"image"]objectAtIndex:indexPath.row] isEqualToString:@""])
    {
        
        NSString *userName = [[resultArray valueForKey:@"fullName"]objectAtIndex:indexPath.row];
        [cell.userThumb setImageWithString:userName color:nil circular:YES];
        
    }
    else
    {
        
        [cell.userThumb setImageURL:[NSURL URLWithString:[[resultArray valueForKey:@"image"]objectAtIndex:indexPath.row]]];
    }
    
//
//    [cell.userThumb setImageURL:[NSURL URLWithString:[[resultArray valueForKey:@"image"]objectAtIndex:indexPath.row]]];
    cell.userThumb.layer.cornerRadius = 30;
    cell.userThumb.clipsToBounds=YES;
    
    cell.nameLabel.text= [[resultArray valueForKey:@"fullName"]objectAtIndex:indexPath.row];
    cell.locationLabel.text= [[resultArray valueForKey:@"address"]objectAtIndex:indexPath.row];
    
    
    cell.dateLabel.layer.cornerRadius = cell.dateLabel.frame.size.height/2;
    cell.dateLabel.clipsToBounds= YES;
    
    
    cell.locationLabel.numberOfLines=2;
    //  cell.dateLabel.backgroundColor=[UIColor blackColor];
    cell.dateLabel.textColor = [UIColor whiteColor];
    
    cell.dateLabel.text= [[resultArray valueForKey:@"bookingDate"]objectAtIndex:indexPath.row];
    
    
    cell.pendingLabel.hidden = YES;
    cell.cratedLabel.hidden = YES;
    cell.priceLabel.text = [NSString stringWithFormat:@"Total: $%.2f",[[[resultArray valueForKey:@"totalAmount"] objectAtIndex:indexPath.row] floatValue]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([self.matchFrom isEqualToString:@"userDetail"]) {
        
    }
    else
    {
//        ChoreDetailViewController * settingsViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"ChoreDetailViewController");
//        
//        settingsViewController.dict = [resultArray objectAtIndex:indexPath.row];
//        
//        UINavigationController * navigationController = [Alert navigationControllerWithVC:settingsViewController];
//        PERSENT_VIEW_CONTOLLER(navigationController, YES);
    }
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
