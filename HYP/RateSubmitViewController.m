//
//  RateSubmitViewController.m
//  UpDriver
//
//  Created by santosh kumar singh on 21/03/21.
//  Copyright © 2021 Nitin Kumar. All rights reserved.
//

#import "RateSubmitViewController.h"
#import "DashboardViewController.h"
#import "MenuViewController.h"


@interface RateSubmitViewController ()<WebServiceDelegate>
{
    AppDelegate * appDelegate;
}

@end

@implementation RateSubmitViewController

- (void)viewDidLoad {
    
    self.title = @"SEND REVIEWS";
    
    
    
    self.rateUpdate.starSize = 20;
    self.rateUpdate.rating = 2.5f;
    self.rateUpdate.canRate = YES;
    self.rateUpdate.starFillColor = [Alert colorFromHexString:@"#F4CC47"];
    
    self.msgtextfiled.clipsToBounds = YES;
    self.msgtextfiled.layer.cornerRadius = 5.0f;
    [self SetTextFieldBorder:self.msgtextfiled];
    self.submitbtn.clipsToBounds = YES;
    self.submitbtn.layer.cornerRadius = 5.0f;
    
    
    self.viewPayment.layer.cornerRadius = 5;
    // border
    [_viewPayment.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [_viewPayment.layer setBorderWidth:1.5f];
    
    // drop shadow
    [_viewPayment.layer setShadowColor:[UIColor grayColor].CGColor];
    [_viewPayment.layer setShadowOpacity:0.8];
    [_viewPayment.layer setShadowRadius:3.0];
    [_viewPayment.layer setShadowOffset:CGSizeMake(1.0, 1.0)];
    
    self.lblDuration.text = [NSString stringWithFormat:@"$ %@",[self.bookingDetail  valueForKey:@"FinalFare"]];
    
    //    self.lblDistance.text = [NSString stringWithFormat:@"%@ Miles",[self.bookingDetail  valueForKey:@"totalDistance"]];
    //
    self.pickupAddressLabel.text = [self.bookingDetail   valueForKey:@"Actual_PickupAddress"];
    
    self.dropAddressLabel.text = [self.bookingDetail  valueForKey:@"Actual_Drop_Address"];
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"d MMM, YYYY"];
    // or @"yyyy-MM-dd hh:mm:ss a" if you prefer the time with AM/PM
    NSLog(@"%@",[dateFormatter stringFromDate:[NSDate date]]);
    
    self.lblBookingDate.text = [dateFormatter stringFromDate:[NSDate date]];
    
    _viewDistance.layer.cornerRadius = 5;
    
    // border
    [_viewDistance.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [_viewDistance.layer setBorderWidth:1.5f];
    
    // drop shadow
    [_viewDistance.layer setShadowColor:[UIColor grayColor].CGColor];
    [_viewDistance.layer setShadowOpacity:0.8];
    [_viewDistance.layer setShadowRadius:3.0];
    [_viewDistance.layer setShadowOffset:CGSizeMake(1.0, 1.0)];
    [self navigationBarConfiguration];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)SetTextFieldBorder :(UITextField *)textField
{
    
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 0.5;
    border.borderColor = [UIColor darkGrayColor].CGColor;
    border.frame = CGRectMake(0, textField.frame.size.height - borderWidth, textField.frame.size.width, textField.frame.size.height);
    border.borderWidth = borderWidth;
    [textField.layer addSublayer:border];
    textField.layer.masksToBounds = YES;
    
}
#pragma mark - button Back Click
-(void)backButtonClick:(id)sender
{
    // Go back (pop view controller)
    GOBACK;
}
#pragma mark - Navigation bar configure
-(void)navigationBarConfiguration
{
    
    self.navigationController.navigationBarHidden = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    UIButton * menuButton  = [UIButton buttonWithType:UIButtonTypeSystem];
    menuButton.frame = CGRectMake(8, 18, 24, 24);
    [menuButton setBackgroundImage:[UIImage imageNamed:IMG_BACK] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(backButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *accountBarItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    self.navigationItem.leftBarButtonItem = accountBarItem;
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor],
                                                                      NSFontAttributeName: [UIFont fontWithName:kFONT size:14]}];
    
    self.navigationController.navigationBar.tintColor = [Alert colorFromHexString:kCOLOR_NAV];
    
}

-(void)rateView:(RateView*)rateView didUpdateRating:(float)rating
{
    self.rateingString = [NSString stringWithFormat:@"%f", rating];
    NSLog(@"rateViewDidUpdateRating: %@",  self.rateingString);
}


- (IBAction)buttonsubmited:(id)sender
{
  
        NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults] dictionaryForKey:kLOGIN];
        NSString * user_id ;
        if ([dictLogin valueForKey:@"id"] == nil)
        {
            user_id  = [dictLogin valueForKey:@"userId"];
        }
        else
        {
            user_id  = [dictLogin valueForKey:@"id"];
        }
        
        WebService * connection = [[WebService alloc]init];
        connection.delegate = self;
        NSMutableDictionary * dict                              = [NSMutableDictionary dictionary];
        
        [dict setObject:@"submitreview"                     forKey:kAPI_ACTION];
        [dict setObject:user_id                     forKey:@"reviewFrom"];
        [dict setObject:[self.bookingDetail valueForKey:@"bookingId"]                     forKey:@"carbookingId"];
        [dict setObject:[self.bookingDetail valueForKey:@"driverId"]                     forKey:@"reviewTo"];
        [dict setObject:[NSString stringWithFormat:@"%f", self.rateUpdate.rating]                     forKey:@"star"];
        [dict setObject:self.msgtextfiled.text         forKey:@"message"];
        
        
        NSLog(@"dict is ---%@",dict);
        [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:@"submitreview"];
    
    
}

-(void)getDataFormWebService:(NSDictionary *)jsonResults urlStr:(NSString *)yourUR methodName:(NSString *)methodName{
    
    SWITCH (methodName) {
        
        CASE (@"submitreview")
        {
            if ([[jsonResults valueForKey:@"status"] isEqualToString:@"Success"])
            {
                
                appDelegate = UIAppDelegate;
                DashboardViewController * homePassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBID_ARRIVED_VC);
                UIAppDelegate.isCall  = YES;
                MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
                [appDelegate manageDrawerRear:menuViewController front:homePassengerViewController];
                
            }
            
            
        }
        break;
        DEFAULT
        {
            break;
        }
    }
}

-(void) webServiceFailWithApplicationServerMSG:(NSString *)msg
{
    
    [Alert svError:msg];
    
}

// WEB SERVICE FAIL WITH REGION TCP/IP
-(void) webServiceFail:(NSError *)error{
    
    
    [Alert svError:[error localizedDescription]];
    
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
