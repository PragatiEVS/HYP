//
//  RateListViewController.h
//  CHORETHING
//
//  Created by santosh kumar singh on 03/04/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface RateListViewController : UIViewController
{
     int startidx;
       NSArray *arr;
     NSMutableArray *resultArray;
}
@property (weak, nonatomic) IBOutlet UILabel * noListAvalible;

@property(weak, nonatomic) IBOutlet UITableView *completedTableView;

@property (strong, nonatomic) NSString *userId;

@property(weak, nonatomic) IBOutlet UIView *rateView;
@property (nonatomic, strong) IBOutlet RateView *rateViewsubmit;

@property(weak, nonatomic) IBOutlet UIButton *rate1Btn;
@property(weak, nonatomic) IBOutlet UIButton *rate2Btn;
@property(weak, nonatomic) IBOutlet UIButton *rate3Btn;
@property(weak, nonatomic) IBOutlet UIButton *rate4Btn;
@property(weak, nonatomic) IBOutlet UIButton *rate5Btn;

@property(weak, nonatomic) IBOutlet UIProgressView *rate1ProgressView;
@property(weak, nonatomic) IBOutlet UIProgressView *rate2ProgressView;
@property(weak, nonatomic) IBOutlet UIProgressView *rate3ProgressView;
@property(weak, nonatomic) IBOutlet UIProgressView *rate4ProgressView;
@property(weak, nonatomic) IBOutlet UIProgressView *rate5ProgressView;

@property(nonatomic,weak) IBOutlet NSLayoutConstraint * rateConstant;

@property(weak, nonatomic) IBOutlet UILabel *rate1Count;
@property(weak, nonatomic) IBOutlet UILabel *rate2Count;
@property(weak, nonatomic) IBOutlet UILabel *rate3Count;
@property(weak, nonatomic) IBOutlet UILabel *rate4Count;
@property(weak, nonatomic) IBOutlet UILabel *rate5Count;


@property(weak, nonatomic) IBOutlet UIButton *totalRating;
@property(weak, nonatomic) IBOutlet UILabel *ratingText;

@end


