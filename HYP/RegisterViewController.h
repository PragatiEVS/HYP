//
//  RegisterViewController.h
//  CHORETHING
//
//  Created by santosh kumar singh on 09/03/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterViewController : UIViewController
{
     int valrible;
}
@property(nonatomic,weak) IBOutlet UIImageView     * bgImageView;

@property(nonatomic,weak) IBOutlet UIButton     * buttonSignInSubmit;
@property(nonatomic,weak) IBOutlet UIButton     * buttonSignUPSubmit;
@property(nonatomic,weak) IBOutlet UIButton     * buttonSignUPMember;

@property (weak, nonatomic) IBOutlet UITableView * tableViewLogin;
@property (weak, nonatomic) IBOutlet UITableView * tableViewRegister;
@property (weak, nonatomic) IBOutlet UITableView * tableViewRegisterMember;

@property(nonatomic,strong) NSString  * selectType;
@property (weak, nonatomic) IBOutlet UITextField *textFieldName;

@property (weak, nonatomic) IBOutlet UITextField *stateText;
@property (weak, nonatomic) IBOutlet UITextField *textFieldAddress;
@property (weak, nonatomic) IBOutlet UITextField *textFieldEmail;
@property (weak, nonatomic) IBOutlet UITextField *textFieldGender;
@property (weak, nonatomic) IBOutlet UITextField *textFielddob;
@property (weak, nonatomic) IBOutlet UITextField *textFieldPassword;
@property (weak, nonatomic) IBOutlet UITextField *textFieldZip;
@property (weak, nonatomic) IBOutlet UITextField *textFieldPhone;
@property (weak, nonatomic) IBOutlet UITextField *textFieldUsernameType;
@property (weak, nonatomic) IBOutlet UITextField *textFieldPasswordType;

@property (strong, nonatomic) NSString *stateId;
@property (weak, nonatomic) IBOutlet UIPickerView *dienstPicker;
@property (weak, nonatomic) IBOutlet UIButton *donePicker;

@property(nonatomic,weak) IBOutlet UIButton     * buttonBusiness;
@property(nonatomic,weak) IBOutlet UIButton     * buttonFreelancer;

@property (weak) IBOutlet NSLayoutConstraint *widthConstraintOfbusiness;
@property (weak) IBOutlet NSLayoutConstraint *widthConstraintOffreelancer;

@property (weak) IBOutlet NSLayoutConstraint *widthConstraintOfZip;

@property(nonatomic,strong) NSString *lat;
@property(nonatomic,strong) NSString *longi;
@property (nonatomic, retain) NSString* researchSelected;
@end


