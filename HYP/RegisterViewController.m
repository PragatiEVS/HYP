//
//  RegisterViewController.m
//  CHORETHING
//
//  Created by santosh kumar singh on 09/03/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import "RegisterViewController.h"
#import "ForgotPasswordViewController.h"
#import "MenuViewController.h"
#import "UIImage+GIF.h"
#import "BankAccountViewController.h"
#import "DashboardViewController.h"
#import "AffiliateViewController.h"
#import "TermsViewController.h"
#import "DriverDashboardViewController.h"

@interface RegisterViewController ()<WebServiceDelegate,STPopupControllerTransitioning,UIPickerViewDataSource,UIPickerViewDelegate,WSCalendarViewDelegate,GMSAutocompleteViewControllerDelegate>
{
    AppDelegate * appDelegate;
    WSCalendarView *calendarView;
}

@property(nonatomic) NSDateFormatter *dateFormatter;
@property(nonatomic) MDTimePickerDialog *datePicker;

@end

@implementation RegisterViewController

-(void)reverGeoCodingUsingGoogle:(CLLocation *)location
{
    [[GMSGeocoder geocoder] reverseGeocodeCoordinate:CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude) completionHandler:^(GMSReverseGeocodeResponse* response, NSError* error)
     {
        
        GMSReverseGeocodeResult *result = response.firstResult;
        
        NSArray   * arrayAdd = result.lines;
        
        NSString  * address1;
        NSString  * address2;
        
        if (arrayAdd.count>1)
        {
            address1 = result.lines[0];
            address2 = result.subLocality;
        }
        else
        {
            address1 = result.lines[0];
        }
        
        self.textFieldAddress.text = address1;
        
        
    }];
    
    
}

- (void)viewDidLoad {
    
   
    CLLocation * upadetLocation = [[CLLocation alloc] initWithLatitude:[[AppDelegate appDelegate].latitude floatValue] longitude:[[AppDelegate appDelegate].longitude floatValue]];
           [self reverGeoCodingUsingGoogle:upadetLocation];
    
   
    
    NSString * str = [[NSUserDefaults standardUserDefaults] stringForKey:@"CheckFrom"];
    
    if ([str isEqualToString:@"Register"])
    {
         self.researchSelected=@"NO";
         self.buttonFreelancer.selected = YES;
         [self.buttonFreelancer setBackgroundImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateSelected];
        self.title=@"SIGN UP";
        self.tableViewRegister.hidden=NO;
        self.tableViewLogin.hidden=YES;
    }
    
    else if ([str isEqualToString:@"Driver"])
    {
        self.researchSelected=@"YES";
        self.buttonBusiness.selected = YES;
         [self.buttonBusiness setBackgroundImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateSelected];
        self.title=@"SIGN UP";
        self.tableViewRegister.hidden=NO;
        self.tableViewLogin.hidden=YES;
    }
    
    else if ([str isEqualToString:@"Provider"])
    {
         self.researchSelected=@"NO";
         self.buttonFreelancer.selected = YES;
        [self.buttonFreelancer setBackgroundImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateSelected];
        self.title=@"SIGN UP";
        self.tableViewRegister.hidden=NO;
        self.tableViewLogin.hidden=YES;
    }
    else
    {
        self.title=@"SIGN IN";
        self.tableViewRegister.hidden=YES;
        self.tableViewLogin.hidden=NO;
        
    }
    
    
    [self configure];
    
    _dateFormatter = [[NSDateFormatter alloc] init];
    
    NSString *path=[[NSBundle mainBundle]pathForResource:@"map2" ofType:@"gif"];
    NSURL *url=[[NSURL alloc] initFileURLWithPath:path];
    NSData *date= [NSData dataWithContentsOfURL:url];
    self.bgImageView.image= [UIImage sd_imageWithGIFData:date];
    
    
    
    calendarView = [[[NSBundle mainBundle] loadNibNamed:@"WSCalendarView" owner:self options:nil] firstObject];
    calendarView.backgroundColor=[UIColor whiteColor];
    calendarView.tappedDayBackgroundColor=[UIColor redColor];
    calendarView.calendarStyle = WSCalendarStyleDialog;
    calendarView.isShowEvent=false;
    [calendarView setupAppearance];
    [self.view addSubview:calendarView];
    calendarView.delegate=self;
    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    
    NSString * str = [[NSUserDefaults standardUserDefaults] stringForKey:@"Editbusinessaddresss"];
    if (str) {
        
        self.longi = [[NSUserDefaults standardUserDefaults] stringForKey:@"long"];
        
        self.lat  = [[NSUserDefaults standardUserDefaults] stringForKey:@"lat"];
        
        self.textFieldAddress.text = str;
    }
    else
        
    {
    }
}

#pragma mark - Configure Navigation Controller

-(void)navigationBarConfiguration
{
    
    self.navigationController.navigationBarHidden = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    UIButton * menuButton  = [UIButton buttonWithType:UIButtonTypeSystem];
    menuButton.frame = CGRectMake(8, 18, 24, 24);
    [menuButton setBackgroundImage:[UIImage imageNamed:IMG_BACK] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *accountBarItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    self.navigationItem.leftBarButtonItem = accountBarItem;
    
    NSMutableArray *arrRightBarItems = [[NSMutableArray alloc] init];
       UIButton *btnSetting = [UIButton buttonWithType:UIButtonTypeCustom];
       [btnSetting setImage:[UIImage imageNamed:@"Frequency"] forState:UIControlStateNormal];
       btnSetting.frame = CGRectMake(0, 0, 40, 40);
       btnSetting.showsTouchWhenHighlighted=YES;
       [btnSetting addTarget:self action:@selector(onSettings:) forControlEvents:UIControlEventTouchUpInside];
       UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSetting];
       
       UIButton *btnLib = [UIButton buttonWithType:UIButtonTypeCustom];
       [btnLib setImage:[UIImage imageNamed:@"logo1.png"] forState:UIControlStateNormal];
       btnLib.frame = CGRectMake(0, 0, 40, 40);
       btnLib.showsTouchWhenHighlighted=YES;
       UIBarButtonItem *barButtonItem2 = [[UIBarButtonItem alloc] initWithCustomView:btnLib];
       [arrRightBarItems addObject:barButtonItem2];
       [arrRightBarItems addObject:barButtonItem];
       self.navigationItem.rightBarButtonItems=arrRightBarItems;
       
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor],
    }];
    
    
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    
    [self.view endEditing:YES];
    return YES;
}

- (void)keyboardDidShow:(NSNotification *)notification
{
    // Assign new frame to your view
    [self.view setFrame:CGRectMake(0,-60,self.view.frame.size.width,self.view.frame.size.height)];
}

-(void)keyboardDidHide:(NSNotification *)notification
{
    
    [self.view setFrame:CGRectMake(0,65,self.view.frame.size.width,self.view.frame.size.height)];
}

-(void)back:(id)sender
{
    DISMISS_VIEW_CONTROLLER;
}

- (IBAction)afiliateYesButton:(UIButton*)btn
{
    self.researchSelected=@"YES";
   btn.selected=YES;
    if (self.buttonBusiness.selected==YES)
    {
        btn.selected=YES;
        self.buttonFreelancer.selected=NO;
        [self.buttonBusiness setBackgroundImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateSelected];
       [self.buttonFreelancer setBackgroundImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
    }
}

- (IBAction)afiliateNoButton:(UIButton*)btn
{
    self.researchSelected=@"NO";
    
   btn.selected=YES;
    if (self.buttonFreelancer.selected==YES)
    {
        btn.selected=YES;
        self.buttonBusiness.selected=NO;
        [self.buttonFreelancer setBackgroundImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateSelected];
          [self.buttonBusiness setBackgroundImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
    }
}

- (IBAction)buttonResetPasswd:(id)sender
{
    ForgotPasswordViewController *forgotPasswordViewController =  GET_VIEW_CONTROLLER(kSBIDForgotPasswordVC);
    
    UINavigationController * navigationController = [Alert navigationControllerWithVC:forgotPasswordViewController];
    PERSENT_VIEW_CONTOLLER(navigationController, YES);
}

- (IBAction)buttonSignupDont:(id)sender
{
    
    self.tableViewLogin.hidden=NO;
    self.tableViewRegister.hidden=YES;
    self.selectType=@"Login";
    
    //    [self.buttonSignIn setTitleColor:[Alert colorFromHexString:kCOLOR_regi] forState:UIControlStateNormal];
    //    [self.buttonSignUP setTitleColor:[Alert colorFromHexString:kCOLOR_TINT_NAV] forState:UIControlStateNormal];
    
    
    [self clearTextField];
}

- (IBAction)buttonSigninDont:(id)sender
{
    self.tableViewLogin.hidden=YES;
    self.tableViewRegister.hidden=NO;
    
    self.selectType=@"Register";
    //    [self.buttonSignUP setTitleColor:[Alert colorFromHexString:kCOLOR_regi] forState:UIControlStateNormal];
    //    [self.buttonSignIn setTitleColor:[Alert colorFromHexString:kCOLOR_TINT_NAV] forState:UIControlStateNormal];
    //
    
    [self clearTextField];
}

#pragma mark - Clear textField
-(void)clearTextField
{
    self.textFieldName.text                = @"";
    self.textFieldGender.text                = @"";
    self.textFieldEmail.text                = @"";
    self.textFieldZip.text             = @"";
    self.textFieldPassword.text             = @"";
    self.textFieldUsernameType.text      = @"";
    self.textFieldPasswordType.text          = @"";
    self.textFieldAddress.text            = @"";
    self.stateText.text   = @"";
}

#pragma mark -  Configure Screen
-(void)configure
{
    
    self.buttonSignInSubmit.layer.cornerRadius = 5.0f;
    self.buttonSignUPMember.layer.cornerRadius = 5.0f;
    self.buttonSignUPSubmit.layer.cornerRadius = 5.0f;
    
  
    [Alert setKeyBoardToolbar:@[self.textFieldUsernameType,self.textFieldPasswordType]];
    
    [Alert setKeyBoardToolbar:@[self.textFieldName,self.textFieldEmail,self.textFieldPassword,self.textFieldPhone,self.textFieldGender,self.textFieldAddress]];
    
    [Alert setLeftPaddingTextField:self.textFieldUsernameType paddingValue:36 image:[UIImage  imageNamed:kIMG_EMAIL]];
    [Alert setLeftPaddingTextField:self.textFieldPasswordType paddingValue:36 image:[UIImage  imageNamed:kIMG_PASSWD]];
    [Alert setLeftPaddingTextField:self.textFieldName paddingValue:36 image:[UIImage  imageNamed:kIMG_USER]];
    [Alert setLeftPaddingTextField:self.textFieldEmail paddingValue:36 image:[UIImage  imageNamed:kIMG_EMAIL]];
    [Alert setLeftPaddingTextField:self.textFieldPassword paddingValue:36 image:[UIImage  imageNamed:kIMG_PASSWD]];
    [Alert setLeftPaddingTextField:self.textFieldPhone paddingValue:36 image:[UIImage  imageNamed:kIMG_PHONE]];
    [Alert setLeftPaddingTextField:self.textFieldGender paddingValue:36 image:[UIImage  imageNamed:kIMG_USER]];
    [Alert setLeftPaddingTextField:self.stateText paddingValue:36 image:[UIImage  imageNamed:@"state"]];
    [Alert setLeftPaddingTextField:self.textFieldZip paddingValue:36 image:[UIImage  imageNamed:@"state"]];
    
    [Alert setLeftPaddingTextField:self.textFieldAddress paddingValue:36 image:[UIImage  imageNamed:@"Address"]];
    
    
    
    
    self.textFieldUsernameType.layer.cornerRadius = 5.0f;
    self.textFieldUsernameType.layer.borderWidth = 0.5f;
    self.textFieldUsernameType.layer.borderColor = [UIColor darkGrayColor].CGColor;
    self.textFieldUsernameType.clipsToBounds = YES;
    
    self.textFieldPasswordType.layer.cornerRadius = 5.0f;
    self.textFieldPasswordType.layer.borderWidth = 0.5f;
    self.textFieldPasswordType.layer.borderColor = [UIColor darkGrayColor].CGColor;
    self.textFieldPasswordType.clipsToBounds = YES;
    
    self.textFieldPhone.layer.cornerRadius = 5.0f;
    self.textFieldPhone.layer.borderWidth=0.5f;
    self.textFieldPhone.layer.borderColor=[UIColor lightGrayColor].CGColor;
    self.textFieldPhone.clipsToBounds = YES;
    
    self.textFieldPassword.layer.cornerRadius = 5.0f;
    self.textFieldPassword.layer.borderWidth=0.5f;
    self.textFieldPassword.layer.borderColor=[UIColor lightGrayColor].CGColor;
    self.textFieldPassword.clipsToBounds = YES;
    
    self.textFieldAddress.layer.cornerRadius = 5.0f;
    self.textFieldAddress.layer.borderWidth=0.5f;
    self.textFieldAddress.layer.borderColor=[UIColor lightGrayColor].CGColor;
    self.textFieldAddress.clipsToBounds = YES;
    
    self.textFieldName.layer.cornerRadius = 5.0f;
    self.textFieldName.layer.borderWidth=0.5f;
    self.textFieldName.layer.borderColor=[UIColor lightGrayColor].CGColor;
    self.textFieldName.clipsToBounds = YES;
    
    self.textFieldEmail.layer.cornerRadius = 5.0f;
    self.textFieldEmail.layer.borderWidth=0.5f;
    self.textFieldEmail.layer.borderColor=[UIColor lightGrayColor].CGColor;
    self.textFieldEmail.clipsToBounds = YES;
    
    self.stateText.layer.cornerRadius = 5.0f;
    self.stateText.layer.borderWidth=0.5f;
    self.stateText.layer.borderColor=[UIColor lightGrayColor].CGColor;
    self.stateText.clipsToBounds = YES;
    
    self.textFieldZip.layer.cornerRadius = 5.0f;
    self.textFieldZip.layer.borderWidth=0.5f;
    self.textFieldZip.layer.borderColor=[UIColor lightGrayColor].CGColor;
    self.textFieldZip.clipsToBounds = YES;
    
    self.textFielddob.layer.cornerRadius = 5.0f;
    self.textFielddob.layer.borderWidth=0.5f;
    self.textFielddob.layer.borderColor=[UIColor lightGrayColor].CGColor;
    self.textFielddob.clipsToBounds = YES;
    
    
    self.textFieldGender.layer.cornerRadius = 5.0f;
    self.textFieldGender.layer.borderWidth=0.5f;
    self.textFieldGender.layer.borderColor=[UIColor lightGrayColor].CGColor;
    self.textFieldGender.clipsToBounds = YES;
    
    NSString *str =@"*";
    
    NSString *username = [NSString stringWithFormat:@"First Name %@",str];
    
    NSMutableAttributedString *attributedString =[[NSMutableAttributedString alloc] initWithString:username];
    
    [attributedString addAttribute:NSForegroundColorAttributeName
                             value:[Alert colorFromHexString:kCOLOR_NAV]
                             range:[username rangeOfString:str]];
    
    self.textFieldName.attributedPlaceholder = attributedString;
    
    NSString *email = [NSString stringWithFormat:@"E-mail Address %@",str];
    
    NSMutableAttributedString *attributedString1 =[[NSMutableAttributedString alloc] initWithString:email];
    
    [attributedString1 addAttribute:NSForegroundColorAttributeName
                              value:[Alert colorFromHexString:kCOLOR_NAV]
                              range:[email rangeOfString:str]];
    
    self.textFieldEmail.attributedPlaceholder = attributedString1;
    
    NSString *password = [NSString stringWithFormat:@"Password %@",str];
    
    NSMutableAttributedString *attributedString2 =[[NSMutableAttributedString alloc] initWithString:password];
    
    [attributedString2 addAttribute:NSForegroundColorAttributeName
                              value:[Alert colorFromHexString:kCOLOR_NAV]
                              range:[password rangeOfString:str]];
    
    self.textFieldPassword.attributedPlaceholder = attributedString2;
    
    NSString *phone = [NSString stringWithFormat:@"Phone %@",str];
    
    NSMutableAttributedString *attributedString3 =[[NSMutableAttributedString alloc] initWithString:phone];
    
    [attributedString3 addAttribute:NSForegroundColorAttributeName
                              value:[Alert colorFromHexString:kCOLOR_NAV]
                              range:[phone rangeOfString:str]];
    
    self.textFieldPhone.attributedPlaceholder = attributedString3;
    
    NSString *gender = [NSString stringWithFormat:@"Last Name %@",str];
    
    NSMutableAttributedString *attributedString4 =[[NSMutableAttributedString alloc] initWithString:gender];
    
    [attributedString4 addAttribute:NSForegroundColorAttributeName
                              value:[Alert colorFromHexString:kCOLOR_NAV]
                              range:[gender rangeOfString:str]];
    
    self.textFieldGender.attributedPlaceholder = attributedString4;
    
    NSString *address = [NSString stringWithFormat:@"Address %@",str];
    
    NSMutableAttributedString *attributedString5 =[[NSMutableAttributedString alloc] initWithString:address];
    
    [attributedString5 addAttribute:NSForegroundColorAttributeName
                              value:[Alert colorFromHexString:kCOLOR_NAV]
                              range:[address rangeOfString:str]];
    
    self.textFieldAddress.attributedPlaceholder = attributedString5;
    
    NSString *state = [NSString stringWithFormat:@"State %@",str];
    
    NSMutableAttributedString *attributedString6 =[[NSMutableAttributedString alloc] initWithString:state];
    
    [attributedString6 addAttribute:NSForegroundColorAttributeName
                              value:[Alert colorFromHexString:kCOLOR_NAV]
                              range:[state rangeOfString:str]];
    
    self.stateText.attributedPlaceholder = attributedString6;
    
    NSString *zipcode = [NSString stringWithFormat:@"Zip Code %@",str];
    
    NSMutableAttributedString *attributedString7 =[[NSMutableAttributedString alloc] initWithString:zipcode];
    
    [attributedString7 addAttribute:NSForegroundColorAttributeName
                              value:[Alert colorFromHexString:kCOLOR_NAV]
                              range:[zipcode rangeOfString:str]];
    
    self.textFieldZip.attributedPlaceholder = attributedString7;
    
    
    [self navigationBarConfiguration];
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    if (valrible==0)
    {
        self.textFieldGender.text = [AppDelegate appDelegate].genderArr[row];
    }
    if (valrible==1)
    {
        self.stateText.text = [[AppDelegate appDelegate].stateArr[row]valueForKey:@"name"];
        self.stateId =[[AppDelegate appDelegate].stateArr[row]valueForKey:@"id"];
        
    }
    
    
    [self.dienstPicker setHidden:YES];
    [self.donePicker setHidden:YES];
}

#pragma mark - UIPickerViewDataSource
-(IBAction)done:(id)sender
{
    [self.dienstPicker setHidden:YES];
    [self.donePicker setHidden:YES];
}


#pragma mark - UIPickerViewDataSource


-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// #4
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    
    
    if (valrible==0)
    {
        return  [AppDelegate appDelegate].genderArr.count;
    }
    else if (valrible==1)
    {
        return  [AppDelegate appDelegate].stateArr.count;
    }
    else
    {
        return 0;
    }
    
    
}

#pragma mark - UIPickerViewDelegate

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (valrible==0)
    {
        return  [AppDelegate appDelegate].genderArr[row];
    }
    else  if (valrible==1)
    {
        return  [[AppDelegate appDelegate].stateArr[row]valueForKey:@"name"];
    }
    
    else
    {
        return 0;
    }
    
}
- (IBAction)showPickerView:(UIButton*)sender
{
    [self.dienstPicker setHidden:NO];
    [self.donePicker setHidden:NO];
    self.dienstPicker.delegate = self;     //#2
    self.dienstPicker.dataSource = self;
    if (sender.tag==0)
    {
        valrible = 0;
    }
    if (sender.tag==1)
    {
        valrible = 1;
    }
    [self.dienstPicker reloadAllComponents];
    
}
//- (IBAction)buttonUserTypeSelectClick:(id)sender
//{
//
//    UIButton * button = (UIButton *)sender;
//
//    switch (button.tag) {
//        case 11:
//        {
//            self.tableViewLogin.hidden=YES;
//            self.tableViewRegister.hidden=NO;
//            self.selectType=@"Register";
//            NSString * str = [[NSUserDefaults standardUserDefaults] stringForKey:@"CheckFrom"];
//
//            if ([str isEqualToString:@"Free"])
//            {
//                _widthConstraintOfbusiness.constant = 0;
//                _widthConstraintOfbusiness.constant = 0;
//                //  _widthConstraintOfZip.constant=0;
//                self.buttonBusiness.hidden=YES;
//                self.buttonFreelancer.hidden=YES;
//
//            }
//            else
//            {
//
//                _widthConstraintOfbusiness.constant = 50;
//                _widthConstraintOfbusiness.constant = 50;
//                //  _widthConstraintOfZip.constant=50;
//                self.buttonBusiness.hidden=NO;
//                self.buttonFreelancer.hidden=NO;
//
//            }
//
////            [self.buttonSignIn setTitleColor:[Alert colorFromHexString:kCOLOR_TINT_NAV] forState:UIControlStateNormal];
////            [self.buttonSignUP setTitleColor:[Alert colorFromHexString:kCOLOR_regi] forState:UIControlStateNormal];
////
//
//
//            [self clearTextField];
//
//            //            _tableViewTextField.contentSize = CGSizeMake(self.view.frame.size.width, 500);
//
//        }
//            break;
//        case 10:
//        {
//            self.tableViewLogin.hidden=NO;
//            self.tableViewRegister.hidden=YES;
//            self.selectType=@"Login";
//
//
//            [self.buttonSignUP setTitleColor:[Alert colorFromHexString:kCOLOR_TINT_NAV] forState:UIControlStateNormal];
//            [self.buttonSignIn setTitleColor:[Alert colorFromHexString:kCOLOR_regi] forState:UIControlStateNormal];
//
//            [self clearTextField];
//
//        }
//            break;
//
//        default:
//            break;
//    }
//
//}
- (IBAction)buttonSignIn:(id)sender
{
    
    if(self.textFieldUsernameType.text.length>0 &&
       self.textFieldPasswordType.text.length>0 )
    {
        // Validate Email Address
        BOOL validateEmail = [Alert validationEmail:self.textFieldUsernameType.text];
        
        if (validateEmail) {
            
            // Network Code With Status
            BOOL network = [Alert networkStatus];
            
            if (network) {
                
                
                [self serviceLoginUserByEmail];
                
            }
            else
            {
                kNETWORK_PROBLEM;
            }
        }
        
        
        else
        {
            [Alert svError:kEmail_Validate];
            
        }
        
    }
    else
    {
        
        [Alert svError:kEmptyFields];
    }
    
}


#pragma mark - service Login by Email
-(void)serviceLoginUserByEmail
{
    // SERVER CONNCETION
    
    [Alert svProgress:@"Sign in..."];
    
    WebService * connection = [[WebService alloc]init];
    connection.delegate = self;
    
    NSMutableDictionary * dict       = [NSMutableDictionary dictionary];
    [dict setObject:kAPI_METHOD_LOGIN                    forKey:kAPI_ACTION];
    [dict setObject:self.textFieldUsernameType.text                forKey:kAPI_USER_EMAIL];
    [dict setObject:self.textFieldPasswordType.text             forKey:kAPI_PASSWORD];
    
    NSString * strToken = [[NSUserDefaults standardUserDefaults] stringForKey:kAPI_DEVICE_TOKEN];
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        strToken = kDEVICE_SIMULATOR_TOKEN;
    }
    [dict setObject:@"ios"      forKey:@"device"];
    [dict setObject:strToken             forKey:@"deviceToken"];
    
    [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:kAPI_METHOD_LOGIN];
}

- (IBAction)buttonRegister:(id)sender
{
    
    if(self.textFieldName.text.length>0 &&
       self.textFieldEmail.text.length>0 &&
       self.textFieldPassword.text.length>0 &&
       self.textFieldPhone.text.length>0  &&
       self.textFieldAddress.text.length>0)
    {
        NSString * str = [[NSUserDefaults standardUserDefaults] stringForKey:@"CheckFrom"];
        if ([str isEqualToString:@"Free"])
        {
            BOOL validateEmail = [Alert validationEmail:self.textFieldEmail.text];
            
            if (validateEmail)
            {
                
                // Network Code With Status
                BOOL network = [Alert networkStatus];
                
                if (network)
                {
                    
                    [self serviceRegisterUserByEmail];
                    
                }
                else
                {
                    kNETWORK_PROBLEM;
                }
                
                
            }
            
            else
            {
                [Alert svError:kEmail_Validate];
                
            }
            
        }
        else
        {
            if (self.researchSelected.length>0) {
                // Validate Email Address
                BOOL validateEmail = [Alert validationEmail:self.textFieldEmail.text];
                
                if (validateEmail)
                {
                    
                    // Network Code With Status
                    BOOL network = [Alert networkStatus];
                    
                    if (network) {
                        
                        
                        [self serviceRegisterUserByEmail];
                        
                    }
                    else
                    {
                        kNETWORK_PROBLEM;
                    }
                    
                }
                
                
                else
                {
                    [Alert svError:kEmail_Validate];
                    
                }
            }
            else{
                [Alert svError:@"Please select vender type"];
            }
        }
    }
    else
    {
        
        [Alert svError:kEmptyFields];
    }
    
}


- (NSString *) unformatPhonenumber:(NSString *)phoneNumber

{
    
    NSString *phNumber = phoneNumber;
    
    if (phNumber)
    {
        phNumber = [phNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
        phNumber = [phNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
        phNumber = [phNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
        phNumber = [phNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    }
    
    
    BOOL valid;
    
    NSCharacterSet *alphaNums = [NSCharacterSet decimalDigitCharacterSet];
    
    NSCharacterSet *inStringSet = [NSCharacterSet characterSetWithCharactersInString:phNumber];
    
    valid = [alphaNums isSupersetOfSet:inStringSet];
    
    if (!valid) //If Not numeric return same
        return phoneNumber;
    else
        return phNumber;
}

- (NSString *) formatPhone: (NSString *) unformattedString

{
    NSString * returnString = [self unformatPhonenumber:unformattedString];
    NSCharacterSet* nonNumbers = [[NSCharacterSet
                                   characterSetWithCharactersInString:@"01234567890"] invertedSet];
    
    NSRange r = [unformattedString rangeOfCharacterFromSet: nonNumbers];
    
    if (r.location == NSNotFound)
    {
        NSRange areaCode = {0, 3};
        NSRange prefix = {3, 3};
        NSRange suffix = {6, 4};
        if ((unformattedString.length == 10) && (! [unformattedString hasPrefix:@"1"]))
        {
            returnString = [NSString stringWithFormat:@"(%@) %@-%@", [unformattedString substringWithRange:areaCode], [unformattedString substringWithRange:prefix],[unformattedString substringWithRange:suffix]];
        }
    }
    
    return returnString;
}


#pragma mark - service Register by Email
-(void)serviceRegisterUserByEmail
{
    // SERVER CONNCETION
    
    [Alert svProgress:@"Sign up..."];
    
    WebService * connection = [[WebService alloc]init];
    connection.delegate = self;
    
    NSMutableDictionary * dict       = [NSMutableDictionary dictionary];
    [dict setObject:kAPI_METHOD_REGISTER                    forKey:kAPI_ACTION];
    [dict setObject:self.textFieldEmail.text                forKey:kAPI_USER_EMAIL];
    [dict setObject:self.textFieldPassword.text             forKey:kAPI_PASSWORD];
    [dict setObject:self.textFieldName.text                forKey:kAPI_USER_FULLNAME];
    [dict setObject:self.textFieldGender.text                forKey:@"lastName"];
    [dict setObject:self.textFieldPhone.text             forKey:kAPI_contactNumber];
    [dict setObject:self.textFieldAddress.text             forKey:kAPI_USER_address];
    NSString * strToken = [[NSUserDefaults standardUserDefaults] stringForKey:kAPI_DEVICE_TOKEN];
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        strToken = kDEVICE_SIMULATOR_TOKEN;
    }
    [dict setObject:@"ios"      forKey:@"device"];
    [dict setObject:strToken             forKey:@"deviceToken"];
    
    NSString * str = [[NSUserDefaults standardUserDefaults] stringForKey:@"CheckFrom"];
      
    if ([str isEqualToString:@"Provider"])
    {
        [dict setObject:@"Partner"                forKey:kAPI_USER_TYPE];
    }
   else if ([str isEqualToString:@"Driver"])
    {
        [dict setObject:@"Driver"                forKey:kAPI_USER_TYPE];
    }
    else
    {
        [dict setObject:@"Member"                forKey:kAPI_USER_TYPE];
       
    }
    
    [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:kAPI_METHOD_REGISTER];
}

-(void)serviceProfile
{
    // SERVER CONNCETION
    
    [Alert svProgress:@"Sign in..."];
    
    WebService * connection = [[WebService alloc]init];
    connection.delegate = self;
    NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults] dictionaryForKey:kLOGIN];
    
    NSString * user_id  = [dictLogin valueForKey:@"userId"];
    
    NSMutableDictionary * dict       = [NSMutableDictionary dictionary];
    [dict setObject:kAPI_METHOD_USER_PROFILE                    forKey:kAPI_ACTION];
    [dict setObject:user_id                forKey:kUSER_ID];
    
    [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:kAPI_METHOD_USER_PROFILE];
}

#pragma mark - Sevice delgate methods

-(void)getDataFormWebService:(NSDictionary *)jsonResults urlStr:(NSString *)yourUR methodName:(NSString *)methodName{
    
    [Alert svSuccess:@"DONE"];
    
    SWITCH (methodName) {
        
        CASE (kAPI_METHOD_LOGIN)
        {
            
            [self gotoFurtherVC:jsonResults];
        }
        
        CASE (kAPI_METHOD_REGISTER)
        {
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"Editbusinessaddresss"];
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"lat"];
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"long"];
            
            NSDictionary * dictoanryLoginData = [Alert removeNSNullClass:(NSMutableDictionary*)[jsonResults objectForKey:@"data"]];
           
            [[NSUserDefaults standardUserDefaults] setObject:dictoanryLoginData forKey:@"ProfileDetail"];
            
            [[NSUserDefaults standardUserDefaults] setObject:dictoanryLoginData forKey:kLOGIN];
            
            [[NSUserDefaults standardUserDefaults]synchronize];
          
              
               TermsViewController *forgotPasswordViewController =  GET_VIEW_CONTROLLER(@"TermsViewController");
                                       
                UINavigationController * navigationController = [Alert navigationControllerWithVC:forgotPasswordViewController];
                                       
                PERSENT_VIEW_CONTOLLER(navigationController, YES);
           
        }
        
        CASE (kAPI_METHOD_USER_PROFILE)
        {
            
            NSDictionary * dictoanryLoginData = [Alert removeNSNullClass:(NSMutableDictionary*)[jsonResults objectForKey:@"data"]];
            [[NSUserDefaults standardUserDefaults] setObject:dictoanryLoginData forKey:@"ProfileDetail"];
            [[NSUserDefaults standardUserDefaults]synchronize];
          
            NSString * str = [[NSUserDefaults standardUserDefaults] stringForKey:@"CheckFrom"];
          
            if ([[dictoanryLoginData valueForKey:@"role"] isEqualToString:@"Driver"]) {
               
                appDelegate = UIAppDelegate;
                DriverDashboardViewController * homePassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"DriverDashboardViewController");
                UIAppDelegate.isCall  = YES;
                MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
                [appDelegate manageDrawerRear:menuViewController front:homePassengerViewController];
    
            }
            else{
                appDelegate = UIAppDelegate;
                DashboardViewController * homePassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBID_ARRIVED_VC);
                UIAppDelegate.isCall  = YES;
                MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
                [appDelegate manageDrawerRear:menuViewController front:homePassengerViewController];
    
            }
        
    }
    break;
    
    DEFAULT
    {
        break;
    }
}

}

#pragma mark - Push VC
-(void)gotoFurtherVC:(NSDictionary *)dict
{
    
    NSDictionary * dictoanryLoginData = [Alert removeNSNullClass:(NSMutableDictionary*)[dict objectForKey:@"data"]];
    
    [[NSUserDefaults standardUserDefaults] setObject:dictoanryLoginData forKey:kLOGIN];
    
    [[NSUserDefaults standardUserDefaults]synchronize];
    SYNCHRONIZED;
    
    [[NSUserDefaults standardUserDefaults] setObject:[dictoanryLoginData valueForKey:@"role"] forKey:@"CheckFrom"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    [self serviceProfile];
    
}
// WEB SERVER ERROR MSG LOCAL SERVER (APPLICATION SERVER)
-(void) webServiceFailWithApplicationServerMSG:(NSString *)msg
{
    
    [Alert svError:msg];
    
}

// WEB SERVICE FAIL WITH REGION TCP/IP
-(void) webServiceFail:(NSError *)error{
    
    
    [Alert svError:[error localizedDescription]];
    
    
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    if (self.textFielddob.editing == YES)
    {
        
        [self.textFielddob resignFirstResponder];
        [calendarView ActiveCalendar:textField];
        
    }
    if (self.textFieldAddress.editing == YES)
    {
        
        [self.textFieldAddress resignFirstResponder];
        
        GMSAutocompleteViewController *acController = [[GMSAutocompleteViewController alloc] init];
             if (@available(iOS 13.0, *)) {
                 if(UIScreen.mainScreen.traitCollection.userInterfaceStyle == UIUserInterfaceStyleDark ){
                     acController.primaryTextColor = UIColor.whiteColor;
                     acController.secondaryTextColor = UIColor.lightGrayColor;
                     acController.tableCellSeparatorColor = UIColor.lightGrayColor;
                     acController.tableCellBackgroundColor = UIColor.darkGrayColor;
                 } else {
                     acController.primaryTextColor = UIColor.blackColor;
                     acController.secondaryTextColor = UIColor.lightGrayColor;
                     acController.tableCellSeparatorColor = UIColor.lightGrayColor;
                     acController.tableCellBackgroundColor = UIColor.whiteColor;
                 }
             }
             acController.delegate = self;
             [[UISearchBar appearance] setBarStyle:UIBarStyleDefault];
             [[UITextField appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]] setTextColor:[UIColor whiteColor]];
             [self presentViewController:acController animated:YES completion:nil];
            
        
//        STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SearchLocationViewController"]];
//        popupController.containerView.layer.cornerRadius = 4;
//        [popupController presentInViewController:self];
    }
}

// Handle the user's selection.
- (void)viewController:(GMSAutocompleteViewController *)viewController
didAutocompleteWithPlace:(GMSPlace *)place {

    [self dismissViewControllerAnimated:YES completion:nil];
    // Do something with the selected place.
    NSLog(@"Place name %@", place.name);
    NSLog(@"Place address %@", place.formattedAddress);
    NSLog(@"Place latitude %f", place.coordinate.latitude);
    NSLog(@"Place longitude %f", place.coordinate.longitude);
   
    [AppDelegate appDelegate].addressString = place.formattedAddress;
    
    [AppDelegate appDelegate].latitude= [NSString stringWithFormat:@"%f",place.coordinate.latitude];
    [AppDelegate appDelegate].longitude= [NSString stringWithFormat:@"%f",place.coordinate.longitude];

    [self.textFieldAddress setText:[NSString stringWithFormat:@"%@", place.name]];

}


- (void)viewController:(GMSAutocompleteViewController *)viewController
didFailAutocompleteWithError:(NSError *)error {
    // TODO: handle the error.
    NSLog(@"error: %ld", [error code]);
    [self dismissViewControllerAnimated:YES completion:nil];
   
}

// User canceled the operation.
- (void)wasCancelled:(GMSAutocompleteViewController *)viewController {
    NSLog(@"Autocomplete was cancelled.");
    
   // _setLocation = nil;
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)didTapLabel:(WSLabel *)lblView withDate:(NSDate *)selectedDate
{
    
    
}

-(void)deactiveWSCalendarWithDate:(NSDate *)selectedDate
{
    NSDateFormatter *monthFormatter=[[NSDateFormatter alloc] init];
    [monthFormatter setDateFormat:@"MM/dd/yyyy"];
    NSString *str=[monthFormatter stringFromDate:selectedDate];
    
    
    NSDate* now = [NSDate date];
    NSDateComponents* ageComponents = [[NSCalendar currentCalendar]
                                       components:NSCalendarUnitYear
                                       fromDate:selectedDate
                                       toDate:now
                                       options:0];
    NSInteger age = [ageComponents year];
    
    if(age>13)
    {
        self.textFielddob.text = str;
        
    }
    else
    {
        [Alert svError:@"No Users Under 13"];
    }
    
}

@end
