//
//  CashoutViewController.m
//  GetSum
//
//  Created by santosh kumar singh on 30/04/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import "CashoutViewController.h"
#import "MenuViewController.h"
#import "AppDelegate.h"
#import "WalletListViewController.h"
#import "DashboardViewController.h"
#import "DriverDashboardViewController.h"

@interface CashoutViewController ()<WebServiceDelegate>
{
    AppDelegate * appDelegate;
}
@end

@implementation CashoutViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
   // [self.view setBackgroundColor:[Alert colorWithPatternImage2:kBG_IMAGE]];
    
    self.submitBtn.layer.cornerRadius = 12.0f;
   
    self.dummyImage.layer.cornerRadius = 12.0f;
   
    self.textamount.layer.cornerRadius = 12.0f;
    self.textamount.clipsToBounds = YES;
    
    [self serviceCategoryList];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self
    action:@selector(singleTapGestureCaptured:)];
    [self.dummyImage addGestureRecognizer:singleTap];
    [self.dummyImage setMultipleTouchEnabled:YES];
    [self.dummyImage setUserInteractionEnabled:YES];
    
    self.textwalletamount.text= @"Total Earning";
   
    self.title = @"CASHOUT";
    [self navigationBarConfiguration];
    // Do any additional setup after loading the view.
}

-(void)singleTapGestureCaptured:(UITapGestureRecognizer *)gestureRecognizer
{
    NSURL *url = [NSURL URLWithString:@"https://www.hypdemand.com/hyptunesradio"];
           if ([[UIApplication sharedApplication] canOpenURL:url])
           {
               [[UIApplication sharedApplication] openURL:url];
           }
    
}

-(void)serviceCategoryList{
    
    // SERVER CONNCETION
    [Alert svProgress:@"Please Wait..."];
 NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults]  objectForKey:kLOGIN];
    
    WebService * connection = [[WebService alloc]init];
    connection.delegate = self;
    NSMutableDictionary * dict                              = [NSMutableDictionary dictionary];
    
    [dict setObject:@"getwallet"                     forKey:kAPI_ACTION];
    [dict setObject:[dictLogin valueForKey:@"userId"]                     forKey:kUSER_ID];
    
    
    NSLog(@"dict is ---%@",dict);
    [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:@"getwallet"];
    
}


-(void)navigationBarConfiguration
{
    
    self.navigationController.navigationBarHidden = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    UIButton * menuButton  = [UIButton buttonWithType:UIButtonTypeSystem];
    menuButton.frame = CGRectMake(8, 18, 24, 26);
    [menuButton setImage:[UIImage imageNamed:IMG_BACK] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *accountBarItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    self.navigationItem.leftBarButtonItem = accountBarItem;
    NSMutableArray *arrRightBarItems = [[NSMutableArray alloc] init];
          UIButton *btnSetting = [UIButton buttonWithType:UIButtonTypeCustom];
          [btnSetting setImage:[UIImage imageNamed:@"Frequency"] forState:UIControlStateNormal];
          btnSetting.frame = CGRectMake(0, 0, 40, 40);
          btnSetting.showsTouchWhenHighlighted=YES;
         [btnSetting addTarget:self action:@selector(onSettings:) forControlEvents:UIControlEventTouchUpInside];
          UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSetting];
        
          UIButton *btnLib = [UIButton buttonWithType:UIButtonTypeCustom];
          [btnLib setImage:[UIImage imageNamed:@"logo1.png"] forState:UIControlStateNormal];
          btnLib.frame = CGRectMake(0, 0, 40, 40);
          btnLib.showsTouchWhenHighlighted=YES;
          [btnLib addTarget:self action:@selector(onMyLibrary:) forControlEvents:UIControlEventTouchUpInside];
          UIBarButtonItem *barButtonItem2 = [[UIBarButtonItem alloc] initWithCustomView:btnLib];
          [arrRightBarItems addObject:barButtonItem2];
          [arrRightBarItems addObject:barButtonItem];
          self.navigationItem.rightBarButtonItems=arrRightBarItems;
       
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor],
    }];
    
}

-(void)onMyLibrary:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}
-(void)onSettings:(id)sender
{
  
    NSString*myurl=@"https://www.hypdemand.com/hyptunesradio";
     NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
       [[UIApplication sharedApplication] openURL:url];
    }
}

-(IBAction)add:(id)sender
{
    
    WalletListViewController *forgotPasswordViewController =  GET_VIEW_CONTROLLER(@"WalletListViewController");
    UINavigationController * navigationController = [Alert navigationControllerWithVC:forgotPasswordViewController];
    PERSENT_VIEW_CONTOLLER(navigationController, YES);
    
}


-(void)back:(id)sender
{
    DISMISS_VIEW_CONTROLLER;
}


- (IBAction)buttonSave:(id)sender
{
    
    if (![self.textamount.text isEqualToString:@"$00.00"] )
    {
        if ([self.textamount.text floatValue] <= [self.priceString floatValue])
        {
            NSLog(@"process");
            [self chargeAmount];
        }
        else
        {
            [Alert svError:@"Please enter low amount"];
        }
        
    }
    else
    {
        [Alert svError:@"Please enter cashout balance"];
    }
}
-(void)chargeAmount
{
   
    [Alert svProgress:@"Please wait...."];
    
    WebService * connection = [[WebService alloc]init];
    connection.delegate = self;
   NSDictionary * dictLogin= [[NSUserDefaults standardUserDefaults]  objectForKey:kLOGIN];
    
    NSString * user_id ;
    if ([dictLogin valueForKey:@"id"] == nil)
    {
        user_id  = [dictLogin valueForKey:@"userId"];
        
    }
    else
    {
        user_id  = [dictLogin valueForKey:@"id"];
    }
    
    NSMutableDictionary * dict = [NSMutableDictionary dictionary];
    [dict setObject:self.textamount.text forKey:@"requestAmount"];
    [dict setObject:user_id forKey:@"userId"];
    [dict setObject:@"cashoutrequest" forKey:kAPI_ACTION];
    
    [connection webServicePostString:[Alert jsonStringWithDictionary:dict] urlString:kURL_BASE methodName:@"cashoutrequest"];
}

#pragma mark - Sevice delgate methods

-(void)getDataFormWebService:(NSDictionary *)jsonResults urlStr:(NSString *)yourUR methodName:(NSString *)methodName{
    
    [SVProgressHUD dismiss];
    
    SWITCH (methodName) {
        CASE (@"cashoutrequest")
        {
            NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults] dictionaryForKey:kLOGIN];
            
            NSLog(@"jsonResults =%@",jsonResults);
            if ([[dictLogin valueForKey:@"role"] isEqualToString:@"Driver"]) {
               
                appDelegate = UIAppDelegate;
                DriverDashboardViewController * homePassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"DriverDashboardViewController");
                UIAppDelegate.isCall  = YES;
                MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
                [appDelegate manageDrawerRear:menuViewController front:homePassengerViewController];
    
            }
            else{
            [Alert svSuccess:[jsonResults objectForKey:@"msg"]];
            appDelegate = UIAppDelegate;
            DashboardViewController * homePassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBID_ARRIVED_VC);
            UIAppDelegate.isCall  = YES;
            MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
            [appDelegate manageDrawerRear:menuViewController front:homePassengerViewController];
            }
            
        }
        CASE (@"getwallet")
        {
            NSLog(@"jsonResults =%@",jsonResults);
            
            self.balanceLabel.text =[NSString stringWithFormat:@"$%.2f",[[jsonResults objectForKey:@"wallet"] floatValue]];
            
            self.withdrawbalanceLabel.text =[NSString stringWithFormat:@"Withdrawable Balance: $%.2f",[[jsonResults objectForKey:@"wallet"] floatValue]];
            
            self.priceString = [jsonResults objectForKey:@"wallet"];
        }
        
        
        break;
        
        DEFAULT
        {
            break;
        }
    }
    
}
-(void) webServiceFailWithApplicationServerMSG:(NSString *)msg
{
    
    [Alert svError:msg];
    
}

// WEB SERVICE FAIL WITH REGION TCP/IP
-(void) webServiceFail:(NSError *)error{
    
    
    [Alert svError:[error localizedDescription]];
    
    
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
