//
//  ManageTableViewCell.h
//  Q-Workerbee
//
//  Created by santosh kumar singh on 17/06/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface ManageTableViewCell : UITableViewCell

@property(nonatomic,strong)  IBOutlet UILabel *priceLabel;
@property(nonatomic,strong)  IBOutlet UILabel *serviceLabel;
@property(nonatomic,strong)  IBOutlet UITextField *pricetextField;
@property(nonatomic,strong)  IBOutlet UITextView *disctextView;
@property(nonatomic,strong)  IBOutlet UIButton *serviceBtn;

@end


