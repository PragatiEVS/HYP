//
//  EarningViewController.m
//  CHORETHING
//
//  Created by santosh kumar singh on 08/04/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import "EarningViewController.h"
#import "ConfirmedTableViewCell.h"
#import "CashoutViewController.h"
#import "ChoreDetailViewController.h"

@interface EarningViewController ()<WebServiceDelegate,UITableViewDelegate,UITableViewDataSource>

@end

@implementation EarningViewController

- (void)viewDidLoad
{
    
    startidx = 1;
    
    self.title = @"EARNING";
    self.totalAmount.layer.cornerRadius = 10;
    self.totalAmount.clipsToBounds= YES;
    
    self.totalCount.layer.cornerRadius = 10;
    self.totalCount.clipsToBounds= YES;
    
    [self navigationBarConfiguration];
    
    resultArray =[[NSMutableArray alloc]init];
    _noListAvalible.hidden=YES;
    
    [self servicegeterningList];
    [self serviceConfirmedList];
    [super viewDidLoad];
    
    
    // Do any additional setup after loading the view.
}

-(void)navigationBarConfiguration
{
    
    self.navigationController.navigationBarHidden = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    
    UIButton * menuButton  = [UIButton buttonWithType:UIButtonTypeSystem];
    menuButton.frame = CGRectMake(8, 18, 24, 24);
    [menuButton setBackgroundImage:[UIImage imageNamed:NAV_SLIDER_IMAGE] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(drawer:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *accountBarItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    self.navigationItem.leftBarButtonItem = accountBarItem;
  
    NSMutableArray *arrRightBarItems = [[NSMutableArray alloc] init];
            UIButton *btnSetting = [UIButton buttonWithType:UIButtonTypeCustom];
            [btnSetting setImage:[UIImage imageNamed:@"Frequency"] forState:UIControlStateNormal];
            btnSetting.frame = CGRectMake(0, 0, 40, 40);
            btnSetting.showsTouchWhenHighlighted=YES;
           [btnSetting addTarget:self action:@selector(onSettings:) forControlEvents:UIControlEventTouchUpInside];
            UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSetting];
          
            UIButton *btnLib = [UIButton buttonWithType:UIButtonTypeCustom];
            [btnLib setImage:[UIImage imageNamed:@"logo1.png"] forState:UIControlStateNormal];
            btnLib.frame = CGRectMake(0, -5, 40, 40);
            btnLib.showsTouchWhenHighlighted=YES;
           [btnLib addTarget:self action:@selector(onMyLibrary:) forControlEvents:UIControlEventTouchUpInside];
            UIBarButtonItem *barButtonItem2 = [[UIBarButtonItem alloc] initWithCustomView:btnLib];
            [arrRightBarItems addObject:barButtonItem2];
            [arrRightBarItems addObject:barButtonItem];
            self.navigationItem.rightBarButtonItems=arrRightBarItems;
           
         
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName :[UIColor whiteColor]}];
}

-(void)onMyLibrary:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}
-(void)onSettings:(id)sender
{
  
    NSString*myurl=@"https://www.hypdemand.com/hyptunesradio";
     NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
       [[UIApplication sharedApplication] openURL:url];
    }
}

#pragma mark - Drawer class
-(void)drawer:(id)sender
{
    
    KYDrawerController *elDrawer = (KYDrawerController*)self.navigationController.parentViewController;
    [elDrawer setDrawerState:KYDrawerControllerDrawerStateOpened animated:YES];
    
    
}
-(void)servicegeterningList{
    
    // SERVER CONNCETION
    [Alert svProgress:@"Please Wait..."];
    
    NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults] dictionaryForKey:kLOGIN];
    
    
    NSString * user_id ;
    if ([dictLogin valueForKey:@"id"] == nil)
    {
        user_id  = [dictLogin valueForKey:@"userId"];
        
    }
    else
    {
        user_id  = [dictLogin valueForKey:@"id"];
    }
    
    WebService * connection = [[WebService alloc]init];
    connection.delegate = self;
    NSMutableDictionary * dict                              = [NSMutableDictionary dictionary];
    
    [dict setObject:@"geterning"                     forKey:kAPI_ACTION];
   
    [dict setObject:user_id                    forKey:kUSER_ID];
    
    [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:@"geterning"];
    
}

-(void)serviceConfirmedList{
    
    // SERVER CONNCETION
    [Alert svProgress:@"Please Wait..."];
    
    NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults] dictionaryForKey:kLOGIN];
    
    NSString * str = [dictLogin valueForKey:@"role"];
    
    NSString * user_id ;
    if ([dictLogin valueForKey:@"id"] == nil)
    {
        user_id  = [dictLogin valueForKey:@"userId"];
        
    }
    else
    {
        user_id  = [dictLogin valueForKey:@"id"];
    }
    WebService * connection = [[WebService alloc]init];
    connection.delegate = self;
    NSMutableDictionary * dict                              = [NSMutableDictionary dictionary];
    
    [dict setObject:@"allbookinglist"                     forKey:kAPI_ACTION];
    [dict setObject:user_id                    forKey:kUSER_ID];
    [dict setObject:str                     forKey:@"usertype"];
    [dict setObject:@"2"                     forKey:@"status"];
    [dict setObject:[NSString stringWithFormat:@"%d", startidx]                     forKey:@"pageNo"];
    NSLog(@"dict is ---%@",dict);
    [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:@"bookinglist"];
    
}


#pragma mark - Sevice delgate methods

-(void)getDataFormWebService:(NSDictionary *)jsonResults urlStr:(NSString *)yourUR methodName:(NSString *)methodName{
    
    [SVProgressHUD dismiss];
    
    SWITCH (methodName) {
        CASE (@"bookinglist")
        {
            NSLog(@"jsonResults =%@",jsonResults);
            arr=[jsonResults valueForKey:@"data"];
            
            
            for ( NSDictionary *dict in [jsonResults valueForKey:@"data"])
            {
                [resultArray addObject:dict];
            }
            
            if (arr.count==0)
            {
                
                if (arr.count ==0 && resultArray.count ==0)
                {
                    _noListAvalible.hidden=NO;
                    self.completedTableView.hidden=YES;
                }
                
            }
            else
            {  _noListAvalible.hidden=YES;
                _completedTableView.hidden=NO;
                self.completedTableView.delegate=self;
                self.completedTableView.dataSource=self;
                [self.completedTableView reloadData];
                
            }
        }
        CASE (@"geterning")
        {
            
            NSLog(@"jsonResults =%@",jsonResults);
            self.totalAmount.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
            // you probably want to center it
            self.totalAmount.titleLabel.textAlignment = NSTextAlignmentCenter; // if you want to
            
           
                [self.totalAmount setTitle:[NSString stringWithFormat:@"$%@ \nYou Earned",[jsonResults objectForKey:@"totalAmount"]] forState: UIControlStateNormal];
                
                self.totalCount.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
                // you probably want to center it
                self.totalCount.titleLabel.textAlignment = NSTextAlignmentCenter; // if you want to
                [self.totalCount setTitle:[NSString stringWithFormat:@"%@ \nCompleted Jobs",[[jsonResults objectForKey:@"totalCount"] stringValue]] forState: UIControlStateNormal];
           
        }
        
        break;
        
        DEFAULT
        {
            break;
        }
    }
    
}
// WEB SERVER ERROR MSG LOCAL SERVER (APPLICATION SERVER)
-(void) webServiceFailWithApplicationServerMSG:(NSString *)msg
{
    
    [Alert svError:msg];
    
}

// WEB SERVICE FAIL WITH REGION TCP/IP
-(void) webServiceFail:(NSError *)error{
    
    
    [Alert svError:[error localizedDescription]];
    
    
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return resultArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    static  NSString *identifierDriver = @"ConfirmedTableViewCell";
    
    ConfirmedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifierDriver forIndexPath:indexPath];
    cell.userThumb.layer.cornerRadius = 30;
    cell.userThumb.clipsToBounds=YES;
    
    if ([[[resultArray valueForKey:@"image"]objectAtIndex:indexPath.row] isEqualToString:@""])
          {
          
              NSString *userName = [[resultArray valueForKey:@"fullName"]objectAtIndex:indexPath.row];
              [cell.userThumb setImageWithString:userName color:nil circular:YES];
             
          }
          else
          {
              
          [cell.userThumb setImageURL:[NSURL URLWithString:[[resultArray valueForKey:@"image"]objectAtIndex:indexPath.row]]];
          }
   
    
    cell.pendingLabel.text =[[resultArray valueForKey:@"bookingDate"]objectAtIndex:indexPath.row];
    
    cell.bgImage.layer.cornerRadius = 10;
    cell.cratedLabel.text= [[resultArray valueForKey:@"slote"]objectAtIndex:indexPath.row];
    
    cell.locationLabel.numberOfLines=2;
    cell.nameLabel.text= [[resultArray valueForKey:@"fullName"]objectAtIndex:indexPath.row];
    
    cell.locationLabel.text= [[resultArray valueForKey:@"address"]objectAtIndex:indexPath.row];
    
    cell.priceLabel.text = [[resultArray valueForKey:@"serviceName"]objectAtIndex:indexPath.row];
    
    
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell: (UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if (indexPath.row == [resultArray count] - 1 )
    {
        if (arr.count==0)
        {
            
        }
        else{
            
            startidx += 1;
            [self serviceConfirmedList];
            
        }
        
        
    }
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    ChoreDetailViewController * settingsViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"ChoreDetailViewController");
    
    settingsViewController.dict = [resultArray objectAtIndex:indexPath.row];
    
    UINavigationController * navigationController = [Alert navigationControllerWithVC:settingsViewController];
    PERSENT_VIEW_CONTOLLER(navigationController, YES);
    
}
- (IBAction)buttonSave:(id)sender
{
    CashoutViewController * bookingHistoryViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDUplaodLicenseDriverVC);
      
    UINavigationController * navigationController = [Alert navigationControllerWithVC:bookingHistoryViewController];
          PERSENT_VIEW_CONTOLLER(navigationController, YES);
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
