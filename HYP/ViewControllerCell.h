//
//  ViewControllerCell.h
//  ExpandableTableView
//
//  Created by milan on 05/05/16.
//  Copyright © 2016 apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewControllerCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblName;

@property (nonatomic, readwrite) BOOL selected;
@property (strong, nonatomic) IBOutlet AsyncImageView *imageview;
@property (strong, nonatomic) IBOutlet UIButton *btnShowHide;
@property (strong, nonatomic) IBOutlet AsyncImageView *bgimageview;
@end
