//
//  AffiliateViewController.m
//  HYP
//
//  Created by santosh kumar singh on 05/09/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import "AffiliateViewController.h"
#import "TermsViewController.h"


@interface AffiliateViewController ()<UIPickerViewDelegate,UIPickerViewDataSource,WebServiceDelegate>

@end

@implementation AffiliateViewController

- (void)viewDidLoad {
    
    self.title = @"AFFILIATE STATUS";
    
      self.firstNameTextField.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 30);
         self.lastNameTextField.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 30);
         self.addressTextField.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 30);
         self.professionTextField.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 30);
         self.phoneTextField.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 30);
         self.sendGiftTextField.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 30);
    
    CLLocation * upadetLocation = [[CLLocation alloc] initWithLatitude:[[AppDelegate appDelegate].latitude floatValue] longitude:[[AppDelegate appDelegate].longitude floatValue]];
             
    [self reverGeoCodingUsingGoogle:upadetLocation];
       
      

       self.firstNameTextField.layer.borderWidth = 0.5f;
       self.firstNameTextField.layer.borderColor = [UIColor blackColor].CGColor;
        
         self.lastNameTextField.layer.borderWidth = 0.5f;
         self.lastNameTextField.layer.borderColor = [UIColor blackColor].CGColor;
         
         self.addressTextField.layer.borderWidth = 0.5f;
         self.addressTextField.layer.borderColor = [UIColor blackColor].CGColor;
         
         self.professionTextField.layer.borderWidth = 0.5f;
         self.professionTextField.layer.borderColor = [UIColor blackColor].CGColor;
         
         self.phoneTextField.layer.borderWidth = 0.5f;
         self.phoneTextField.layer.borderColor = [UIColor blackColor].CGColor;
         
         self.sendGiftTextField.layer.borderWidth = 0.5f;
         self.sendGiftTextField.layer.borderColor = [UIColor blackColor].CGColor;
      
       
         self.firstNameTextField.layer.cornerRadius=8;
         self.lastNameTextField.layer.cornerRadius=8;
         self.addressTextField.layer.cornerRadius=8;
         self.professionTextField.layer.cornerRadius=8;
         self.phoneTextField.layer.cornerRadius=8;
         self.sendGiftTextField.layer.cornerRadius=8;
       
         self.btnEditProfile.layer.cornerRadius=8;
         
      
         self.firstNameTextField.clipsToBounds = YES;
         self.lastNameTextField.clipsToBounds = YES;
         self.addressTextField.clipsToBounds = YES;
         self.professionTextField.clipsToBounds = YES;
         self.phoneTextField.clipsToBounds = YES;
         self.sendGiftTextField.clipsToBounds = YES;
        
      
         [Alert setKeyBoardToolbar:@[self.firstNameTextField,self.lastNameTextField,self.addressTextField,self.phoneTextField]];
         
    [self navigationBarConfiguration];
    self.view.backgroundColor = [Alert colorWithPatternImage2:kBG_IMAGE];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)reverGeoCodingUsingGoogle:(CLLocation *)location
{
     [[GMSGeocoder geocoder] reverseGeocodeCoordinate:CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude) completionHandler:^(GMSReverseGeocodeResponse* response, NSError* error)
     {
        
        GMSReverseGeocodeResult *result = response.firstResult;
        
        NSArray   * arrayAdd = result.lines;
        
        NSString  * address1;
        NSString  * address2;
        
        if (arrayAdd.count>1)
        {
            address1 = result.lines[0];
            address2 = result.subLocality;
        }
        else
        {
            address1 = result.lines[0];
        }
        
        self.addressTextField.text = address1;
        
        
    }];
    
    
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
   
    if (self.addressTextField.editing == YES)
    {
        
        [self.addressTextField resignFirstResponder];
        
        GMSAutocompleteViewController *acController = [[GMSAutocompleteViewController alloc] init];
             if (@available(iOS 13.0, *)) {
                 if(UIScreen.mainScreen.traitCollection.userInterfaceStyle == UIUserInterfaceStyleDark ){
                     acController.primaryTextColor = UIColor.whiteColor;
                     acController.secondaryTextColor = UIColor.lightGrayColor;
                     acController.tableCellSeparatorColor = UIColor.lightGrayColor;
                     acController.tableCellBackgroundColor = UIColor.darkGrayColor;
                 } else {
                     acController.primaryTextColor = UIColor.blackColor;
                     acController.secondaryTextColor = UIColor.lightGrayColor;
                     acController.tableCellSeparatorColor = UIColor.lightGrayColor;
                     acController.tableCellBackgroundColor = UIColor.whiteColor;
                 }
             }
             acController.delegate = self;
             [[UISearchBar appearance] setBarStyle:UIBarStyleDefault];
             [[UITextField appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]] setTextColor:[UIColor whiteColor]];
             [self presentViewController:acController animated:YES completion:nil];
            
        
//        STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SearchLocationViewController"]];
//        popupController.containerView.layer.cornerRadius = 4;
//        [popupController presentInViewController:self];
    }
}

// Handle the user's selection.
- (void)viewController:(GMSAutocompleteViewController *)viewController
didAutocompleteWithPlace:(GMSPlace *)place {

    [self dismissViewControllerAnimated:YES completion:nil];
    // Do something with the selected place.
    NSLog(@"Place name %@", place.name);
    NSLog(@"Place address %@", place.formattedAddress);
    NSLog(@"Place latitude %f", place.coordinate.latitude);
    NSLog(@"Place longitude %f", place.coordinate.longitude);
   
    [AppDelegate appDelegate].addressString = place.formattedAddress;
    
    [AppDelegate appDelegate].latitude= [NSString stringWithFormat:@"%f",place.coordinate.latitude];
    [AppDelegate appDelegate].longitude= [NSString stringWithFormat:@"%f",place.coordinate.longitude];

    [self.addressTextField setText:[NSString stringWithFormat:@"%@", place.name]];

}


- (void)viewController:(GMSAutocompleteViewController *)viewController
didFailAutocompleteWithError:(NSError *)error {
    // TODO: handle the error.
    NSLog(@"error: %ld", [error code]);
    [self dismissViewControllerAnimated:YES completion:nil];
   
}

// User canceled the operation.
- (void)wasCancelled:(GMSAutocompleteViewController *)viewController {
    NSLog(@"Autocomplete was cancelled.");
    
   // _setLocation = nil;
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void)navigationBarConfiguration
{
    
    self.navigationController.navigationBarHidden = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    UIButton * menuButton  = [UIButton buttonWithType:UIButtonTypeSystem];
    menuButton.frame = CGRectMake(8, 18, 24, 26);
    [menuButton setImage:[UIImage imageNamed:IMG_BACK] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *accountBarItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    self.navigationItem.leftBarButtonItem = accountBarItem;
   
    NSMutableArray *arrRightBarItems = [[NSMutableArray alloc] init];
    UIButton *btnSetting = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnSetting setImage:[UIImage imageNamed:@"Frequency"] forState:UIControlStateNormal];
    btnSetting.frame = CGRectMake(0, 0, 40, 40);
    btnSetting.showsTouchWhenHighlighted=YES;
    [btnSetting addTarget:self action:@selector(onSettings:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSetting];
    
    UIButton *btnLib = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnLib setImage:[UIImage imageNamed:@"logo1.png"] forState:UIControlStateNormal];
    btnLib.frame = CGRectMake(0, 0, 40, 40);
    btnLib.showsTouchWhenHighlighted=YES;
     [btnLib addTarget:self action:@selector(onMyLibrary:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButtonItem2 = [[UIBarButtonItem alloc] initWithCustomView:btnLib];
    [arrRightBarItems addObject:barButtonItem2];
    [arrRightBarItems addObject:barButtonItem];
    self.navigationItem.rightBarButtonItems=arrRightBarItems;
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor],
    }];
    
}



-(void)back:(id)sender
{
    DISMISS_VIEW_CONTROLLER;
}
-(void)onMyLibrary:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}

-(void)onSettings:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/hyptunesradio";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}


-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    
    if (valrible==1)
    {
        self.professionTextField.text = [[AppDelegate appDelegate].professionArr[row]valueForKey:@"name"];
        self.professionId =[[AppDelegate appDelegate].professionArr[row]valueForKey:@"id"];
        
    }
    if (valrible==0)
    {
        self.sendGiftTextField.text = [AppDelegate appDelegate].sendArr[row];
        
    }
    
    [self.dienstPicker setHidden:YES];
    [self.donePicker setHidden:YES];
}


#pragma mark - UIPickerViewDataSource
-(IBAction)done:(id)sender
{
    [self.dienstPicker setHidden:YES];
    [self.donePicker setHidden:YES];
}


#pragma mark - UIPickerViewDataSource


-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// #4
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (valrible==1)
    {
        return  [AppDelegate appDelegate].professionArr.count;
    }
    else if (valrible==0)
    {
        return  [AppDelegate appDelegate].sendArr.count;
    }
    else
    {
        return 0;
    }
    
    
    
}

#pragma mark - UIPickerViewDelegate

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    
    
    if (valrible==1)
    {
        return  [[AppDelegate appDelegate].professionArr[row]valueForKey:@"name"];
    }
    else  if (valrible==0)
    {
        return  [AppDelegate appDelegate].sendArr[row];
    }
    
    else
    {
        return 0;
    }
    
    
}

- (IBAction)showPickerView:(UIButton*)sender
{
    
    if (sender.tag==0)
    {
        valrible = 1;
    }
    if (sender.tag==1)
    {
        valrible = 0;
    }
    [self.dienstPicker setHidden:NO];
    [self.donePicker setHidden:NO];
    self.dienstPicker.delegate = self;     //#2
    self.dienstPicker.dataSource = self;
    
    [self.dienstPicker reloadAllComponents];
    
}

- (IBAction)buttonSubmitPasswd:(id)sender
{
     if(self.firstNameTextField.text.length>0 &&
       self.lastNameTextField.text.length>0 &&
       self.addressTextField.text.length>0 &&
       self.phoneTextField.text.length>0 &&
       self.sendGiftTextField.text.length>0 &&
       self.professionTextField.text.length>0)
     {
     
         [self updateState];
      
     }
     else
     {
      
       [Alert alertControllerTitle:kAPPICATION_TITLE msg:kEmptyFields ok:kOK controller:self.navigationController];
     }
}

- (void)updateState
{
    [Alert svProgress:@"Please Wait..."];
    
 NSDictionary * dictLogin  = [[NSUserDefaults standardUserDefaults]  objectForKey:kLOGIN];
    
    NSString * user_id  = [dictLogin valueForKey:@"userId"];
        
    
    WebService * connection = [[WebService alloc]init];
    connection.delegate = self;
    NSMutableDictionary * dict                              = [NSMutableDictionary dictionary];
    
    [dict setObject:@"becomeaffiliate"                     forKey:kAPI_ACTION];
    [dict setObject:user_id                     forKey:kUSER_ID];
    [dict setObject:@"support@hyp.xyz"                    forKey:@"email"];
    [dict setObject:[NSString stringWithFormat:@"%@, %@",self.firstNameTextField.text,self.lastNameTextField.text]                   forKey:@"FullName"];
    [dict setObject:self.phoneTextField.text               forKey:@"PhoneNumber"];
    [dict setObject:self.addressTextField.text                    forKey:@"Address"];
    [dict setObject:self.sendGiftTextField.text                forKey:@"PaymentMethod"];
    [dict setObject:self.professionId                forKey:@"profeesion"];
         
    NSLog(@"dict is ---%@",dict);
    [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:@"becomeaffiliate"];
    
    
}

#pragma mark - <WebServiveDelegate>
-(void)getDataFormWebService:(NSDictionary *)jsonResults urlStr:(NSString *)yourUR methodName:(NSString *)methodName{

   SWITCH (methodName) {
        CASE (@"becomeaffiliate")
        {
            
        NSLog(@"jsonResults =%@",jsonResults);
        [Alert svSuccess:[jsonResults objectForKey:@"msg"]];
      
        TermsViewController *forgotPasswordViewController =  GET_VIEW_CONTROLLER(@"TermsViewController");
                               
        forgotPasswordViewController.matchFrom=@"Techinicians";
        UINavigationController * navigationController = [Alert navigationControllerWithVC:forgotPasswordViewController];
                               
        PERSENT_VIEW_CONTOLLER(navigationController, YES);
                          
        }
        break;
        
        DEFAULT
        {
            break;
        }
    }
    
}

// WEB SERVER ERROR MSG LOCAL SERVER (APPLICATION SERVER)
-(void) webServiceFailWithApplicationServerMSG:(NSString *)msg
{
    
    [Alert svError:msg];
    
}

// WEB SERVICE FAIL WITH REGION TCP/IP
-(void) webServiceFail:(NSError *)error{
    
    
    [Alert svError:[error localizedDescription]];
    
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
