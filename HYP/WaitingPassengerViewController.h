//
//  WaitingPassengerViewController.h
//  ROLUX
//
//  Created by Nitin Kumar on 09/01/17.
//  Copyright © 2017 Nitin Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WaitingPassengerViewController : UIViewController
{
        float latitude , latSearch;
        float longitude, lngSearch;
        CLLocationCoordinate2D coordinate;
        CLLocationCoordinate2D startPoint;
        GMSCameraPosition *camera;
        GMSMapView *mapView_;
        GMSMarker *locationMarker_;
         int childerCount;
  

}

@property (nonatomic,strong) NSDictionary * dictionaryNotification;
@property (nonatomic,strong) NSDictionary * firebaseDictinary;

@property (nonatomic, strong) CLLocationManager *locationManager;
@property(nonatomic,weak) IBOutlet UIView * mapView;
@property(nonatomic,weak) IBOutlet UIView * viewBottum;
//@property(nonatomic,weak) IBOutlet UIButton * labelBookingID;
@property(nonatomic,weak) IBOutlet UILabel * labelPhnNum;
@property(nonatomic,weak) IBOutlet UILabel * labelEmailID;
@property(nonatomic,weak) IBOutlet UIButton * buttonCall;
@property(nonatomic,weak) IBOutlet UIButton * buttonCencel;
@property (strong, nonatomic) NSMutableArray *waypoints;
@property (strong, nonatomic) NSMutableArray *waypointStrings;
@property (strong, nonatomic) NSMutableArray *landmarksOnRoute;
@property(nonatomic,weak) IBOutlet UILabel * labelTimeDuration;
@property(nonatomic,weak) IBOutlet UILabel * labelDistance;

@property(nonatomic,weak) IBOutlet UIImageView * imageViewDriver;

@property(nonatomic,weak) IBOutlet UIView * viewDriverInfo;
@property(nonatomic,weak) IBOutlet UILabel * labelDriverName;
@property(nonatomic,weak) IBOutlet UILabel * labelDriverPhoneNumber;
@property(nonatomic,weak) IBOutlet UILabel * labelDriverEmailAddress;
@property(nonatomic,weak) IBOutlet UILabel * labelDriverCarNumber;
@property(nonatomic,weak) IBOutlet UILabel * labelRating;

@property(nonatomic,weak) IBOutlet UIButton * buttonDriverInfoCencel;
@property(nonatomic,weak) IBOutlet UIButton * buttonDriverInfo;

@property (strong, nonatomic)IBOutlet UIView * userDetailView;

@property (strong, nonatomic)IBOutlet UIView * detailView;
@property (strong, nonatomic)IBOutlet UIView * messageView;
@property(nonatomic,weak) IBOutlet UIView * viewLocation;
@property (weak, nonatomic) IBOutlet UILabel *picLoc;

@property(nonatomic,weak) IBOutlet UITextField * messageText;
@property (weak, nonatomic) IBOutlet UILabel *dropoffLoc;

@property(nonatomic,strong) IBOutlet RateView   *starRating;
-(IBAction)buttonDriverInfo:(id)sender;


@property(nonatomic,weak) IBOutlet UIButton * labelBookingID;

-(IBAction)buttonDriverInfoCencelClick:(id)sender;
-(IBAction)buttonCallClick:(id)sender;
-(IBAction)buttonCencelClick:(id)sender;
@end
