//
//  SeheduleChoreViewController.m
//  CHORETHING
//
//  Created by santosh kumar singh on 06/04/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import "SeheduleChoreViewController.h"
#import "RegisterSuccessViewController.h"
#import <FSCalendar.h>

@interface SeheduleChoreViewController ()<WebServiceDelegate,MDTimePickerDialogDelegate,FSCalendarDataSource,FSCalendarDelegate,FSCalendarDelegateAppearance>
{
    // WSCalendarView *calendarViewEvent;
}

@property (strong , nonatomic) FSCalendar *calendar;
@property(nonatomic) NSDateFormatter *dateFormatter;
@end

@implementation SeheduleChoreViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.title = @"SELECT DATE & TIME";
    
    self.imageview.layer.cornerRadius = self.imageview.frame.size.width/2;
    self.imageview.clipsToBounds = YES;
    
    
    self.nameLable.text = [NSString stringWithFormat:@"Selected Service: %@",[_dict objectForKey:@"name"]];
    
    
    
    self.txtHours.layer.borderWidth = 0.5f;
    self.txtHours.layer.borderColor = [UIColor darkGrayColor].CGColor;
    
    
    self.txtHours.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 30);
    
    self.txtHours.layer.cornerRadius = 5.0f;
    self.txtHours.clipsToBounds = YES;
    
    NSDateFormatter *monthFormatter=[[NSDateFormatter alloc] init];
    [monthFormatter setDateFormat:@"dd MMMM yyyy"];
    
    NSString *str=[monthFormatter stringFromDate:[NSDate date]];
    
    self.dateLabel.text =[NSString stringWithFormat:@"Selected Service Date: %@",str];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    dateString = [dateFormat stringFromDate:[NSDate date]];
    
    NSDate * now = [NSDate date];
   
   NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
   [dateFormatter2 setDateFormat:@"hh:mm a"];
   NSLog(@"%@", [dateFormatter2 stringFromDate:now]);
   self.txtHours.text = [dateFormatter2 stringFromDate:now];
   
   
    [self navigationBarConfiguration];
    // Do any additional setup after loading the view.
}

- (void)calendar:(FSCalendar *)calendar didSelectDate:(NSDate *)date atMonthPosition:(FSCalendarMonthPosition)monthPosition
{
    //  NSLog(@"did select %@",[self.dateFormatter stringFromDate:date]);
    
    NSDateFormatter *monthFormatter=[[NSDateFormatter alloc] init];
    [monthFormatter setDateFormat:@"dd MMMM yyyy"];
    NSString *str=[monthFormatter stringFromDate:date];
    self.dateLabel.text =[NSString stringWithFormat:@"Selected Service Date: %@",str];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    dateString = [dateFormat stringFromDate:date];
}



-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (self.txtHours.editing == YES)
    {
        [self.txtHours resignFirstResponder];
        
        MDTimePickerDialog *timePicker = [[MDTimePickerDialog alloc] init];
        timePicker.theme = MDTimePickerThemeLight;
        timePicker.delegate = self;
        [timePicker show];
        
    }
}


- (void)timePickerDialog:(MDTimePickerDialog *)timePickerDialog
           didSelectHour:(NSInteger)hour
               andMinute:(NSInteger)minute
{
    
    
    self.txtHours.text = [NSString stringWithFormat:@"%.2li:%.2li.00", (long)hour, (long)minute];
    
    NSString *stringDate = [NSString stringWithFormat:@"%.2li:%.2li.00", (long)hour, (long)minute];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    NSDate *date1 = [dateFormatter dateFromString:stringDate];
    NSLog(@"%@", date1);
    
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
    [dateFormatter2 setDateFormat:@"hh:mm a"];
    NSLog(@"%@", [dateFormatter2 stringFromDate:date1]);
    self.txtHours.text = [dateFormatter2 stringFromDate:date1];
    
    
}


-(void)back:(id)sender
{
    DISMISS_VIEW_CONTROLLER;
}

#pragma mark - Configure Navigation Controller

-(void)navigationBarConfiguration
{
    
    self.navigationController.navigationBarHidden = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    UIButton * menuButton  = [UIButton buttonWithType:UIButtonTypeSystem];
    menuButton.frame = CGRectMake(8, 18, 24, 24);
    [menuButton setBackgroundImage:[UIImage imageNamed:IMG_BACK] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *accountBarItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    self.navigationItem.leftBarButtonItem = accountBarItem;
    
    
    NSMutableArray *arrRightBarItems = [[NSMutableArray alloc] init];
    UIButton *btnSetting = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnSetting setImage:[UIImage imageNamed:@"Frequency"] forState:UIControlStateNormal];
    btnSetting.frame = CGRectMake(0, 0, 40, 40);
    btnSetting.showsTouchWhenHighlighted=YES;
    [btnSetting addTarget:self action:@selector(onSettings:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSetting];
    
    UIButton *btnLib = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnLib setImage:[UIImage imageNamed:@"logo1.png"] forState:UIControlStateNormal];
    btnLib.frame = CGRectMake(0, 0, 40, 40);
    btnLib.showsTouchWhenHighlighted=YES;
    [btnLib addTarget:self action:@selector(onMyLibrary:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButtonItem2 = [[UIBarButtonItem alloc] initWithCustomView:btnLib];
    [arrRightBarItems addObject:barButtonItem2];
    [arrRightBarItems addObject:barButtonItem];
    self.navigationItem.rightBarButtonItems=arrRightBarItems;
    
    
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor],
    }];
    
}
-(void)onMyLibrary:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}
-(void)onSettings:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
      [[UIApplication sharedApplication] openURL:url];
    }
}

-(IBAction)seheduleNowBtn:(UIButton*)sender
{
    
    if (!dateString)
    {
        [Alert svError:@"Select Chore Date"];
    }
    else if (self.txtHours.text.length<0)
    {
        [Alert svError:@"Select Chore Time"];
    }
    else
    {
        [Alert svProgress:@"Please Wait..."];
        NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults] dictionaryForKey:kLOGIN];
        NSString * user_id ;
        if ([dictLogin valueForKey:@"id"] == nil)
        {
            user_id  = [dictLogin valueForKey:@"userId"];
            
        }
        else
        {
            user_id  = [dictLogin valueForKey:@"id"];
        }
        
        
        
        
        WebService * connection = [[WebService alloc]init];
        connection.delegate = self;
        
        NSMutableDictionary * dict       = [NSMutableDictionary dictionary];
        [dict setObject:@"addbooking"                    forKey:kAPI_ACTION];
        [dict setObject:user_id                    forKey:kUSER_ID];
        [dict setObject:[_dict objectForKey:@"id"]                 forKey:@"categoryId"];
        [dict setObject:self.addCurrentlocaton                    forKey:@"address"];
        [dict setObject:dateString                 forKey:@"bookingDate"];
        [dict setObject:self.txtHours.text                 forKey:@"slote"];
        [dict setObject:self.latitudeString                 forKey:@"latitude"];
        [dict setObject:self.longitudeString                    forKey:@"longitude"];
        [dict setObject:@""                 forKey:@"totalAmount"];
        [dict setObject:@""                    forKey:@"vendorId"];
              
        [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:@"addbooking"];
    }
}

#pragma mark - <WebServiveDelegate>
-(void)getDataFormWebService:(NSDictionary *)jsonResults urlStr:(NSString *)yourUR methodName:(NSString *)methodName{
    
    [Alert svSuccess:@"Request Successfully Submitted"];
    
    
    SWITCH (methodName) {
        CASE (@"addbooking")
        {
            
            self.groupid = [NSString stringWithFormat:@"%@", [jsonResults valueForKey:@"bookingGroupId"]];
            
            RegisterSuccessViewController *forgotPasswordViewController =  GET_VIEW_CONTROLLER(@"RegisterSuccessViewController");
            forgotPasswordViewController.servicename = [_dict objectForKey:@"name"];
            UINavigationController * navigationController = [Alert navigationControllerWithVC:forgotPasswordViewController];
            PERSENT_VIEW_CONTOLLER(navigationController, YES);
            
            
        }
        
        break;
        
        DEFAULT
        {
            break;
        }
    }
    
}

-(void)serviceBusinessType
{
    // SERVER CONNCETION
    
    [Alert svProgress:@"Please Wait..."];
    
    WebService * connection = [[WebService alloc]init];
    connection.delegate = self;
    
    NSMutableDictionary * dict       = [NSMutableDictionary dictionary];
    [dict setObject:@"selectpackage"                    forKey:kAPI_ACTION];
    [dict setObject:[_dict objectForKey:@"id"]                    forKey:@"categoryId"];
    [dict setObject:self.addressType                    forKey:@"vendorType"];
    
    [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:@"selectpackage"];
}

// WEB SERVER ERROR MSG LOCAL SERVER (APPLICATION SERVER)
-(void) webServiceFailWithApplicationServerMSG:(NSString *)msg
{
    
    [Alert svError:msg];
    
}

// WEB SERVICE FAIL WITH REGION TCP/IP
-(void) webServiceFail:(NSError *)error{
    
    
    [Alert svError:[error localizedDescription]];
    
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
