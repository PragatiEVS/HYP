//
//  UserDetailViewController.m
//  Q-Workerbee
//
//  Created by santosh kumar singh on 08/06/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import "UserDetailViewController.h"
#import "EditViewController.h"
#import "MDSwitch.h"
#import "CompleteBookingViewController.h"
#import "RateListViewController.h"


@interface UserDetailViewController ()<WebServiceDelegate>

@end

@implementation UserDetailViewController

- (void)viewDidLoad {
    
    self.title = @"SERVICE PROVIDER PROFILE";
    
    self.rateView.starSize = 20;
    self.rateView.starNormalColor = [UIColor whiteColor];
    
    
    [self navigationBarConfiguration];
    [self SetTextFieldBorder:_textSearch];
    [self SetTextViewBorder:_textDescription];
    [self SetTextFieldBorder:_textEmail];
    // [self SetTextFieldBorder:_dobtextfield];
    [self SetTextFieldBorder:_phoneTextfield];
    [self SetTextViewBorder:_servicesTextfield];
    [self updateState];
    
    
    self.modelImage.layer.borderWidth=10.0f;
    self.modelImage.layer.borderColor=[UIColor whiteColor].CGColor;
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(IBAction)buttonEmailClick:(id)sender
{
        RateListViewController * bookingHistoryViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"RateListViewController");
         bookingHistoryViewController.userId = [dictLoginInfo valueForKey:@"userId"];
         UINavigationController * navigationController    = [Alert navigationControllerWithVC:bookingHistoryViewController];
         PERSENT_VIEW_CONTOLLER(navigationController, YES);
}

-(IBAction)buttonCompleteClick:(id)sender
{
   
     CompleteBookingViewController * bookingHistoryViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"CompleteBookingViewController");
    bookingHistoryViewController.matchFrom = @"userDetail";
       bookingHistoryViewController.dicttionary = dictLoginInfo;
      UINavigationController * navigationController    = [Alert navigationControllerWithVC:bookingHistoryViewController];
      PERSENT_VIEW_CONTOLLER(navigationController, YES);

}

-(void)SetTextViewBorder :(UITextView *)textField
{
    
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 0.5;
    border.borderColor = [UIColor blackColor].CGColor;
    border.frame = CGRectMake(0, textField.frame.size.height - borderWidth, textField.frame.size.width, textField.frame.size.height);
    border.borderWidth = borderWidth;
    [textField.layer addSublayer:border];
    textField.layer.masksToBounds = YES;
    
}
-(void)SetTextFieldBorder :(UITextField *)textField
{
    
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 0.5;
    border.borderColor = [UIColor blackColor].CGColor;
    border.frame = CGRectMake(0, textField.frame.size.height - borderWidth, textField.frame.size.width, textField.frame.size.height);
    border.borderWidth = borderWidth;
    [textField.layer addSublayer:border];
    textField.layer.masksToBounds = YES;
    
}

#pragma mark - Configure Navigation Controller

-(void)navigationBarConfiguration
{
    
    self.navigationController.navigationBarHidden = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    UIButton * menuButton  = [UIButton buttonWithType:UIButtonTypeSystem];
    menuButton.frame = CGRectMake(8, 18, 24, 24);
    [menuButton setImage:[UIImage imageNamed:IMG_BACK] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *accountBarItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    self.navigationItem.leftBarButtonItem = accountBarItem;
 
    NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"ProfileDetail"];
       
       NSString * str = [dictLogin valueForKey:@"role"];
       
       if ([str isEqualToString:@"Member"])
       {
         UIButton * chatButton  = [UIButton buttonWithType:UIButtonTypeSystem];
           chatButton.frame = CGRectMake(8, 18, 24, 24);
           [chatButton setBackgroundImage:[UIImage imageNamed:@"Call"] forState:UIControlStateNormal];
           [chatButton addTarget:self action:@selector(chatBtn:) forControlEvents:UIControlEventTouchUpInside];
           UIBarButtonItem *accountBarItem1 = [[UIBarButtonItem alloc] initWithCustomView:chatButton];
                
           self.navigationItem.rightBarButtonItem = accountBarItem1;
           
       }
       else
       {
          
       }
    NSMutableArray *arrRightBarItems = [[NSMutableArray alloc] init];
     UIButton *btnSetting = [UIButton buttonWithType:UIButtonTypeCustom];
     [btnSetting setImage:[UIImage imageNamed:@"Frequency"] forState:UIControlStateNormal];
     btnSetting.frame = CGRectMake(0, 0, 50, 50);
     btnSetting.showsTouchWhenHighlighted=YES;
     [btnSetting addTarget:self action:@selector(onSettings:) forControlEvents:UIControlEventTouchUpInside];
     UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSetting];
     
     UIButton *btnLib = [UIButton buttonWithType:UIButtonTypeCustom];
     [btnLib setImage:[UIImage imageNamed:@"logo1.png"] forState:UIControlStateNormal];
     btnLib.frame = CGRectMake(0, 0, 50, 40);
     btnLib.showsTouchWhenHighlighted=YES;
     [btnLib addTarget:self action:@selector(onMyLibrary:) forControlEvents:UIControlEventTouchUpInside];
     UIBarButtonItem *barButtonItem2 = [[UIBarButtonItem alloc] initWithCustomView:btnLib];
     [arrRightBarItems addObject:barButtonItem2];
     [arrRightBarItems addObject:barButtonItem];
     self.navigationItem.rightBarButtonItems=arrRightBarItems;
     
  
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName :[UIColor whiteColor]}];
}

-(void)onMyLibrary:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}

-(void)onSettings:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/hyptunesradio";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}

-(void)chatBtn:(id)sender
{
  if (TARGET_OS_SIMULATOR)
     {
         [Alert alertControllerTitle:kAPPICATION_TITLE msg:kDEVICE_NOT_SUPPORT ok:kOK controller:self.navigationController];
     }
     else
     {
         
         [Alert makePhoneCall:[dictLoginInfo objectForKey:@"contactNumber"]];
     }
}

- (void)updateState
{
    [Alert svProgress:@"Please Wait..."];
    
    
    WebService * connection = [[WebService alloc]init];
    connection.delegate = self;
    NSMutableDictionary * dict                              = [NSMutableDictionary dictionary];
    
    [dict setObject:@"profile"                     forKey:kAPI_ACTION];
    [dict setObject:self.vendorId                     forKey:kUSER_ID];
    [dict setObject:[AppDelegate appDelegate].longitude                    forKey:@"logitude"];
    [dict setObject:[AppDelegate appDelegate].latitude                 forKey:@"latitude"];
    
    NSLog(@"dict is ---%@",dict);
    [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:@"profile"];
    
    
}

-(NSString *)neededStringWithString:(NSString *)aString {
    // if the string has less than or 4 characters, return nil
    if([aString length] <= 4) {
        return nil;
    }
    NSUInteger countOfCharToReplace = [aString length] - 4;
    NSString *firstPart = @"*";
    while(--countOfCharToReplace) {
        firstPart = [firstPart stringByAppendingString:@"*"];
    }
    // range for the last four
    NSRange lastFourRange = NSMakeRange([aString length] - 4, 4);
    // return the combined string
    return [firstPart stringByAppendingString:
            [aString substringWithRange:lastFourRange]];
}
//
//-(NSString *)neededStringWithString:(NSString *)aString {
//
//    NSInteger starUpTo = [aString length] - 4;
//    if (starUpTo > 0)
//    {
//        NSString *stars = [@"" stringByPaddingToLength:starUpTo withString:@"*" startingAtIndex:0];
//        return [aString stringByReplacingCharactersInRange:NSMakeRange(0, starUpTo) withString:stars];
//    } else {
//        return aString;
//    }
//
//}

#pragma mark - Sevice delgate methods

-(void)getDataFormWebService:(NSDictionary *)jsonResults urlStr:(NSString *)yourUR methodName:(NSString *)methodName{
    
    [SVProgressHUD dismiss];
    
    SWITCH (methodName) {
        
        CASE (@"profile")
        {
            NSLog(@"jsonResults =%@",jsonResults);
            
            dictLoginInfo = [jsonResults valueForKey:@"data"];
            
            [self.postcount setTitle:[NSString stringWithFormat:@"%@",[dictLoginInfo valueForKey:@"TotalJobCompleted"]] forState:UIControlStateNormal];
           
            [self.reviewbtn setTitle:[NSString stringWithFormat:@"%@",[dictLoginInfo valueForKey:@"reviewCount"]] forState:UIControlStateNormal];
                      
            
            NSArray *arr1 = [dictLoginInfo objectForKey:@"categoryList"];
            if (arr1.count == nil) {
                
                self.dobtextfield.text = @"";
            }
            else
            {
                NSMutableArray * resultarr =[[NSMutableArray alloc]init];
                for ( int i=0; i<arr1.count; i++)
                {
                    
                    [resultarr addObject:[[arr1 objectAtIndex:i] valueForKey:@"name"]];
                    
                }
                
                NSString * result = [resultarr componentsJoinedByString:@","];
                
                
                [self.servicesTextfield setText:result];
        
               
            }
           
            if ([[[dictLoginInfo objectForKey:@"AVGRating"]stringValue] isEqualToString:@""])
            {
                self.rateView.starNormalColor = [UIColor whiteColor];
            }
            else
            {
                self.rateView.rating = [[dictLoginInfo objectForKey:@"AVGRating"] floatValue];
                self.rateView.starFillColor =  [Alert colorFromHexString:@"#F4CC47"];
                
            }
            
            if ([[dictLoginInfo objectForKey:@"image"] isEqualToString:@""])
            {
                
                [self.modelImage setImage:[UIImage imageNamed:@"profile.png"]];
                
            }
            else
            {
                NSData *data= [NSData dataWithContentsOfURL:[NSURL URLWithString:[dictLoginInfo objectForKey:@"image"]]];
                
                [self.modelImage setImage:[UIImage imageWithData:data]];
                [self.modelBgImage setImage:[UIImage imageWithData:data]];
            }
            
            if ([[dictLoginInfo objectForKey:@"fullName"] isEqualToString:@""]) {
                
            }
            else
            {
                
                self.nameModel.text = [dictLoginInfo objectForKey:@"fullName"];
            }
            if ([[dictLoginInfo objectForKey:@"email"] isEqualToString:@""]) {
                
            }
            else
            {
                self.textEmail.text = [dictLoginInfo objectForKey:@"email"];
            }
            if ([[dictLoginInfo objectForKey:@"contactNumber"] isEqualToString:@""]) {
                
            }
            else
            {
                NSMutableString * str1 = [[NSMutableString alloc]initWithString:[dictLoginInfo objectForKey:@"contactNumber"]];
                
//                NSString *code = [str1 substringFromIndex: [str1 length] - 4];
//
//                NSRange r = NSMakeRange([str1 length] - 4, [code length]);
//                [str1 replaceCharactersInRange:r withString:[[NSString string] stringByPaddingToLength:r.length withString:@"*" startingAtIndex:0]];
//                NSLog(@"%@",str1);
                
                self.phoneTextfield.text = str1;
            }
//            if ([[dictLoginInfo objectForKey:@"serviceDescription"] isEqualToString:@""]) {
//
//            }
//            else
//            {
//                self.textDescription.text = [dictLoginInfo objectForKey:@"serviceDescription"];
//            }
            if ([[dictLoginInfo objectForKey:@"address"] isEqualToString:@""]) {
                
            }
            else
            {
                [self.locationLabel setTitle:[NSString stringWithFormat:@"%@",[dictLoginInfo valueForKey:@"address"]] forState:UIControlStateNormal];
                            
                [self.textDescription setText:[dictLoginInfo valueForKey:@"address"]];
               
                
            }
         
                [self.textSearch setText:[NSString stringWithFormat:@"%.2f Miles away",[[dictLoginInfo objectForKey:@"distance"]floatValue]]];
           
            
        }
        
        break;
        DEFAULT
        {
            break;
        }
    }
}

-(void) webServiceFailWithApplicationServerMSG:(NSString *)msg
{
    
    [Alert svError:msg];
    
}

// WEB SERVICE FAIL WITH REGION TCP/IP
-(void) webServiceFail:(NSError *)error{
    
    
    [Alert svError:[error localizedDescription]];
    
    
}
-(IBAction)callBtn:(id)sender
{
  if (TARGET_OS_SIMULATOR)
     {
         [Alert alertControllerTitle:kAPPICATION_TITLE msg:kDEVICE_NOT_SUPPORT ok:kOK controller:self.navigationController];
     }
     else
     {
         
          [Alert makePhoneCall:CALL_US];
     }
}


-(void)back:(id)sender
{
    DISMISS_VIEW_CONTROLLER;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
