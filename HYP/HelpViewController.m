//
//  HelpViewController.m
//  Boccia
//
//  Created by IOSdev on 12/31/19.
//  Copyright © 2019 Pragati Porwal. All rights reserved.
//

#import "HelpViewController.h"
#import <MessageUI/MessageUI.h>
@interface HelpViewController ()<MFMailComposeViewControllerDelegate>

@end

@implementation HelpViewController

- (void)viewDidLoad {
    
    self.view.backgroundColor = [Alert colorWithPatternImage2:kBG_IMAGE];
    
    [self navigationBarConfiguration];
    self.title = @"SUPPORT";
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


#pragma mark - Configure Navigation Controller

-(void)navigationBarConfiguration
{
    
    self.navigationController.navigationBarHidden = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    UIButton * menuButton  = [UIButton buttonWithType:UIButtonTypeSystem];
    menuButton.frame = CGRectMake(8, 18, 24, 24);
    [menuButton setBackgroundImage:[UIImage imageNamed:NAV_SLIDER_IMAGE] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(drawer:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *accountBarItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    self.navigationItem.leftBarButtonItem = accountBarItem;
   
    NSMutableArray *arrRightBarItems = [[NSMutableArray alloc] init];
         UIButton *btnSetting = [UIButton buttonWithType:UIButtonTypeCustom];
         [btnSetting setImage:[UIImage imageNamed:@"Frequency"] forState:UIControlStateNormal];
         btnSetting.frame = CGRectMake(0, 0, 40, 40);
         btnSetting.showsTouchWhenHighlighted=YES;
       [btnSetting addTarget:self action:@selector(onSettings:) forControlEvents:UIControlEventTouchUpInside];
         UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSetting];
       
         UIButton *btnLib = [UIButton buttonWithType:UIButtonTypeCustom];
         [btnLib setImage:[UIImage imageNamed:@"logo1.png"] forState:UIControlStateNormal];
         btnLib.frame = CGRectMake(0, 0, 40, 40);
         btnLib.showsTouchWhenHighlighted=YES;
       [btnLib addTarget:self action:@selector(onMyLibrary:) forControlEvents:UIControlEventTouchUpInside];
         UIBarButtonItem *barButtonItem2 = [[UIBarButtonItem alloc] initWithCustomView:btnLib];
         [arrRightBarItems addObject:barButtonItem2];
         [arrRightBarItems addObject:barButtonItem];
         self.navigationItem.rightBarButtonItems=arrRightBarItems;
        
      
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName :[UIColor whiteColor]}];
}
-(void)onMyLibrary:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}

-(void)onSettings:(id)sender
{
  
    NSString*myurl=@"https://www.hypdemand.com/hyptunesradio";
     NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
       [[UIApplication sharedApplication] openURL:url];
    }
}

#pragma mark - Drawer class
-(void)drawer:(id)sender
{
    
    KYDrawerController *elDrawer = (KYDrawerController*)self.navigationController.parentViewController;
    [elDrawer setDrawerState:KYDrawerControllerDrawerStateOpened animated:YES];
    
    
}


-(IBAction)buttonEmailClick:(id)sender
{
    
    
    if (TARGET_OS_SIMULATOR) {
        
        [Alert alertControllerTitle:kAPPICATION_TITLE msg:kDEVICE_NOT_SUPPORT ok:kOK controller:self.navigationController];
    }
    else
    {
        MFMailComposeViewController *comp=[[MFMailComposeViewController alloc]init];
        [comp setMailComposeDelegate:self];
        if([MFMailComposeViewController canSendMail])
        {
            
            [comp setToRecipients:[NSArray arrayWithObjects:@"support@hyp.com", nil]];
            [comp setSubject:@""];
            [comp setMessageBody:@"" isHTML:NO];
            [comp setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
            [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentModalViewController:comp animated:YES];
           
        }
        else
        {
       
          [Alert alertControllerTitle:@"HELP" msg:kDEVICE_NOT_SUPPORT ok:kOK controller:self.navigationController];
       
        }
    }
    
    
}

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result
                       error:(NSError *)error
{
    
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    switch (result) {
        case MFMailComposeResultCancelled:
        {
            [Alert alertControllerTitle:@"HELP" msg:@"Mail Cancelled" ok:kOK controller:self.navigationController];
        }
            break;
        case MFMailComposeResultSaved:
        {
            [Alert alertControllerTitle:@"HELP" msg:@"Mail Saved" ok:kOK controller:self.navigationController];
        }
            break;
        case MFMailComposeResultSent:
        {
            [Alert alertControllerTitle:@"HELP" msg:@"Mail Sent Successfully!" ok:kOK controller:self.navigationController];
        }
            break;
        case MFMailComposeResultFailed:
        {
            [Alert alertControllerTitle:@"HELP" msg:@"Unable to Send Email?" ok:kOK controller:self.navigationController];
        }
            break;
            
        default:
            break;
    }
    
    
}
-(IBAction)buttonCallClick:(id)sender
{
    
    if (TARGET_OS_SIMULATOR)
    {
        [Alert alertControllerTitle:kAPPICATION_TITLE msg:kDEVICE_NOT_SUPPORT ok:kOK controller:self.navigationController];
    }
    else
    {
        
        [Alert makePhoneCall:CALL_US];
    }
    
}
-(IBAction)buttonFeeClick:(id)sender
{
  
 NSString*myurl=@"http://app.chorething.xyz/partnerfee";
 NSURL *url = [NSURL URLWithString:myurl];
 [[UIApplication sharedApplication] openURL:url];
    
}

-(IBAction)buttonwebsiteClick:(id)sender
{
  
 NSString*myurl=@"https://chorething.xyz/";
 NSURL *url = [NSURL URLWithString:myurl];
 [[UIApplication sharedApplication] openURL:url];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
