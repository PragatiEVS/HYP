//
//  AppConstants.h
//  ROLUX
//
//  Created by Nitin Kumar on 06/12/16.
//  Copyright © 2016 Nitin Kumar. All rights reserved.
//

#ifndef AppConstants_h
#define AppConstants_h

// TIMER

#define TIMER_INERVAL  60.0;

static NSString *const  kTIMER_KEY   = @"loc_timer";
#define MAIL_US  @"rolux@rolux.com"
#define CALL_US  @"7274724673"

#define CASE(str)                       if ([__s__ isEqualToString:(str)])
#define SWITCH(s)                       for (NSString *__s__ = (s); ; )
#define DEFAULT


static NSString *const kNOTIFICATION_KEY_DRIVER  = @"driver_notification";
static NSString *const kNOTIFICATION_KEY_USER    = @"user_notification";
static NSString *const kNOTIFICATION_RIDEEND    = @"ariveEnd";

static NSString *const  kDEVICE_SIMULATOR_TOKEN =  @"169a11a09c38188e36e2f7197a2949be7477d0a5e1c68741708a2e2dfe686d86";

#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

//static NSString *const kKEY_GOOGLE_MAP  = @"AIzaSyCkuzAfxMAumuzw1b4n2CRxebEHTeWofpI";

static NSString *const kKEY_GOOGLE_MAP  = @"AIzaSyDEYP9nenPV-zJjlCSbPuyf9eYSOfBdKlk";

// Version 8 Configre........
#define IS_iOS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue]>= 8.0)

// Hight and width of Screen
#define kSCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define kSCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)

#pragma mark - Cusatom Method of ViewController

//************** UIColor & COLOR CODES *************************
 
#define  kCOLOR_WHITE       [UIColor whiteColor]
#define  kCOLOR_BLACK       [UIColor blackColor]
#define  kCOLOR_CLEAR       [UIColor clearColor]
#define  kCOLOR_DARKGRAY    [UIColor darkGrayColor]
#define  kCOLOR_GRAY        [UIColor grayColor]
#define  kCOLOR_RED         [UIColor redColor]
#define  kCOLOR_GREEN       [UIColor greenColor]
#define  kCOLOR_BLUE        [UIColor blueColor]
#define  kCOLOR_CYAN        [UIColor cyanColor]
#define  kCOLOR_YELLOW      [UIColor yellowColor]
#define  kCOLOR_MANGENTA    [UIColor magentaColor]
#define  kCOLOR_ORANGE      [UIColor orangeColor]
#define  kCOLOR_PURPLE      [UIColor purpleColor]
#define  kCOLOR_BROWN       [UIColor brownColor]




static NSString *const  kCOLOR_NAV                                  = @"#2F46C8";
static NSString *const  kCOLOR_TINT_NAV                             = @"#FFFFFF";
static NSString *const  kCOLOR_STROKE                               = @"#545567";
static NSString *const  kCOLOR_regi                               = @"#F6D601";

static NSString *const  kCOLOR_select                               = @"#FF007D";
static NSString *const  kCOLOR_unselect                               = @"#FFFFFF";
static NSString *const  kCOLOR_SendTD                               = @"#35DEFF";



#define kBG_IMAGE       [UIImage imageNamed:@"backgroud"]

#define kMENUBG_IMAGE   [UIImage imageNamed:@"menu_bg@3x.jpg"]

#define IS_IPAD_iPad_SCREEN [[UIScreen mainScreen] bounds].size.height == 1024.0f
#define IS_IPHONE_6Plus_SCREEN [[UIScreen mainScreen] bounds].size.height == 736.0f
#define IS_IPHONE_6_SCREEN [[UIScreen mainScreen] bounds].size.height == 667.0f
#define IS_IPHONE_5_SCREEN [[UIScreen mainScreen] bounds].size.height == 568.0f
#define IS_IPHONE_4_SCREEN [[UIScreen mainScreen] bounds].size.height >= 480.0f && [[UIScreen mainScreen] bounds].size.height < 568.0f



static NSString *const  kCOLOR_TXT_PLACEHODER                       = @"#F7F9F8";
static NSString *const  kCOLOR_VIEW_BORDER                          = @"#C0BEBE";
static NSString *const  kCOLOR_CELLSEPRATOR_BORDER                  = @"#464557";

static NSString *const  kCOLOR_category                          = @"#9D9D9D";
static NSString *const  kCOLOR_category_select                  = @"#00D142";



static NSString *const  kCOLOR_VIEW_ALPHA                           = @"#26273D";
static NSString *const kCOLOR_MESSAGE                               = @"#938E8E";
static NSString *const kCOLOR_BUTTON                                = @"#DAA732";
static NSString *const kCOLOR_MENU_TXT                              = @"#A3A1B9";

//****************************** FONTS

static NSString *const  kFONT                                     = @"D-DIN";

static NSString *const  kFONTLight                    = @"Montserrat-Light";
static NSString *const  KTERMS_CONDITIONS                           = @"terms";




// Image Constants 
static NSString *const kAPI_USER_DEVICE_NAME                = @"ios";
static NSString *const kAPI_DEVICE_TOKEN                    = @"deviceToken";
static NSString *const  kIMG_USER              = @"User";
static NSString *const  kIMG_PASSWD             = @"Password";
static NSString *const  kIMG_EMAIL              = @"Email";
static NSString *const  kIMG_PHONE             = @"Phone";
static NSString *const  kIMG_DELEGATETYPE            = @"Calendar";
static NSString *const  kIMG_DOWN_ARROW         = @"downArrow.png";
static NSString *const  kIMG_SUCCESS            = @"tick.png";
static NSString *const  kIMG_WAITING            = @"gender";
static NSString *const  NAV_SLIDER_IMAGE        = @"navi";
static NSString *const  IMG_BACK                = @"back";
static NSString *const  NAV_PLUS      = @"plus.png";
static NSString *const  NAV_SEARCH              = @"search.png";
static NSString *const  kIMG_STER_SELECT        = @"star.png";
static NSString *const  kIMG_USERSELECT         = @"registerBlank.png";
static NSString *const  kIMG_USERUNSELECT       = @"registerFill.png";

static NSString *const  kIMG_SELECT      = @"checkbox.png";
static NSString *const  kIMG_UNSELECT    = @"unchecked-checkbox.png";
static NSString *const  kIMG_CAR_MARKER         = @"car1.png";

static NSString *const  kIMG_DOT_CIRCLE_MARKER         = @"StartMark";

static NSString *const  kIMG_USER_DESTI_MARKER         = @"car1.png";

static NSString *const  kIMG_DEST_MARKER        = @"custom_pin.png";
static NSString *const  kIMG_DIRECTION          = @"get-direction.png";
static NSString *const  kIMG_HOME               = @"home.png";

static NSString *const  kIMG_CHECK              = @"check_black";
static NSString *const  kIMG_UNCHECK            = @"checkbox_gray";


//************** Messages and Other Constants *************************


static NSString *const kPwdLengh                = @"Password would be minimum 6 characters.";
static NSString *const kOK                      = @"OK";
static NSString *const kAPPICATION_TITLE        = @"HYP";
static NSString *const kNETWORK_ISSUE           = @"The Internet connection appears to be offline.";
static NSString *const kCancel                  = @"Cancel";
static NSString *const kEmptyFields             = @"Fields can't be blank!";
static NSString *const kEmail_Validate          = @"Please enter a valid Email ID!";
static NSString *const kLOGIN                   = @"Login";
static NSString *const kFORGOT_PASSWORD         = @"Forgot password";
static NSString *const kFREATURE_NOT_IMPLIMENT  = @"This feature is not implemented yet.";
static NSString *const kCheckDetail               = @"CheckDetail";
static NSString *const kPasswordMissmatch       = @"Password Missmatch!";
static NSString *const kLastName                = @"Please enter a valid Last name.";
static NSString *const kFirstName               = @"Please enter a valid First name.";
static NSString *const kUserName                = @"Please enter a valid User name.";
static NSString *const kPassword                = @"Please enter your password.";
static NSString *const kLOGOUT_CONFIRMATION     = @"Are you sure you want to sign out?";
static NSString *const kLOGOUT                  = @"You have successfully logged out.";
static NSString *const kCNG_PWD                 = @"Change password";
static NSString *const kPHONE_NUMBER            = @"Phone number would be 10 digits.";
static NSString *const kSUBJECT                 = @"Subject would be minimum 2 characters.";
static NSString *const kLOGOUT_TITLE            = @"Logout";
static NSString *const kLOGIN_CONFIRM           = @"You’ve successfully logged in!";
static NSString *const kMSG                     = @"Message would be minimum 20 characters.";
static NSString *const kDEVICE_NOT_SUPPORT      = @"Your Device is not compatible with this features.";

static NSString *const kEmail_Composer_not_support = @"Your device doesn't support the composer sheet.";
static NSString *const kCarTYPE                 = @"Please Select your Car Type.";

static NSString *const kMSG_HISTORY_EMPTY       = @"No Booking histories are currently available. Please pull down to refresh.";

static NSString *const kMSG_Pending_EMPTY       = @"No Booking histories are currently available. Please pull down to refresh.";


static NSString *const kMSG_DROPOFF_LOCATION    = @"Please select a Drop off location first!";

static NSString *const kMSG_DRIVER_NOT_AVALABLE = @"Driver are not available near by this location.\n Please Choose another location.";

#pragma mark - Cusatom Method of ViewController

//************** Custom Medthods for ViewControllers  *************************

#define IS_NSArray_Class(array)         [array isKindOfClass:[NSArray class]]

#define IS_NSNull_Class(value)          [value isKindOfClass:[NSNull class]]
#define IS_NSDictinory_Class(dict)      [dict isKindOfClass:[NSDictionary class]]

#define IS_NSArrrayClass(dict)          [dict isKindOfClass:[NSArray class]]


#define NSUserDefaults_Set_Object(object, key) \
[[NSUserDefaults standardUserDefaults] setObject:object forKey:key]; \
[[NSUserDefaults standardUserDefaults] synchronize]; \

#define NSUserDefaults_Get_Object(key)      [[NSUserDefaults standardUserDefaults] valueForKey:key]


#define SET_IMAGE(img) [UIImage imageNamed:img]

#define GET_VIEW_CONTROLLER(viewController) [self.storyboard instantiateViewControllerWithIdentifier:viewController]


#define GET_VIEW_CONTROLLER_STORYBOARD(viewController) [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:viewController]

#define MOVE_VIEW_CONTROLLER(viewController,animation)    [self.navigationController pushViewController:viewController animated:animation];

#define PERSENT_VIEW_CONTOLLER(viewController,animation) [self.navigationController presentViewController:viewController animated:animation completion:nil];

#define GOBACK                              [self.navigationController popViewControllerAnimated:YES]

#define GOBACK_ROOT                         [self.navigationController popToRootViewControllerAnimated:YES];

#define DISMISS_VIEW_CONTROLLER             [self.navigationController dismissViewControllerAnimated:YES completion:nil];

#define  SYNCHRONIZED   [[NSUserDefaults standardUserDefaults]synchronize];

#define JOIN_STRING(str1,str2)      [str1 stringByAppendingString:str2]

#define UIAppDelegate \
((AppDelegate *)[UIApplication sharedApplication].delegate)

#define kNETWORK_PROBLEM    [Alert svError:kNETWORK_ISSUE];

#define isNull(value) value == nil || [value isKindOfClass:[NSNull class]]


#ifndef __OPTIMIZE__
#define NSLog(...) NSLog(__VA_ARGS__)
#else
#define NSLog(...) {}
#endif

// StoryBoard Identifier 


static NSString *const kSBIDTerms                             = @"TermsViewController";

static NSString *const kSBIDRegisterVC                          = @"RegisterViewController";

static NSString *const kSBIDChangePasswordVC    = @"ChangePasswordViewController";

static NSString *const kSBIDForgotPasswordVC    = @"ForgotPasswordViewController";

static NSString *const kSBIDSuccessfullVC       =@"LoginViewController";

static NSString *const kSBIDHomePassengerVC     = @"SelectLocationViewController";

static NSString *const kSBIDMenuVC               = @"MenuViewController";

static NSString *const kSBIDSaerchVC             = @"ServicesViewController";

static NSString *const kSBIDCheckDetailVC                           = @"CompleteBookingViewController";

static NSString *const kSBIDMatchesVC                        = @"BankAccountViewController";

static NSString *const kSBIDBookingDetailsVC                    = @"ServicesAddressViewController";
static NSString *const kSBIDHelpVC                              = @"HelpViewController";
static NSString *const kSBIDShowsDriverVC      = @"PendingBookingViewController";

static NSString *const kSBIDCheckSummaryVC                              = @"RateListViewController";
static NSString *const kSBIDUplaodLicenseDriverVC                              = @"CashoutViewController";

static NSString *const kSBIDSeheduleChoreVC                              = @"SeheduleChoreViewController";
static NSString *const kSBIDEARNINGDRIVERVC                              = @"EarningDriverVC";

static NSString *const kSBIPAYOUTDRIVER                             = @"PayoutDriverID";

static NSString *const kSBIDInvoiceScreenVC                     = @"InvoiceScreenViewController";
static NSString *const kSBIDCarTypeVC                           = @"RegisterInfoViewController";
static NSString *const kSBIDSettingVC                           = @"SettingsViewController";
static NSString *const kSBIDRatingVC                      =
@"SearchResultViewController";

#define		VIDEO_LENGTH						5

static NSString *const kSBIDEditProfileVC                       = @"BusinessInfoViewController";
static NSString *const kSBIDUpdateLocVC                         = @"EarningViewController";
static NSString *const kSBIDWaitingPassengerVC                  = @"ChoreHistoryViewController";
static NSString *const kSBIDPaymentVC                           =            @"PaymentViewController";
static NSString *const kSBIDNavigationWebVC                     = @"TechenicanViewController";

static NSString *const kSBID_ARRIVED_VC                     = @"DashboardViewController";

static NSString *const kSBID_CANCEL_RIDE_VC                     = @"EditViewController";

static NSString *const kSBID_RECIEVEPAYMENT_VC                     = @"RecievePaymentViewController";



static NSString *const  kFONT_REGULAR            = @"Montserrat-Regular";
static NSString *const  kFONT_BOLD               = @"Montserrat-Bold";
static NSString *const  kFONT_MEDIUM             = @"Montserrat-SemiBold";




//  Google Map 
#define ZOOM            15

#define kDRIVING_MODE @"DRIVING"

//ReqSubmitViewController

#endif /* AppConstants_h */
