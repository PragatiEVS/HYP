//
//  RequestTableViewCell.h
//  InstaModel
//
//  Created by santosh kumar singh on 05/02/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface RequestTableViewCell : UITableViewCell

@property(nonatomic,strong)  IBOutlet UILabel *nameLabel;

@property(nonatomic,strong)  IBOutlet UILabel *cratedLabel;
@property(nonatomic,strong)  IBOutlet UILabel *dateLabel;
@property(nonatomic,strong)  IBOutlet UILabel *pendingLabel;


@property(nonatomic,strong)  IBOutlet UILabel *totalLabel;
@property(nonatomic,strong)  IBOutlet UILabel *locationLabel;
@property(nonatomic,strong)  IBOutlet UIImageView *userThumb;
@property(nonatomic,strong)  IBOutlet UIButton *acceptBtn;
@property(nonatomic,strong)  IBOutlet UIButton *rejectBtn;
@property(nonatomic,strong)  IBOutlet UIButton *chatBtn;

@end


