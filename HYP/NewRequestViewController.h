//
//  NewRequestViewController.h
//  Q-Workerbee
//
//  Created by santosh kumar singh on 15/06/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewRequestViewController : UIViewController
{
    NSMutableArray *resultArray;
    int startidx;
    NSArray *arr;
}

@property(nonatomic,weak)  NSString     * matchFrom;

@property (weak, nonatomic) IBOutlet UILabel * noListAvalible;
@property(weak, nonatomic) IBOutlet UITableView *completedTableView;

@end


