//
//  BookingConfirmedViewController.m
//  HYP
//
//  Created by santosh kumar singh on 11/09/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import "BookingConfirmedViewController.h"
#import "DashboardViewController.h"
#import "MenuViewController.h"
#import "UserDetailViewController.h"
#import "BookingCompletedViewController.h"

@interface BookingConfirmedViewController ()<WebServiceDelegate,CLLocationManagerDelegate,GMSMapViewDelegate>
{
    AppDelegate * appDelegate;
}

@end

@implementation BookingConfirmedViewController

- (void)viewDidLoad {
    
    
    CLLocationCoordinate2D coordinate = [self getLocation];
    
    latitude = coordinate.latitude;
    longitude = coordinate.longitude;
    
    
    self.bokkingDateLabel.layer.borderColor = (__bridge CGColorRef _Nullable)([Alert colorFromHexString:kCOLOR_select]);
    self.bokkingDateLabel.layer.borderWidth = 2.0;
    self.bokkingDateLabel.layer.cornerRadius = 5.0;
    self.bokkingDateLabel.clipsToBounds = YES;
    
    // self.bokkingDateLabel.textColor = [Alert colorFromHexString:kCOLOR_select];
    
    
    
    
    
    self.detailView.layer.cornerRadius = 10;
    self.detailView.clipsToBounds=YES;
    
    self.title = @"BOOKING CONFIRM";
    
    
    if (!self.notificationDict) {
        self.username.text= [self.dict valueForKey:@"fullName"];
        
        [self.bokkingDateLabel setText:[NSString stringWithFormat:@"%@",[self.dict valueForKey:@"bookingDate"]]];
        if ([[self.dict valueForKey:@"image"] isEqualToString:@""])
        {
            
            NSString *userName = [self.dict valueForKey:@"fullName"];
            [self.userImage setImageWithString:userName color:nil circular:YES];
            
        }
        else
        {
            
            [self.userImage setImageURL:[NSURL URLWithString:[self.dict valueForKey:@"image"]]];
        }
    }
    else
    {
        
        [self.bokkingDateLabel setText:[NSString stringWithFormat:@"%@",[self.notificationDict valueForKey:@"bookingDate"]]];
        self.username.text= [self.notificationDict valueForKey:@"vendorFullName"];
        
        if ([[self.notificationDict valueForKey:@"vendorImage"] isEqualToString:@""])
        {
            
            NSString *userName = [self.notificationDict valueForKey:@"vendorFullName"];
            [self.userImage setImageWithString:userName color:nil circular:YES];
            
        }
        else
        {
            
            [self.userImage setImageURL:[NSURL URLWithString:[self.notificationDict valueForKey:@"vendorImage"]]];
        }
    }
    
    [self confirmBtn];
    
    self.userImage.layer.cornerRadius = 35;
    self.userImage.clipsToBounds=YES;
    
    self.rateview.starSize = 20;
    
    self.rateview.starFillColor = [Alert colorFromHexString:@"#F4CC47"];
    self.rateview.starNormalColor = [UIColor grayColor];
    
    self.rateview.rating= [[self.dict valueForKey:@"AVGRating"]floatValue];
    
    
    
    NSInteger borderThickness = 1;
    UIView *leftBorder = [UIView new];
    leftBorder.backgroundColor = [UIColor blackColor];
    leftBorder.frame = CGRectMake(0, 0, borderThickness, self.cancelBtn.frame.size.height);
    [self.cancelBtn addSubview:leftBorder];
    
    //    if ([[self.dict valueForKey:@"image"] isEqualToString:@""])
    //    {
    //        NSString *userName = [self.dict valueForKey:@"vendorName"];
    //        [self.userImage setImageWithString:userName color:nil circular:YES];
    //
    //    }
    //    else
    //    {
    //
    //        [self.userImage setImageURL:[NSURL URLWithString:[self.dict valueForKey:@"image"]]];
    //    }
    
    
    self.serviceNameLabel.text = [NSString stringWithFormat:@"Service Name: %@",self.servicename];
    
    self.toatlAmountLabel.text = [NSString stringWithFormat:@"Service Charge: $%@",self.totalAmount];
    [super viewDidLoad];
    
    
    UIView *topBorder = [UIView new];
    topBorder.backgroundColor = [UIColor blackColor];
    topBorder.frame = CGRectMake(0, 0, self.userDetailView.frame.size.width, borderThickness);
    [self.userDetailView addSubview:topBorder];
    
    UIView *bottomBorder = [UIView new];
    bottomBorder.backgroundColor = [UIColor blackColor];
    bottomBorder.frame = CGRectMake(0, self.userDetailView.frame.size.height - borderThickness, self.userDetailView.frame.size.width, borderThickness);
    [self.userDetailView addSubview:bottomBorder];
    
    self.geocoder = [[GMSGeocoder alloc]init];
    [self showGoogleMap];
    
    
    [self navigationBarConfiguration];
    
    // Do any additional setup after loading the view.
}


-(CLLocationCoordinate2D) getLocation
{
    CLLocationManager *locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager startUpdatingLocation];
    CLLocation *location = [locationManager location];
    CLLocationCoordinate2D coordinate = [location coordinate];
    
    return coordinate;
}

#pragma mark --- : GOOGLE MAP :----
-(void)showGoogleMap
{
    
    CLLocation * upadetLocation = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
    
    CGRect frame = CGRectMake(0, 0, kSCREEN_WIDTH,kSCREEN_HEIGHT);
    camera = [GMSCameraPosition cameraWithLatitude:latitude longitude:longitude zoom:15];
    mapView_ = [GMSMapView mapWithFrame:frame camera:camera];
    mapView_.delegate = self;
    
    [mapView_ setCamera:camera];
    
    self->mapView_.myLocationEnabled              = YES;
    self->mapView_.indoorEnabled                  = YES;
    self->mapView_.settings.myLocationButton      = NO;
    self->mapView_.settings.scrollGestures        = YES;
    
    [_mapView addSubview:mapView_];
    
    locationMarker_ = [[GMSMarker alloc] init];
    locationMarker_.position = CLLocationCoordinate2DMake(latitude, longitude);
    locationMarker_.icon = [UIImage imageNamed:@"car1.png"];
    locationMarker_.map = mapView_;
    
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    
    [self.locationManager stopUpdatingLocation];
    //  CLLocation *mostRecentLocation = [locations objectAtIndex:0];
    
    
}

-(void)back:(id)sender
{
    if (!self.notificationDict)
    {
        
        DISMISS_VIEW_CONTROLLER;
    }
    else
    {
        
        KYDrawerController *elDrawer = (KYDrawerController*)self.navigationController.parentViewController;
        [elDrawer setDrawerState:KYDrawerControllerDrawerStateOpened animated:YES];
        
        
    }
    
}

#pragma mark - Configure Navigation Controller

-(void)navigationBarConfiguration
{
    
    self.navigationController.navigationBarHidden = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    UIButton * menuButton  = [UIButton buttonWithType:UIButtonTypeSystem];
    menuButton.frame = CGRectMake(8, 18, 24, 24);
    [menuButton setBackgroundImage:[UIImage imageNamed:IMG_BACK] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *accountBarItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    self.navigationItem.leftBarButtonItem = accountBarItem;
    
    NSMutableArray *arrRightBarItems = [[NSMutableArray alloc] init];
    UIButton *btnSetting = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnSetting setImage:[UIImage imageNamed:@"Frequency"] forState:UIControlStateNormal];
    btnSetting.frame = CGRectMake(0, 0, 40, 40);
    btnSetting.showsTouchWhenHighlighted=YES;
    [btnSetting addTarget:self action:@selector(onSettings:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSetting];
    
    UIButton *btnLib = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnLib setImage:[UIImage imageNamed:@"logo1.png"] forState:UIControlStateNormal];
    btnLib.frame = CGRectMake(0, 0, 40, 40);
    btnLib.showsTouchWhenHighlighted=YES;
   [btnLib addTarget:self action:@selector(onMyLibrary:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButtonItem2 = [[UIBarButtonItem alloc] initWithCustomView:btnLib];
    [arrRightBarItems addObject:barButtonItem2];
    [arrRightBarItems addObject:barButtonItem];
    self.navigationItem.rightBarButtonItems=arrRightBarItems;
    
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor],
    }];
    
}
-(void)onMyLibrary:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}
-(void)onSettings:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}

-(void)confirmBtn
{
    
    [Alert svProgress:@"Please Wait..."];
    
    NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults]  objectForKey:kLOGIN];
    
    WebService * connection = [[WebService alloc]init];
    connection.delegate = self;
    NSMutableDictionary * dict                              = [NSMutableDictionary dictionary];
    
    [dict setObject:@"userconfirm"                     forKey:kAPI_ACTION];
    [dict setObject:[dictLogin valueForKey:@"userId"]                     forKey:kUSER_ID];
    
    if (!self.notificationDict)
    {
        [dict setObject:[self.dict valueForKey:@"bookingId"]                    forKey:@"bookingId"];
        
    }
    else
    {
        [dict setObject:[self.notificationDict valueForKey:@"bookingId"]                    forKey:@"bookingId"];
        
    }
    
    NSLog(@"dict is ---%@",dict);
    [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:@"userconfirm"];
    
}

#pragma mark - Sevice delgate methods

-(void)getDataFormWebService:(NSDictionary *)jsonResults urlStr:(NSString *)yourUR methodName:(NSString *)methodName{
    
    [SVProgressHUD dismiss];
    
    SWITCH (methodName) {
        
        CASE (@"userconfirm")
        {
            [Alert svSuccess:[jsonResults objectForKey:@"msg"]];
            
            if (!self.notificationDict)
            {
                if ([[self.dict valueForKey:@"today"] isEqualToString:@"today"])
                {
                    BookingCompletedViewController * bookingHistoryViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"BookingCompletedViewController");
                    if (!self.notificationDict)
                    {
                        bookingHistoryViewController.dict = _dict;
                    }
                    else
                    {
                        bookingHistoryViewController.dict = _notificationDict;
                    }
                    
                    UINavigationController * navigationController    = [Alert navigationControllerWithVC:bookingHistoryViewController];
                    PERSENT_VIEW_CONTOLLER(navigationController, YES);
                    
                }
                else{
                    NSLog(@"jsonResults =%@",jsonResults);
                    appDelegate = UIAppDelegate;
                    DashboardViewController * homePassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBID_ARRIVED_VC);
                    UIAppDelegate.isCall  = YES;
                    MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
                    [appDelegate manageDrawerRear:menuViewController front:homePassengerViewController];
                }
                
                
            }
            else
            {
                
                if (self.notificationDict)
                {
                    if ([[self.notificationDict valueForKey:@"today"] isEqualToString:@"today"])
                    {
                        BookingCompletedViewController * bookingHistoryViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"BookingCompletedViewController");
                        if (!self.notificationDict)
                        {
                            bookingHistoryViewController.dict = _dict;
                        }
                        else
                        {
                            bookingHistoryViewController.dict = _notificationDict;
                        }
                        
                        UINavigationController * navigationController    = [Alert navigationControllerWithVC:bookingHistoryViewController];
                        PERSENT_VIEW_CONTOLLER(navigationController, YES);
                        
                        
                    }
                    else{
                        NSLog(@"jsonResults =%@",jsonResults);
                        appDelegate = UIAppDelegate;
                        DashboardViewController * homePassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBID_ARRIVED_VC);
                        UIAppDelegate.isCall  = YES;
                        MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
                        [appDelegate manageDrawerRear:menuViewController front:homePassengerViewController];
                    }
                    
                }
                
                
                
            }
            
            
        }
        
        CASE (@"canclerequest")
        {
            
            NSLog(@"jsonResults =%@",jsonResults);
            appDelegate = UIAppDelegate;
            DashboardViewController * homePassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBID_ARRIVED_VC);
            UIAppDelegate.isCall  = YES;
            MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
            [appDelegate manageDrawerRear:menuViewController front:homePassengerViewController];
            
        }
        
        break;
        
        DEFAULT
        {
            break;
        }
    }
    
}
// WEB SERVER ERROR MSG LOCAL SERVER (APPLICATION SERVER)
-(void) webServiceFailWithApplicationServerMSG:(NSString *)msg
{
    
    [Alert svError:msg];
    
}

// WEB SERVICE FAIL WITH REGION TCP/IP
-(void) webServiceFail:(NSError *)error{
    
    
    [Alert svError:[error localizedDescription]];
    
}


-(IBAction)profileBtn:(id)sender
{
    
    UserDetailViewController *forgotPasswordViewController =  GET_VIEW_CONTROLLER(@"UserDetailViewController");
    if (!self.notificationDict)
    {
        
        forgotPasswordViewController.vendorId=  [self.dict valueForKey:@"userId"];
        
    }
    else
    {
        
        forgotPasswordViewController.vendorId=  [self.notificationDict valueForKey:@"vendorID"];
        
    }
    
    
    //
    UINavigationController * navigationController = [Alert navigationControllerWithVC:forgotPasswordViewController];
    PERSENT_VIEW_CONTOLLER(navigationController, YES);
    
}
-(IBAction)cancelBtn:(id)sender
{
    
    [Alert svProgress:@"Please Wait..."];
    
    WebService * connection = [[WebService alloc]init];
    connection.delegate = self;
    NSMutableDictionary * dict                              = [NSMutableDictionary dictionary];
    
    [dict setObject:@"canclerequest"                     forKey:kAPI_ACTION];
    
     if (!self.notificationDict)
     {
        
        [dict setObject:[self.dict valueForKey:@"userId"]                     forKey:kUSER_ID];
        [dict setObject:[self.dict valueForKey:@"groupId"]                    forKey:@"groupId"];
    }
    else
    {
        [dict setObject:[self.notificationDict valueForKey:@"vendorID"]                     forKey:kUSER_ID];
        
        [dict setObject:[self.notificationDict valueForKey:@"bookingId"]                    forKey:@"groupId"];
    }
    
    
    NSLog(@"dict is ---%@",dict);
    [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:@"canclerequest"];
    
}

-(IBAction)callBtn:(id)sender
{
    if (TARGET_OS_SIMULATOR)
    {
        [Alert alertControllerTitle:kAPPICATION_TITLE msg:kDEVICE_NOT_SUPPORT ok:kOK controller:self.navigationController];
    }
    else
    {
        [Alert makePhoneCall:[self.dict valueForKey:@"contactNumber"]];
    }
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
