//
//  UserBookingViewController.h
//  HYP
//
//  Created by VA pvt ltd on 14/09/21.
//  Copyright © 2021 Pragati Porwal. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface UserBookingViewController : UIViewController
{
     NSMutableArray *resultArray;
     int startidx;
     NSArray *arr;
}
@property(weak, nonatomic) IBOutlet MDTabBar *tabBar;
@property(weak, nonatomic)  NSString *matchRequest;
@property (weak, nonatomic) IBOutlet UILabel * noListAvalible;
@property(weak, nonatomic) IBOutlet UITableView *completedTableView;
@property(weak, nonatomic) IBOutlet UITableView *serviceTableView;
@end

