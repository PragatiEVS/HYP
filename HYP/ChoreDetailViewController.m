

#import "ChoreDetailViewController.h"
#import "EarningViewController.h"
#import "RateListViewController.h"
#import "BookingCompletedViewController.h"
#import "BookingProgressViewController.h"
#import "InvoiceDetailViewController.h"
#import "CompleteDetailViewController.h"
#import "MenuViewController.h"
#import "DashboardViewController.h"
#import "CompleteMemberViewController.h"


@interface ChoreDetailViewController ()
{
    AppDelegate * appDelegate;
    
}
@end

@implementation ChoreDetailViewController

- (void)viewDidLoad {
    
    self.memberCompltedView.hidden = YES;
    self.memberPendingView.hidden = YES;
    self.prviderCompltedView.hidden = YES;
    self.prviderJobDoneView.hidden = YES;
    self.memberConfirmedView.hidden = YES;
    self.jobView.hidden = YES;
    
    _addReviewClick.hidden=YES;
    self.viewaddClick.hidden= YES;
    self.acceptView.hidden = YES;
    self.jobView.hidden = YES;
    self.rateview.hidden = YES;
    self.title = @"BOOKING DETAILS";
    
    [self navigationBarConfiguration];
    
    self.imageuploadedby.hidden = YES;
    self.imageuploadedbyuser.hidden = YES;
    
    [Alert setKeyBoardToolbar:@[self.msgtextfiled]];
    
    self.fivteenBtn.layer.borderWidth = 0.5f;
    self.fivteenBtn.clipsToBounds = YES;
    self.fivteenBtn.layer.borderColor = [UIColor darkGrayColor].CGColor;
    self.fivteenBtn.layer.cornerRadius = 5.0f;
    
    
    self.tenBtn.layer.borderWidth = 0.5f;
    self.tenBtn.clipsToBounds = YES;
    self.tenBtn.layer.borderColor = [UIColor darkGrayColor].CGColor;
    self.tenBtn.layer.cornerRadius = 5.0f;
    
    self.twentyfiveBtn.layer.borderWidth = 0.5f;
    self.twentyfiveBtn.clipsToBounds = YES;
    self.twentyfiveBtn.layer.borderColor = [UIColor darkGrayColor].CGColor;
    self.twentyfiveBtn.layer.cornerRadius = 5.0f;
    
    self.customFiled.layer.cornerRadius = 5.0f;
    self.customFiled.clipsToBounds = YES;
    self.customFiled.layer.borderColor = [UIColor darkGrayColor].CGColor;
    self.customFiled.layer.borderWidth = 0.5f;
    
    self.stackView.hidden= YES;
    self.rateText.hidden = YES;
    
    
    self.donebtn.clipsToBounds = YES;
    self.donebtn.layer.cornerRadius = 5.0f;
    
    self.acceptbtn.clipsToBounds = YES;
    self.acceptbtn.layer.cornerRadius = 5.0f;
    
    self.rejectbtn.clipsToBounds = YES;
    self.rejectbtn.layer.cornerRadius = 5.0f;
    
    self.viewaddClick.clipsToBounds = YES;
    self.viewaddClick.layer.cornerRadius = 5.0f;
    
    self.msgtextfiled.clipsToBounds = YES;
    self.msgtextfiled.layer.cornerRadius = 5.0f;
    
    self.submitbtn.clipsToBounds = YES;
    self.submitbtn.layer.cornerRadius = 5.0f;
    
    
    self.addReviewbtn.clipsToBounds = YES;
    self.addReviewbtn.layer.cornerRadius = 5.0f;
    
    self.reviewListbtn.clipsToBounds = YES;
    self.reviewListbtn.layer.cornerRadius = 5.0f;
    
    self.ratingButton.clipsToBounds = YES;
    self.ratingButton.layer.cornerRadius = 5.0f;
    
    self.ratingViewClick.clipsToBounds = YES;
    self.ratingViewClick.layer.cornerRadius = 5.0f;
    
    NSInteger borderThickness = 1;
    
    //    UIView *topBorder = [UIView new];
    //      topBorder.backgroundColor = [UIColor grayColor];
    //      topBorder.frame = CGRectMake(0, 0, self.totalView.frame.size.width, borderThickness);
    //      [self.totalView addSubview:topBorder];
    //
    UIView *bottomBorder = [UIView new];
    bottomBorder.backgroundColor = [UIColor grayColor];
    bottomBorder.frame = CGRectMake(0, self.totalView.frame.size.height - borderThickness, self.totalView.frame.size.width, borderThickness);
    [self.totalView addSubview:bottomBorder];
    
    
    UIView *bottomBorder1 = [UIView new];
    bottomBorder1.backgroundColor = [UIColor grayColor];
    bottomBorder1.frame = CGRectMake(0, self.serviceView.frame.size.height - borderThickness, self.serviceView.frame.size.width, borderThickness);
    [self.serviceView addSubview:bottomBorder1];
    
    UIView *bottomBorder2 = [UIView new];
    bottomBorder2.backgroundColor = [UIColor grayColor];
    bottomBorder2.frame = CGRectMake(0, self.dateView.frame.size.height - borderThickness, self.totalView.frame.size.width, borderThickness);
    [self.dateView addSubview:bottomBorder2];
    
    
    self.collectionView.hidden= YES;
    self.collectionView.backgroundColor = [UIColor whiteColor];
    self.collectionViewuser.hidden= YES;
    self.collectionViewuser.backgroundColor = [UIColor whiteColor];
    
    imageUserArray =[[NSMutableArray alloc]init];
    imageBusinessArray =[[NSMutableArray alloc]init];
    
    [self serviceBusinessType];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)onSettings:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}


-(void)back:(id)sender
{
    
    appDelegate = UIAppDelegate;
    DashboardViewController * homePassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBID_ARRIVED_VC);
    UIAppDelegate.isCall  = YES;
    MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
    [appDelegate manageDrawerRear:menuViewController front:homePassengerViewController];
    
}

#pragma mark - Configure Navigation Controller

-(void)navigationBarConfiguration
{
    
    self.navigationController.navigationBarHidden = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    UIButton * menuButton  = [UIButton buttonWithType:UIButtonTypeSystem];
    menuButton.frame = CGRectMake(8, 18, 24, 24);
    [menuButton setBackgroundImage:[UIImage imageNamed:IMG_BACK] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *accountBarItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    self.navigationItem.leftBarButtonItem = accountBarItem;
    
    
    NSMutableArray *arrRightBarItems = [[NSMutableArray alloc] init];
    UIButton *btnSetting = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnSetting setImage:[UIImage imageNamed:@"Frequency"] forState:UIControlStateNormal];
    btnSetting.frame = CGRectMake(0, 0, 40, 40);
    btnSetting.showsTouchWhenHighlighted=YES;
    [btnSetting addTarget:self action:@selector(onSettings:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSetting];
    
    UIButton *btnLib = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnLib setImage:[UIImage imageNamed:@"logo1.png"] forState:UIControlStateNormal];
    btnLib.frame = CGRectMake(0, 0, 40, 40);
    btnLib.showsTouchWhenHighlighted=YES;
    [btnLib addTarget:self action:@selector(onMyLibrary:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButtonItem2 = [[UIBarButtonItem alloc] initWithCustomView:btnLib];
    [arrRightBarItems addObject:barButtonItem2];
    [arrRightBarItems addObject:barButtonItem];
    self.navigationItem.rightBarButtonItems=arrRightBarItems;
    
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor],
    }];
    
}
-(void)onMyLibrary:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}

-(IBAction)btnSelected:(UIButton*)sender
{
    
    if (sender.tag ==1)
    {
        // self.stringMatchtotal=@"toatal";
        
        NSString * str = [_twentyfiveBtn.titleLabel.text stringByReplacingOccurrencesOfString:@"%" withString:@""];
        self.tenBtn.backgroundColor= [Alert colorFromHexString:kCOLOR_unselect];
        self.fivteenBtn.backgroundColor= [Alert colorFromHexString:kCOLOR_unselect];
        self.customFiled.backgroundColor=[Alert colorFromHexString:kCOLOR_unselect];
        self.customFiled.text =@"";
        
        
        
        NSString * price = [self.servicesAmount.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
        
        float ntotalPrice = [price floatValue];
        
        _totalTip = ([str floatValue]*ntotalPrice)/100;
        _tipString = [NSString stringWithFormat:@"%.f",_totalTip];
        
        NSLog(@"_totalNow=%f",_totalTip);
        _twentyfiveBtn.backgroundColor = [Alert colorFromHexString:kCOLOR_select];
        
    }
    if (sender.tag ==2)
    {
        
        self.twentyfiveBtn.backgroundColor= [Alert colorFromHexString:kCOLOR_unselect];
        self.tenBtn.backgroundColor= [Alert colorFromHexString:kCOLOR_unselect];
        self.customFiled.backgroundColor=[Alert colorFromHexString:kCOLOR_unselect];
        
        NSString * str = [_fivteenBtn.titleLabel.text stringByReplacingOccurrencesOfString:@"%" withString:@""];
        
        NSString * price = [self.servicesAmount.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
        
        float ntotalPrice = [price floatValue];
        
        _totalTip = ([str floatValue]*ntotalPrice)/100;
        _tipString = [NSString stringWithFormat:@"%.f",_totalTip];
        
        NSLog(@"_totalNow=%f",_totalTip);
        _fivteenBtn.backgroundColor = [Alert colorFromHexString:kCOLOR_select];
        
        
    }
    if (sender.tag ==3)
    {
        
        NSString * str = [_tenBtn.titleLabel.text stringByReplacingOccurrencesOfString:@"%" withString:@""];
        self.twentyfiveBtn.backgroundColor= [Alert colorFromHexString:kCOLOR_unselect];
        self.fivteenBtn.backgroundColor= [Alert colorFromHexString:kCOLOR_unselect];
        self.customFiled.backgroundColor=[Alert colorFromHexString:kCOLOR_unselect];
        
        NSString * price = [self.servicesAmount.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
        
        float ntotalPrice = [price floatValue];
        
        _totalTip = ([str floatValue]*ntotalPrice)/100;
        _tipString = [NSString stringWithFormat:@"%.f",_totalTip];
        
        NSLog(@"_totalNow=%f",_totalTip);
        _tenBtn.backgroundColor = [Alert colorFromHexString:kCOLOR_select];
        
    }
}

-(void)serviceBusinessType
{
    // SERVER CONNCETION
    
    
    if (!_dict) {
        _dict = [AppDelegate appDelegate].bookingId;
        
    }
    
    [Alert svProgress:@"Please Wait..."];
    NSDictionary * dictLogin= [[NSUserDefaults standardUserDefaults]  objectForKey:kLOGIN];
    //  NSDictionary * dictLogin = [NSKeyedUnarchiver unarchiveObjectWithData:data1];
    
    NSString * user_id ;
    if ([dictLogin valueForKey:@"id"] == nil)
    {
        user_id  = [dictLogin valueForKey:@"userId"];
        
    }
    else
    {
        user_id  = [dictLogin valueForKey:@"id"];
    }
    
    WebService * connection = [[WebService alloc]init];
    connection.delegate = self;
    
    NSMutableDictionary * dict       = [NSMutableDictionary dictionary];
    [dict setObject:@"bookingdetail"                    forKey:kAPI_ACTION];
    [dict setObject:user_id                    forKey:kUSER_ID];
    
    if (self.dict[@"bookingID"])
    {
        [dict setObject:[_dict objectForKey:@"bookingID"]                     forKey:@"bookingId"];
    }
    else{
        [dict setObject:[_dict objectForKey:@"bookingId"]                     forKey:@"bookingId"];
    }
    
    
    [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:@"bookingdetail"];
}

#pragma mark - Sevice delgate methods

-(void)getDataFormWebService:(NSDictionary *)jsonResults urlStr:(NSString *)yourUR methodName:(NSString *)methodName{
    
    [SVProgressHUD dismiss];
    
    SWITCH (methodName) {
        
        CASE (@"bookingdetail")
        {
            NSLog(@"jsonResults =%@",jsonResults);
            NSArray *array = [jsonResults valueForKeyPath:@"historyList"];
            self.dict1 = [jsonResults valueForKeyPath:@"historyList"];
            
            self.dict = [jsonResults valueForKeyPath:@"historyList"];
            
            NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults]  objectForKey:kLOGIN];
            
            
            NSString * str = [dictLogin valueForKey:@"role"];
            
            if ([str isEqualToString:@"Partner"])
            {
                
                self.stackView.hidden= YES;
                self.rateText.hidden = YES;
                
                if ([[array valueForKey:@"userImage"] isEqualToString:@""])
                {
                    
                    NSString *userName = [array valueForKey:@"userName"];
                    [self.userBg setImageWithString:userName color:nil circular:YES];
                    
                }
                else
                {
                    
                    [self.userBg setImageURL:[NSURL URLWithString:[array valueForKey:@"userImage"]]];
                }
                
                self.address.numberOfLines=2;
                
                self.username.text =[array valueForKey:@"userName"];
                self.phone.text =[array valueForKey:@"userPhone"];
                self.address.text =[array valueForKey:@"address"];
                
                self.jobView.hidden = YES;
                NSString *str =[[array valueForKey:@"status"] stringValue];
                if ([str isEqual:@"0"])
                {
                    self.acceptView.hidden = NO;
                    //                    self.jobView.hidden = ye;
                    //                    [self.donebtn setTitle:@"START JOB" forState:UIControlStateNormal];
                    //                    [self.donebtn addTarget:self action:@selector(btnStartJob:) forControlEvents:UIControlEventTouchUpInside];
                    
                }
                if ([str isEqual:@"1"])
                {
                    
                    NSString *strinvoice = [[[self.dict1 valueForKey:@"invoice"]valueForKey:@"status"]stringValue];
                    
                    if ([[[array valueForKey:@"jobstart"] stringValue] isEqualToString:@"0"] && [strinvoice isEqual:@"1"])
                    {
                        self.prviderJobDoneView.hidden = NO;
                        
                    }
                    if ([[[array valueForKey:@"jobstart"] stringValue] isEqualToString:@"1"] && [strinvoice isEqual:@"1"])
                    {
                        self.prviderJobDoneView.hidden = NO;
                        
                    }
                    if ([[[array valueForKey:@"jobstart"] stringValue] isEqualToString:@"0"] && [strinvoice isEqual:@"2"])
                    {
                        
                        self.prviderJobDoneView.hidden = NO;
                        
                    }
                    if ([[[array valueForKey:@"jobstart"] stringValue] isEqualToString:@"1"] && [strinvoice isEqual:@"2"])
                    {
                        self.prviderJobDoneView.hidden = NO;
                        
                    }
                    if ([[[array valueForKey:@"jobstart"] stringValue] isEqualToString:@"0"] && [strinvoice isEqual:@"0"])
                    {
                        self.jobView.hidden = NO;
                        [self.donebtn setTitle:@"START JOB" forState:UIControlStateNormal];
                        [self.donebtn addTarget:self action:@selector(btnStartJob:) forControlEvents:UIControlEventTouchUpInside];
                    }
                    
                    if ([[[array valueForKey:@"jobstart"] stringValue] isEqualToString:@"1"] && [strinvoice isEqual:@"0"])
                    {
                        self.jobView.hidden = NO;
                        [self.donebtn setTitle:@"WORK IN PROGRESS" forState:UIControlStateNormal];
                        [self.donebtn addTarget:self action:@selector(btnTapped:) forControlEvents:UIControlEventTouchUpInside];
                    }
                    
                }
                if ([str isEqual:@"2"])
                {
                    
                    self.jobView.hidden = YES;
                    self.memberCompltedView.hidden = YES;
                    self.prviderCompltedView.hidden = NO;
                    self.rateview.hidden =NO;
                    [self.donebtn setTitle:@"COMPLETED" forState:UIControlStateNormal];
                    
                }
                if ([str isEqual:@"3"])
                {
                    self.jobView.hidden = NO;
                    self.rateview.hidden =YES;
                    [self.donebtn setTitle:@"REQUEST CANCELLED" forState:UIControlStateNormal];
                    self.stackView.hidden= NO;
                    self.donebtn.userInteractionEnabled = NO;
                    // [self.donebtn addTarget:self action:@selector(jobDoneBtn:) forControlEvents:UIControlEventTouchUpInside];
                    
                }
            }
            else
            {
                if ([[array valueForKey:@"vendorImage"] isEqualToString:@""])
                {
                    
                    NSString *userName = [array valueForKey:@"vendorName"];
                    [self.userBg setImageWithString:userName color:nil circular:YES];
                    
                }
                else
                {
                    
                    [self.userBg setImageURL:[NSURL URLWithString:[array valueForKey:@"vendorImage"]]];
                }
                
                
                self.address.numberOfLines=2;
                
                self.username.text =[array valueForKey:@"vendorName"];
                self.phone.text =[array valueForKey:@"vendorPhone"];
                self.address.text =[array valueForKey:@"address"];
                
                
                self.rateUpdate.starSize = 25;
                self.rateUpdate.rating = 4.0f;
                self.rateUpdate.canRate = YES;
                self.rateUpdate.starFillColor = [Alert colorFromHexString:kCOLOR_select];
                self.rateText.hidden = YES;
                self.viewaddClick.hidden = YES;
                
                NSString *str =[[array valueForKey:@"status"] stringValue];
                self.addReviewClick.hidden = NO;
                
                if ([str isEqual:@"0"])
                {
                    self.memberPendingView.hidden = NO;
                    self.memberCompltedView.hidden = YES;
                    self.prviderCompltedView.hidden = YES;
                    self.memberConfirmedView.hidden = YES;
                }
                if ([str isEqual:@"1"])
                {
                    
                    self.jobView.hidden = YES;
                    self.memberCompltedView.hidden = YES;
                    self.prviderCompltedView.hidden = YES;
                    self.memberConfirmedView.hidden = NO;
                    
                }
                if ([str isEqual:@"2"])
                {
                    self.jobView.hidden = YES;
                    self.memberCompltedView.hidden = NO;
                    self.memberConfirmedView.hidden = YES;
                    self.prviderCompltedView.hidden = YES;
                    self.rateview.hidden =NO;
                    [self.donebtn setTitle:@"COMPLETED" forState:UIControlStateNormal];
                    self.stackView.hidden= NO;
                    self.donebtn.userInteractionEnabled = YES;
                    [self.donebtn addTarget:self action:@selector(jobDoneBtn:) forControlEvents:UIControlEventTouchUpInside];
                }
                if ([str isEqual:@"3"])
                {
                    self.jobView.hidden = NO;
                    self.rateview.hidden =YES;
                    self.memberCompltedView.hidden = YES;
                    self.memberConfirmedView.hidden = YES;
                    self.prviderCompltedView.hidden = YES;
                    [self.donebtn setTitle:@"REQUEST CANCELLED" forState:UIControlStateNormal];
                    self.stackView.hidden= NO;
                    self.donebtn.userInteractionEnabled = NO;
                    // [self.donebtn addTarget:self action:@selector(jobDoneBtn:) forControlEvents:UIControlEventTouchUpInside];
                    
                }
            }
            
            
            self.servicesAmount.text =[NSString stringWithFormat:@"$%@", [array valueForKey:@"serviceAmount"]];
            
            self.tipAmount.text = [NSString stringWithFormat:@"Tip Amount: $%.2f",[[array valueForKey:@"TIP"]floatValue]];
            
            self.discontAmount.text = [NSString stringWithFormat:@"Discount Amount: $%.2f", [[array valueForKey:@"discountAmount"]floatValue]];
            
            self.toatalAmmount.text = [array valueForKey:@"slote"];
            
            self.startTimeLabel.text = [array valueForKey:@"categoryName"];
            
            self.endTimeLabel.text = [NSString stringWithFormat:@"%@",[array valueForKey:@"bookingDate"]];
            
            
            [self.categoryImage  setImageURL:[NSURL URLWithString: [array valueForKey:@"categoryImage"]]];
            
            
            self.datetimeLabel.text= [NSString stringWithFormat:@"%@ | %@",[array valueForKey:@"bookingDate"],[array valueForKey:@"slote"]];
            
            self.categoryImage.layer.cornerRadius = self.categoryImage.frame.size.width/2;
            self.categoryImage.clipsToBounds = YES;
            
            self.userBg.layer.cornerRadius = self.userBg.frame.size.width/2;
            self.userBg.clipsToBounds = YES;
            
            imageBusinessArray = [array valueForKey:@"multiImage"];
            
            imageUserArray = [array valueForKey:@"multiImage_2"];
            
            if ((imageBusinessArray.count ==0) && (imageUserArray.count ==0))
            {
                self.imageuploadedby.hidden = YES;
                self.imageuploadedbyuser.hidden = YES;
                
                
                self.collectionViewWidthConstraint.constant=0.0f;
                self.imageuselabelconstant.constant=0.0f;
                self.collectionViewWidthConstraintuser.constant=0.0f;
            }
            else
            {
                if (imageBusinessArray.count ==0)
                {
                    self.imageuploadedby.hidden = YES;
                    self.imageuploadedbyuser.hidden = NO;
                    
                    self.collectionViewuser.hidden= NO;
                    self.collectionViewWidthConstraint.constant=0.0f;
                    self.collectionViewuser.dataSource  = self;
                    
                    self.imageuploadedbyuser.text = [NSString stringWithFormat:@"Images uploaded by %@", [array valueForKey:@"userName"]];
                    
                    //                    self.imageuploadedbyuser.text = [NSString stringWithFormat:@"Images uploaded by %@", [array valueForKey:@"vendorName"]];
                    
                    [self calculateAndApplyBestSpacing];
                    
                }
                else  if (imageUserArray.count ==0)
                {
                    self.imageuploadedby.hidden = NO;
                    self.imageuploadedbyuser.hidden = YES;
                    
                    //                    self.imageuploadedby.text = [NSString stringWithFormat:@"Images uploaded by %@", [array valueForKey:@"userName"]];
                    //
                    //
                    self.imageuploadedby.text = [NSString stringWithFormat:@"Images uploaded by %@", [array valueForKey:@"vendorName"]];
                    
                    self.imageuselabelconstant.constant=0.0f;
                    self.collectionViewWidthConstraintuser.constant=0.0f;
                    self.collectionView.hidden= NO;
                    self.collectionView.dataSource  = self;
                    [self calculateAndApplyBestSpacing];
                    
                }
                else
                {
                    self.imageuploadedby.hidden = NO;
                    self.imageuploadedbyuser.hidden = NO;
                    
                    if (imageBusinessArray.count  <=3 )
                    {
                        self.collectionViewWidthConstraint.constant=130.0f;
                        
                    }
                    else  if (imageUserArray.count <=3)
                        
                    {
                        self.imageuselabelconstant.constant=30.0f;
                        self.collectionViewWidthConstraintuser.constant=120.0f;
                    }
                    
                    
                    self.imageuploadedby.text = [NSString stringWithFormat:@"Images uploaded by %@", [array valueForKey:@"vendorName"]];
                    
                    self.imageuploadedbyuser.text = [NSString stringWithFormat:@"Images uploaded by %@", [array valueForKey:@"userName"]];
                    
                    self.collectionView.hidden= NO;
                    self.collectionView.backgroundColor = [UIColor whiteColor];
                    self.collectionView.dataSource  = self;
                    
                    self.collectionViewuser.hidden= NO;
                    self.collectionViewuser.backgroundColor = [UIColor whiteColor];
                    self.collectionViewuser.dataSource  = self;
                    
                    [self calculateAndApplyBestSpacing];
                    
                }
                
            }
            
        }
        
        CASE (@"startjob")
        {
            NSLog(@"jsonResults =%@",jsonResults);
            
            if ([[jsonResults valueForKey:@"status"] isEqualToString:@"success"])
            {
                [Alert svSuccess:[jsonResults valueForKey:@"msg"]];
                BookingProgressViewController * bookingHistoryViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"BookingProgressViewController");
                bookingHistoryViewController.dict = _dict1;
                UINavigationController * navigationController    = [Alert navigationControllerWithVC:bookingHistoryViewController];
                PERSENT_VIEW_CONTOLLER(navigationController, YES);
            }
            
        }
        CASE (@"changebookingstatus")
        {
            NSLog(@"jsonResults =%@",jsonResults);
            self.acceptView.hidden= YES;
            if (([[jsonResults objectForKey:@"msg"] isEqualToString:@"Booking accepted successfully."]))
            {
                [Alert svSuccess:[jsonResults valueForKey:@"msg"]];
                [self viewDidLoad];
                //                BookingProgressViewController * bookingHistoryViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"BookingProgressViewController");
                //                bookingHistoryViewController.dict = _dict1;
                //                UINavigationController * navigationController    = [Alert navigationControllerWithVC:bookingHistoryViewController];
                //                PERSENT_VIEW_CONTOLLER(navigationController, YES);
            }
            else
            {
                
                [Alert svSuccess:[jsonResults valueForKey:@"msg"]];
                
                [self viewDidLoad];
            }
            
        }
        CASE (@"submitreview")
        {
            NSLog(@"jsonResults =%@",jsonResults);
            
            if ([[jsonResults valueForKey:@"status"] isEqualToString:@"Success"])
            {
                
                [Alert svSuccess:[jsonResults valueForKey:@"msg"]];
                self.msgtextfiled.text = @"";
                self.rateUpdate.starSize = 25;
                self.rateUpdate.rating = 4.0f;
                self.rateUpdate.canRate = YES;
                self.rateUpdate.starFillColor = [Alert colorFromHexString:@"#F4CC47"];
                
            }
            
        }
        break;
        DEFAULT
        {
            break;
        }
    }
    
}

- (void)calculateAndApplyBestSpacing
{
    CGFloat availableSize = self.collectionViewWidthConstraint.constant;
    CGFloat itemSize = self.collectionViewFlowLayout.itemSize.width;
    CGFloat minimumGutter = 2;
    
    BOOL success = NO;
    
    // No calcuations, so just apply settings
    self.collectionViewFlowLayout.minimumLineSpacing = minimumGutter;
    self.collectionViewFlowLayout.minimumInteritemSpacing = minimumGutter;
    self.collectionViewFlowLayout.itemSize = CGSizeMake(itemSize, itemSize);
    self.collectionViewFlowLayout.sectionInset = UIEdgeInsetsMake(minimumGutter, minimumGutter, minimumGutter, minimumGutter);
    
    self.collectionViewFlowLayoutUser.minimumLineSpacing = minimumGutter;
    self.collectionViewFlowLayoutUser.minimumInteritemSpacing = minimumGutter;
    self.collectionViewFlowLayoutUser.itemSize = CGSizeMake(itemSize, itemSize);
    self.collectionViewFlowLayoutUser.sectionInset = UIEdgeInsetsMake(minimumGutter, minimumGutter, minimumGutter, minimumGutter);
    
    if (!success) {
        
    }
}
-(void) webServiceFailWithApplicationServerMSG:(NSString *)msg
{
    
    [Alert svError:msg];
    
}

// WEB SERVICE FAIL WITH REGION TCP/IP
-(void) webServiceFail:(NSError *)error{
    
    
    [Alert svError:[error localizedDescription]];
    
    
}


#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView == self.collectionViewuser)
    {
        return imageUserArray.count;
    }
    
    else
    {
        return imageBusinessArray.count;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    if (collectionView == self.collectionViewuser)
    {
        UIImageView *imageView = [[UIImageView alloc]init];
        
        [imageView setFrame:CGRectMake(0, 0, cell.frame.size.width, cell.frame.size.height)];
        imageView.layer.cornerRadius = 8.0f;
        imageView.clipsToBounds = YES;
        
        [imageView setImageURL:[NSURL URLWithString:[imageUserArray objectAtIndex:indexPath.row]]];
        
        [cell.contentView addSubview:imageView];
    }
    else
    {
        UIImageView *imageView = [[UIImageView alloc]init];
        
        [imageView setFrame:CGRectMake(0, 0, cell.frame.size.width, cell.frame.size.height)];
        imageView.layer.cornerRadius = 8.0f;
        imageView.clipsToBounds = YES;
        
        [imageView setImageURL:[NSURL URLWithString:[imageBusinessArray objectAtIndex:indexPath.row]]];
        
        [cell.contentView addSubview:imageView];
    }
    return cell;
}

-(IBAction)btnStartJob:(UIButton*)sender
{  // SERVER CONNCETION
    if (!_dict) {
        _dict = [AppDelegate appDelegate].bookingId;
        
    }
    [Alert svProgress:@"Please Wait..."];
    
    WebService * connection = [[WebService alloc]init];
    connection.delegate = self;
    NSMutableDictionary * dict                              = [NSMutableDictionary dictionary];
    
    [dict setObject:@"startjob"                     forKey:kAPI_ACTION];
    if (self.dict[@"bookingID"])
    {
        [dict setObject:[_dict objectForKey:@"bookingID"]                     forKey:@"bookingId"];
    }
    else{
        [dict setObject:[_dict objectForKey:@"bookingId"]                     forKey:@"bookingId"];
    }
    
    NSLog(@"dict is ---%@",dict);
    
    [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:@"startjob"];
    
}
-(IBAction)btnJobCompleted:(UIButton*)sender
{  // SERVER CONNCETION
    
    NSString *str =[[self.dict1  valueForKey:@"jobstart"] stringValue];
    NSString *status = [[[self.dict1 valueForKey:@"invoice"]valueForKey:@"status"] stringValue];
    
    if ([[self.dict1 valueForKey:@"today"] isEqualToString:@"today"])
    {
        
        if ([status isEqualToString:@"1"])
        {
            [Alert svError:@"Payment Pending By User Side!!"];
        }
        else
        {
            [Alert svProgress:@"Please Wait..."];
            
            NSDictionary * dictLogin= [[NSUserDefaults standardUserDefaults]  objectForKey:kLOGIN];
            if (!_dict) {
                _dict = [AppDelegate appDelegate].bookingId;
                
            }
            
            NSString * user_id ;
            if ([dictLogin valueForKey:@"id"] == nil)
            {
                user_id  = [dictLogin valueForKey:@"userId"];
                
            }
            else
            {
                user_id  = [dictLogin valueForKey:@"id"];
            }
            WebService * connection = [[WebService alloc]init];
            connection.delegate = self;
            NSMutableDictionary * dict                              = [NSMutableDictionary dictionary];
            
            [dict setObject:@"changebookingstatus"                     forKey:kAPI_ACTION];
            [dict setObject:user_id                     forKey:@"driverId"];
            if (self.dict[@"bookingID"])
            {
                [dict setObject:[_dict objectForKey:@"bookingID"]                     forKey:@"bookingId"];
            }
            else{
                [dict setObject:[_dict objectForKey:@"bookingId"]                     forKey:@"bookingId"];
            }
            
            [dict setObject:[_dict objectForKey:@"groupId"]                     forKey:@"groupId"];
            
            [dict setObject:@"2"                     forKey:@"status"];
            
            NSLog(@"dict is ---%@",dict);
            [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:@"changebookingstatus"];
        }
        
    }
    else
    {
        
        //    if ([status isEqualToString:@"2"] && [str isEqual:@"1"])
        //    {
        //        [Alert svError:@"Payment Pending By User Side!!"];
        //    }
        
        
        if ([status isEqualToString:@"1"] && [str isEqual:@"1"])
        {
            [Alert svError:@"Payment Pending By User Side!!"];
        }
        else
        {
            [Alert svProgress:@"Please Wait..."];
            
            NSDictionary * dictLogin= [[NSUserDefaults standardUserDefaults]  objectForKey:kLOGIN];
            if (!_dict) {
                _dict = [AppDelegate appDelegate].bookingId;
                
            }
            
            NSString * user_id ;
            if ([dictLogin valueForKey:@"id"] == nil)
            {
                user_id  = [dictLogin valueForKey:@"userId"];
                
            }
            else
            {
                user_id  = [dictLogin valueForKey:@"id"];
            }
            WebService * connection = [[WebService alloc]init];
            connection.delegate = self;
            NSMutableDictionary * dict                              = [NSMutableDictionary dictionary];
            
            [dict setObject:@"changebookingstatus"                     forKey:kAPI_ACTION];
            [dict setObject:user_id                     forKey:@"driverId"];
            if (self.dict[@"bookingID"])
            {
                [dict setObject:[_dict objectForKey:@"bookingID"]                     forKey:@"bookingId"];
            }
            else{
                [dict setObject:[_dict objectForKey:@"bookingId"]                     forKey:@"bookingId"];
            }
            
            [dict setObject:[_dict objectForKey:@"groupId"]                     forKey:@"groupId"];
            
            [dict setObject:@"2"                     forKey:@"status"];
            
            NSLog(@"dict is ---%@",dict);
            [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:@"changebookingstatus"];
        }
    }
    
}

-(IBAction)btnTapped:(UIButton*)sender
{  // SERVER CONNCETION
    
    if ([self.donebtn.titleLabel.text  isEqualToString:@"WORK IN PROGRESS"])
    {
        
        BookingProgressViewController * bookingHistoryViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"BookingProgressViewController");
        bookingHistoryViewController.dict = _dict1;
        UINavigationController * navigationController    = [Alert navigationControllerWithVC:bookingHistoryViewController];
        PERSENT_VIEW_CONTOLLER(navigationController, YES);
        
        
    }
    else
    {
        
        [Alert svProgress:@"Please Wait..."];
        
        NSDictionary * dictLogin= [[NSUserDefaults standardUserDefaults]  objectForKey:kLOGIN];
        if (!_dict) {
            _dict = [AppDelegate appDelegate].bookingId;
            
        }
        
        NSString * user_id ;
        if ([dictLogin valueForKey:@"id"] == nil)
        {
            user_id  = [dictLogin valueForKey:@"userId"];
            
        }
        else
        {
            user_id  = [dictLogin valueForKey:@"id"];
        }
        WebService * connection = [[WebService alloc]init];
        connection.delegate = self;
        NSMutableDictionary * dict                              = [NSMutableDictionary dictionary];
        
        [dict setObject:@"changebookingstatus"                     forKey:kAPI_ACTION];
        [dict setObject:user_id                     forKey:@"driverId"];
        
        if (self.dict[@"bookingID"])
        {
            [dict setObject:[_dict objectForKey:@"bookingID"]                     forKey:@"bookingId"];
        }
        else{
            [dict setObject:[_dict objectForKey:@"bookingId"]                     forKey:@"bookingId"];
        }
        [dict setObject:[_dict objectForKey:@"groupId"]                     forKey:@"groupId"];
        if (sender.tag == 0)
        {
            
            [dict setObject:@"1"                     forKey:@"status"];
            
        }
        else
        {
            [dict setObject:@"3"                     forKey:@"status"];
        }
        
        NSLog(@"dict is ---%@",dict);
        [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:@"changebookingstatus"];
    }
}

- (IBAction)buttonsubmited:(id)sender
{
    if (self.msgtextfiled.text.length>0)
    {
        [Alert svProgress:@"Please Wait..."];
        
        NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults]  objectForKey:kLOGIN];
        
        
        NSString * user_id ;
        if ([dictLogin valueForKey:@"id"] == nil)
        {
            user_id  = [dictLogin valueForKey:@"userId"];
            
        }
        else
        {
            user_id  = [dictLogin valueForKey:@"id"];
        }
        WebService * connection = [[WebService alloc]init];
        connection.delegate = self;
        NSMutableDictionary * dict                              = [NSMutableDictionary dictionary];
        
        [dict setObject:@"submitreview"                     forKey:kAPI_ACTION];
        [dict setObject:user_id                     forKey:@"reviewFrom"];
        [dict setObject:[_dict objectForKey:@"userId"]                     forKey:@"reviewTo"];
        [dict setObject:[NSString stringWithFormat:@"%f", self.rateUpdate.rating]                     forKey:@"star"];
        [dict setObject:self.msgtextfiled.text                     forKey:@"message"];
        
        
        NSLog(@"dict is ---%@",dict);
        [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:@"submitreview"];
        
        
    }
    
    else
    {
        
        [Alert svError:@"Enter Review"];
        
        
    }
    
    
}
-(IBAction)showEarningBtn:(id)sender
{
    
    appDelegate = UIAppDelegate;
    EarningViewController * homePassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"EarningViewController");
    UIAppDelegate.isCall  = YES;
    MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
    [appDelegate manageDrawerRear:menuViewController front:homePassengerViewController];
    
    
}

-(IBAction)showInvoiceBtn:(id)sender
{  // SERVER CONNCETION
    
    NSArray *invoiceArr = [[self.dict1 valueForKey:@"invoice"]valueForKey:@"jsonCode"];
    if (invoiceArr.count== 0)
    {
        [Alert svError:@"Invoice is Not Generate"];
    }
    else
    {
        //        if ([[invoiceArr valueForKey:@"status"]integerValue] == 1)
        //        {
        //            [Alert svError:@"Invoice is Not Generate"];
        //        }
        //        else
        //        {
        NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults]  objectForKey:kLOGIN];
        
        NSString * str = [dictLogin valueForKey:@"role"];
        
        if ([str isEqualToString:@"Partner"])
        {
            
            CompleteDetailViewController * bookingHistoryViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"CompleteDetailViewController");
            bookingHistoryViewController.dict = _dict1;
            UINavigationController * navigationController    = [Alert navigationControllerWithVC:bookingHistoryViewController];
            PERSENT_VIEW_CONTOLLER(navigationController, YES);
        }
        else{
            
            CompleteMemberViewController * bookingHistoryViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"CompleteMemberViewController");
            bookingHistoryViewController.dict = _dict1;
            UINavigationController * navigationController    = [Alert navigationControllerWithVC:bookingHistoryViewController];
            PERSENT_VIEW_CONTOLLER(navigationController, YES);
        }
        
        
    }
    
    
}


-(IBAction)showTrackBtn:(id)sender
{  // SERVER CONNCETION
    
    
    BookingCompletedViewController * bookingHistoryViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"BookingCompletedViewController");
    bookingHistoryViewController.dict = _dict1;
    UINavigationController * navigationController    = [Alert navigationControllerWithVC:bookingHistoryViewController];
    PERSENT_VIEW_CONTOLLER(navigationController, YES);
    
}



-(IBAction)addReviewBtn:(id)sender
{  // SERVER CONNCETION
    
    self.viewaddClick.layer.masksToBounds = NO;
    self.viewaddClick.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    self.viewaddClick.layer.shadowOffset = CGSizeMake(-5.0f, -5.0f);
    self.viewaddClick.layer.shadowOpacity = 1.0f;
    
    self.viewaddClick.hidden= NO;
    _ratingViewClick.hidden=YES;
    self.rateUpdate.starSize = 25;
    self.rateUpdate.rating = 2.5f;
    self.rateUpdate.canRate = YES;
    self.rateUpdate.starFillColor = [Alert colorFromHexString:@"#F4CC47"];
    
}

-(void)rateView:(RateView*)rateView didUpdateRating:(float)rating
{
    self.rateingString = [NSString stringWithFormat:@"%f", rating];
    NSLog(@"rateViewDidUpdateRating: %@",  self.rateingString);
}

-(IBAction)cancelBtn:(id)sender
{  // SERVER CONNCETION
    
    _addReviewClick.hidden=YES;
    self.viewaddClick.hidden= YES;
    _ratingViewClick.hidden=YES;
    
    
}

-(IBAction)reviewList:(id)sender
{
    
    RateListViewController * bookingHistoryViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"RateListViewController");
    
    NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults]  objectForKey:kLOGIN];
    
    NSString * str = [dictLogin valueForKey:@"role"];
    
    if ([str isEqualToString:@"Partner"])
    {
        bookingHistoryViewController.userId = [_dict1 valueForKey:@"vendorId"];
    }
    else
    {
        bookingHistoryViewController.userId = [_dict1 valueForKey:@"userId"];
    }
    
    UINavigationController * navigationController    = [Alert navigationControllerWithVC:bookingHistoryViewController];
    PERSENT_VIEW_CONTOLLER(navigationController, YES);
    
    
}
#pragma mark - Login Button
-(IBAction)buttonReviewItem:(id)sender{
    
    _addReviewClick.hidden=NO;
    _ratingViewClick.hidden=NO;
    
}



-(IBAction)jobDoneBtn:(id)sender
{
    
    if (![_customFiled.text isEqualToString:@""])
    {
        self.tipString = _customFiled.text;
    }
    else
    {
        
    }
    NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults]  objectForKey:kLOGIN];
    //   NSDictionary * dictLogin = [NSKeyedUnarchiver unarchiveObjectWithData:data1];
    
    
    NSString * str = [dictLogin valueForKey:@"role"];
    
    if ([str isEqualToString:@"Customers"])
    {
        
        //        AddMoneyViewController * booking= GET_VIEW_CONTROLLER_STORYBOARD(@"AddMoneyViewController");
        //        booking.matchString = @"Tip";
        //        booking.tipString =  self.tipString;
        //        booking.servicename =  [_dict objectForKey:@"serviceName"];
        //        booking.groupId = [_dict objectForKey:@"groupId"];
        //        booking.priceString =[_dict objectForKey:@"serviceAmount"];
        //        UINavigationController * navigationController    = [Alert navigationControllerWithVC:booking];
        //        PERSENT_VIEW_CONTOLLER(navigationController, YES);
        //
        
    }
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
