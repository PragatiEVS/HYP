//
//  InvoiceDetailViewController.m
//  HYP
//
//  Created by santosh kumar singh on 10/09/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import "InvoiceDetailViewController.h"
#import "InvoiceTableViewCell.h"
#import "PaymentViewController.h"
#import "DashboardViewController.h"
#import "MenuViewController.h"


@interface InvoiceDetailViewController ()
{
    AppDelegate * appDelegate;
    
}
@end

@implementation InvoiceDetailViewController

- (void)viewDidLoad
{
    self.title = @"INVOICE DETAILS";
    
    [super viewDidLoad];
    
    NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults]  objectForKey:kLOGIN];
    
    NSString * str = [dictLogin valueForKey:@"role"];
    
    if ([str isEqualToString:@"Partner"])
    {
        
        [self.donebtn setTitle:@"Payment Done" forState:UIControlStateNormal];
        self.donebtn.userInteractionEnabled = NO;
        
        
        if ([[self.dict valueForKey:@"userImage"] isEqualToString:@""])
        {
            
            NSString *userName = [self.dict valueForKey:@"userName"];
            [self.userBg setImageWithString:userName color:nil circular:YES];
            
        }
        else
        {
            
            [self.userBg setImageURL:[NSURL URLWithString:[self.dict valueForKey:@"userImage"]]];
        }
        
        
        self.address.numberOfLines=2;
        
        self.username.text =[self.dict valueForKey:@"userName"];
        self.phone.text =[self.dict valueForKey:@"userPhone"];
        self.address.text =[self.dict valueForKey:@"address"];
        
    }
    else
    {
        if (!_notificationDict)
        {
            invoiceArr = [[self.dict valueForKey:@"invoice"]valueForKey:@"jsonCode"];
            
            self.datetimeLabel.text= [NSString stringWithFormat:@"%@", [self.dict valueForKey:@"bookingDate"]];
            
            [self.amountbtn setTitle:[NSString stringWithFormat:@"Total Price: $%.2f", [[[self.dict valueForKey:@"invoice"]valueForKey:@"totalPrice"]floatValue]] forState:UIControlStateNormal];
            
            if ([[[[self.dict valueForKey:@"invoice"]valueForKey:@"status"]stringValue] isEqualToString:@"1"])
            {
                [self.donebtn setTitle:@"Confirm & Pay Now" forState:UIControlStateNormal];
                self.donebtn.userInteractionEnabled = YES;
                [self.donebtn addTarget:self action:@selector(buttonConfirmClick)
                       forControlEvents:UIControlEventTouchUpInside];
                
            }
            else
            {
                [self.donebtn setTitle:@"Home" forState:UIControlStateNormal];
                self.donebtn.userInteractionEnabled = YES;
                [self.donebtn addTarget:self action:@selector(clickHomeBtn)
                       forControlEvents:UIControlEventTouchUpInside];
                
            }
            
            if (!self.dict[@"image"])
            {
                if ([[self.dict valueForKey:@"vendorImage"] isEqualToString:@""])
                {
                    
                    NSString *userName = [self.dict valueForKey:@"vendorName"];
                    [self.userBg setImageWithString:userName color:nil circular:YES];
                    
                }
                else
                {
                    
                    [self.userBg setImageURL:[NSURL URLWithString:[self.dict valueForKey:@"vendorImage"]]];
                }
            }
            else
            {
                if ([[self.dict valueForKey:@"image"] isEqualToString:@""])
                {
                    
                    NSString *userName = [self.dict valueForKey:@"fullName"];
                    [self.userBg setImageWithString:userName color:nil circular:YES];
                    
                }
                else
                {
                    
                    [self.userBg setImageURL:[NSURL URLWithString:[self.dict valueForKey:@"image"]]];
                }
            }
            
            
            self.address.numberOfLines=2;
            if (!self.dict[@"vendorName"])
            {
                self.username.text =[self.dict valueForKey:@"fullName"];
            }
            else
            {
                self.username.text =[self.dict valueForKey:@"vendorName"];
            }
            if (!self.dict[@"vendorPhone"])
            {
                self.phone.text =[self.dict valueForKey:@"contactNumber"];
            }
            else
            {
                self.phone.text =[self.dict valueForKey:@"vendorPhone"];
            }
            if (!self.dict[@"address"])
            {
                self.address.text =[self.dict valueForKey:@"vendorAddress"];
            }
            else
            {
                self.address.text =[self.dict valueForKey:@"address"];
            }
            
            // self.phone.text =[self.dict valueForKey:@"vendorPhone"];
            //self.address.text =[self.dict valueForKey:@"vendorAddress"];
        }
        
        else
        {
            
            NSString *colorArray = [self.notificationDict valueForKey:@"invoice"];
            invoiceArr = [NSJSONSerialization JSONObjectWithData:[colorArray dataUsingEncoding:NSUTF8StringEncoding]
                                                         options:0 error:NULL];
            NSLog(@"jsonObject=%@", invoiceArr);
            
            
            self.datetimeLabel.text= [NSString stringWithFormat:@"%@", [self.notificationDict valueForKey:@"bookingDate"]];
            
            [self.amountbtn setTitle:[NSString stringWithFormat:@"Total Price: $%.2f", [[self.notificationDict valueForKey:@"totalPrice"]floatValue]] forState:UIControlStateNormal];
            
            if ([[self.notificationDict valueForKey:@"message"] containsString:@"sent you invoice"])
            {
                [self.donebtn setTitle:@"Confirm & Pay Now" forState:UIControlStateNormal];
                self.donebtn.userInteractionEnabled = YES;
                [self.donebtn addTarget:self action:@selector(buttonConfirmClick)
                       forControlEvents:UIControlEventTouchUpInside];
                
                
            }
            else
            {
                [self.donebtn setTitle:@"Home" forState:UIControlStateNormal];
                self.donebtn.userInteractionEnabled = YES;
                [self.donebtn addTarget:self action:@selector(clickHomeBtn)
                       forControlEvents:UIControlEventTouchUpInside];
                
            }
                if ([[self.notificationDict valueForKey:@"vendorImage"] isEqualToString:@""])
                {
                    
                    NSString *userName = [self.notificationDict valueForKey:@"vendorFullName"];
                    [self.userBg setImageWithString:userName color:nil circular:YES];
                    
                }
                else
                {
                    
                    [self.userBg setImageURL:[NSURL URLWithString:[self.notificationDict valueForKey:@"vendorImage"]]];
                }
                
                
                self.address.numberOfLines=2;
                
                self.username.text =[self.notificationDict valueForKey:@"vendorFullName"];
                self.phone.text =[self.notificationDict valueForKey:@"vendorPhone"];
                // self.address.text =[self.notificationDict valueForKey:@"address"];
                
           
            
        }
    }
    
    self.userBg.layer.cornerRadius = self.userBg.frame.size.width/2;
    self.userBg.clipsToBounds = YES;
    
    
    [self navigationBarConfiguration];
    // Do any additional setup after loading the view.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    
    return invoiceArr.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static  NSString *identifierDriver = @"InvoiceTableViewCell";
    
    InvoiceTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifierDriver forIndexPath:indexPath];
    
    cell.servicesName.text = [[invoiceArr valueForKey:@"itemName"]objectAtIndex:indexPath.row];
    
    cell.servicesName.numberOfLines = 2;
    cell.invoiceDiscription.numberOfLines = 3;
    cell.invoiceDiscription.text = [[invoiceArr valueForKey:@"description"]objectAtIndex:indexPath.row];
    
    cell.servicesAmount.text = [NSString stringWithFormat:@"$%@",[[invoiceArr valueForKey:@"price"]objectAtIndex:indexPath.row]];
    
    
    return cell;
    
}


#pragma mark - Configure Navigation Controller
-(void)navigationBarConfiguration
{
    
    self.navigationController.navigationBarHidden = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    UIButton * menuButton  = [UIButton buttonWithType:UIButtonTypeSystem];
    menuButton.frame = CGRectMake(8, 18, 24, 24);
    [menuButton setBackgroundImage:[UIImage imageNamed:IMG_BACK] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *accountBarItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    self.navigationItem.leftBarButtonItem = accountBarItem;
    
    NSMutableArray *arrRightBarItems = [[NSMutableArray alloc] init];
    UIButton *btnSetting = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnSetting setImage:[UIImage imageNamed:@"Frequency"] forState:UIControlStateNormal];
    btnSetting.frame = CGRectMake(0, 0, 40, 40);
    btnSetting.showsTouchWhenHighlighted=YES;
    [btnSetting addTarget:self action:@selector(onSettings:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSetting];
    
    UIButton *btnLib = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnLib setImage:[UIImage imageNamed:@"logo1.png"] forState:UIControlStateNormal];
    btnLib.frame = CGRectMake(0, 0, 40, 40);
    btnLib.showsTouchWhenHighlighted=YES;
    [btnLib addTarget:self action:@selector(onMyLibrary:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButtonItem2 = [[UIBarButtonItem alloc] initWithCustomView:btnLib];
    [arrRightBarItems addObject:barButtonItem2];
    [arrRightBarItems addObject:barButtonItem];
    self.navigationItem.rightBarButtonItems=arrRightBarItems;
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor],
    }];
    
}
-(void)onMyLibrary:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}
-(void)onSettings:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}


-(void)back:(id)sender
{
     if (!self.notificationDict)
       {
           
           DISMISS_VIEW_CONTROLLER;
       }
       else
       {
           
           KYDrawerController *elDrawer = (KYDrawerController*)self.navigationController.parentViewController;
           [elDrawer setDrawerState:KYDrawerControllerDrawerStateOpened animated:YES];
           
           
       }
       
}

-(void)buttonConfirmClick
{
    
    PaymentViewController * bookingHistoryViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"PaymentViewController");
    if (_notificationDict)
    {
        bookingHistoryViewController.dict = self.notificationDict;
        bookingHistoryViewController.groupId = [self.notificationDict valueForKey:@"bookingId"];
        bookingHistoryViewController.invoiceId = [self.notificationDict valueForKey:@"invoiceId"];
        bookingHistoryViewController.ammount = [self.notificationDict valueForKey:@"totalPrice"];
        
    }
    else{
        bookingHistoryViewController.dict = self.dict;
        bookingHistoryViewController.groupId = [self.dict valueForKey:@"bookingID"];
        bookingHistoryViewController.invoiceId = [[self.dict valueForKey:@"invoice"]valueForKey:@"invoiceId"];
        bookingHistoryViewController.ammount = [[self.dict valueForKey:@"invoice"]valueForKey:@"totalPrice"];
    }
    UINavigationController * navigationController    = [Alert navigationControllerWithVC:bookingHistoryViewController];
    PERSENT_VIEW_CONTOLLER(navigationController, YES);
    
}
-(void)clickHomeBtn
{  // SERVER CONNCETION
    
    appDelegate = UIAppDelegate;
    DashboardViewController * homePassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBID_ARRIVED_VC);
    UIAppDelegate.isCall  = YES;
    MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
    [appDelegate manageDrawerRear:menuViewController front:homePassengerViewController];
    
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
