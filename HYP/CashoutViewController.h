//
//  CashoutViewController.h
//  GetSum
//
//  Created by santosh kumar singh on 30/04/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface CashoutViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
@property (weak, nonatomic) IBOutlet UIImageView *dummyImage;
@property (weak, nonatomic) IBOutlet UITextField *textamount;
@property (weak, nonatomic) IBOutlet UILabel *textwalletamount;
@property (weak, nonatomic) IBOutlet UILabel * balanceLabel;

@property (weak, nonatomic) IBOutlet UILabel * withdrawbalanceLabel;

@property (strong, nonatomic) NSString * priceString;
@end


