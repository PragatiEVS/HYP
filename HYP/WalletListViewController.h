//
//  WalletListViewController.h
//  GetSum
//
//  Created by santosh kumar singh on 25/04/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface WalletListViewController : UIViewController
{
     NSMutableArray *resultArray;
     int startidx;
     NSArray *arr;
}
@property (weak, nonatomic) IBOutlet UILabel * noListAvalible;
@property (weak, nonatomic) IBOutlet UILabel * balanceLabel;
@property(weak, nonatomic) IBOutlet UITableView *completedTableView;

@property (weak) IBOutlet NSLayoutConstraint * widthConstraint;

@property (weak, nonatomic) IBOutlet UILabel * challangeName;
@property (weak, nonatomic) IBOutlet UILabel * challengeAmount;
@property (weak, nonatomic) IBOutlet UILabel * senderName;
@property (weak, nonatomic) IBOutlet UILabel * challengeType;
@property (weak, nonatomic) IBOutlet UILabel * termWager;
@property (weak, nonatomic) IBOutlet UILabel * tesmscondtion;
@property (weak, nonatomic) IBOutlet UIView  * challengeDetal;
@property (weak, nonatomic) IBOutlet UILabel  * status;
@property (weak, nonatomic) IBOutlet UIButton * closebtn;
@end


