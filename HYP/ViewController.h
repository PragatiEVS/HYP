//
//  ViewController.h
//  HYP
//
//  Created by santosh kumar singh on 29/08/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController


@property(nonatomic,weak) IBOutlet UIButton     * providerSignUp;
@property(nonatomic,weak) IBOutlet UIButton     * userSignUp;
@property(nonatomic,weak) IBOutlet UIButton     * affiliateSignUp;
@end

