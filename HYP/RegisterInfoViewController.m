//
//  RegisterInfoViewController.m
//  Q-Workerbee
//
//  Created by santosh kumar singh on 09/05/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import "RegisterInfoViewController.h"
#import "ServicesViewController.h"
#import "DashboardViewController.h"
#import "MembershipViewController.h"
#import "MenuViewController.h"




@interface RegisterInfoViewController ()<WebServiceDelegate,UIActionSheetDelegate,WSCalendarViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,GMSAutocompleteViewControllerDelegate,UIPickerViewDelegate,UIPickerViewDataSource>
{
    AppDelegate *appDelegate;
}

@end

@implementation RegisterInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title=@"COMPLETE PROFILE";
    
    self.buttonUpdate.layer.cornerRadius = 5.0f;
    self.buttonUpdate.clipsToBounds = YES;
    
    self.view.backgroundColor = [Alert colorWithPatternImage2:kBG_IMAGE];
    
    self.longi = [AppDelegate appDelegate].latitude;
    self.lat = [AppDelegate appDelegate].longitude;
    
    
    
    
    calendarView = [[[NSBundle mainBundle] loadNibNamed:@"WSCalendarView" owner:self options:nil] firstObject];
    calendarView.backgroundColor=[UIColor whiteColor];
    calendarView.tappedDayBackgroundColor=[UIColor blackColor];
    calendarView.calendarStyle = WSCalendarStyleDialog;
    calendarView.isShowEvent=false;
    [calendarView setupAppearance];
    [self.view addSubview:calendarView];
    
    calendarView.delegate=self;
    [self configure];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    
    if ([self.match isEqualToString:@"Edit"])
    {
        self.serviceConstraint.constant = 0;
    }
    else
    {
        NSString * service1 = [[NSUserDefaults standardUserDefaults] stringForKey:@"SelectedServicesName"];
        
        self.serviceProvText.text = service1;
    }
}

#pragma mark - configure
-(void)configure
{
    
    NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults]  objectForKey:kLOGIN];
    // NSDictionary * dictLogin = [NSKeyedUnarchiver unarchiveObjectWithData:data1];
    
    if ([[dictLogin objectForKey:@"Baddress"] isEqualToString:@""]) {
        
        CLLocation * upadetLocation = [[CLLocation alloc] initWithLatitude:[[AppDelegate appDelegate].latitude floatValue] longitude:[[AppDelegate appDelegate].longitude floatValue]];
        [self reverGeoCodingUsingGoogle:upadetLocation];
        
    }
    else
    {
        self.addressText.text =[dictLogin objectForKey:@"Baddress"];
        self.longi = [dictLogin objectForKey:@"longitude"];
           self.lat = [dictLogin objectForKey:@"latitude"];
          
        
    }
    
    
    self.firstNametext.text =[dictLogin objectForKey:@"BFirstname"];
    self.dobtextfield.text =[dictLogin objectForKey:@"dob"];
    self.lastNameTextfield.text =[dictLogin objectForKey:@"BLastName"];
    self.businessNameText.text =[dictLogin objectForKey:@"businessName"];
    self.socialSNText.text =[dictLogin objectForKey:@"SSN"];
    self.phoneText.text = [dictLogin objectForKey:@"BTelephoneNumber"];
    self.addressText.text =[dictLogin objectForKey:@"Baddress"];
    self.genderText.text =[dictLogin objectForKey:@"gender"];
    self.iTINText.text =[dictLogin objectForKey:@"EIN"];
    self.licenseText.text =[dictLogin objectForKey:@"areYouLicence"];
    self.insuranceText.text =[dictLogin objectForKey:@"areYouInsured"];
    
    if ([[[dictLogin objectForKey:@"stateId"] stringValue] isEqualToString:@""]) {
        
    }
    else
    {
        for ( int i=0; i<=[AppDelegate appDelegate].stateArr.count; i++)
        {
            
            if ([[[AppDelegate appDelegate].stateArr objectAtIndex:i] valueForKey:@"id"]   == [dictLogin objectForKey:@"stateId"])
            {
                self.stateId = [[[AppDelegate appDelegate].stateArr objectAtIndex:i] valueForKey:@"id"];
                self.stateText.text = [[[AppDelegate appDelegate].stateArr objectAtIndex:i]valueForKey:@"name"];
                break;
            }
        }
    }
    
    if ([[dictLogin objectForKey:@"areYouLicence"] isEqualToString:@"Yes"])
    {
        
        self.driverConstraint.constant = 133;
        
    }
    else{
        self.driverConstraint.constant = 0;
    }
    if ([[dictLogin objectForKey:@"areYouInsured"] isEqualToString:@"Yes"])
    {
        self.insuranceConstraint.constant = 133;
        
    }
    else{
        self.insuranceConstraint.constant = 0;
    }
    
    
    
    if ([[dictLogin objectForKey:@"drivlingImage"] isEqualToString:@""])
    {
        
        
    }
    else
    {
        imageDataDriver= [NSData dataWithContentsOfURL:[NSURL  URLWithString:[dictLogin objectForKey:@"drivlingImage"]]];
        
        [self.drivngImageViewFront setBackgroundImage:[UIImage imageWithData:imageDataDriver] forState:UIControlStateNormal];
        
    }
    if ([[dictLogin objectForKey:@"image"] isEqualToString:@""])
    {
        
        
    }
    else
    {
        imageDataAuto = [NSData dataWithContentsOfURL:[NSURL  URLWithString:[dictLogin objectForKey:@"image"]]];
        
        [self.autoImageViewFront setBackgroundImage:[UIImage imageWithData:imageDataAuto] forState:UIControlStateNormal];
        
    }
    
    if ([[dictLogin objectForKey:@"AutoInsurance"] isEqualToString:@""])
    {
        
        
    }
    else
    {
        imageDataProfile= [NSData dataWithContentsOfURL:[NSURL  URLWithString:[dictLogin objectForKey:@"AutoInsurance"]]];
        
        [self.drivngProfileImage setBackgroundImage:[UIImage imageWithData:imageDataProfile] forState:UIControlStateNormal];
        
    }
    
    
    self.autoImageViewFront.layer.borderColor=[[UIColor colorWithRed:0 green:0 blue:0 alpha:1.0f]CGColor];
    self.autoImageViewFront.layer.borderWidth= 0.50f;
    self.autoImageViewFront.layer.cornerRadius = 5.0f;
    self.autoImageViewFront.clipsToBounds= YES;
    
    self.drivngProfileImage.layer.cornerRadius = 5.0f;
    self.drivngProfileImage.clipsToBounds= YES;
    
    self.drivngImageViewFront.layer.cornerRadius = 5.0f;
    self.drivngImageViewFront.clipsToBounds= YES;
    
    self.firstNametext.layer.borderColor=[UIColor blackColor].CGColor;
    self.firstNametext.layer.cornerRadius = 5.0f;
    self.firstNametext.layer.borderWidth= 0.50f;
    self.firstNametext.clipsToBounds= YES;
    
    self.serviceProvText.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 30);
    self.dobtextfield.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 30);
    self.addressText.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 30);
    self.firstNametext.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 30);
    self.genderText.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 30);
    self.socialSNText.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 30);
    self.lastNameTextfield.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 30);
    self.iTINText.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 30);
    self.insuranceText.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 30);
    self.licenseText.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 30);
    self.phoneText.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 30);
    self.dobtextfield.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 30);
    self.stateText.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 30);
    self.businessNameText.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 30);
    
    self.dobtextfield.layer.borderWidth = 0.5f;
    self.dobtextfield.layer.borderColor = [UIColor blackColor].CGColor;
    
    self.serviceProvText.layer.borderWidth = 0.5f;
    self.serviceProvText.layer.borderColor = [UIColor blackColor].CGColor;
    
    self.addressText.layer.borderWidth = 0.5f;
    self.addressText.layer.borderColor = [UIColor blackColor].CGColor;
    
    self.firstNametext.layer.borderWidth = 0.5f;
    self.firstNametext.layer.borderColor = [UIColor blackColor].CGColor;
    
    self.genderText.layer.borderWidth = 0.5f;
    self.genderText.layer.borderColor = [UIColor blackColor].CGColor;
    
    self.socialSNText.layer.borderWidth = 0.5f;
    self.socialSNText.layer.borderColor = [UIColor blackColor].CGColor;
    
    self.iTINText.layer.borderWidth = 0.5f;
    self.iTINText.layer.borderColor = [UIColor blackColor].CGColor;
    
    self.businessNameText.layer.borderWidth = 0.5f;
    self.businessNameText.layer.borderColor = [UIColor blackColor].CGColor;
    
    self.stateText.layer.borderWidth = 0.5f;
    self.stateText.layer.borderColor = [UIColor blackColor].CGColor;
    
    self.phoneText.layer.borderWidth = 0.5f;
    self.phoneText.layer.borderColor = [UIColor blackColor].CGColor;
    
    self.licenseText.layer.borderWidth = 0.5f;
    self.licenseText.layer.borderColor = [UIColor blackColor].CGColor;
    
    self.insuranceText.layer.borderWidth = 0.5f;
    self.insuranceText.layer.borderColor = [UIColor blackColor].CGColor;
    
    self.businessNameText.layer.borderWidth = 0.5f;
    self.businessNameText.layer.borderColor = [UIColor blackColor].CGColor;
    
    
    [Alert setKeyBoardToolbar:@[self.businessNameText,self.firstNametext,self.lastNameTextfield,self.socialSNText,self.iTINText,self.phoneText]];
    
    self.dobtextfield.layer.cornerRadius=8;
    self.serviceProvText.layer.cornerRadius=8;
    self.addressText.layer.cornerRadius=8;
    self.firstNametext.layer.cornerRadius=8;
    self.genderText.layer.cornerRadius=8;
    self.socialSNText.layer.cornerRadius=8;
    self.lastNameTextfield.layer.cornerRadius=8;
    self.iTINText.layer.cornerRadius=8;
    self.insuranceText.layer.cornerRadius=8;
    self.licenseText.layer.cornerRadius=8;
    self.phoneText.layer.cornerRadius=8;
    self.dobtextfield.layer.cornerRadius=8;
    self.businessNameText.layer.cornerRadius=8;
    self.stateText.layer.cornerRadius=8;
    
    self.dobtextfield.clipsToBounds = YES;
    self.serviceProvText.clipsToBounds = YES;
    self.addressText.clipsToBounds = YES;
    self.firstNametext.clipsToBounds = YES;
    self.genderText.clipsToBounds = YES;
    self.socialSNText.clipsToBounds = YES;
    self.lastNameTextfield.clipsToBounds = YES;
    self.iTINText.clipsToBounds = YES;
    self.insuranceText.clipsToBounds = YES;
    self.licenseText.clipsToBounds = YES;
    self.phoneText.clipsToBounds = YES;
    self.dobtextfield.clipsToBounds = YES;
    self.stateText.clipsToBounds = YES;
    self.businessNameText.clipsToBounds = YES;
    
    [self navigationBarConfiguration];
}

-(void)reverGeoCodingUsingGoogle:(CLLocation *)location
{
    [[GMSGeocoder geocoder] reverseGeocodeCoordinate:CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude) completionHandler:^(GMSReverseGeocodeResponse* response, NSError* error)
     {
        
        GMSReverseGeocodeResult *result = response.firstResult;
        
        NSArray   * arrayAdd = result.lines;
        
        NSString  * address1;
        NSString  * address2;
        
        if (arrayAdd.count>1)
        {
            address1 = result.lines[0];
            address2 = result.subLocality;
        }
        else
        {
            address1 = result.lines[0];
        }
        
        NSLog(@"coordinate.latitude=%f", result.coordinate.latitude);
               NSLog(@"coordinate.longitude=%f", result.coordinate.longitude);
           
        self.lat = [NSString stringWithFormat:@"%f",result.coordinate.latitude];
        
        self.longi = [NSString stringWithFormat:@"%f",result.coordinate.longitude];
              
        self.addressText.text = address1;
        
        
    }];
    
    
}

-(void)navigationBarConfiguration
{
    
    self.navigationController.navigationBarHidden = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.navigationController.navigationBarHidden = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    UIButton * menuButton  = [UIButton buttonWithType:UIButtonTypeSystem];
    menuButton.frame = CGRectMake(8, 18, 24, 26);
    [menuButton setImage:[UIImage imageNamed:IMG_BACK] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *accountBarItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    self.navigationItem.leftBarButtonItem = accountBarItem;
    
    NSMutableArray *arrRightBarItems = [[NSMutableArray alloc] init];
     UIButton *btnSetting = [UIButton buttonWithType:UIButtonTypeCustom];
     [btnSetting setImage:[UIImage imageNamed:@"Frequency"] forState:UIControlStateNormal];
     btnSetting.frame = CGRectMake(0, 0, 50, 50);
     btnSetting.showsTouchWhenHighlighted=YES;
     [btnSetting addTarget:self action:@selector(onSettings:) forControlEvents:UIControlEventTouchUpInside];
     UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSetting];
     
     UIButton *btnLib = [UIButton buttonWithType:UIButtonTypeCustom];
     [btnLib setImage:[UIImage imageNamed:@"logo1.png"] forState:UIControlStateNormal];
     btnLib.frame = CGRectMake(0, 0, 50, 40);
     btnLib.showsTouchWhenHighlighted=YES;
     [btnLib addTarget:self action:@selector(onMyLibrary:) forControlEvents:UIControlEventTouchUpInside];
     UIBarButtonItem *barButtonItem2 = [[UIBarButtonItem alloc] initWithCustomView:btnLib];
     [arrRightBarItems addObject:barButtonItem2];
     [arrRightBarItems addObject:barButtonItem];
     self.navigationItem.rightBarButtonItems=arrRightBarItems;
     
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor],
    }];
    
}

-(void)onMyLibrary:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}

-(void)onSettings:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/hyptunesradio";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}

-(void)back:(id)sender
{
    DISMISS_VIEW_CONTROLLER;
}


#pragma mark - button UserImage Click
- (IBAction)addImage:(UIButton*)btn
{
    UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:@"Select image from"
                                                        delegate:self
                                               cancelButtonTitle:@"Cancel"
                                          destructiveButtonTitle:nil
                                               otherButtonTitles:@"From camera",@"From library", nil];
    
    [action showInView:self.view];
    if (btn.tag ==2)
    {
        self.checkImageFrom =@"Driver";
    }
    else if (btn.tag ==0)
    {
        self.checkImageFrom =@"Insurance";
    }
    else
    {
        self.checkImageFrom=@"";
    }
    
    
    
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    if (valrible==0)
    {
        self.genderText.text = [AppDelegate appDelegate].genderArr[row];
        
    }
    if (valrible==1)
    {
        self.stateText.text = [[AppDelegate appDelegate].stateArr[row]valueForKey:@"name"];
        self.stateId =[[AppDelegate appDelegate].stateArr[row]valueForKey:@"id"];
    }
    if (valrible==2)
    {
        self.licenseText.text = [AppDelegate appDelegate].yesNoArr[row];
        if ( [self.licenseText.text isEqualToString:@"Yes"])
        {
            
            self.driverConstraint.constant = 133;
        }
        else
        {
            self.driverConstraint.constant = 0;
        }
        
    }
    if (valrible==3)
    {
        self.insuranceText.text = [AppDelegate appDelegate].yesNoArr[row];
        if ( [self.insuranceText.text isEqualToString:@"Yes"])
        {
            self.insuranceConstraint.constant = 133;
        }
        else{
            self.insuranceConstraint.constant = 0;
        }
    }
    
    
    [self.dienstPicker setHidden:YES];
    [self.donePicker setHidden:YES];
}

#pragma mark - UIPickerViewDataSource
-(IBAction)done:(id)sender
{
    [self.dienstPicker setHidden:YES];
    [self.donePicker setHidden:YES];
}


#pragma mark - UIPickerViewDataSource


-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// #4
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    
    if (valrible==0)
    {
        return  [AppDelegate appDelegate].genderArr.count;
    }
    else if (valrible==1)
    {
        return  [AppDelegate appDelegate].stateArr.count;
    }
    else  if (valrible==2)
    {
        return  [AppDelegate appDelegate].yesNoArr.count;
    }
    else  if (valrible==3)
    {
        return  [AppDelegate appDelegate].yesNoArr.count;
    }
    else
    {
        return 0;
    }
    
}

#pragma mark - UIPickerViewDelegate

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (valrible==0)
    {
        return  [AppDelegate appDelegate].genderArr[row];;
    }
    else  if (valrible==1)
    {
        return  [[AppDelegate appDelegate].stateArr[row]valueForKey:@"name"];
    }
    else  if (valrible==2)
    {
        return  [AppDelegate appDelegate].yesNoArr[row];
    }
    else  if (valrible==3)
    {
        return  [AppDelegate appDelegate].yesNoArr[row];
    }
    else
    {
        return 0;
    }
    
}

- (IBAction)serviceSelect:(UIButton*)sender
{
    
    ServicesViewController * homePassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDSaerchVC);
    UINavigationController * navigationController    = [Alert navigationControllerWithVC:homePassengerViewController];
    PERSENT_VIEW_CONTOLLER(navigationController, YES);
    
}

- (IBAction)showPickerView:(UIButton*)sender
{
    if (sender.tag==0)
    {
        valrible = 0;
    }
    if (sender.tag==1)
    {
        valrible = 1;
    }
    if (sender.tag==2)
    {
        valrible = 2;
    }
    if (sender.tag==3)
    {
        valrible = 3;
    }
    [self.dienstPicker setHidden:NO];
    [self.donePicker setHidden:NO];
    self.dienstPicker.delegate = self;     //#2
    self.dienstPicker.dataSource = self;
    
    [self.dienstPicker reloadAllComponents];
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if( buttonIndex == 0 ) {
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            [self cameraUsingiPad];
            return;
        }
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            UIImagePickerController *pickerView =[[UIImagePickerController alloc]init];
            pickerView.allowsEditing = YES;
            pickerView.delegate=self;
            pickerView.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:pickerView animated:YES completion:nil];
        }
    }else if( buttonIndex == 1 ) {
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            [self libraryUsingiPad];
            return;
        }
        
        UIImagePickerController *pickerView = [[UIImagePickerController alloc] init];
        pickerView.allowsEditing = YES;
        pickerView.delegate=self;
        [pickerView setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        [self presentViewController:pickerView animated:YES completion:nil];
    }
}

-(void) cameraUsingiPad {
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.allowsEditing = NO;
        picker.modalPresentationStyle = self.popOver;
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:picker];
            [popover presentPopoverFromRect:CGRectMake(self.view.frame.size.width, self.view.frame.size.height, self.view.frame.size.width, 400) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
            self.popOver = popover;
        }];
    }
}

-(void)libraryUsingiPad {
    
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        UIImagePickerController *pickerView = [[UIImagePickerController alloc] init];
        pickerView.allowsEditing = YES;
        pickerView.delegate=self;
        [pickerView setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        
        pickerView.modalPresentationStyle = self.popOver;
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:pickerView];
            [popover presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2-200, self.view.frame.size.height/2 - 300, 400, 400) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
            self.popOver = popover;
        }];
    }
}

- (void)imagePickerController:(UIImagePickerController *)imagePicker
        didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    if ([_checkImageFrom isEqualToString:@"Driver"])
    {
        imageDataDriver =  UIImageJPEGRepresentation(image, 1.0);
        [self.drivngImageViewFront  setBackgroundImage:image forState:UIControlStateNormal] ;
        
    }
    else if ([_checkImageFrom isEqualToString:@"Insurance"])
    {
        imageDataProfile =  UIImageJPEGRepresentation(image, 1.0);
        [self.drivngProfileImage  setBackgroundImage:image forState:UIControlStateNormal] ;
        
    }
    else
    {
        imageDataAuto =  UIImageJPEGRepresentation(image, 1.0);
        [self.autoImageViewFront setBackgroundImage:image forState:UIControlStateNormal];
        
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    if (self.serviceProvText.editing == YES)
    {
        
        ServicesViewController * homePassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDSaerchVC);
        UINavigationController * navigationController    = [Alert navigationControllerWithVC:homePassengerViewController];
        PERSENT_VIEW_CONTOLLER(navigationController, YES);
        
    }
    if (self.dobtextfield.editing == YES)
    {
        [self.dobtextfield resignFirstResponder];
        [calendarView ActiveCalendar:self.dobtextfield];
        //           ServicesViewController * homePassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDSaerchVC);
        //           UINavigationController * navigationController    = [Alert navigationControllerWithVC:homePassengerViewController];
        //           PERSENT_VIEW_CONTOLLER(navigationController, YES);
        
    }
    
    if (self.addressText.editing == YES)
    {
        
        [self.addressText resignFirstResponder];
        
        GMSAutocompleteViewController *acController = [[GMSAutocompleteViewController alloc] init];
        acController.delegate = self;
        [[UISearchBar appearance] setBarStyle:UIBarStyleBlack];
        [self presentViewController:acController animated:YES completion:nil];
    }
}
// Handle the user's selection.
- (void)viewController:(GMSAutocompleteViewController *)viewController
didAutocompleteWithPlace:(GMSPlace *)place {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    // Do something with the selected place.
    NSLog(@"Place name %@", place.name);
    
    NSLog(@"Place address %@", place.formattedAddress);
    NSLog(@"Place latitude %f", place.coordinate.latitude);
    NSLog(@"Place longitude %f", place.coordinate.longitude);
    
    self.addressText.text = place.formattedAddress;
    
    self.lat = [NSString stringWithFormat:@"%f",place.coordinate.latitude];
    self.longi = [NSString stringWithFormat:@"%f",place.coordinate.longitude];
    
    
    
}
- (void)viewController:(GMSAutocompleteViewController *)viewController
didFailAutocompleteWithError:(NSError *)error {
    // TODO: handle the error.
    NSLog(@"error: %@", error);
    
    NSLog(@"error: %ld", [error code]);
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

// User canceled the operation.
- (void)wasCancelled:(GMSAutocompleteViewController *)viewController {
    NSLog(@"Autocomplete was cancelled.");
    
    
    [self dismissViewControllerAnimated:YES completion:nil];
}



-(void)didTapLabel:(WSLabel *)lblView withDate:(NSDate *)selectedDate
{
    
}

-(void)deactiveWSCalendarWithDate:(NSDate *)selectedDate
{
    NSDateFormatter *monthFormatter=[[NSDateFormatter alloc] init];
    [monthFormatter setDateFormat:@"MM/dd/yyyy"];
    NSString *str=[monthFormatter stringFromDate:selectedDate];
    
    self.dobtextfield.text = str;
}

- (IBAction)buttonSubmitPasswd:(id)sender
{
    
    
    if(self.dobtextfield.text.length>0   &&
       self.socialSNText.text.length>0   &&
       self.genderText.text.length>0 &&
       self.firstNametext.text.length>0 &&
       self.addressText.text.length>0 &&
       self.iTINText.text.length>0   &&
       self.phoneText.text.length>0   &&
       self.insuranceText.text.length>0 &&
       self.licenseText.text.length>0 &&
       self.stateText.text.length>0 &&
       self.genderText.text.length>0 &&
       self.firstNametext.text.length>0 &&
       self.businessNameText.text.length>0 &&
       self.lastNameTextfield.text.length>0)
    {
        if ([self.match isEqualToString:@"Edit"])
        {
            if (imageDataDriver == nil && imageDataProfile == nil && imageDataAuto == nil)
            {
                
                [Alert alertControllerTitle:kAPPICATION_TITLE msg:@"Please Select All Image" ok:kOK controller:self.navigationController];
                
            }
            else
            {
                
                [self addCheckServices];
            }
        }
        else
        {
            if(self.serviceProvText.text.length>0)
            {
                if (imageDataDriver == nil && imageDataProfile == nil && imageDataAuto == nil)
                {
                    
                    [Alert alertControllerTitle:kAPPICATION_TITLE msg:@"Please Select All Image" ok:kOK controller:self.navigationController];
                    
                }
                else
                {
                    
                    [self addCheckServices];
                }
            }
            else{
                [Alert alertControllerTitle:kAPPICATION_TITLE msg:kEmptyFields ok:kOK controller:self.navigationController];
            }
            
        }
        
        
    }
    else
    {
        [Alert alertControllerTitle:kAPPICATION_TITLE msg:kEmptyFields ok:kOK controller:self.navigationController];
    }
    
    
}

-(void)addCheckServices
{
    
    [Alert svProgress:@"Please Wait..."];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // time-consuming task
        
        NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
        
        NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults]  objectForKey:kLOGIN];
        
        NSString * user_id ;
        if ([dictLogin valueForKey:@"id"] == nil)
        {
            user_id  = [dictLogin valueForKey:@"userId"];
            
        }
        else
        {
            user_id  = [dictLogin valueForKey:@"id"];
        }
        
        NSString * service = [[NSUserDefaults standardUserDefaults] stringForKey:@"SelectedServices"];
        
        
        [_params setObject:@"editprofile"  forKey:@"action"];
        [_params setObject:user_id  forKey:kUSER_ID];
        [_params setObject:self.firstNametext.text                forKey:@"BFirstname"];
        [_params setObject:self.lastNameTextfield.text             forKey:@"BLastName"];
        [_params setObject:self.genderText.text             forKey:@"gender"];
        [_params setObject:self.addressText.text                forKey:@"Baddress"];
        [_params setObject:self.socialSNText.text             forKey:@"SSN"];
        [_params setObject:self.longi     forKey:@"BusinessLongitude"];
        [_params setObject:self.lat     forKey:@"BusinessLatitude"];
        [_params setObject:self.dobtextfield.text         forKey:@"dob"];
        [_params setObject:self.businessNameText.text         forKey:@"businessName"];
        [_params setObject:self.stateId       forKey:@"stateId"];
        [_params setObject:self.iTINText.text         forKey:@"EIN"];
        [_params setObject:self.licenseText.text       forKey:@"areYouLicence"];
        [_params setObject:self.insuranceText.text         forKey:@"areYouInsured"];
        [_params setObject:self.phoneText.text       forKey:@"BTelephoneNumber"];
        
        if ([self.match isEqualToString:@"Edit"])
        {
        }
        else
        {
            [_params setObject:service         forKey:@"categoryID"];
            
        }
        
        
        NSLog(@"_params is ----%@",_params);
        NSString* FileParamConstant1;
        
        NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
        
        
        NSURL* requestURL = [NSURL URLWithString:kURL_BASE];
        
        
        // create request
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
        [request setHTTPShouldHandleCookies:NO];
        [request setTimeoutInterval:30];
        [request setHTTPMethod:@"POST"];
        
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
        [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        // post body
        NSMutableData *body = [NSMutableData data];
        
        // add params (all params are strings)
        for (NSString *param in _params)
        {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        // add image data
        if (imageDataDriver) {
            
            FileParamConstant1= @"";
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"drivlingImage\"; filename=\"image1.jpg\"\r\n", FileParamConstant1] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:imageDataDriver];
            [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        if (imageDataProfile) {
            
            FileParamConstant1= @"";
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"image\"; filename=\"image.jpg\"\r\n", FileParamConstant1] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:imageDataProfile];
            [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        
        [body appendData:[[NSString stringWithFormat:@"--%@--\r\n\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // setting the body of the post to the reqeust
        [request setHTTPBody:body];
        
        // set the content-length
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        // set URL
        [request setURL:requestURL];
        
        NSURLResponse *response = nil;
        NSError *requestError = nil;
        NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
        
        [SVProgressHUD dismiss];
        if (requestError == nil)
        {
            
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:returnData options:kNilOptions error:&requestError];
            
            NSLog(@"json is ---%@",json);
            
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"Editbusinessaddresss"];
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"lat"];
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"long"];
            
            
            if ([[json objectForKey:@"status"] isEqualToString:@"success"])
            {
                [Alert svSuccess:[json objectForKey:@"msg"]];
                
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"SelectedServices"];
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"SelectedServicesName"];
                
                
                NSDictionary * dictoanryLoginData = [Alert removeNSNullClass:(NSMutableDictionary*)[json objectForKey:@"data"]];
                [[NSUserDefaults standardUserDefaults] setObject:dictoanryLoginData forKey:kLOGIN];
                [[NSUserDefaults standardUserDefaults] setObject:dictoanryLoginData forKey:@"ProfileDetail"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                
            }
            
            
        }
        else
        {
            [Alert svError:requestError.localizedDescription];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^
                       {
            if ([self.match isEqualToString:@"Edit"])
            {
                
                appDelegate = UIAppDelegate;
                DashboardViewController * homePassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBID_ARRIVED_VC);
                UIAppDelegate.isCall  = YES;
                MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
                [appDelegate manageDrawerRear:menuViewController front:homePassengerViewController];
                
                
            }
            else
            {
                
                MembershipViewController *forgotPasswordViewController =  GET_VIEW_CONTROLLER(@"MembershipViewController");
                UINavigationController * navigationController = [Alert navigationControllerWithVC:forgotPasswordViewController];
                
                PERSENT_VIEW_CONTOLLER(navigationController, YES);
            }
        });
    });
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
