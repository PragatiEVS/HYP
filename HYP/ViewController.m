//
//  ViewController.m
//  HYP
//
//  Created by santosh kumar singh on 29/08/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import "ViewController.h"
#import "RegisterViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    
    self.title = @"WELCOME";
   
    self.providerSignUp.layer.cornerRadius = 8;
    self.providerSignUp.layer.borderColor = [UIColor whiteColor].CGColor;
    self.providerSignUp.layer.borderWidth = 1.0f;
    self.providerSignUp.layer.shadowOpacity = 0.5f;
    self.providerSignUp.layer.shadowRadius = 3.0f;
    self.providerSignUp.layer.shadowColor = [UIColor whiteColor].CGColor;
    
    self.affiliateSignUp.layer.cornerRadius = 8;
    self.affiliateSignUp.layer.borderColor = [UIColor whiteColor].CGColor;
    self.affiliateSignUp.layer.borderWidth = 1.0f;
    self.affiliateSignUp.layer.shadowOpacity = 0.5f;
    self.affiliateSignUp.layer.shadowRadius = 3.0f;
    self.affiliateSignUp.layer.shadowColor = [UIColor whiteColor].CGColor;
    
    self.userSignUp.layer.cornerRadius = 8;
    self.userSignUp.layer.borderColor = [UIColor whiteColor].CGColor;
    self.userSignUp.layer.borderWidth = 1.0f;
    self.userSignUp.layer.shadowOpacity = 0.5f;
    self.userSignUp.layer.shadowRadius = 3.0f;
    self.userSignUp.layer.shadowColor = [UIColor whiteColor].CGColor;
    
    [self.view setBackgroundColor:[Alert colorWithPatternImage2:kBG_IMAGE]];
    [super viewDidLoad];
    
    [self navigationBarConfiguration];
    // Do any additional setup after loading the view.
}


-(void)navigationBarConfiguration
{
    
    self.navigationController.navigationBarHidden = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    UIButton * menuButton  = [UIButton buttonWithType:UIButtonTypeSystem];
    menuButton.frame = CGRectMake(8, 18, 24, 24);
    [menuButton setBackgroundImage:[UIImage imageNamed:IMG_BACK] forState:UIControlStateNormal];
    //[menuButton addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *accountBarItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    self.navigationItem.leftBarButtonItem = accountBarItem;
    
    UIButton *btnLib = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnLib setImage:[UIImage imageNamed:@"Call"] forState:UIControlStateNormal];
        btnLib.frame = CGRectMake(0, 0, 50, 40);
        btnLib.showsTouchWhenHighlighted=YES;
        [btnLib addTarget:self action:@selector(onMyLibrary:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *barButtonItem2 = [[UIBarButtonItem alloc] initWithCustomView:btnLib];
        self.navigationItem.rightBarButtonItem=barButtonItem2;
    
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor],
    }];
    
    
}
-(void)onMyLibrary:(id)sender
{
    
    
     if (TARGET_OS_SIMULATOR)
     {
         [Alert alertControllerTitle:kAPPICATION_TITLE msg:kDEVICE_NOT_SUPPORT ok:kOK controller:self.navigationController];
     }
     else
     {
         
         [Alert makePhoneCall:@"7274724673"];
     }
}

- (IBAction)buttonUserRegister:(id)sender
{
    
    [[NSUserDefaults standardUserDefaults] setObject:@"Register" forKey:@"CheckFrom"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
   RegisterViewController *forgotPasswordViewController =  GET_VIEW_CONTROLLER(kSBIDRegisterVC);
   UINavigationController * navigationController = [Alert navigationControllerWithVC:forgotPasswordViewController];
   
   PERSENT_VIEW_CONTOLLER(navigationController, YES);
}

- (IBAction)buttonAffiliatedRegister:(id)sender
{
    
    [[NSUserDefaults standardUserDefaults] setObject:@"Driver" forKey:@"CheckFrom"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
   RegisterViewController *forgotPasswordViewController =  GET_VIEW_CONTROLLER(kSBIDRegisterVC);
   UINavigationController * navigationController = [Alert navigationControllerWithVC:forgotPasswordViewController];
   
   PERSENT_VIEW_CONTOLLER(navigationController, YES);
}

- (IBAction)buttonProviderRegister:(id)sender
{
    
    [[NSUserDefaults standardUserDefaults] setObject:@"Provider" forKey:@"CheckFrom"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
   RegisterViewController *forgotPasswordViewController =  GET_VIEW_CONTROLLER(kSBIDRegisterVC);
   UINavigationController * navigationController = [Alert navigationControllerWithVC:forgotPasswordViewController];
   
   PERSENT_VIEW_CONTOLLER(navigationController, YES);
}

- (IBAction)buttonUserLogin:(id)sender
{
    
    [[NSUserDefaults standardUserDefaults] setObject:@"Login" forKey:@"CheckFrom"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
   RegisterViewController *forgotPasswordViewController =  GET_VIEW_CONTROLLER(kSBIDRegisterVC);
   UINavigationController * navigationController = [Alert navigationControllerWithVC:forgotPasswordViewController];
   
   PERSENT_VIEW_CONTOLLER(navigationController, YES);
}

@end
