//
//  EditViewController.m
//  CHORETHING
//
//  Created by santosh kumar singh on 01/04/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import "EditViewController.h"
#import "MenuViewController.h"
#import "ServicesViewController.h"
#import "BankAccountViewController.h"
#import "RegisterInfoViewController.h"
#import "DriverRegisterViewController.h"

@interface EditViewController ()<UINavigationControllerDelegate,UIImagePickerControllerDelegate,WSCalendarViewDelegate,UIPickerViewDataSource,UIPickerViewDelegate,UIActionSheetDelegate,WSCalendarViewDelegate>
{
    AppDelegate * appDelegate;
}

@property(nonatomic) NSDateFormatter *dateFormatter;
@end

@implementation EditViewController

- (void)viewDidLoad {
    
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"Editbusinessaddresss"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"lat"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"long"];
         
   
    self.title = @"UPDATE PROFILE";
    self.buttonUpdate.layer.cornerRadius = 8.0f;
     self.buttonBank.layer.cornerRadius = 8.0f;
     self.buttonService.layer.cornerRadius = 8.0f;
     self.buttonBusinessInfo.layer.cornerRadius = 8.0f;
    
    calendarView = [[[NSBundle mainBundle] loadNibNamed:@"WSCalendarView" owner:self options:nil] firstObject];
    calendarView.backgroundColor=[UIColor whiteColor];
    calendarView.tappedDayBackgroundColor=[UIColor redColor];
    calendarView.calendarStyle = WSCalendarStyleDialog;
    calendarView.isShowEvent=false;
    [calendarView setupAppearance];
    [self.view addSubview:calendarView];
    calendarView.delegate=self;
    
    self.buttonImage.layer.cornerRadius = self.buttonImage.frame.size.height/2;
    self.buttonImage.clipsToBounds = YES;
    
    NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults] dictionaryForKey:kLOGIN];
    
    NSString * str = [dictLogin valueForKey:@"role"];
    
    if ([str isEqualToString:@"Partner"])
    {
        self.driverView.hidden = YES;
        self.providerView.hidden = NO;
    }
    else if ([str isEqualToString:@"Driver"])
    {
        self.driverView.hidden = NO;
        self.providerView.hidden = YES;
    }
    else
    {
        
        self.driverView.hidden= YES;
        self.providerView.hidden= YES;
       
        
    }
    
    [self configure];
    
    
    
    [Alert SetToolBarONKeyBoard:self.tableView andFields:@[self.textFName,self.textLName,self.textEmail,self.phoneTextfield,self.addressTextfield]];
    
    [self SetTextFieldBorder:_textFName];
    [self SetTextFieldBorder:_textEmail];
    [self SetTextFieldBorder:_phoneTextfield];
    [self SetTextFieldBorder:_addressTextfield];
    [self SetTextFieldBorder:_textLName];
   
    [self navigationBarConfiguration];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


-(void)viewWillAppear:(BOOL)animated
{
    
    NSString * str = [[NSUserDefaults standardUserDefaults] stringForKey:@"Editbusinessaddresss"];
    
    if (![str isEqualToString:@""]) {
        
        self.longi = [[NSUserDefaults standardUserDefaults] stringForKey:@"long"];
        
        self.lat  = [[NSUserDefaults standardUserDefaults] stringForKey:@"lat"];
        
        self.addressTextfield.text = str;
    }
    else
    {
        
    }
    
}


#pragma mark -  Configure Screen
-(void)configure
{
    
    NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"ProfileDetail"];
    
    if ([[dictLogin objectForKey:@"image"] isEqualToString:@""])
    {
        
        [self.buttonImage setBackgroundImage:[UIImage imageNamed:@"profile.png"] forState:UIControlStateNormal];
        
        
    }
    else
    {
        NSData *data= [NSData dataWithContentsOfURL:[NSURL URLWithString:[dictLogin objectForKey:@"image"]]];
        
        [self.buttonImage setBackgroundImage:[UIImage imageWithData:data] forState:UIControlStateNormal];
    }
    
    if ([[dictLogin objectForKey:@"fullName"] isEqualToString:@""]) {
        
    }
    else
    {
        self.textFName.text = [dictLogin objectForKey:@"fullName"];
    }

    if ([[dictLogin objectForKey:@"email"] isEqualToString:@""]) {
        
    }
    else
    {
        self.textEmail.text = [dictLogin objectForKey:@"email"];
    }
    if ([[dictLogin objectForKey:@"contactNumber"] isEqualToString:@""]) {
        
    }
    else
    {
        self.phoneTextfield.text = [dictLogin objectForKey:@"contactNumber"];
    }
   
    if ([[dictLogin objectForKey:@"address"] isEqualToString:@""]) {
        
    }
    else
    {
        self.addressTextfield.text = [dictLogin objectForKey:@"address"];
    }

    if ([[dictLogin objectForKey:@"lastName"] isEqualToString:@""])
    {
        
    }
    else
    {
        self.textLName.text = [dictLogin objectForKey:@"lastName"];
    }
    
   
    
}

-(void)SetTextFieldBorder :(UITextField *)textField
{
    
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 0.5;
    border.borderColor = [UIColor lightGrayColor].CGColor;
    border.frame = CGRectMake(0, textField.frame.size.height - borderWidth, textField.frame.size.width, textField.frame.size.height);
    border.borderWidth = borderWidth;
    [textField.layer addSublayer:border];
    textField.layer.masksToBounds = YES;
    
}

-(void)navigationBarConfiguration
{
    self.navigationController.navigationBarHidden = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    if ([_matchFrom isEqualToString:@"Dashboard"])
    {
        
        UIButton * menuButton  = [UIButton buttonWithType:UIButtonTypeSystem];
        menuButton.frame = CGRectMake(8, 18, 24, 26);
        [menuButton setImage:[UIImage imageNamed:IMG_BACK] forState:UIControlStateNormal];
        [menuButton addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *accountBarItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
        self.navigationItem.leftBarButtonItem = accountBarItem;
        
    }
    else
    {
        UIButton * menuButton  = [UIButton buttonWithType:UIButtonTypeSystem];
        menuButton.frame = CGRectMake(8, 18, 24, 24);
        [menuButton setBackgroundImage:[UIImage imageNamed:NAV_SLIDER_IMAGE] forState:UIControlStateNormal];
        [menuButton addTarget:self action:@selector(drawer:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *accountBarItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
        self.navigationItem.leftBarButtonItem = accountBarItem;
        
    }
    
    NSMutableArray *arrRightBarItems = [[NSMutableArray alloc] init];
       UIButton *btnSetting = [UIButton buttonWithType:UIButtonTypeCustom];
       [btnSetting setImage:[UIImage imageNamed:@"Frequency"] forState:UIControlStateNormal];
       btnSetting.frame = CGRectMake(0, 0, 40, 40);
       btnSetting.showsTouchWhenHighlighted=YES;
     [btnSetting addTarget:self action:@selector(onSettings:) forControlEvents:UIControlEventTouchUpInside];
       UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSetting];
     
       UIButton *btnLib = [UIButton buttonWithType:UIButtonTypeCustom];
       [btnLib setImage:[UIImage imageNamed:@"logo1.png"] forState:UIControlStateNormal];
       btnLib.frame = CGRectMake(0, 0, 40, 40);
       btnLib.showsTouchWhenHighlighted=YES;
      [btnLib addTarget:self action:@selector(onMyLibrary:) forControlEvents:UIControlEventTouchUpInside];
       UIBarButtonItem *barButtonItem2 = [[UIBarButtonItem alloc] initWithCustomView:btnLib];
       [arrRightBarItems addObject:barButtonItem2];
       [arrRightBarItems addObject:barButtonItem];
       self.navigationItem.rightBarButtonItems=arrRightBarItems;
    
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor],
    }];
    
}
-(void)onMyLibrary:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}
-(void)onSettings:(id)sender
{
  
    NSString*myurl=@"https://www.hypdemand.com/";
     NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
       [[UIApplication sharedApplication] openURL:url];
    }
}

-(void)back:(id)sender
{
    DISMISS_VIEW_CONTROLLER;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    if (valrible==0)
    {
        self.textgender.text = [AppDelegate appDelegate].genderArr[row];
    }
    if (valrible==1)
    {
        self.stateText.text = [[AppDelegate appDelegate].stateArr[row]valueForKey:@"name"];
        self.stateId =[[AppDelegate appDelegate].stateArr[row]valueForKey:@"id"];
    }
    
    
    [self.dienstPicker setHidden:YES];
    [self.donePicker setHidden:YES];
}

#pragma mark - UIPickerViewDataSource
-(IBAction)done:(id)sender
{
    [self.dienstPicker setHidden:YES];
    [self.donePicker setHidden:YES];
}


#pragma mark - UIPickerViewDataSource


-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// #4
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    
    
    if (valrible==0)
    {
        return  [AppDelegate appDelegate].genderArr.count;
    }
    else if (valrible==1)
    {
        return  [AppDelegate appDelegate].stateArr.count;
    }
    else
    {
        return 0;
    }
    
    
}

#pragma mark - UIPickerViewDelegate

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (valrible==0)
    {
        return  [AppDelegate appDelegate].genderArr[row];
    }
    else  if (valrible==1)
    {
        return  [[AppDelegate appDelegate].stateArr[row]valueForKey:@"name"];
    }
    
    else
    {
        return 0;
    }
    
}
- (IBAction)showPickerView:(UIButton*)sender
{
    [self.dienstPicker setHidden:NO];
    [self.donePicker setHidden:NO];
    self.dienstPicker.delegate = self;     //#2
    self.dienstPicker.dataSource = self;
    if (sender.tag==0)
    {
        valrible = 0;
    }
    if (sender.tag==1)
    {
        valrible = 1;
    }
    [self.dienstPicker reloadAllComponents];
    
}
#pragma mark - Drawer class
-(IBAction)drawer:(id)sender
{
    
    KYDrawerController *elDrawer = (KYDrawerController*)self.navigationController.parentViewController;
    [elDrawer setDrawerState:KYDrawerControllerDrawerStateOpened animated:YES];
    
    
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    if (self.dobtextfield.editing == YES)
    {
        
        [self.dobtextfield resignFirstResponder];
        [calendarView ActiveCalendar:textField];
        
    }
    if (self.addressTextfield.editing == YES)
    {
        
        [self.addressTextfield resignFirstResponder];
//        SearchLocationViewController *forgotPasswordViewController =  GET_VIEW_CONTROLLER(@"SearchLocationViewController");
//        UINavigationController * navigationController = [Alert navigationControllerWithVC:forgotPasswordViewController];
//        PERSENT_VIEW_CONTOLLER(navigationController, YES);
        
    }
}
-(void)didTapLabel:(WSLabel *)lblView withDate:(NSDate *)selectedDate
{
    
}

- (IBAction)buttonBusinessClick:(id)sender
{
    
    RegisterInfoViewController * bookingHistoryViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDCarTypeVC);
     bookingHistoryViewController.match = @"Edit";
             
     UINavigationController * navigationController = [Alert navigationControllerWithVC:bookingHistoryViewController];
      PERSENT_VIEW_CONTOLLER(navigationController, YES);
      

}
- (IBAction)buttonBusinessDriverClick:(id)sender
{
    
    DriverRegisterViewController * bookingHistoryViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"DriverRegisterViewController");
     bookingHistoryViewController.match = @"Edit";
             
     UINavigationController * navigationController = [Alert navigationControllerWithVC:bookingHistoryViewController];
      PERSENT_VIEW_CONTOLLER(navigationController, YES);
      

}


- (IBAction)buttonAddBankClick:(id)sender
{
    
    BankAccountViewController * bookingHistoryViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"BankAccountViewController");
    UINavigationController * navigationController = [Alert navigationControllerWithVC:bookingHistoryViewController];
    PERSENT_VIEW_CONTOLLER(navigationController, YES);
    
}

- (IBAction)addServicesButton:(UIButton*)btn
{
    ServicesViewController *reqSubmitViewController = GET_VIEW_CONTROLLER(kSBIDSaerchVC);
    reqSubmitViewController.match  = @"NEW_USER1";
    UINavigationController * navigationController    = [Alert navigationControllerWithVC:reqSubmitViewController];
    
    PERSENT_VIEW_CONTOLLER(navigationController, YES);
    
}

- (IBAction)buttonSubmitClick:(id)sender
{
    
    if(self.textFName.text.length>0   &&
       self.textEmail.text.length>0   &&
       self.phoneTextfield.text.length>0 &&
       self.textLName.text.length>0 &&
       self.addressTextfield.text.length>0 )
    {
        // Validate Email Address
        
        
        BOOL validateEmail = [Alert validationEmail:self.textEmail.text];
        
        if (validateEmail)
        {
         
            BOOL network = [Alert networkStatus];
            
            if (network)
            {
                [Alert svProgress:@"Please Wait..."];
                [self serviceRegisterUserByEmail];
                
            }
            else
            {
                kNETWORK_PROBLEM;
            }
           
        }
        else
        {
            [Alert svError:kEmail_Validate];
        }
        
    }
    else
    {
        [Alert svError:kEmptyFields];
    }
    
}

- (void)serviceRegisterUserByEmail
{
    [Alert svProgress:@"Please Wait..."];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // time-consuming task
        
        NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults] dictionaryForKey:kLOGIN];
        NSURL* requestURL = [NSURL URLWithString:kURL_BASE];
        
        NSString * user_id  = [dictLogin valueForKey:@"userId"];
        
        NSMutableDictionary * dict                              = [NSMutableDictionary dictionary];
        [dict setObject:kAPI_METHOD_EDIT_PROFILE                    forKey:kAPI_ACTION];
        [dict setObject:user_id                     forKey:@"userId"];
        [dict setObject:self.textFName.text  forKey:@"fullName"];
        [dict setObject:self.phoneTextfield.text  forKey:@"contactNumber"];
        [dict setObject:self.addressTextfield.text  forKey:@"address"];
        [dict setObject:self.textLName.text  forKey:@"lastName"];
      //  [dict setObject:@"IOS"  forKey:@"device"];
        
        NSLog(@"dict is ---%@",dict);
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setHTTPShouldHandleCookies:NO];
        [request setTimeoutInterval:60];
        [request setHTTPMethod:@"POST"];
        NSString *boundary = @"------VohpleBoundary4QuqLuM1cE5lMwCy";
        NSString* FileParamConstant = @"image";
        
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
        [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        
        for (NSString *param in dict)
        {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@\r\n", [dict objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        if (imageData)
        {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"image\"; filename=\"image.jpg\"\r\n", FileParamConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:imageData];
            [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        
        [body appendData:[[NSString stringWithFormat:@"--%@--\r\n\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // setting the body of the post to the reqeust
        [request setHTTPBody:body];
        
        // set the content-length
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        // set URL
        [request setURL:requestURL];
        
        NSURLResponse *response = nil;
        NSError *requestError = nil;
        NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
        
        
        if (requestError == nil)
        {
            [SVProgressHUD dismiss];
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:returnData options:kNilOptions error:&requestError];
            
            NSLog(@"json is ---%@",json);
            
            
            
            if ([[json objectForKey:@"status"] isEqualToString:@"success"])
            {
                [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"Editbusinessaddresss"];
                [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"lat"];
                [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"long"];
                
                
                NSDictionary * dictoanryLoginData = [Alert removeNSNullClass:(NSMutableDictionary*)[json objectForKey:@"data"]];
               
                [[NSUserDefaults standardUserDefaults] setObject:dictoanryLoginData forKey:kLOGIN];
                [[NSUserDefaults standardUserDefaults] setObject:dictoanryLoginData forKey:@"ProfileDetail"];
                [[NSUserDefaults standardUserDefaults]synchronize];
              
                
            }
            
            
        }
        else
        {
            [Alert svError:requestError.localizedDescription];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
           
            [SVProgressHUD dismiss];
            NSString * str = [[NSUserDefaults standardUserDefaults] stringForKey:@"CheckFrom"];
                          
//                          if (![str isEqualToString:@"Member"])
//                          {
//
//
//                              appDelegate = UIAppDelegate;
//                              DashboardViewController * homePassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBID_ARRIVED_VC);
//                              UIAppDelegate.isCall  = YES;
//                              MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
//                              [appDelegate manageDrawerRear:menuViewController front:homePassengerViewController];
//
//                          }
//                          else
//                          {
//                              appDelegate = UIAppDelegate;
//
//                              ServicesViewController * homePassengerViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDSaerchVC);
//                              UIAppDelegate.isCall  = YES;
//                              MenuViewController *menuViewController = GET_VIEW_CONTROLLER_STORYBOARD(kSBIDMenuVC);
//                              [appDelegate manageDrawerRear:menuViewController front:homePassengerViewController];
//                          }
//
        });
    });
    
}


#pragma mark - button UserImage Click
- (IBAction)addImage:(UIButton*)btn
{
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped do nothing.
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Take photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // take photo button tapped.
        [self cameraUsingiPad];
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Choose photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // choose photo button tapped.
        [self libraryUsingiPad];
        
    }]];
    
    [actionSheet setModalPresentationStyle:UIModalPresentationPopover];
    UIPopoverPresentationController *popPresenter = [actionSheet
                                                     popoverPresentationController];
    popPresenter.sourceView = btn;
    popPresenter.sourceRect = btn.bounds;
    
    [self presentViewController:actionSheet animated:YES completion:nil];
    
    
}


-(void) cameraUsingiPad {
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIImagePickerController *imagePicker =
        [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.sourceType =
        UIImagePickerControllerSourceTypeCamera;
        
        imagePicker.allowsEditing = NO;
        imagePicker.modalPresentationStyle = UIModalPresentationFullScreen;
        
        [self presentViewController:imagePicker
                           animated:YES completion:nil];
    }
}

-(void)libraryUsingiPad {
    
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
    {
        UIImagePickerController *pickerView = [[UIImagePickerController alloc] init];
        pickerView.allowsEditing = YES;
        pickerView.delegate=self;
        [pickerView setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            
            pickerView.modalPresentationStyle = UIModalPresentationFullScreen;
            [self presentViewController:pickerView animated:YES completion:nil];
            
        }
        //for iPad
        else {
            // Change Rect as required
            pickerView.modalPresentationStyle = self.popOver;
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:pickerView];
                [popover presentPopoverFromRect:CGRectMake(0, self.view.frame.size.height - 300, self.view.frame.size.width, self.view.frame.size.height-300) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
                self.popOver = popover;
            }];
        }
        
        
    }
}

- (void)imagePickerController:(UIImagePickerController *)imagePicker
        didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    imageData =  UIImageJPEGRepresentation(image, 1.0);
    [self.buttonImage  setBackgroundImage:image forState:UIControlStateNormal] ;
    
    
}

-(void)deactiveWSCalendarWithDate:(NSDate *)selectedDate
{
    NSDateFormatter *monthFormatter=[[NSDateFormatter alloc] init];
    [monthFormatter setDateFormat:@"MM/dd/yyyy"];
    NSString *str=[monthFormatter stringFromDate:selectedDate];
   
  
    NSDate* now = [NSDate date];
    NSDateComponents* ageComponents = [[NSCalendar currentCalendar]
                                       components:NSCalendarUnitYear
                                       fromDate:selectedDate
                                       toDate:now
                                       options:0];
    NSInteger age = [ageComponents year];
  
    if(age>13)
    {
         self.dobtextfield.text = str;
        
    }
    else{
        [Alert svError:@"No Users Under 13"];
    }
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
