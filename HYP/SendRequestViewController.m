//
//  SendRequestViewController.m
//  HYP
//
//  Created by VA pvt ltd on 14/09/21.
//  Copyright © 2021 Pragati Porwal. All rights reserved.
//

#import "SendRequestViewController.h"
#import "RegisterSuccessViewController.h"


@interface SendRequestViewController ()<UINavigationControllerDelegate,UIImagePickerControllerDelegate,GMSAutocompleteViewControllerDelegate>

@end

@implementation SendRequestViewController

- (void)viewDidLoad {
    
    self.locationType = @"DropLocation";
    CLLocation * upadetLocation = [[CLLocation alloc] initWithLatitude:[[AppDelegate appDelegate].latitude floatValue] longitude:[[AppDelegate appDelegate].longitude floatValue]];
    
    self.droplongi = [AppDelegate appDelegate].longitude;
    self.droplat  = [AppDelegate appDelegate].latitude;
    
    [self reverGeoCodingUsingGoogle:upadetLocation];
   
    self.title = @"SEND REQUEST";
    
    [self.vehicleImageView setImageURL:[NSURL URLWithString:self.vehicleImagee]];
    self.vehicleNameLabel.text =self.vehicleName;
    self.DeliveryTypeLabel.text = self.deleviryType;
    
    self.dropLocation.layer.borderColor = [Alert colorFromHexString:@"#F35FF3"].CGColor;
    self.dropLocation.layer.borderWidth = 2.0;
    self.dropLocation.titleLabel.numberOfLines =2;
    
    self.pickupLocation.layer.borderColor = [Alert colorFromHexString:@"#F35FF3"].CGColor;
    self.pickupLocation.layer.borderWidth = 2.0;
    self.pickupLocation.titleLabel.numberOfLines =2;
    
    self.uploadBtn.layer.borderColor = [Alert colorFromHexString:@"#F35FF3"].CGColor;
    self.uploadBtn.layer.borderWidth = 2.0;
    
    self.invoiceTextfield.layer.borderColor = [Alert colorFromHexString:@"#F35FF3"].CGColor;
    self.invoiceTextfield.layer.borderWidth = 2.0;
    self.invoiceTextfield.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 30);
    
    
    [super viewDidLoad];
    
    [self navigationBarConfiguration];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    
    NSString * str = [[NSUserDefaults standardUserDefaults] stringForKey:@"Editbusinessaddresss"];
    if (str.length>0) {
        
        if ([self.locationType isEqualToString:@"PickupLocation"])
        {
            [self.pickupLocation setTitle:[NSString stringWithFormat:@"%@", str] forState:UIControlStateNormal];
             self.pickupLong = [[NSUserDefaults standardUserDefaults] stringForKey:@"long"];
             self.pickupLat  = [[NSUserDefaults standardUserDefaults] stringForKey:@"lat"];
           
        }
        if ([self.locationType isEqualToString:@"DropLocation"])
        {
            [self.dropLocation setTitle:[NSString stringWithFormat:@"%@", str] forState:UIControlStateNormal];
             self.droplongi = [[NSUserDefaults standardUserDefaults] stringForKey:@"long"];
             self.droplat  = [[NSUserDefaults standardUserDefaults] stringForKey:@"lat"];
           
        }
    }
    else
    {
        
    }
}

- (IBAction)DropLocation:(UIButton *)btn
{
    
    self.locationType = @"DropLocation";
    
    GMSAutocompleteViewController *acController = [[GMSAutocompleteViewController alloc] init];
    if (@available(iOS 13.0, *)) {
        if(UIScreen.mainScreen.traitCollection.userInterfaceStyle == UIUserInterfaceStyleDark ){
            acController.primaryTextColor = UIColor.whiteColor;
            acController.secondaryTextColor = UIColor.lightGrayColor;
            acController.tableCellSeparatorColor = UIColor.lightGrayColor;
            acController.tableCellBackgroundColor = UIColor.darkGrayColor;
        } else {
            acController.primaryTextColor = UIColor.blackColor;
            acController.secondaryTextColor = UIColor.lightGrayColor;
            acController.tableCellSeparatorColor = UIColor.lightGrayColor;
            acController.tableCellBackgroundColor = UIColor.whiteColor;
        }
    }
    acController.delegate = self;
    [[UISearchBar appearance] setBarStyle:UIBarStyleDefault];
    [[UITextField appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]] setTextColor:[UIColor whiteColor]];
    [self presentViewController:acController animated:YES completion:nil];
    
}
- (IBAction)PickupLocation:(UIButton *)btn
{
    
    self.locationType = @"PickupLocation";
    
    GMSAutocompleteViewController *acController = [[GMSAutocompleteViewController alloc] init];
    if (@available(iOS 13.0, *)) {
        if(UIScreen.mainScreen.traitCollection.userInterfaceStyle == UIUserInterfaceStyleDark ){
            acController.primaryTextColor = UIColor.blackColor;
            acController.secondaryTextColor = UIColor.lightGrayColor;
            acController.tableCellSeparatorColor = UIColor.lightGrayColor;
            acController.tableCellBackgroundColor = UIColor.darkGrayColor;
        } else {
            acController.primaryTextColor = UIColor.blackColor;
            acController.secondaryTextColor = UIColor.lightGrayColor;
            acController.tableCellSeparatorColor = UIColor.lightGrayColor;
            acController.tableCellBackgroundColor = UIColor.whiteColor;
        }
    }
    acController.delegate = self;
    [[UISearchBar appearance] setBarStyle:UIBarStyleDefault];
    [[UITextField appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]] setTextColor:[UIColor blackColor]];
    [self presentViewController:acController animated:YES completion:nil];
    
}
// Handle the user's selection.
- (void)viewController:(GMSAutocompleteViewController *)viewController
didAutocompleteWithPlace:(GMSPlace *)place {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    // Do something with the selected place.
    NSLog(@"Place name %@", place.name);
    NSLog(@"Place address %@", place.formattedAddress);
    NSLog(@"Place latitude %f", place.coordinate.latitude);
    NSLog(@"Place longitude %f", place.coordinate.longitude);
    
//    [AppDelegate appDelegate].addressString = place.formattedAddress;
//
//    [AppDelegate appDelegate].latitude= [NSString stringWithFormat:@"%f",place.coordinate.latitude];
//    [AppDelegate appDelegate].longitude= [NSString stringWithFormat:@"%f",place.coordinate.longitude];
   
    if ([self.locationType isEqualToString:@"PickupLocation"])
    {
        [self.pickupLocation setTitle:[NSString stringWithFormat:@"%@", place.name] forState:UIControlStateNormal];
        self.pickupLong = [NSString stringWithFormat:@"%f",place.coordinate.longitude];
        self.pickupLat  = [NSString stringWithFormat:@"%f",place.coordinate.latitude];
    }
    
    if ([self.locationType isEqualToString:@"DropLocation"])
    {
        [self.dropLocation setTitle:[NSString stringWithFormat:@"%@", place.name] forState:UIControlStateNormal];
        self.droplongi = [NSString stringWithFormat:@"%f",place.coordinate.longitude];
        self.droplat  = [NSString stringWithFormat:@"%f",place.coordinate.latitude];
    }
    
}


- (void)viewController:(GMSAutocompleteViewController *)viewController
didFailAutocompleteWithError:(NSError *)error {
    // TODO: handle the error.
    NSLog(@"error: %ld", [error code]);
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

// User canceled the operation.
- (void)wasCancelled:(GMSAutocompleteViewController *)viewController {
    NSLog(@"Autocomplete was cancelled.");
    
    // _setLocation = nil;
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void)navigationBarConfiguration
{
    
    self.navigationController.navigationBarHidden = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.navigationController.navigationBarHidden = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    UIButton * menuButton  = [UIButton buttonWithType:UIButtonTypeSystem];
    menuButton.frame = CGRectMake(8, 18, 24, 26);
    [menuButton setImage:[UIImage imageNamed:IMG_BACK] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *accountBarItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    self.navigationItem.leftBarButtonItem = accountBarItem;
    
    //    NSMutableArray *arrRightBarItems = [[NSMutableArray alloc] init];
    //     UIButton *btnSetting = [UIButton buttonWithType:UIButtonTypeCustom];
    //     [btnSetting setImage:[UIImage imageNamed:@"Frequency"] forState:UIControlStateNormal];
    //     btnSetting.frame = CGRectMake(0, 0, 50, 50);
    //     btnSetting.showsTouchWhenHighlighted=YES;
    //     [btnSetting addTarget:self action:@selector(onSettings:) forControlEvents:UIControlEventTouchUpInside];
    //     UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSetting];
    //
    //     UIButton *btnLib = [UIButton buttonWithType:UIButtonTypeCustom];
    //     [btnLib setImage:[UIImage imageNamed:@"logo1.png"] forState:UIControlStateNormal];
    //     btnLib.frame = CGRectMake(0, 0, 50, 40);
    //     btnLib.showsTouchWhenHighlighted=YES;
    //     [btnLib addTarget:self action:@selector(onMyLibrary:) forControlEvents:UIControlEventTouchUpInside];
    //     UIBarButtonItem *barButtonItem2 = [[UIBarButtonItem alloc] initWithCustomView:btnLib];
    //     [arrRightBarItems addObject:barButtonItem2];
    //     [arrRightBarItems addObject:barButtonItem];
    //     self.navigationItem.rightBarButtonItems=arrRightBarItems;
    //
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor],
    }];
    
}

-(void)onMyLibrary:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}

-(void)onSettings:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/hyptunesradio";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}

-(void)back:(id)sender
{
    DISMISS_VIEW_CONTROLLER;
}

-(void)reverGeoCodingUsingGoogle:(CLLocation *)location
{
    [[GMSGeocoder geocoder] reverseGeocodeCoordinate:CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude) completionHandler:^(GMSReverseGeocodeResponse* response, NSError* error)
     {
        
        GMSReverseGeocodeResult *result = response.firstResult;
        
        NSArray   * arrayAdd = result.lines;
        
        NSString  * address1;
        NSString  * address2;
        
        if (arrayAdd.count>1)
        {
            address1 = result.lines[0];
            address2 = result.subLocality;
        }
        else
        {
            address1 = result.lines[0];
        }
        
        [self.dropLocation setTitle:address1 forState:UIControlStateNormal];
        
    }];
    
    
}


#pragma mark - button UserImage Click
- (IBAction)addImage:(UIButton*)btn
{
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped do nothing.
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Take photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // take photo button tapped.
        [self cameraUsingiPad];
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Choose photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // choose photo button tapped.
        [self libraryUsingiPad];
        
    }]];
    
    [actionSheet setModalPresentationStyle:UIModalPresentationPopover];
    UIPopoverPresentationController *popPresenter = [actionSheet
                                                     popoverPresentationController];
    popPresenter.sourceView = btn;
    popPresenter.sourceRect = btn.bounds;
    
    [self presentViewController:actionSheet animated:YES completion:nil];
    
    
}


-(void) cameraUsingiPad {
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIImagePickerController *imagePicker =
        [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.sourceType =
        UIImagePickerControllerSourceTypeCamera;
        
        imagePicker.allowsEditing = NO;
        imagePicker.modalPresentationStyle = UIModalPresentationFullScreen;
        
        [self presentViewController:imagePicker
                           animated:YES completion:nil];
    }
}

-(void)libraryUsingiPad {
    
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
    {
        UIImagePickerController *pickerView = [[UIImagePickerController alloc] init];
        pickerView.allowsEditing = YES;
        pickerView.delegate=self;
        [pickerView setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            
            pickerView.modalPresentationStyle = UIModalPresentationFullScreen;
            [self presentViewController:pickerView animated:YES completion:nil];
            
        }
        //for iPad
        else {
            // Change Rect as required
            pickerView.modalPresentationStyle = self.popOver;
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:pickerView];
                [popover presentPopoverFromRect:CGRectMake(0, self.view.frame.size.height - 300, self.view.frame.size.width, self.view.frame.size.height-300) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
                self.popOver = popover;
            }];
        }
        
        
    }
}

- (void)imagePickerController:(UIImagePickerController *)imagePicker
        didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo
{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    imageData =  UIImageJPEGRepresentation(image, 1.0);
    self.invoiceImageView.image = image;
    
}

- (IBAction)buttonLinkClick:(id)sender
{

NSURL *url = [NSURL URLWithString:@"https://www.hypdemand.com/hypdelivers"];
    
if ([[UIApplication sharedApplication] canOpenURL:url])
{
    [[UIApplication sharedApplication] openURL:url];
}
}
- (IBAction)buttonSignIn:(id)sender
{
    
    if(self.dropLocation.titleLabel.text.length>0 &&
       self.pickupLocation.titleLabel.text.length>0)
    {
        // Validate Email Address
               // Network Code With Status
            BOOL network = [Alert networkStatus];
            
            if (network) {
                
        
                RegisterSuccessViewController *forgotPasswordViewController =  GET_VIEW_CONTROLLER(@"RegisterSuccessViewController");
                forgotPasswordViewController.servicename = self.deleviryType;   forgotPasswordViewController.invoiceId = self.invoiceTextfield.text;
                forgotPasswordViewController.vehicleId = self.vehicleId;
                forgotPasswordViewController.vehicleName = self.vehicleName;
                forgotPasswordViewController.pickLocation = self.pickupLocation.titleLabel.text;
                forgotPasswordViewController.picklatlong = [NSString stringWithFormat:@"%@,%@",self.pickupLat, self.pickupLong];
                forgotPasswordViewController.dropLocation = self.dropLocation.titleLabel.text;
                forgotPasswordViewController.droplatlong = [NSString stringWithFormat:@"%@,%@",self.droplat, self.droplongi];
                forgotPasswordViewController.fromScreen = @"Carbooking";
                forgotPasswordViewController.imagedata = imageData;
                UINavigationController * navigationController = [Alert navigationControllerWithVC:forgotPasswordViewController];
                PERSENT_VIEW_CONTOLLER(navigationController, YES);

              
            }
            else
            {
                kNETWORK_PROBLEM;
            }
        
    }
    else
    {
        
        [Alert svError:kEmptyFields];
    }
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
