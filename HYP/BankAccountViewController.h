//
//  BankAccountViewController.h
//  Q-Workerbee
//
//  Created by santosh kumar singh on 25/06/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface BankAccountViewController : UIViewController

@property(nonatomic,weak) IBOutlet UILabel     * privacyLabel;
@property (weak, nonatomic) IBOutlet UITextField *holderTextField;
@property (weak, nonatomic) IBOutlet UITextField *bankNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *branchTextField;
@property (weak, nonatomic) IBOutlet UITextField *routinNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *accountNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *accountTypeTextfiled;

@property (weak, nonatomic) IBOutlet UITextField *bankLNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *paypalIdTextfiled;


@property (weak, nonatomic) IBOutlet  UIButton *btnEditProfile;
@end


