//
//  CustomAnnotationView.m
//  CustomAnnotation
//
//  Created by akshay on 8/17/12.
//  Copyright (c) 2012 raw engineering, inc. All rights reserved.
//

#import "CustomAnnotationView.h"
#import "Annotation.h"

@implementation CustomAnnotationView

@synthesize calloutView;

- (void)setSelected:(BOOL)selected animated:(BOOL)animated{
    [super setSelected:selected animated:animated];

    Annotation *ann = self.annotation;
    if(selected)
    {
        
            calloutView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"Address-box.png"]];
            _calloutViewlbl= [[UILabel alloc]init];

           _calloutViewlbl.text=ann.title;
        _calloutViewtsub= [[UILabel alloc]init];
        
        _calloutViewtsub.text=ann.subtitle;
            


        [calloutView setFrame:CGRectMake(3, -65, 0, 0)];
        [calloutView sizeToFit];
        [_calloutViewlbl setFrame:CGRectMake(10,-79, 100, 60)];
        _calloutViewlbl.font=[UIFont fontWithName:@"Cabin-Bold" size:11.0];
        _calloutViewlbl.numberOfLines=2;
        
        _calloutViewlbl.textColor=[UIColor whiteColor];
       
        [_calloutViewtsub setFrame:CGRectMake(10,-44, 100, 30)];
        _calloutViewtsub.font=[UIFont fontWithName:@"Cabin-Bold" size:11.0];
        _calloutViewtsub.textColor=[UIColor whiteColor];
        
        [self animateCalloutAppearance];
        [self addSubview:calloutView];
        [self addSubview:_calloutViewlbl];
        [self addSubview:_calloutViewtsub];
    
    }
    else
    {
        //Remove your custom view...
        [_calloutViewtsub removeFromSuperview];
        [calloutView removeFromSuperview];
        [_calloutViewlbl removeFromSuperview];
    }
}

- (void)didAddSubview:(UIView *)subview{
    Annotation *ann = self.annotation;
    if (![ann.locationType isEqualToString:@"dropped"]) {
        if ([[[subview class] description] isEqualToString:@"UICalloutView"]) {
            for (UIView *subsubView in subview.subviews) {
                if ([subsubView class] == [UIImageView class]) {
                    UIImageView *imageView = ((UIImageView *)subsubView);
                    [imageView removeFromSuperview];
                }else if ([subsubView class] == [UILabel class]) {
                    UILabel *labelView = ((UILabel *)subsubView);
                    [labelView removeFromSuperview];
                }
            }
        }
    }
}

- (void)animateCalloutAppearance {
    CGFloat scale = 0.001f;
    calloutView.transform = CGAffineTransformMake(scale, 0.0f, 0.0f, scale, 0, -50);
    _calloutViewlbl.transform = CGAffineTransformMake(scale, 0.0f, 0.0f, scale, 0, -50);
    
    [UIView animateWithDuration:0.15 delay:0 options:UIViewAnimationCurveEaseOut animations:^{
        CGFloat scale = 1.1f;
        calloutView.transform = CGAffineTransformMake(scale, 0.0f, 0.0f, scale, 0, 2);
        _calloutViewlbl.transform = CGAffineTransformMake(scale, 0.0f, 0.0f, scale, 0, 2);
        _calloutViewtsub.transform = CGAffineTransformMake(scale, 0.0f, 0.0f, scale, 0, 2);

    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.1 delay:0 options:UIViewAnimationCurveEaseInOut animations:^{
            CGFloat scale = 0.95;
            calloutView.transform = CGAffineTransformMake(scale, 0.0f, 0.0f, scale, 0, -2);
            _calloutViewlbl.transform=CGAffineTransformMake(scale, 0.0f, 0.0f, scale, 0, -2);
             _calloutViewtsub.transform=CGAffineTransformMake(scale, 0.0f, 0.0f, scale, 0, -2);

        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.075 delay:0 options:UIViewAnimationCurveEaseInOut animations:^{
                CGFloat scale = 1.0;
                calloutView.transform = CGAffineTransformMake(scale, 0.0f, 0.0f, scale, 0, 0);
                _calloutViewlbl.transform= CGAffineTransformMake(scale, 0.0f, 0.0f, scale, 0, 0);
                 _calloutViewtsub.transform= CGAffineTransformMake(scale, 0.0f, 0.0f, scale, 0, 0);

            } completion:nil];
        }];
    }];
}

@end
