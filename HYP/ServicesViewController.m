//
//  ServicesViewController.m
//  CHORETHING
//
//  Created by santosh kumar singh on 24/03/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import "ServicesViewController.h"
#import "ViewControllerCell.h"
#import "ViewControllerCellHeader.h"
#import "SelectLocationViewController.h"
#import "AffiliateViewController.h"


@interface ServicesViewController ()<UITableViewDataSource, UITableViewDelegate,WebServiceDelegate,UISearchBarDelegate>
{
    IBOutlet UITableView *tblView;
    NSMutableArray *arrSelectedSectionIndex;
    BOOL isMultipleExpansionAllowed;
    int count ;
}
@end

@implementation ServicesViewController

- (void)viewDidLoad
{
    _selectedIndexDictionary=[[NSMutableArray alloc]init];
    _selectedIndexNameDictionary=[[NSMutableArray alloc]init];
    
    //  [self.view setBackgroundColor:[Alert colorWithPatternImage2:kBG_IMAGE]];
    
    self.contentSizeInPopup = CGSizeMake(self.view.frame.size.width-40, self.view.frame.size.height-180);
    
    
    if ([self.match isEqualToString:@"NEW_USER1"])
    {
        self.title=@"SELECT SERVICES PROVIDED";
        [self.continuebtn setTitle:@"UPDATE SERVICES" forState:UIControlStateNormal];
        tblView.allowsMultipleSelectionDuringEditing = true;
    }
    else
    {
         self.title=@"SELECT SERVICES PROVIDED";
        [self.continuebtn setTitle:@"DONE" forState:UIControlStateNormal];
        tblView.allowsMultipleSelectionDuringEditing = true;
        
    }
    
    self.referBtnView.titleLabel.numberOfLines = 2;
    self.linkBtnView.titleLabel.numberOfLines = 3;
    
    self.referBtnView.titleLabel.textAlignment = UITextAlignmentCenter;
    self.linkBtnView.titleLabel.textAlignment = UITextAlignmentCenter;
    
    [super viewDidLoad];
    
    [self navigationBarConfiguration];
  
    [self serviceCategoryList];
    
    count= 0;
    
    isMultipleExpansionAllowed = NO;
    
    arrSelectedSectionIndex = [[NSMutableArray alloc] init];
    
    
}

-(void)navigationBarConfiguration
{
    
    self.navigationController.navigationBarHidden = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    UIButton * menuButton  = [UIButton buttonWithType:UIButtonTypeSystem];
    menuButton.frame = CGRectMake(8, 18, 24, 26);
    [menuButton setImage:[UIImage imageNamed:IMG_BACK] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *accountBarItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    self.navigationItem.leftBarButtonItem = accountBarItem;
        
    
      NSMutableArray *arrRightBarItems = [[NSMutableArray alloc] init];
           UIButton *btnSetting = [UIButton buttonWithType:UIButtonTypeCustom];
           [btnSetting setImage:[UIImage imageNamed:@"Frequency"] forState:UIControlStateNormal];
           btnSetting.frame = CGRectMake(0, 0, 40, 40);
           btnSetting.showsTouchWhenHighlighted=YES;
           [btnSetting addTarget:self action:@selector(onSettings:) forControlEvents:UIControlEventTouchUpInside];
           UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSetting];
           
           UIButton *btnLib = [UIButton buttonWithType:UIButtonTypeCustom];
           [btnLib setImage:[UIImage imageNamed:@"logo1.png"] forState:UIControlStateNormal];
           btnLib.frame = CGRectMake(0, 0, 40, 40);
           btnLib.showsTouchWhenHighlighted=YES;
           UIBarButtonItem *barButtonItem2 = [[UIBarButtonItem alloc] initWithCustomView:btnLib];
           [arrRightBarItems addObject:barButtonItem2];
           [arrRightBarItems addObject:barButtonItem];
           self.navigationItem.rightBarButtonItems=arrRightBarItems;
           
        
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor],
    }];
    
}

-(void)onMyLibrary:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}

-(void)onSettings:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/hyptunesradio";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}

-(void)back:(id)sender
{
    DISMISS_VIEW_CONTROLLER;
    
}


#pragma mark - Drawer class
-(IBAction)drawer:(id)sender
{
    
    KYDrawerController *elDrawer = (KYDrawerController*)self.navigationController.parentViewController;
    [elDrawer setDrawerState:KYDrawerControllerDrawerStateOpened animated:YES];
    
    
}


-(void)serviceCategoryList{
    
    // SERVER CONNCETION
    [Alert svProgress:@"Please Wait..."];
    
    WebService * connection = [[WebService alloc]init];
    connection.delegate = self;
    NSMutableDictionary * dict                              = [NSMutableDictionary dictionary];
    
    [dict setObject:kAPI_METHOD_CATERGORY                     forKey:kAPI_ACTION];
    
    NSLog(@"dict is ---%@",dict);
    [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:kAPI_METHOD_CATERGORY];
    
}
#pragma mark - Sevice delgate methods

-(void)getDataFormWebService:(NSDictionary *)jsonResults urlStr:(NSString *)yourUR methodName:(NSString *)methodName{
    
    [SVProgressHUD dismiss];
    
    SWITCH (methodName) {
        
        CASE (kAPI_METHOD_EDIT_PROFILE)
        {
            
            NSString *strmeasge=[NSString stringWithFormat:@"%@",[jsonResults objectForKey:@"msg"]];
            NSDictionary * dictoanryLoginData = [Alert removeNSNullClass:(NSMutableDictionary*)[jsonResults objectForKey:@"data"]];
            [[NSUserDefaults standardUserDefaults] setObject:dictoanryLoginData forKey:kLOGIN];
            [[NSUserDefaults standardUserDefaults] setObject:dictoanryLoginData forKey:@"ProfileDetail"];
             
            [[NSUserDefaults standardUserDefaults]synchronize];
           
            DISMISS_VIEW_CONTROLLER
          
            
        }
        CASE (kAPI_METHOD_CATERGORY)
        {
            
            if ([self.match isEqualToString:@"NEW_USER1"])
            {
                NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults] dictionaryForKey:kLOGIN];
                
                //  [serviceTypeArr appendString:[dictLogin objectForKey:@"categoryID"]];
                
                if ([[dictLogin objectForKey:@"categoryID"] isEqual:@""])
                {
                    
                }
                else
                {
                    self.serviceTypeArr1  = [dictLogin objectForKey:@"categoryID"];
                    
                    myWords = [[dictLogin objectForKey:@"categoryID"] componentsSeparatedByString:@","];
                    
                    _selectedIndexDictionary = [[dictLogin objectForKey:@"categoryID"] componentsSeparatedByString:@","];
                    
                    _selectedIndexNameDictionary = [[dictLogin objectForKey:@"name"] componentsSeparatedByString:@","];
                }
            }
            resultArray = [[NSMutableArray alloc]init];
            resultArray=[jsonResults valueForKey:@"data"];
            tblView.delegate= self;
            tblView.dataSource= self;
            [tblView reloadData];
            
            
            
        }
        break;
        
        DEFAULT
        {
            break;
        }
    }
    
}
-(void) webServiceFailWithApplicationServerMSG:(NSString *)msg
{
    
    [Alert svError:msg];
    
}

// WEB SERVICE FAIL WITH REGION TCP/IP
-(void) webServiceFail:(NSError *)error{
    
    
    [Alert svError:[error localizedDescription]];
    
    
}

#pragma mark - TableView methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return resultArray.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([arrSelectedSectionIndex containsObject:[NSNumber numberWithInteger:section]])
    {
        NSDictionary *section1 = resultArray[section];
        NSArray *rows = section1[@"SubCat"];
        return rows.count;
    }
    else
    {
        return 0;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ViewControllerCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ViewControllerCell"];
    
    if (cell ==nil)
    {
        [tblView registerClass:[ViewControllerCell class] forCellReuseIdentifier:@"ViewControllerCell"];
        
        cell = [tblView dequeueReusableCellWithIdentifier:@"ViewControllerCell"];
    }
    
    NSDictionary *section = resultArray[indexPath.section];
    NSArray *rows = section[@"SubCat"];
    
    i = indexPath.section;
    
    NSDictionary *row = rows[indexPath.row];
    
    NSString *title = row[@"name"];
    
    cell.lblName.text = title;
    
         for (int k=0; k<[_selectedIndexDictionary count]; k++)
        {
        
        if ([[NSString stringWithFormat:@"%@",row[@"id"]] isEqualToString: [_selectedIndexDictionary objectAtIndex:k]])
        {
            
            [[cell btnShowHide] setImage:[UIImage imageNamed:@"Radio"] forState:UIControlStateNormal];
            break;
        }
        else
        {
            [[cell btnShowHide] setImage:nil forState:UIControlStateNormal];
        }
        }
     
    if (indexPath.row == rows.count-1)
    {
        
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:cell.bgimageview.bounds byRoundingCorners:( UIRectCornerBottomLeft | UIRectCornerBottomRight) cornerRadii:CGSizeMake(7.0, 7.0)];
        
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = self.view.bounds;
        maskLayer.path  = maskPath.CGPath;
        cell.bgimageview.layer.mask = maskLayer;
        
    }
    
    
    [[cell btnShowHide] setTag:indexPath.row];
    
    //  [[cell btnShowHide] addTarget:self action:@selector(btnTapSection:) forControlEvents:UIControlEventTouchUpInside];
    
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 65;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    ViewControllerCellHeader *headerView = [tableView dequeueReusableCellWithIdentifier:@"ViewControllerCellHeader"];
    
    if (headerView ==nil)
    {
        [tblView registerClass:[ViewControllerCellHeader class] forCellReuseIdentifier:@"ViewControllerCellHeader"];
        
        headerView = [tableView dequeueReusableCellWithIdentifier:@"ViewControllerCellHeader"];
    }
    
    headerView.lbTitle.text = [[resultArray valueForKey:@"name"]objectAtIndex:section];
    
    if ([arrSelectedSectionIndex containsObject:[NSNumber numberWithInteger:section]])
    {
        
        headerView.btnShowHide.selected = YES;
        
        //[headerView.bgimageview setBackgroundColor:[Alert colorFromHexString:kCOLOR_category_select]];
        
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:headerView.bgimageview.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerTopRight) cornerRadii:CGSizeMake(7.0, 7.0)];
        
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = self.view.bounds;
        maskLayer.path  = maskPath.CGPath;
        headerView.bgimageview.layer.mask = maskLayer;
        
    }
    else
    {
        //  [headerView.bgimageview setBackgroundColor:[Alert colorFromHexString:kCOLOR_category_select]];
        
        headerView.bgimageview.layer.cornerRadius = 7;
        headerView.bgimageview.clipsToBounds = YES;
    }
    
    
    [[headerView btnShowHide] setTag:section];
    
    [[headerView btnShowHide] addTarget:self action:@selector(btnTapShowHideSection:) forControlEvents:UIControlEventTouchUpInside];
    
    return headerView.contentView;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSDictionary *section = resultArray[i];
    NSArray *rows = section[@"SubCat"];
    
    NSDictionary *row = rows[indexPath.row];
    
    NSString *title =[NSString stringWithFormat:@"%@", row[@"id"]];
    NSString *titlename =[NSString stringWithFormat:@"%@", row[@"name"]];
    
    if (![_selectedIndexDictionary containsObject:title])
    {
        [_selectedIndexDictionary addObject:title];
        [_selectedIndexNameDictionary addObject:titlename];
        
    }
    else
    {
         [_selectedIndexDictionary removeObject:title];
         [_selectedIndexNameDictionary removeObject:titlename];
            
    }
        self.serviceTypeArr1  = [_selectedIndexDictionary componentsJoinedByString: @","];
        
        [[NSUserDefaults standardUserDefaults] setObject:self.serviceTypeArr1 forKey:@"SelectedServices"];
        [[NSUserDefaults standardUserDefaults] setObject:[_selectedIndexNameDictionary componentsJoinedByString: @","] forKey:@"SelectedServicesName"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        NSLog(@"_selectedIndexDictionary =%@",_selectedIndexDictionary);

        [tblView reloadData];
    
}



-(IBAction)btnTapShowHideSection:(UIButton*)sender
{
    if (!sender.selected)
    {
        if (!isMultipleExpansionAllowed)
        {
            if (!arrSelectedSectionIndex)
            {
                
                [arrSelectedSectionIndex addObject:[NSNumber numberWithInteger:sender.tag]];
            }
            else
            {
                
                [arrSelectedSectionIndex addObject:[NSNumber numberWithInteger:sender.tag]];
                //   [arrSelectedSectionIndex replaceObjectAtIndex:0 withObject:[NSNumber numberWithInteger:sender.tag]];
            }
        }
        else {
            [arrSelectedSectionIndex addObject:[NSNumber numberWithInteger:sender.tag]];
        }
        
        sender.selected = YES;
    }else
    {
        sender.selected = NO;
        
        if ([arrSelectedSectionIndex containsObject:[NSNumber numberWithInteger:sender.tag]])
        {
            [arrSelectedSectionIndex removeObject:[NSNumber numberWithInteger:sender.tag]];
        }
    }
    
    if (!isMultipleExpansionAllowed) {
        [tblView reloadData];
    }
    else
    {
        [tblView reloadSections:[NSIndexSet indexSetWithIndex:sender.tag] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}

-(IBAction)btnContinue:(UIButton*)sender
{
    
    if ([self.continuebtn.titleLabel.text  isEqualToString:@"UPDATE SERVICES"])
    {
        if ([self.serviceTypeArr1 isEqual:@""]) {
            
            [Alert svError:@"Please Select Services First!"];
        }
        else
        {
           
            [Alert svProgress:@"Please Wait..."];
            NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults] dictionaryForKey:kLOGIN];
            NSString * user_id ;
            if ([dictLogin valueForKey:@"id"] == nil)
            {
                user_id  = [dictLogin valueForKey:@"userId"];
            }
            else
            {
                user_id  = [dictLogin valueForKey:@"id"];
            }
            
            WebService * connection = [[WebService alloc]init];
            connection.delegate = self;
            
            NSMutableDictionary * dict       = [NSMutableDictionary dictionary];
            [dict setObject:kAPI_METHOD_EDIT_PROFILE                    forKey:kAPI_ACTION];
            [dict setObject:user_id                    forKey:kUSER_ID];
            [dict setObject:self.serviceTypeArr1                    forKey:@"categoryID"];
            
            [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:kAPI_METHOD_EDIT_PROFILE];
        }
    }
    else
    {
        if (!self.serviceTypeArr1)
        {
            [Alert svError:@"Please Select Services First!"];
        }
        else
        {
            DISMISS_VIEW_CONTROLLER;
        }
    }
}

-(IBAction)btnLinkClick:(UIButton*)sender
{
    NSString *title = @"http://ailserunz.com";
    
    NSURL *url = [NSURL URLWithString:title];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}

-(IBAction)btnReferClick:(UIButton*)sender
{
    AffiliateViewController * termConditionViewController = GET_VIEW_CONTROLLER_STORYBOARD(@"AffiliateViewController");
    UINavigationController * navigationController = [Alert navigationControllerWithVC:termConditionViewController];
    
    PERSENT_VIEW_CONTOLLER(navigationController, YES);
    
    
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
