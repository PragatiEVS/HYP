//
//  ArrivingViewController.h
//  ROLUX
//
//  Created by Nitin Kumar on 06/07/17.
//  Copyright © 2017 Nitin Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ArrivingViewController : UIViewController
{
    GMSCameraPosition *camera;
    GMSMapView *mapView_;
    GMSMarker *locationMarker_;
    CLLocationCoordinate2D coordinate;
}
@property(nonatomic,strong) NSString * matchFrom;
@property(nonatomic,strong) NSString * name;
@property(nonatomic,strong) NSString * dropaddress;
@property(nonatomic,strong) NSString * bookingid;
@property(nonatomic,strong) NSString * driverId;
@property(nonatomic,strong) NSString * address;
@property(nonatomic,strong) NSString * phone;
@property(nonatomic,strong) NSDictionary * dictArrived;
@property (nonatomic, retain) CLLocationManager *locationManager;

@property(nonatomic,strong) IBOutlet UIView * viewTopAddressesInfo;
@property(nonatomic,strong) IBOutlet UIView * viewBottomAddressesInfo;
@property(nonatomic,strong) IBOutlet UIView * viewMap;
@property(nonatomic,weak) IBOutlet UILabel * labelBookingID;
@property(nonatomic,weak) IBOutlet UILabel * labelPhone;
@property(nonatomic,weak) IBOutlet UILabel * labelName;
@property(nonatomic,weak) IBOutlet UILabel * labelocation;

@property (nonatomic,strong) NSDictionary * firebaseDictinary;
@property(nonatomic,weak) IBOutlet UIImageView * imageViewUser;

@property(nonatomic,weak) IBOutlet UIButton * buttonPhn;
@property(nonatomic,weak) IBOutlet UIButton * buttonEmail;
@property(nonatomic,weak) IBOutlet UIButton * buttonOptions;


@property(nonatomic,strong)  NSString * uuserPhone;
@property(nonatomic,strong)  NSString * userName;
@property(nonatomic,strong)  NSString * userImage;
@property(nonatomic,strong)  NSString * pickupFinal;

@property(nonatomic,weak) IBOutlet UIButton * buttonDirection;

@property(nonatomic,weak) IBOutlet UILabel * labelDistance;
@property(nonatomic,weak) IBOutlet UIImageView * invoiceImage;
// Payment

@property(nonatomic,weak) IBOutlet UIView * viewPayment;
@property(nonatomic,weak) IBOutlet UIButton * buttonPaymentOk;
@property(nonatomic,weak) IBOutlet UILabel * labelPaymentAmount;


@property (weak, nonatomic) IBOutlet UILabel *picLoc;
@property (weak, nonatomic) IBOutlet UILabel *dropoffLoc;

-(IBAction)buttonDirectionClick:(id)sender;
-(IBAction)buttonEmailClick:(id)sender;
-(IBAction)buttonPhnClick:(id)sender;
-(IBAction)buttonOptionsClick:(id)sender;
-(IBAction)buttonPaymentOkClick:(id)sender;
@end
