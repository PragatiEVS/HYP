//
//  DriverBookingViewController.h
//  HYP
//
//  Created by VA pvt ltd on 13/09/21.
//  Copyright © 2021 Pragati Porwal. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DriverBookingViewController : UIViewController
{
    NSMutableArray *resultArray;
    int startidx;
    NSArray *arr;
}

@property(nonatomic,weak)  NSString     * matchFrom;

@property (weak, nonatomic) IBOutlet UILabel * noListAvalible;
@property(weak, nonatomic) IBOutlet UITableView *completedTableView;

@end

NS_ASSUME_NONNULL_END
