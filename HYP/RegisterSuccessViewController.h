//
//  RegisterSuccessViewController.h
//  Q-Workerbee
//
//  Created by santosh kumar singh on 09/05/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface RegisterSuccessViewController : UIViewController

@property(nonatomic,weak) IBOutlet UIImageView     * bgImageView;
@property (strong, nonatomic) IBOutlet UILabel *lblName;

@property (strong, nonatomic)  NSString * fromScreen;
@property (strong, nonatomic)  NSString * servicename;
@property (strong, nonatomic)  NSString * invoiceId;
@property (strong, nonatomic)  NSString * vehicleId;
@property (strong, nonatomic)  NSString * vehicleName;
@property (strong, nonatomic)  NSData * imagedata;
@property (strong, nonatomic)  NSString * dropLocation;
@property (strong, nonatomic)  NSString * pickLocation;
@property (strong, nonatomic)  NSString * picklatlong;
@property (strong, nonatomic)  NSString * droplatlong;
@end


