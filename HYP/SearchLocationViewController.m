//
//  SearchLocationViewController.m
//  CHORETHING
//
//  Created by santosh kumar singh on 28/04/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import "SearchLocationViewController.h"

@interface SearchLocationViewController ()<UITableViewDelegate,UITableViewDataSource>

@end

@implementation SearchLocationViewController

- (void)viewDidLoad
{
    self.contentSizeInPopup = CGSizeMake(self.view.frame.size.width-40, self.view.frame.size.height-180);
    
    self.searchTextField.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 30);
    
    [super viewDidLoad];
    
    self.geocoder = [[GMSGeocoder alloc]init];
    [self showGoogleMap];
    self.tableView.hidden= YES;
    
   self.title= @"SEARCH LOCATION";
    
  // [self navigationBarConfiguration];
  // Do any additional setup after loading the view.
}


-(void)back:(id)sender
{
    DISMISS_VIEW_CONTROLLER;
}

#pragma mark --- : GOOGLE MAP :----
-(void)showGoogleMap
{
    
        coordinate = [self getLocation];
        latitude = coordinate.latitude;
        longitude = coordinate.longitude;

      
    CLLocation * upadetLocation = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
    
    CGRect frame = CGRectMake(0, 0, kSCREEN_WIDTH,kSCREEN_HEIGHT);
    camera = [GMSCameraPosition cameraWithLatitude:latitude longitude:longitude zoom:15];
    mapView_ = [GMSMapView mapWithFrame:frame camera:camera];
    mapView_.delegate = self;
  
    [mapView_ setCamera:camera];
    
     
    [self reverGeoCodingUsingGoogle:upadetLocation];
        
        self->mapView_.myLocationEnabled              = YES;
          self->mapView_.indoorEnabled                  = YES;
         self->mapView_.settings.myLocationButton      = NO;
       self->mapView_.settings.scrollGestures        = YES;
  
    [_mapView addSubview:mapView_];
    
    locationMarker_ = [[GMSMarker alloc] init];
    locationMarker_.position = CLLocationCoordinate2DMake(latitude, longitude);
    locationMarker_.icon = [UIImage imageNamed:@"map"];
    locationMarker_.map = mapView_;

}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
     [self.searchTextField resignFirstResponder];
   
    GMSAutocompleteViewController *acController = [[GMSAutocompleteViewController alloc] init];
      if (@available(iOS 13.0, *)) {
          if(UIScreen.mainScreen.traitCollection.userInterfaceStyle == UIUserInterfaceStyleDark ){
              acController.primaryTextColor = UIColor.whiteColor;
              acController.secondaryTextColor = UIColor.lightGrayColor;
              acController.tableCellSeparatorColor = UIColor.lightGrayColor;
              acController.tableCellBackgroundColor = UIColor.darkGrayColor;
          } else {
              acController.primaryTextColor = UIColor.blackColor;
              acController.secondaryTextColor = UIColor.lightGrayColor;
              acController.tableCellSeparatorColor = UIColor.lightGrayColor;
              acController.tableCellBackgroundColor = UIColor.whiteColor;
          }
      }
      acController.delegate = self;
      [[UISearchBar appearance] setBarStyle:UIBarStyleDefault];
      [[UITextField appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]] setTextColor:[UIColor whiteColor]];
      [self presentViewController:acController animated:YES completion:nil];
      
}

// Handle the user's selection.
- (void)viewController:(GMSAutocompleteViewController *)viewController
didAutocompleteWithPlace:(GMSPlace *)place {

    [self dismissViewControllerAnimated:YES completion:nil];
    // Do something with the selected place.
    NSLog(@"Place name %@", place.name);
    NSLog(@"Place address %@", place.formattedAddress);
    NSLog(@"Place latitude %f", place.coordinate.latitude);
    NSLog(@"Place longitude %f", place.coordinate.longitude);
   
    [AppDelegate appDelegate].addressString = place.formattedAddress;
    
    [AppDelegate appDelegate].latitude= [NSString stringWithFormat:@"%f",place.coordinate.latitude];
    [AppDelegate appDelegate].longitude= [NSString stringWithFormat:@"%f",place.coordinate.longitude];

    [self.searchTextField setText:[NSString stringWithFormat:@"%@", place.name]];

    
    [mapView_ clear];
     
    CLLocation * upadetLocation = [[CLLocation alloc] initWithLatitude:place.coordinate.latitude longitude:place.coordinate.longitude];
       
       coordinate = CLLocationCoordinate2DMake(place.coordinate.latitude, place.coordinate.longitude);
       locationMarker_ = [[GMSMarker alloc] init];
       locationMarker_.position = CLLocationCoordinate2DMake(place.coordinate.latitude, place.coordinate.longitude);
       mapView_.myLocationEnabled = YES;
    locationMarker_.title =  place.name;
       locationMarker_.icon = [UIImage imageNamed:@"map"];
       camera = [GMSCameraPosition cameraWithLatitude:coordinate.latitude longitude:coordinate.longitude zoom:ZOOM];
       [mapView_ animateToCameraPosition:camera];
       locationMarker_.map = mapView_;
       
       [self reverGeoCodingUsingGoogle:upadetLocation];
}


- (void)viewController:(GMSAutocompleteViewController *)viewController
didFailAutocompleteWithError:(NSError *)error {
    // TODO: handle the error.
    NSLog(@"error: %ld", [error code]);
    [self dismissViewControllerAnimated:YES completion:nil];
   
}

// User canceled the operation.
- (void)wasCancelled:(GMSAutocompleteViewController *)viewController {
    NSLog(@"Autocomplete was cancelled.");
    
   // _setLocation = nil;
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void)reverGeoCodingUsingGoogle:(CLLocation *)location
{
    [[GMSGeocoder geocoder] reverseGeocodeCoordinate:CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude) completionHandler:^(GMSReverseGeocodeResponse* response, NSError* error)
     {
         
         GMSReverseGeocodeResult *result = response.firstResult;
         
         NSArray   * arrayAdd = result.lines;
         
         NSString  * address1;
         NSString  * address2;
         
         if (arrayAdd.count>1)
         {
             address1 = result.lines[0];
             address2 = result.subLocality;
         }
         else
         {
             address1 = result.lines[0];
         }
        
  
     }];
    
   
}

-(CLLocationCoordinate2D) getLocation
{
    CLLocationManager *locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager startUpdatingLocation];
    CLLocation *location = [locationManager location];
    CLLocationCoordinate2D coordinate = [location coordinate];
    
    return coordinate;
}


-(void)textFieldDidChange:(UITextField*)textField
{
    if (![textField.text isEqualToString:@""])
     {
                 self.tableView.hidden= NO;

    [self getLocationFromAddressString:self.searchTextField.text];
           
     }
    else
    [self.searchTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    {
         self.tableView.hidden= YES;
    }
   
}

-(void)getLocationFromAddressString:(NSString*)addressStr
{
    
  NSString *str =  [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/geocode/json?address=%@&sensor=false&key=AIzaSyB3c9PHeW4ZWtM9bYix5uZYhuNBiT4v5b0", [addressStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
   
    NSURL *url = [NSURL URLWithString:str];

    NSURLSession *session = [NSURLSession sharedSession];

    NSURLSessionDataTask *data = [session dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error)
    
    {
        NSError *erro = nil;

        if (data!=nil) {

            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&erro ];

            if (json.count > 0) {

                self.array = [json objectForKey:@"results"];

            }
        
            NSLog(@"Autocomplete was cancelled.%@",self.array);
          
        }
        
        dispatch_sync(dispatch_get_main_queue(),^{
             self.tableView.hidden= NO;
            self.tableView.delegate= self;
            self.tableView.dataSource= self;
            [self.tableView reloadData];
        });
    }];

    [data resume];
    
}
//
//
//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//
//    [AppDelegate appDelegate].addressString=[(NSString *)[ self.array objectAtIndex:indexPath.row]valueForKey:@"formatted_address"];
//
//
//     [AppDelegate appDelegate].latitude= [NSString stringWithFormat:@"%@",[[[[self.array objectAtIndex:indexPath.row]valueForKey:@"geometry"]valueForKey:@"location"]valueForKey:@"lat"]];
//
//     [AppDelegate appDelegate].logitude= [NSString stringWithFormat:@"%@",[[[[self.array objectAtIndex:indexPath.row]valueForKey:@"geometry"]valueForKey:@"location"]valueForKey:@"lng"]];
//
//
//     [[NSUserDefaults standardUserDefaults] setObject:[AppDelegate appDelegate].addressString forKey:@"Editbusinessaddresss"];
//
//     [[NSUserDefaults standardUserDefaults] setObject:[AppDelegate appDelegate].latitude forKey:@"lat"];
//
//     [[NSUserDefaults standardUserDefaults] setObject:[AppDelegate appDelegate].logitude forKey:@"long"];
//      [[NSUserDefaults standardUserDefaults]synchronize];
//       DISMISS_VIEW_CONTROLLER;
//}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
