//
//  RegisterInfoViewController.h
//  Q-Workerbee
//
//  Created by santosh kumar singh on 09/05/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface RegisterInfoViewController : UIViewController
{
     WSCalendarView *calendarView;
       NSData *imageData;
     int valrible;
      NSData *imageDataDriver;
      NSData *imageDataProfile;
    NSData *imageDataAuto;
}


@property (weak) IBOutlet NSLayoutConstraint *insuranceConstraint;
@property (weak) IBOutlet NSLayoutConstraint *driverConstraint;
@property (weak) IBOutlet NSLayoutConstraint *serviceConstraint;

@property (weak, nonatomic) IBOutlet UIPickerView *dienstPicker;
@property (weak, nonatomic) IBOutlet UIButton *donePicker;

@property (strong, nonatomic) NSString *stateId;
@property (strong, nonatomic) NSString *categoryId;
@property(nonatomic,weak) IBOutlet UITextField * socialSNText;
@property(nonatomic,weak) IBOutlet UITextField * iTINText;
@property(nonatomic,weak) IBOutlet UITextField * businessNameText;
@property(nonatomic,weak) IBOutlet UITextField * phoneText;
@property(nonatomic,weak) IBOutlet UITextField * firstNametext;
@property(nonatomic,weak) IBOutlet UITextField * genderText;
@property(nonatomic,weak) IBOutlet UITextField * addressText;
@property (weak, nonatomic) IBOutlet UITextField *dobtextfield;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTextfield;
@property(nonatomic,weak) IBOutlet UITextField * insuranceText;
@property(nonatomic,weak) IBOutlet UITextField * licenseText;
@property(nonatomic,weak) IBOutlet UITextField * stateText;
@property(nonatomic,weak) IBOutlet UITextField * serviceProvText;

@property(nonatomic,strong) NSString *userid;
@property(nonatomic,strong) NSString *match;
@property(nonatomic,weak) IBOutlet UIButton     * buttonUpdate;
@property(nonatomic,weak) IBOutlet UIButton     * buttonGender;
@property (nonatomic, strong) UIPopoverController *popOver;
@property(nonatomic,strong) NSString *lat;
@property(nonatomic,strong) NSString *longi;
@property (weak, nonatomic) NSString *checkImageFrom;

@property (nonatomic, strong) IBOutlet UIButton *drivngImageViewFront;
@property (nonatomic, strong) IBOutlet UIButton *autoImageViewFront;
@property (nonatomic, strong)IBOutlet UIButton *drivngProfileImage;
@end


