//
//  CompleteBookingViewController.h
//  Q-Workerbee
//
//  Created by santosh kumar singh on 15/06/20.
//  Copyright © 2020 Pragati Porwal. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface CompleteBookingViewController : UIViewController
{
    NSMutableArray *resultArray;
    int startidx;
    NSArray *arr;
}
@property (strong, nonatomic) IBOutlet UILabel * noListAvalible;
@property (strong, nonatomic)  NSDictionary * dicttionary;

@property (strong, nonatomic) NSString * matchFrom;
@property(strong, nonatomic) IBOutlet UITableView *completedTableView;
@end


