//
//  ChangePasswordViewController.m
//  Boccia
//
//  Created by IOSdev on 12/31/19.
//  Copyright © 2019 Pragati Porwal. All rights reserved.
//

#import "ChangePasswordViewController.h"
#import "HelpViewController.h"

@interface ChangePasswordViewController ()<WebServiceDelegate>

@end

@implementation ChangePasswordViewController

- (void)viewDidLoad {
    
    [self configure];
    self.title=@"CHANGE PASSWORD";
    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
#pragma mark - configure
-(void)configure
{
    self.textFieldConfirmPasswd.layer.borderWidth=1.0f;
    self.textFieldConfirmPasswd.layer.borderColor=[UIColor lightGrayColor].CGColor;
    self.textFieldNewPasswd.layer.borderWidth=1.0f;
    self.textFieldNewPasswd.layer.borderColor=[UIColor lightGrayColor].CGColor;
    self.textFieldOldPasswd.layer.borderWidth=1.0f;
    self.textFieldOldPasswd.layer.borderColor=[UIColor lightGrayColor].CGColor;
    
    
    self.textFieldConfirmPasswd.layer.cornerRadius=8;
    self.textFieldOldPasswd.layer.cornerRadius=8;
    self.textFieldNewPasswd.layer.cornerRadius=8;
    self.textFieldNewPasswd.clipsToBounds = YES;
    self.textFieldOldPasswd.clipsToBounds = YES;
    self.textFieldConfirmPasswd.clipsToBounds = YES;
    
    [Alert setKeyBoardToolbar:@[self.textFieldOldPasswd,self.textFieldNewPasswd,self.textFieldConfirmPasswd]];


    NSString *str =@"*";
    
    NSString *username = [NSString stringWithFormat:@"Old Password %@",str];
    
    NSMutableAttributedString *attributedString =[[NSMutableAttributedString alloc] initWithString:username];
    
    [attributedString addAttribute:NSForegroundColorAttributeName
                             value:[Alert colorFromHexString:kCOLOR_NAV]
                             range:[username rangeOfString:str]];
    
    self.textFieldOldPasswd.attributedPlaceholder = attributedString;
    
    NSString *username1 = [NSString stringWithFormat:@"New Password %@",str];
    
    NSMutableAttributedString *attributedString1 =[[NSMutableAttributedString alloc] initWithString:username1];
    
    [attributedString1 addAttribute:NSForegroundColorAttributeName
                              value:[Alert colorFromHexString:kCOLOR_NAV]
                              range:[username1 rangeOfString:str]];
    
    self.textFieldNewPasswd.attributedPlaceholder = attributedString1;
    
    NSString *username2 = [NSString stringWithFormat:@"Confirm Password %@",str];
    
    NSMutableAttributedString *attributedString2 =[[NSMutableAttributedString alloc] initWithString:username2];
    
    [attributedString2 addAttribute:NSForegroundColorAttributeName
                              value:[Alert colorFromHexString:kCOLOR_NAV]
                              range:[username2 rangeOfString:str]];
    
    self.textFieldConfirmPasswd.attributedPlaceholder = attributedString2;
    
    
    _buttonChangePasswd.layer.cornerRadius = 8;
    
    [Alert setLeftPaddingTextField:self.textFieldOldPasswd
                      paddingValue:36
                             image:[UIImage  imageNamed:kIMG_PASSWD]];
    [Alert setLeftPaddingTextField:self.textFieldNewPasswd
                      paddingValue:36
                             image:[UIImage  imageNamed:kIMG_PASSWD]];
    [Alert setLeftPaddingTextField:self.textFieldConfirmPasswd
                      paddingValue:36
                             image:[UIImage  imageNamed:kIMG_PASSWD]];
    [self navigationBarConfiguration];
}

#pragma mark - Drawer class
-(void)drawer:(id)sender
{
    
    KYDrawerController *elDrawer = (KYDrawerController*)self.navigationController.parentViewController;
    [elDrawer setDrawerState:KYDrawerControllerDrawerStateOpened animated:YES];
    
    
}

-(IBAction)buttonChangePass:(id)sender
{
    if(_textFieldOldPasswd.text.length>0 &&
       _textFieldNewPasswd.text.length>0 &&
       _textFieldConfirmPasswd.text.length>0)
    {
        // validate new password
        BOOL validateNewPwd = [Alert ValidateTextFieldValidate:_textFieldNewPasswd andLenght:5];
        
        if (validateNewPwd) {
            
            // validate confirm password
            BOOL validateConfirm = [Alert isPasswordMatch:_textFieldNewPasswd.text withConfirmPwd:_textFieldConfirmPasswd.text];
            
            if (validateConfirm) {
                
                BOOL network = [Alert networkStatus];
                
                if (network) {
                    
                    // service
                    
                    [self serviceChangePassword];
                }
                else
                {
                    kNETWORK_PROBLEM;
                }
            }
            else
            {
                [Alert svError:kPasswordMissmatch];
                
            }
            
        }
        else
        {
            
            [Alert svError:kPwdLengh];
        }
        
    }
    else
    {
        [Alert svError:kEmptyFields];
        
    }
}


#pragma mark - Change Password
-(void)serviceChangePassword
{
    // SERVER CONNCETION
    
    [Alert svProgress:@"Please Wait..."];
    
    // Dictionary Login
    NSDictionary * dictLogin = [[NSUserDefaults standardUserDefaults] dictionaryForKey:kLOGIN];
    
    NSString * user_id  = [dictLogin valueForKey:@"userId"];
    
    // service
    WebService * connection = [[WebService alloc]init];
    connection.delegate = self;
    
    NSMutableDictionary * dict                              = [NSMutableDictionary dictionary];
    [dict setObject:kAPI_METHOD_CNG_PSSWD                     forKey:kAPI_ACTION];
    [dict setObject:user_id                        forKey:kUSER_ID];
    [dict setObject:self.textFieldOldPasswd.text                        forKey:k_USER_OLDPASSWD];
    [dict setObject:self.textFieldNewPasswd.text                        forKey:k_USER_NEWPASSWD];
    
    [connection webServicePostString:[Alert jsonStringWithDictionary:[dict mutableCopy]] urlString:kURL_BASE methodName:kAPI_METHOD_CNG_PSSWD];
}

#pragma mark - <WebServiveDelegate>
-(void)getDataFormWebService:(NSDictionary *)jsonResults urlStr:(NSString *)yourUR methodName:(NSString *)methodName{
    
    [Alert svSuccess:@"Successfully updated"];
    
    
    SWITCH (methodName) {
        CASE (kAPI_METHOD_CNG_PSSWD)
        {
            
            
            NSLog(@"jsonResults =%@",jsonResults);
            
            
            self.textFieldOldPasswd.text=@"";
            self.textFieldNewPasswd.text=@"";
            self.textFieldConfirmPasswd.text=@"";
            
        }
        break;
        
        DEFAULT
        {
            break;
        }
    }
    
}

// WEB SERVER ERROR MSG LOCAL SERVER (APPLICATION SERVER)
-(void) webServiceFailWithApplicationServerMSG:(NSString *)msg
{
    
    [Alert svError:msg];
    
}

// WEB SERVICE FAIL WITH REGION TCP/IP
-(void) webServiceFail:(NSError *)error{
    
    
    [Alert svError:[error localizedDescription]];
    
    
}


- (BOOL)validateEmailWithString:(NSString*)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}


#pragma mark - Configure Navigation Controller

-(void)navigationBarConfiguration
{
    
    self.navigationController.navigationBarHidden = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    UIButton * menuButton  = [UIButton buttonWithType:UIButtonTypeSystem];
    menuButton.frame = CGRectMake(8, 18, 24, 24);
    [menuButton setBackgroundImage:[UIImage imageNamed:NAV_SLIDER_IMAGE] forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(drawer:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *accountBarItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    self.navigationItem.leftBarButtonItem = accountBarItem;
    
    NSMutableArray *arrRightBarItems = [[NSMutableArray alloc] init];
         UIButton *btnSetting = [UIButton buttonWithType:UIButtonTypeCustom];
         [btnSetting setImage:[UIImage imageNamed:@"Frequency"] forState:UIControlStateNormal];
         btnSetting.frame = CGRectMake(0, 0, 40, 40);
         btnSetting.showsTouchWhenHighlighted=YES;
        [btnSetting addTarget:self action:@selector(onSettings:) forControlEvents:UIControlEventTouchUpInside];
         UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSetting];
       
         UIButton *btnLib = [UIButton buttonWithType:UIButtonTypeCustom];
         [btnLib setImage:[UIImage imageNamed:@"logo1.png"] forState:UIControlStateNormal];
         btnLib.frame = CGRectMake(0, -5, 40, 40);
         btnLib.showsTouchWhenHighlighted=YES;
        [btnLib addTarget:self action:@selector(onMyLibrary:) forControlEvents:UIControlEventTouchUpInside];
         UIBarButtonItem *barButtonItem2 = [[UIBarButtonItem alloc] initWithCustomView:btnLib];
         [arrRightBarItems addObject:barButtonItem2];
         [arrRightBarItems addObject:barButtonItem];
         self.navigationItem.rightBarButtonItems=arrRightBarItems;
        
      
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName :[UIColor whiteColor]}];
}

-(void)onMyLibrary:(id)sender
{
    
    NSString*myurl=@"https://www.hypdemand.com/";
    NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
}
-(void)onSettings:(id)sender
{
  
    NSString*myurl=@"https://www.hypdemand.com/hyptunesradio";
     NSURL *url = [NSURL URLWithString:myurl];
    if ([[UIApplication sharedApplication] canOpenURL:url])
    {
       [[UIApplication sharedApplication] openURL:url];
    }
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
