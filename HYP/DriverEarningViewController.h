//
//  DriverEarningViewController.h
//  HYP
//
//  Created by VA pvt ltd on 13/09/21.
//  Copyright © 2021 Pragati Porwal. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DriverEarningViewController : UIViewController
{
    int startidx;
         NSArray *arr;
     NSMutableArray *resultArray;
}

@property(weak, nonatomic) IBOutlet MDTabBar *tabBar;
@property(weak, nonatomic)  NSString *matchRequest;
@property (weak, nonatomic) IBOutlet UILabel * noListAvalible;
@property (weak, nonatomic) IBOutlet UIButton * totalCount;
@property (weak, nonatomic) IBOutlet UIButton * totalAmount;
@property(weak, nonatomic) IBOutlet UITableView *completedTableView;
@end

NS_ASSUME_NONNULL_END
